package com.almoayyed.waseldelivery;

import android.app.Activity;
import android.app.Application;
import android.support.multidex.MultiDexApplication;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;

import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created on 14 Nov 2016 5:08 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class WaselApplication extends MultiDexApplication implements Application.ActivityLifecycleCallbacks{

    private static WaselApplication waselApplication;
    private static Cart cart = new Cart();

    private int activityCount;
    private boolean appInBackground = true;

    @Override
    public void onCreate() {
        super.onCreate();

        waselApplication = this;
        //Initializing crashlytics
        // why are you initializing it here?
        // why not somewhere else?
        Fabric.with(this, new Crashlytics());

        //TODO: Need to change it to online service at the time of release
        WaselDeliveryService.setServiceType(WaselDeliveryService.SERVICE_ONLINE);

        //Registering application for activity lifecycle call backs
        registerActivityLifecycleCallbacks(this);
    }

    public static WaselApplication getContext()
    {
        return waselApplication;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

        boolean appInBackgroundTemp = appInBackground;
        activityCount++;

        if(activityCount != 0){
            appInBackground = false;
        }
        else
        {
            appInBackground = true;
        }



    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

        activityCount--;

        if(activityCount == 0){
            appInBackground = true;

        }
        else
        {
            appInBackground = false;
        }
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    public boolean isAppInBackground()
    {
        return appInBackground;
    }

    public static Cart getCart() {
        return cart;
    }
}
