package com.almoayyed.waseldelivery.constants;

import android.graphics.drawable.Drawable;

import com.almoayyed.waseldelivery.R;

/**
 * Created on 21 Nov 2016 7:15 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class AmenityConstants {
    private static int id ;
    public static final int setAppropriateCatagoryImage(int amenityId){

        if(amenityId==1){
            id = R.drawable.rectangle_path;
        }else if(amenityId==2){
            id = R.drawable.ph_electronics;
        }else if(amenityId==3){
            id = R.drawable.ph_fashion;
        }else if(amenityId==4){
            id = R.drawable.ph_grocery;
        }else if(amenityId==5){
            id = R.drawable.ph_jewellery;
        }else if(amenityId==6){
            id = R.drawable.ph_sports;
        }else if(amenityId==7){
            id = R.drawable.ph_furniture;
        }else if(amenityId==8){
            id = R.drawable.ph_pharma;
        }else if(amenityId==9){
            id = R.drawable.ph_drinks;
        }else if(amenityId==10){
            id = R.drawable.ph_home_care;
        }else if(amenityId==11){
            id = R.drawable.ph_baby_care;
        }else if(amenityId==12){
            id = R.drawable.ph_beauty_care;
        }else if(amenityId==13){
            id = R.drawable.ph_flowers;
        }else if(amenityId==14){
            id = R.drawable.ph_books;
        }else if(amenityId==15){
            id = R.drawable.ph_home_decor;
        }else if(amenityId==16){
            id = R.drawable.ph_home_appliance;
        }else if(amenityId==17){
            id = R.drawable.ph_automotive;
        }else if(amenityId==18){
            id = R.drawable.ph_pet_care;
        }else if(amenityId==19){
            id = R.drawable.ph_stationary;
        }else if(amenityId==20){
            id = R.drawable.ph_toys;
        }
        return id;
    }
}
