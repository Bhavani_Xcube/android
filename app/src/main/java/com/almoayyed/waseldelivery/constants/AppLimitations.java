package com.almoayyed.waseldelivery.constants;

import com.almoayyed.waseldelivery.BuildConfig;

/**
 * Created on 08 Dec 2016 07:19 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */


public class AppLimitations {
    public static final int MAX_CART_ITEMS = 99;
    public static final String MOBILE_NO_PREFIX = BuildConfig.COUNTRY_CODE;
    public static final int SPECIAL_ORDER_DELIVERY_CHARGE = 2;
    public static final String EMPTY_SUB_CATEGORY_HEADING = "Menu";
}
