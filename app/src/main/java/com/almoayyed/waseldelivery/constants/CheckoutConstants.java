package com.almoayyed.waseldelivery.constants;

/**
 * Created on 28 Nov 2016 11:19 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Hold the key string of data passed between entities
 */
public class CheckoutConstants {

    public static final String KEY_DATA = "cartData";
    public static final String KEY_ORDER_ID = "orderId";
    public static final String KEY_ORDER_STATUS = "orderStatus";
    public static final String KEY_ORDER_TRACKING_LINK = "orderTrackLink";
    public static final String KEY_CUSTOMIZATION_ITEM = "customization_item";
    public static final String KEY_CUSTOMIZATION_DATA = "customization";
    public static final String KEY_COUPON_DATA = "coupon";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_ADDRESS_PG_NO = "pageNo";
    public static final String KEY_FEEDBACK_DATA = "feedbackInfo";

    public static final String KEY_CHECKOUT_FLOW = "checkoutFlow";
    public static final int KEY_CHECKOUT_ORDER_ANY_THING = 0;
    public static final int KEY_CHECKOUT_CART_ITEMS = 1;
    public static final int KEY_CHECKOUT_REPEAT_NORMAL_ORDER = 2;
    public static final int KEY_CHECKOUT_REPEAT_SPECIAL_ORDER = 3;
    public static final int KEY_CHECKOUT_CLICK_PUSH = 4;
    public static final int KEY_CHECKOUT_HISTORY_DETAILS = 5;

    public static final String KEY_ADD_NEW_ADDRESS_FLOW = "addNewAddressFlowDone";
    public static final String KEY_CHECKOUT_FLOW_DONE = "checkoutFlowDone";
    public static final String KEY_ITEMS_UPDATED_FROM_SERVER = "itemsUpdated";

    //Indicates type of checkout order
    public static final String CHECKOUT_NORMAL_ORDER = "NORMAL";
    public static final String CHECKOUT_SPECIAL_ORDER = "SPECIAL";

    //Customization item types
    public static final String CUSTOM_ITEM_MULTIPLE = "MULTIPLE";
    public static final String CUSTOM_ITEM_ANY_ONE = "ANY_ONE";
    public static final String CUSTOM_ITEM_QTY = "QTY";
}
