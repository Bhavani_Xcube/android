package com.almoayyed.waseldelivery.constants;

/**
 * Created on 08 Dec 2016 07:19 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */


//Each fragment is given a index to load that specific fragment into "LoadFragmentActivity"
public class FragmentsIndex {
    public static String FRAGMENT_TYPE = "fragmentType";
    public static String FRAGMENT_DATA = "fragmentData";
    public static int FRAGMENT_OUTLET_DETAILS = 1;
    public static int FRAGMENT_EDIT_PROFILE = 2;
    public static int FRAGMENT_MANAGE_ADDRESS = 3;
    public static int FRAGMENT_COUPONS = 4;
    public static int FRAGMENT_HELP_SUPPORT = 5;
    public static int FRAGMENT_ABOUT_WASEL = 6;
    public static int FRAGMENT_LEGAL = 7;
}
