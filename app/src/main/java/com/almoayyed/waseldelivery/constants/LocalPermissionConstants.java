package com.almoayyed.waseldelivery.constants;

/**
 * Created on 4 Oct 2016.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class LocalPermissionConstants {

    public static final int REQUEST_SEARCH_CLEAR = 104;
    public static final int REQUEST_APPLY_COUPON = 105;
    public static final int REQUEST_SPECIAL_ORDER_ADDRESS = 106;

    public static final int REQUEST_PUSH_NOTIFICATION = 110;
}
