package com.almoayyed.waseldelivery.constants;

/**
 * Created on 8 Dec 2016 17:00 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */


public class OrderConstants {
    //Order status
    public static final String ORDER_STATUS_PENDING = "PENDING";
    public static final String ORDER_STATUS_CONFIRM = "CONFIRM";
    public static final String ORDER_STATUS_PREPARING = "PACKED"; //PREPARING
    public static final String ORDER_STATUS_ON_THE_WAY = "ON_THE_WAY";
    public static final String ORDER_STATUS_COMPLETED = "COMPLETED";
    public static final String ORDER_STATUS_FAILED = "FAILED";
    public static final String ORDER_STATUS_CANCELLED = "CANCELLED";
    public static final String ORDER_STATUS_DRIVER_ACCEPT  = "REQUEST_RECEIVED";
}
