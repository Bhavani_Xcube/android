package com.almoayyed.waseldelivery.constants;

/**
 * Created on 28 Nov 2016 11:19 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Hold the outlet item extra fields values
 */
public class OutletItemCustomFieldConstants {

    public static final String FIELD_SPICY = "Spicy";
    public static final String FIELD_VEG = "Veg"; ///Non-veg

}
