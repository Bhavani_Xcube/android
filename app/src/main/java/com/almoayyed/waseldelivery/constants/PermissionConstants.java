package com.almoayyed.waseldelivery.constants;

/**
 * Created on 4 Oct 2016.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class PermissionConstants {

    public static final int REQUEST_LOCATION_PERMISSION = 101;
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 102;
}
