package com.almoayyed.waseldelivery.constants;

/**
 * Created on 19 Dec 2016 04:10 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class ProfileConstants {
    public static final String NAME = "Name";
    public static final String EMAIL = "Email";
    public static final String PHONE_NO = "PhoneNo";
    public static final String FROM = "from";
    public static final String FROM_PROFILE = "profile";
    public static final String FROM_COUPON = "coupon";
    public static final String FROM_ORDER = "order";
    public static final String FROM_SP_ORDER = "specialOrder";
    public static final String IMAGE_DIRECTORY_NAME = "WaselImages";
    public static final String IMAGE_STR = "image/*";

    public static final int CAMERA_RESULT = 100;
    public static final int GALLERY_RESULT = 101;
    public static final int MEDIA_TYPE_IMAGE = 11;

    public static final int EDIT_PROFILE_FRAGMENT = 103;
    public static final String CAMERA = "camera";
    public static final String GALLERY = "gallery";
    public static final String PROFILE_PIC_UPLOAD_TYPE = "upload_type";
}
