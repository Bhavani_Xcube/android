package com.almoayyed.waseldelivery.constants;

/**
 * Created on 3 Jan 2011 17:40 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */


public class PushNotificationConstants {

    //Order status and their corresponding push notification id's
    public static final int ORDER_STATUS_PENDING = 0;
    public static final int ORDER_STATUS_CONFIRM = 1;
    public static final int ORDER_STATUS_PREPARING = 2; //PACKED
    public static final int ORDER_STATUS_ON_THE_WAY = 3;
    public static final int ORDER_STATUS_COMPLETE = 4;
    /*public static final String ORDER_STATUS_FAILED = "FAILED";
    public static final String ORDER_STATUS_CANCELLED = "CANCELLED";*/

    //Offers related push notification id
    public static final int OFFERS = 5;

    //Broadcast receiver intent filter name
    public static final String orderStatusFilter = "orderStatusFilter";
}
