package com.almoayyed.waseldelivery.constants;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Hold the key string of data passed between entities
 */

public class RegistrationBundleKeyConstants {

    public static final String KEY_REGISTRATION_DATA = "registrationData";
    public static final String KEY_MOBILE_NO = "mobileNo";

    public static final int ACC_TYPE_WASEL = 1;
    public static final int ACC_TYPE_FACEBOOK = 2;
    public static final int ACC_TYPE_GOOGLE = 3;
    public static final int ACC_TYPE_FORGOT_PWD = 4;

    //Used to identify the login from splash or checkout
    public static final String KEY_LOGIN_FROM = "login_from";
    public static final int LOGIN_FROM_SPLASH = 1;
    public static final int LOGIN_FROM_PROFILE_TAB = 2;
    public static final int LOGIN_FROM_CHECKOUT_NORMAL = 3;
    public static final int LOGIN_FROM_CHECKOUT_SPECIAL = 4;
    public static final int LOGIN_FROM_ORDER_HISTORY = 5;
}
