package com.almoayyed.waseldelivery.constants;

import com.almoayyed.waseldelivery.BuildConfig;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class ServerConstants {

    public static final boolean workingOffline = true;

    //Local Puspa server
    /*public static final String ENDPOINT = "http://192.168.4.200:8071/api/v1/";*/

//    public static final String ENDPOINT = "http://192.168.4.200:8080/WaselDelivery-0.1.0/api/v1/";
//    public static final String ENDPOINT = "http://ec2-54-205-245-56.compute-1.amazonaws.com:8071/WaselDelivery-0.1.0/api/v1/";
//    public static final String ENDPOINT = "http://ec2-54-205-245-56.compute-1.amazonaws.com:8071/api/v1/";

    //Development server
    /*public static final String ENDPOINT = "http://ec2-54-165-243-253.compute-1.amazonaws.com:8071/api/v1/";*/

    //Client server
    public static final String ENDPOINT = BuildConfig.BASE_URL;

    public static final String RESPONSE_STATUS_CODE_SUCCESS = "WD200";
}

