package com.almoayyed.waseldelivery.data_saving;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.models.Amenity;
import com.almoayyed.waseldelivery.utils.log.Loggers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created on 29 Nov 2016 11:30 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class AmenityPreference {
    private static final String TAG = AmenityPreference.class.getSimpleName();
    private static final String PREFERENCES_NAME = "AmenityPreference";

    private static final String AMENITY_ID  = "amenity_id";

    private SharedPreferences sharedPreferences;
    private static AmenityPreference amenityPreference;

    AmenityPreference(){
        loadSharedPreferences();
    }

    public static AmenityPreference getAmenityPreference(){
        if (amenityPreference == null) {
            amenityPreference = new AmenityPreference();
        }
        return amenityPreference;
    }

    public void loadSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = WaselApplication.getContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        }
    }

    public Set<String> getAmenityIds() {
        try {
            return sharedPreferences.getStringSet(AMENITY_ID,Collections.<String>emptySet());
        } catch(NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return Collections.<String>emptySet();
    }

    public boolean saveAmenityIds(Set<String> amenityIds) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (amenityIds.size() > 0) {
                editor.putStringSet(AMENITY_ID, amenityIds);
            }
            editor.commit();
            return true;
        } catch(Exception ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return false;
    }

}
