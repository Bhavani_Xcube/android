package com.almoayyed.waseldelivery.data_saving;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ParseException;
import android.util.Log;

import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.utils.log.Loggers;

/**
 * Created on 17 Nov 2016 6:01 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * LocationPreferences that persists
 * Note: not in sue for now, added to use in future
 */
public class ImageUrlPrefixPreference {

    private static final String TAG = ImageUrlPrefixPreference.class.getSimpleName();
    private static final String PREFERENCES_NAME = "ImageUrlPrefixPreference";

    private static final String CONST_IMAGE_URL_PREFIX = "imageUrlPrefix";

    private static final String DEFAULT_PREFIX = "http://wasel.s3.amazonaws.com/";

    private static SharedPreferences sharedPreferences;

    private static void loadSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = WaselApplication.getContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        }
    }

    public static void setImagePrefix(String imagePrefix) {
        loadSharedPreferences();
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(CONST_IMAGE_URL_PREFIX, imagePrefix);
            editor.commit();
        }
    }

    public static String getImagePrefix() {
        loadSharedPreferences();
        if (sharedPreferences != null) {
            return sharedPreferences.getString(CONST_IMAGE_URL_PREFIX, DEFAULT_PREFIX);
        }
        return DEFAULT_PREFIX;
    }

}

