package com.almoayyed.waseldelivery.data_saving;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ParseException;
import android.util.Log;

import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.utils.log.Loggers;

/**
 * Created on 17 Nov 2016 6:01 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * LocationPreferences that persists
 * Note: not in sue for now, added to use in future
 */
public class LocationPreference {

    private static final String TAG = LocationPreference.class.getSimpleName();
    private static final String PREFERENCES_NAME        = "LocationPreference";

    private static final String CONST_CURRENT_LOC_LAG  = "currentLocLat";
    private static final String CONST_CURRENT_LOC_LNG  = "currentLocLng";
    private static final String CONST_CURRENT_LOC_NAME  = "currentLocName";
    private static final String CONST_COUNTRY_NAME ="country";

    private static final String DEFAULT_LNG = "50.58605";
    private static final String DEFAULT_LAT = "26.228516";
    private static final String DEFAULT_LOC_NAME = "Location ▼";
    private static final String DEFAULT_COUNTRY_NAME = "Bahrain";

    private static SharedPreferences sharedPreferences;

    private static void loadSharedPreferences() {
            if (sharedPreferences == null) {
                sharedPreferences = WaselApplication.getContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
            }
    }

    public static boolean saveCurrentLocation(UserLocation loc) {

        loadSharedPreferences();
        if(sharedPreferences != null) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(CONST_CURRENT_LOC_LAG, String.valueOf(loc.getLatitude()));
                editor.putString(CONST_CURRENT_LOC_LNG, String.valueOf(loc.getLongitude()));
                editor.putString(CONST_CURRENT_LOC_NAME, loc.getFormattedAddress());
                editor.putString(CONST_COUNTRY_NAME,loc.getCountry());
                editor.commit();
                return true;
        }
        return false;
    }

    public static double getCurrentLat() {

        loadSharedPreferences();
        if(sharedPreferences != null) {
            try {
                return Double.parseDouble(sharedPreferences.getString(CONST_CURRENT_LOC_LAG, DEFAULT_LAT));

            } catch(ParseException ex) {
               Loggers.error(Log.getStackTraceString(ex));
            }
        }
        return Double.parseDouble(DEFAULT_LAT);
    }

    public static double getCurrentLng() {

        loadSharedPreferences();
        if(sharedPreferences != null) {
            try {
                return Double.parseDouble(sharedPreferences.getString(CONST_CURRENT_LOC_LNG, DEFAULT_LNG));

            } catch(ParseException ex) {
                Loggers.error(Log.getStackTraceString(ex));
            }
        }
        return Double.parseDouble(DEFAULT_LNG);
    }

    public static String getCurrentLocName() {

        loadSharedPreferences();
        if(sharedPreferences != null) {
            try {
                return sharedPreferences.getString(CONST_CURRENT_LOC_NAME, DEFAULT_LOC_NAME) ;

            } catch(NumberFormatException ex) {
                Loggers.error(Log.getStackTraceString(ex));
            }
        }
        return DEFAULT_LOC_NAME;
    }

    public  static String getCountryName(){
        loadSharedPreferences();
        if(sharedPreferences != null) {
            try {
                return sharedPreferences.getString(CONST_COUNTRY_NAME, DEFAULT_COUNTRY_NAME) ;

            } catch(NullPointerException ex) {
                Loggers.error(Log.getStackTraceString(ex));
            }
        }
        return DEFAULT_COUNTRY_NAME;
    }

    public static boolean hasLocation(){
        if(getCurrentLocName().equalsIgnoreCase(DEFAULT_LOC_NAME)){
            return false;
        }
        return true;
    }
}

