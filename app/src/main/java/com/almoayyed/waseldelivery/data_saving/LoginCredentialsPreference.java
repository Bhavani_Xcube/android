package com.almoayyed.waseldelivery.data_saving;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.utils.ObjectSerializer;
import com.almoayyed.waseldelivery.utils.log.Loggers;

import java.util.ArrayList;

/**
 * Created on 14 Nov 2016 5:57 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * LocationPreferences that persists
 * Note: not in use for now, added to use in future
 */
public class LoginCredentialsPreference {

    private static final String TAG = LoginCredentialsPreference.class.getSimpleName();
    private static final String PREFERENCES_NAME = "LoginCredentialsPreference";

    private static final String CONST_ID = "id";
    private static final String CONST_NAME = "Name";
    private static final String CONST_EMAIL = "Email";
    private static final String CONST_PHONE_NO = "PhoneNo";
    private static final String CONST_LOGIN_TYPE = "SignUpType";
    private static final String CONST_IMG_URL = "imageUrl";
    private static final String CONST_ADDRESS = "address";
    private static final String CONST_ACCESS_TOKEN = "accessToken";
    private static final String CONST_REFRESH_TOKEN = "refereshToken";
    private static final String CONST_REG_ID = "pushNotificationId";

    private SharedPreferences sharedPreferences;
    private static LoginCredentialsPreference loginCredentialsPreference;

    LoginCredentialsPreference() {
        loadSharedPreferences();
    }

    public static LoginCredentialsPreference getLoginPreference() {
        if (loginCredentialsPreference == null) {
            loginCredentialsPreference = new LoginCredentialsPreference();
        }
        return loginCredentialsPreference;
    }

    public void loadSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = WaselApplication.getContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        }

    }

    public boolean saveUserCredentials(UserLoginDetails loginDetails) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(CONST_ID, loginDetails.getId());
            editor.putString(CONST_NAME, loginDetails.getName());
            editor.putString(CONST_EMAIL, loginDetails.getEmail());
            editor.putString(CONST_PHONE_NO, loginDetails.getMobile());
            editor.putString(CONST_IMG_URL, loginDetails.getImageUrl());
            editor.putString(CONST_LOGIN_TYPE, String.valueOf(loginDetails.getAccountType()));
            editor.putString(CONST_ADDRESS, ObjectSerializer.serialize((ArrayList) loginDetails.getAddress()));
            if(!TextUtils.isEmpty(loginDetails.getAccessToken())){
                editor.putString(CONST_ACCESS_TOKEN, loginDetails.getAccessToken());
                editor.putString(CONST_REFRESH_TOKEN, loginDetails.getRefreshToken());
            }
            editor.commit();
            return true;
        } catch (Exception ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return false;
    }

    public UserLoginDetails getUserCredentials() {
        UserLoginDetails profile = new UserLoginDetails();
        try {
            profile.setId(sharedPreferences.getString(CONST_ID, ""));
            profile.setName(sharedPreferences.getString(CONST_NAME, ""));
            profile.setEmail(sharedPreferences.getString(CONST_EMAIL, ""));
            profile.setMobile(sharedPreferences.getString(CONST_PHONE_NO, ""));
            profile.setImageUrl(sharedPreferences.getString(CONST_IMG_URL, "image"));
            profile.setAccountType(Integer.parseInt(sharedPreferences.getString(CONST_LOGIN_TYPE, "0")));
            profile.setAddress(new ArrayList<Address>());
            profile.setAccessToken(sharedPreferences.getString(CONST_ACCESS_TOKEN, ""));
            profile.setRefreshToken(sharedPreferences.getString(CONST_REFRESH_TOKEN, ""));
            return profile;
        } catch (NullPointerException ex) {
            Log.e(TAG, Log.getStackTraceString(ex));
        }
        return profile;
    }

    public boolean setId(String id) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(CONST_ID, id);
            editor.commit();
            return true;
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return false;
    }

    public boolean setName(String name) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(CONST_NAME, name);
            editor.commit();
            return true;
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return false;
    }

    public boolean saveRegId(String regId) {
        try {
            if (TextUtils.isEmpty(regId)) {
                Log.e(TAG + " reg id", "empty");
                return false;
            }
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(CONST_REG_ID, regId);
            editor.commit();
            return true;
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return false;
    }

    public boolean setAccessToken(String accessToken) {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(CONST_ACCESS_TOKEN, accessToken);
            editor.commit();
            return true;
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return false;
    }


    public String getRegId() {
        try {
            Log.e(TAG + " reg id", sharedPreferences.getString(CONST_REG_ID, ""));
            return sharedPreferences.getString(CONST_REG_ID, "");
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return "";
    }

    public String getId() {
        try {
            return sharedPreferences.getString(CONST_ID, "");
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return "";
    }


    public String getImgUrl() {
        try {
            return sharedPreferences.getString(CONST_IMG_URL, "");
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return "";
    }

    public String getName() {
        try {
            return sharedPreferences.getString(CONST_NAME, "");
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return "";
    }

    public String getAccessToken() {
        try {
            return sharedPreferences.getString(CONST_ACCESS_TOKEN, "");
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return "";
    }

    public String getEmail() {
        try {
            return sharedPreferences.getString(CONST_EMAIL, "");
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return "";
    }

    public String getPhoneNo() {
        try {
            return sharedPreferences.getString(CONST_PHONE_NO, "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ");
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return "";
    }

    public String getLoginType() {
        try {
            return sharedPreferences.getString(CONST_LOGIN_TYPE, "");
        } catch (NullPointerException ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return "";
    }

    public Boolean isUserLoggedIn() {

        try {
            return sharedPreferences.getString(CONST_ID, "0") != "0";
        } catch (Exception ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return false;
    }

    public void clearLoginDetails() {
        try {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            String regId = getLoginPreference().getRegId();
            editor.clear();
            editor.apply();
            saveRegId(regId);
        } catch (Exception ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
    }
}

