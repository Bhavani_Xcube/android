package com.almoayyed.waseldelivery.data_saving;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.almoayyed.waseldelivery.models.Search;
import com.almoayyed.waseldelivery.models.UserLocation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created on 11 Nov 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Creates SQlite DB to be utilize throughout the app
 * Req1: Saving recent search locations
 * Req2: Saving recent search outlets
 */
public class WaselDeliveryDB extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "waselDeliveryDB";

    // Contacts table name
    private static final String TABLE_LOCATION = "Location";

    //Search table Name
    private static final String TABLE_SEARCH = "Search";

    private static final String KEY_ID = "id";

    // Contacts Table Columns names
    private static final String KEY_SUB_LOCALITY3 = "subLocality3";
    private static final String KEY_SUB_LOCALITY2 = "subLocality2";
    private static final String KEY_SUB_LOCALITY1 = "subLocality1";
    private static final String KEY_CITY = "city";
    private static final String KEY_STATE = "state";
    private static final String KEY_COUNTRY = "country";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_USED_ON = "usedOn";

    //Search Table column names
    private static final String KEY_SEARCHED_TEXT = "searched_text";
    private static final String KEY_SEARCHED_TEXT_ID = "searched_id";


    static String findLocationQuery = "SELECT  * FROM " + TABLE_LOCATION + " WHERE " + KEY_LATITUDE + " = ? and " + KEY_LONGITUDE + " = ?";
    static String getLocationWithInRangeQuery = "SELECT  * FROM " + TABLE_LOCATION + " WHERE "
            + KEY_ID + " >= ? and "+ KEY_ID + " <= ?  ORDER BY "+ KEY_USED_ON +" DESC";
    static String getLocationWithLimitQuery = "SELECT  * FROM " + TABLE_LOCATION
            + " ORDER BY "+ KEY_USED_ON +" DESC LIMIT ?";
    static String getLocationWithoutLimitQuery = "SELECT  * FROM " + TABLE_LOCATION
            + " ORDER BY "+ KEY_USED_ON + " DESC";
    static String getSearchedText = "SELECT * FROM " + TABLE_SEARCH
            + " ORDER BY "+ KEY_USED_ON + " DESC" ;
    static String findsearchedTextQuery = "SELECT  * FROM " + TABLE_SEARCH + " WHERE " + KEY_SEARCHED_TEXT + " = ?";

    public WaselDeliveryDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_LOCATION + "("
                + KEY_SUB_LOCALITY3 + " TEXT," + KEY_SUB_LOCALITY2 + " TEXT,"
                + KEY_SUB_LOCALITY1 + " TEXT," + KEY_CITY + " TEXT,"
                + KEY_STATE + " TEXT," + KEY_COUNTRY + " TEXT,"
                + KEY_LATITUDE + " TEXT," + KEY_LONGITUDE + " TEXT,"
                + KEY_USED_ON + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
                + KEY_ID + " INTEGER PRIMARY KEY" + ")";
        db.execSQL(CREATE_LOCATION_TABLE);

        String CREATE_SEARCH_TABLE = "CREATE TABLE " +TABLE_SEARCH + "("
                + KEY_SEARCHED_TEXT + " TEXT, "
                + KEY_USED_ON + " DATETIME DEFAULT CURRENT_TIMESTAMP,"
                + KEY_SEARCHED_TEXT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL" + ")";
        db.execSQL(CREATE_SEARCH_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long saveSearchedText(String text){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SEARCHED_TEXT, text);
        values.put(KEY_USED_ON,getDateTime());

        long searchedId = db.insert(TABLE_SEARCH, null, values);
        return searchedId;
    }


    public long deleteSearchedText(String id){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] selectionArgs = {id};
        String where = KEY_SEARCHED_TEXT + " = ?";
        return db.delete(TABLE_SEARCH, where, selectionArgs);
    }

    public long updateSearchedText(String id){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] selectionArgs = {id};
        String where = KEY_SEARCHED_TEXT + " = ?";
        ContentValues values = new ContentValues();
        values.put(KEY_USED_ON,getDateTime());
        return db.update(TABLE_SEARCH, values, where, selectionArgs);
    }

    public long checkAndAddSearchedText(String text){
        if(hasSearchedTextSaved(text)){
            return updateSearchedText(text);
        }
        return saveSearchedText(text);
    }

    public boolean hasSearchedTextSaved(@NonNull String text){
        SQLiteDatabase db = this.getReadableDatabase();

        //finding in saved location with latitude & longitude values
        Cursor c = null;
        boolean bool = false;
        try{
            String[] selectionArgs = {text};
            c =db.rawQuery(findsearchedTextQuery,selectionArgs);
            bool = c.moveToFirst();
        }finally {
            if (c!= null){
                c.close();
            }
        }

        return bool;
    }

    /*
   * Fetches Recently searched text
   * */
    public List<Search> getSearchedText(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = null;
        List<Search> searcheditems = null;

        try{
            c =db.rawQuery(getSearchedText,null);

            searcheditems = new ArrayList<>();
            while (c != null && c.moveToNext()){
                Search search = new Search();
                search.setSearchedId(c.getString(c.getColumnIndex(KEY_SEARCHED_TEXT_ID)));
                search.setSearchedName(c.getString(c.getColumnIndex(KEY_SEARCHED_TEXT)));
                searcheditems.add(search);
            }
        }finally {
            if(c!=null) {
                c.close();
            }
        }


        return searcheditems;
    }

    public void clearSearchHistory() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from "+ TABLE_SEARCH);
    }

    public long addLocation(UserLocation userLocation){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SUB_LOCALITY3, userLocation.getSubLocality3());
        values.put(KEY_SUB_LOCALITY2, userLocation.getSubLocality2());
        values.put(KEY_SUB_LOCALITY1, userLocation.getSubLocality1());
        values.put(KEY_CITY, userLocation.getCity());
        values.put(KEY_STATE, userLocation.getState());
        values.put(KEY_COUNTRY, userLocation.getCountry());
        values.put(KEY_LATITUDE, String.valueOf(userLocation.getLatitude()));
        values.put(KEY_LONGITUDE, userLocation.getLongitude());
        values.put(KEY_USED_ON,getDateTime());

        // insert row
        long location_id = db.insert(TABLE_LOCATION, null, values);

        return location_id;
    }

    public long deleteLocation(UserLocation userLocation){
        SQLiteDatabase db = this.getReadableDatabase();
        String[] selectionArgs = {String.valueOf(userLocation.getLatitude()), String.valueOf(userLocation.getLongitude())};
        String where = KEY_LATITUDE + " = ? and " + KEY_LONGITUDE + " = ?";
        return db.delete(TABLE_LOCATION,where,selectionArgs);
    }

    public long checkAndAddLocation(UserLocation userLocation){
        if(hasLocationSaved(userLocation)){
            //update the used time
            return updateLocation(userLocation);
        }else {
            return addLocation(userLocation);
        }
    }

    public long updateLocation(UserLocation userLocation) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] selectionArgs = {String.valueOf(userLocation.getLatitude()), String.valueOf(userLocation.getLongitude())};
        String where = KEY_LATITUDE + " = ? and " + KEY_LONGITUDE + " = ?";
        ContentValues values = new ContentValues();
        values.put(KEY_USED_ON,getDateTime());
        return db.update(TABLE_LOCATION,values,where,selectionArgs);
    }

    public boolean hasLocationSaved(@NonNull UserLocation userLocation){
        SQLiteDatabase db = this.getReadableDatabase();

        //finding in saved location with latitude & longitude values
        Cursor c = null;
        boolean bool = false;
        try{
            String[] selectionArgs = {String.valueOf(userLocation.getLatitude()), String.valueOf(userLocation.getLongitude())};
            c =db.rawQuery(findLocationQuery,selectionArgs);
            bool = c.moveToFirst();
        }finally {
            if (c!= null){
                c.close();
            }
        }

        return bool;
    }

    /*
    * Fetches list of locations between from & to id's
    * */
    public List<UserLocation> getLocations(int fromId, int toId){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = null;
        List<UserLocation> userLocations = null;

        try{
            String[] selectionArgs = {String.valueOf(fromId), String.valueOf(toId)};
            c =db.rawQuery(getLocationWithInRangeQuery,selectionArgs);
            userLocations = new ArrayList<>();
            while (c != null && c.moveToNext()){
                UserLocation userLocation = new UserLocation();
                userLocation.setSubLocality3(c.getString(c.getColumnIndex(KEY_SUB_LOCALITY3)));
                userLocation.setSubLocality2(c.getString(c.getColumnIndex(KEY_SUB_LOCALITY2)));
                userLocation.setSubLocality1(c.getString(c.getColumnIndex(KEY_SUB_LOCALITY1)));
                userLocation.setCity(c.getString(c.getColumnIndex(KEY_CITY)));
                userLocation.setState(c.getString(c.getColumnIndex(KEY_STATE)));
                userLocation.setCountry(c.getString(c.getColumnIndex(KEY_COUNTRY)));
                userLocation.setLatitude(Double.valueOf(c.getString(c.getColumnIndex(KEY_LATITUDE))));
                userLocation.setLongitude(Double.valueOf(c.getString(c.getColumnIndex(KEY_LONGITUDE))));
                userLocations.add(userLocation);
            }

        }finally {
            if(c!=null){
                c.close();
            }
        }

        return userLocations;
    }

    /*
    * Fetches 'limit' number of the latest locations
    * */
    public List<UserLocation> getLocations(int limit){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = null;
        List<UserLocation> userLocations = null;

        try{
            if(limit > 0){
                String[] selectionArgs = {String.valueOf(limit)};
                c =db.rawQuery(getLocationWithLimitQuery,selectionArgs);
            }else {
                c =db.rawQuery(getLocationWithoutLimitQuery,null);
            }

            userLocations = new ArrayList<>();
            while (c != null && c.moveToNext()){
                UserLocation userLocation = new UserLocation();
                userLocation.setSubLocality3(c.getString(c.getColumnIndex(KEY_SUB_LOCALITY3)));
                userLocation.setSubLocality2(c.getString(c.getColumnIndex(KEY_SUB_LOCALITY2)));
                userLocation.setSubLocality1(c.getString(c.getColumnIndex(KEY_SUB_LOCALITY1)));
                userLocation.setCity(c.getString(c.getColumnIndex(KEY_CITY)));
                userLocation.setState(c.getString(c.getColumnIndex(KEY_STATE)));
                userLocation.setCountry(c.getString(c.getColumnIndex(KEY_COUNTRY)));
                userLocation.setLatitude(Double.valueOf(c.getString(c.getColumnIndex(KEY_LATITUDE))));
                userLocation.setLongitude(Double.valueOf(c.getString(c.getColumnIndex(KEY_LONGITUDE))));
                userLocations.add(userLocation);
            }
        }finally {
            if(c!= null){
                c.close();
            }
        }

        return userLocations;
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}
