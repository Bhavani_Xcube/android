package com.almoayyed.waseldelivery.databinding_eventhandler;

import android.view.View;

/**
 * Created on 09 Nov 2016 4:44 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Holds the reference to view click listeners
 */
public class ClickEventHandler {
    public View.OnClickListener onClickListener;
}
