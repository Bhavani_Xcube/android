package com.almoayyed.waseldelivery.databinding_eventhandler;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.annotation.Dimension;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.data_saving.ImageUrlPrefixPreference;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.utils.Converters;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.IllegalFormatException;

/**
 * Created on 18 Nov 2016 12:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Load image when ever imageView is set with 'imageUrl' data
 */
public class LoadImage {
    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String url) {
        try {
            if(WaselDeliveryService.getServiceType() == WaselDeliveryService.SERVICE_OFFLINE){
                int resID = WaselApplication.getContext().getResources().getIdentifier(url, "drawable", WaselApplication.getContext().getPackageName());
                view.setImageResource(resID);
            }else {
                //TODO: load image into view. (when actual data is loading from server)
                Picasso.with(view.getContext()).load(url)
                        .placeholder(R.drawable.rectangle_path).into(view);
            }
        }
        catch (IllegalArgumentException e) {
            view.setImageResource(R.drawable.rectangle_path);
        }
    }

    @BindingAdapter({"bind:imageUrl","bind:defaultImageUrl"})
    public static void loadImage(ImageView view, String url, Drawable defaultImageUrl) {
        try {
            if(WaselDeliveryService.getServiceType() == WaselDeliveryService.SERVICE_OFFLINE){
                int resID = WaselApplication.getContext().getResources().getIdentifier(url, "drawable", WaselApplication.getContext().getPackageName());
                view.setImageResource(resID);
            }else {
                //TODO: load image into view. (when actual data is loading from server)
                Picasso.with(WaselApplication.getContext())
                        .load(ImageUrlPrefixPreference.getImagePrefix() + url)
                        .placeholder(defaultImageUrl).into(view);
            }
        }catch (IllegalArgumentException e) {
            view.setImageDrawable(defaultImageUrl);
        }
    }

    @BindingAdapter({"bind:imageUrl","bind:defaultImageUrl", "bind:downloadWidth", "bind:downloadHeight"})
    public static void loadImage(ImageView view, String url, Drawable defaultImageUrl, float downloadWidth, float downloadHeight) {
        try {
            if(WaselDeliveryService.getServiceType() == WaselDeliveryService.SERVICE_OFFLINE){
                int resID = WaselApplication.getContext().getResources().getIdentifier(url, "drawable", WaselApplication.getContext().getPackageName());
                view.setImageResource(resID);
            }else {
                //TODO: load image into view. (when actual data is loading from server)
                Picasso.with(WaselApplication.getContext())
                        .load(ImageUrlPrefixPreference.getImagePrefix() + url)
                        .resize((int)downloadWidth, (int)downloadHeight)
                        .centerCrop()
                        .placeholder(defaultImageUrl).into(view);
                //Resize is temporary sol. Used to min the download of large images (eg: 2.3MB of outlet item image)
            }
        }catch (IllegalArgumentException e) {
            view.setImageDrawable(defaultImageUrl);
        }
    }

    @BindingAdapter({"bind:closeImg"})
    public static void loadImage(EditText view, int size) {
        //As of now close button should be displayed irrespective of text at special order item
        view.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.close,0);
        /*if(size == 0){
            view.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        }else{
            view.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.close,0);
        }*/
    }
}
