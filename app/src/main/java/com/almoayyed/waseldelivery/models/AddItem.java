package com.almoayyed.waseldelivery.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import java.io.Serializable;
import com.almoayyed.waseldelivery.BR;

/**
 * Created on 11 Nov 2016 2:12 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class AddItem extends BaseObservable implements Serializable{

    String itemName;
    @Bindable
    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
        notifyPropertyChanged(BR.itemName);
    }

}
