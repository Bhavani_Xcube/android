package com.almoayyed.waseldelivery.models;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
<<<<<<< HEAD
 * Created on 11 Nov 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class Address implements Serializable{
    public static final String ADDRESS_TYPE_HOME = "HOME";
    public static final String ADDRESS_TYPE_OFFICE = "OFFICE";
    public static final String ADDRESS_TYPE_OTHER = "OTHER";

    private String id;

    private double latitude;

    private double longitude;

    private double deliveryCharge;

    private String landmark;

    private String doorNumber;

    private String zipCode;

    private String addressType;

    private String location;

    private String country;

    private int countryCode;

    private boolean castOff;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getDoorNumber() {
        return doorNumber;
    }

    public void setDoorNumber(String doorNumber) {
        this.doorNumber = doorNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public double getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(double deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }

    public String getCompleteAddress(){

        //StringBuffer addressBuffer = new StringBuffer();
        /* commented as doornumber, landmark & country fields are present in 'location' variable*/
        List<String> filterStr = filterEmpty(doorNumber,landmark,location);

        /*addressBuffer.append(doorNumber);
        addressBuffer.append(", \n"+landmark);
        addressBuffer.append(", \n"+location);*/
//        List<String> filterStr = filterEmpty(location);
        if(Integer.parseInt(zipCode) != 0){
            filterStr.add(zipCode);
            //addressBuffer.append(", \n"+zipCode);
        }
        return TextUtils.join(", \n",filterStr.toArray()).toString();
        //return addressBuffer.toString();
    }

    List<String> filterEmpty(String... args) {
        List<String> list = new ArrayList<String>();
        for (String s : args) {
            if (!TextUtils.isEmpty(s))
                list.add(s);
        }
        return list;
    }

    public boolean isCastOff() {
        return castOff;
    }

    public void setCastOff(boolean castOff) {
        this.castOff = castOff;
    }
}
