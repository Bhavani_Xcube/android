package com.almoayyed.waseldelivery.models;

import java.io.Serializable;

/**
 * Created on 17 Nov 2016 6:36 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class Amenity implements Serializable,Comparable<Amenity> {

    private Integer id;
    private Integer imageUrl;
    private String name;
    private Object description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Integer getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Integer imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public int compareTo(Amenity obj) {
        if (id < obj.getId()) {
            return -1;
        }
        else if (id > obj.getId()) {
            return 1;
        }
        else {
            return 0;
        }

    }
}