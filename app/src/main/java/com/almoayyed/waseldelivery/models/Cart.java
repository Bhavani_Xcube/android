package com.almoayyed.waseldelivery.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.ui.MainActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 28 Nov 2016 4:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Hold the items added to cart
 */
public class Cart extends BaseObservable implements Serializable{
    int itemCount;
    int amenityId;
    Outlet outlet;
    List<OutletItem> items = new ArrayList<>();

    private String id;
    private String orderNumber;
    private String status;
    private String orderType;

    //Pickup location
    private String pickUpLocation;
    private double latitude;
    private double longitude;

    private double orderCharge;
    private double deliveryCharge;
    private double grandTotal;
    private double discountAmount;
    private String orderInstruction;
    private String comment;
    private float rating;
    private String phoneNumber;
    private String paymentType;
    private String jobPickupDatetime;
    private String jobDeliveryDatetime;
    private UserLoginDetails user;
    private Coupons coupon;
    private Address shippingAddress;

    @Bindable
    public int getItemCount() {
        itemCount = 0;
        for(OutletItem item : items){
            itemCount += item.getCartQuantity();
        }
        return itemCount;
    }

    public void setItemCount(int itemCount){
        this.itemCount = itemCount;
    }

    @Bindable
    public double getOrderCharge() {
        orderCharge = 0;
        for(OutletItem item : items){
            orderCharge += item.getTotalPrice();
        }
        grandTotal = orderCharge;
        return orderCharge ;
    }

    public OutletItem getOutletItemAtPos(int pos){
        if(pos>=0 && pos<items.size()){
            return items.get(pos);
        }
        return null;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public int getAmenityId() {
        return amenityId;
    }

    public void setAmenityId(int amenityId) {
        this.amenityId = amenityId;
    }

    public List<OutletItem> getItems() {
        return items;
    }

    public void setItems(List<OutletItem> items) {
        this.items.clear();
        this.items.addAll(items);
    }

    public void clear() {
        grandTotal = 0;
        orderCharge = 0;
        itemCount = 0;
        paymentType = "";
        outlet = null;
        coupon = null;
        discountAmount = 0;
        items.clear();
    }

    public void addItem(@NonNull OutletItem outletItem) {

        //Check whether item previously added
        for(OutletItem item : items){
            if(item.id == outletItem.id){
                outletItem.incCartQuantity();
                notifyPropertyChanged(BR.itemCount);
                notifyPropertyChanged(BR.orderCharge);
                return;
            }
        }
        outletItem.incCartQuantity();

        items.add(outletItem);

        notifyPropertyChanged(BR.itemCount);
        notifyPropertyChanged(BR.orderCharge);
    }

    public void addItem(@NonNull List<OutletItem> outletItem) {
        items.clear();
        items.addAll(outletItem);

        notifyPropertyChanged(BR.itemCount);
        notifyPropertyChanged(BR.orderCharge);
    }
    public void removeItem(OutletItem outletItem) {
        //Check whether item previously added
        int removeIndex = -1;
        for(int index = 0; index <items.size(); index++){
            OutletItem item = items.get(index);
            if(item.id.equalsIgnoreCase(outletItem.id)){
                removeIndex  = index;
                item.decCartQuantity();

                //Dec total item count
                notifyPropertyChanged(BR.itemCount);
            }
        }
        //Remove item from cart when its quantity is zero
        if(outletItem.cartQuantity == 0){
            outletItem.clearCustomization();
            items.remove(removeIndex);
        }
        notifyPropertyChanged(BR.orderCharge);
    }

    public void removeItem(int pos) {

        OutletItem outletItem = items.get(pos);
        outletItem.decCartQuantity();
        if(outletItem.cartQuantity == 0){
            outletItem.clearCustomization();
            items.remove(pos);
        }
        notifyPropertyChanged(BR.itemCount);
        notifyPropertyChanged(BR.orderCharge);
    }


    public Outlet getOutlet() {
        return outlet;
    }

    public void setOutlet(Outlet outlet) {
        this.outlet = outlet;
        this.deliveryCharge = outlet==null ? 0 : outlet.getDeliveryCharge();
    }

    public boolean isEmpty() {
        if(getItemCount() <= 0){
            return true;
        }
        return false;
    }

    public void notifyTotalPriceChanged(){
        notifyPropertyChanged(BR.orderCharge);
    }

    public void setTotalCost(float orderCharge) {
        this.orderCharge = orderCharge;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setOrderCharge(float orderCharge) {
        this.orderCharge = orderCharge;
    }

    public double getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(double deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
        notifyPropertyChanged(BR.discountAmount);
        notifyPropertyChanged(BR.grandTotal);
    }

    @Bindable
    public double getGrandTotal() {
        grandTotal = getOrderCharge()  + getDeliveryCharge() - getDiscountAmount();
        return grandTotal;
    }

    public void setGrandTotal(float grandTotal) {
        this.grandTotal = grandTotal;
    }

    @Bindable
    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
        notifyPropertyChanged(BR.discountAmount);
        notifyPropertyChanged(BR.grandTotal);
    }

    public String getOrderInstruction() {
        return orderInstruction;
    }

    public void setOrderInstruction(String orderInstruction) {
        this.orderInstruction = orderInstruction;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getJobPickupDatetime() {
        return jobPickupDatetime;
    }

    public void setJobPickupDatetime(String jobPickupDatetime) {
        this.jobPickupDatetime = jobPickupDatetime;
    }

    public String getJobDeliveryDatetime() {
        return jobDeliveryDatetime;
    }

    public void setJobDeliveryDatetime(String jobDeliveryDatetime) {
        this.jobDeliveryDatetime = jobDeliveryDatetime;
    }

    public UserLoginDetails getUser() {
        return user;
    }

    public void setUser(UserLoginDetails user) {
        this.user = user;
    }

    @Bindable
    public Coupons getCoupon() {
        return coupon;
    }

    public boolean setCoupon(Coupons coupon) {
        if(coupon!=null && getOrderCharge()>=coupon.getMinOrderValue()){
            this.coupon = coupon;
            //Coupon applyed only if current order reaches minOrderValue
            if(coupon.getOfferType().equalsIgnoreCase(Coupons.TYPE_AMOUNT)){
                //Applying amount based coupon
                setDiscountAmount(Math.min(getOrderCharge(),coupon.getDiscountAmount()));
            }else {
                //Applying percentage based coupon
                double discountAmount = Math.min((getOrderCharge() * coupon.getValue()) / 100f, coupon.getDiscountAmount());
                setDiscountAmount(Math.min(getOrderCharge(),discountAmount));
            }
            notifyPropertyChanged(BR.coupon);
            notifyPropertyChanged(BR.couponAvailable);
            notifyPropertyChanged(BR.couponNotAvailableForNormalOrder);
            return true;
        }
        return false;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Order convertToOrder(){
        Order order = new Order();

        user = LoginCredentialsPreference.getLoginPreference().getUserCredentials();
        order.setUser(user);

        if(getOrderType().equalsIgnoreCase(CheckoutConstants.CHECKOUT_NORMAL_ORDER)){
            order.setOutlet(outlet);
        }/*else {
            newOutlet.setId("");
            newOutlet.setName("");
        }*/
        order.setCoupon(coupon);

        order.setOrderType(getOrderType());

        //Pickup details for special order
        order.setPickUpLocation(getPickUpLocation());
        order.setLatitude(getLatitude());
        order.setLongitude(getLongitude());

        order.setOrderCharge(getOrderCharge());
        order.setGrandTotal(getGrandTotal());
        order.setDeliveryCharge(getDeliveryCharge());
        order.setDiscountAmount(getDiscountAmount());
        order.setOrderInstruction(getOrderInstruction());
        order.setPhoneNumber(String.valueOf(LoginCredentialsPreference.getLoginPreference().getPhoneNo()));
        order.setPaymentType(getPaymentType());

        List<OutletItem> items = getItems();
        List<OrderItem> orderItems = new ArrayList<>();
        for (OutletItem item : items){
            orderItems.add(item.convertToOrderItem());

        }
        order.setItems(orderItems);
        order.setShippingAddress(getShippingAddress());


        return  order;
    }

    @Bindable
    public boolean isCouponNotAvailableForNormalOrder(){
        return (CheckoutConstants.CHECKOUT_NORMAL_ORDER.equalsIgnoreCase(orderType) && coupon == null );
    }

    @Bindable
    public boolean isCouponAvailable(){
        return coupon != null;
    }
    public void clearCoupon() {
        coupon = null;
        discountAmount = 0;
        notifyPropertyChanged(BR.couponNotAvailableForNormalOrder);
        notifyPropertyChanged(BR.couponAvailable);
        notifyPropertyChanged(BR.coupon);
        notifyPropertyChanged(BR.grandTotal);
    }
}
