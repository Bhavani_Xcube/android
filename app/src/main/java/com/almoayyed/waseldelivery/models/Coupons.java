package com.almoayyed.waseldelivery.models;

import java.io.Serializable;

/**
 * Created on 17 Nov 2016 2:19 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class Coupons implements Serializable {

    public static final String TYPE_PERCENTAGE = "PercentageBased";
    public static final String TYPE_AMOUNT = "AmountBased";

    private long id;
    private String name;
    private String code;
    private String description;
    private float discountAmount;
    private float value;
    private float minOrderValue;
    private int noOfuse;
    private String offerType;
    private String userType;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(float discountAmount) {
        this.discountAmount = discountAmount;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getMinOrderValue() {
        return minOrderValue;
    }

    public void setMinOrderValue(float  minOrderValue) {
        this.minOrderValue = minOrderValue;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public int getNoOfuse() {
        return noOfuse;
    }

    public void setNoOfuse(int noOfuse) {
        this.noOfuse = noOfuse;
    }
}
