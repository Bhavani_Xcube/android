package com.almoayyed.waseldelivery.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created on 1 Nov 2016 11:49 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class Cuisine implements Serializable {
    @SerializedName("cuisine_id")
    public String id;

    @SerializedName("name")
    public String name;

    public boolean isCheckBoxEnabled;

   /*
   @SerializedName("restaurant_id")
    public Integer restaurantId;

    @SerializedName("restaurant_cuisine_id")
    public String restaurantCuisineId;

    @SerializedName("created_by")
    public String createdBy;

    @SerializedName("created_date")
    public String createdDate;

    @SerializedName("updated_by")
    public String updatedBy;

    @SerializedName("updated_date")
    public String updatedDate;

    @SerializedName("is_active")
    public Integer isActive;*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheckBoxEnabled() {
        return isCheckBoxEnabled;
    }

    public void setCheckBoxEnabled(boolean isCheckBoxEnabled) {
        this.isCheckBoxEnabled = isCheckBoxEnabled;
    }
}