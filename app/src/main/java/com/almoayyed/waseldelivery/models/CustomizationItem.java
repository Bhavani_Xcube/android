package com.almoayyed.waseldelivery.models;

/**
 * Created on 17 Nov 2016 6:36 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

    public class CustomizationItem{
        int id;
        String  description;
        float price;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }
    }