package com.almoayyed.waseldelivery.models;


import java.io.Serializable;

/**
 * Created on 11 Nov 2016 5:38 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class DeviceIds implements Serializable {
    public boolean isAndroid = true;
    public String deviceToken;

    public boolean isAndroid() {
        return isAndroid;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

}
