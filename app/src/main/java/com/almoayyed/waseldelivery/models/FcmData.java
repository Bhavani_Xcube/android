package com.almoayyed.waseldelivery.models;

/**
 * Created on 08 Dec 2016 4:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class FcmData {

    public String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
