package com.almoayyed.waseldelivery.models;

/**
 * Created on 19 Dec 2016 03:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class Feedback {
    String orderId;
    float deliveryRating; //delivery rating
    float rating; //product rating
    String comment;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public float getDeliveryRating() {
        return deliveryRating;
    }

    public void setDeliveryRating(float deliveryRating) {
        this.deliveryRating = deliveryRating;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
