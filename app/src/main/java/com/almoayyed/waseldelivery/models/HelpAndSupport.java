package com.almoayyed.waseldelivery.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created on 24 Jan 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class HelpAndSupport{

    private String email;

    private String mobile;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
