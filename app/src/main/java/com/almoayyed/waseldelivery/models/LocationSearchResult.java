package com.almoayyed.waseldelivery.models;

import java.io.Serializable;

/**
 * Created on 1 Nov 2016 11:49 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Holds the google search places results
 */
public class LocationSearchResult implements Serializable {
    String place_id;
    String description;

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
