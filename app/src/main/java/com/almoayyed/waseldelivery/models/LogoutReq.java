package com.almoayyed.waseldelivery.models;


import java.io.Serializable;

/**
 * Created on 27 Dec 2016 8:02 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class LogoutReq implements Serializable {
    private String id;
    DeviceIds device;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DeviceIds getDevice() {
        return device;
    }

    public void setDevice(DeviceIds device) {
        this.device = device;
    }
}
