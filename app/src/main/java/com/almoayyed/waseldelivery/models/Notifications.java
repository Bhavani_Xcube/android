package com.almoayyed.waseldelivery.models;

import java.io.Serializable;

/**
 * Created on 06 Feb 2017 5:00 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public class Notifications implements Serializable {
    public boolean isAndroid = true;
    public String deviceToken;
    private String userId;

    public boolean isAndroid() {
        return isAndroid;
    }

    public void setAndroid(boolean android) {
        isAndroid = android;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
