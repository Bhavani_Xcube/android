package com.almoayyed.waseldelivery.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.OrderConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletItemDetails;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 28 Nov 2016 4:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Hold the items added to cart
 */
public class Order extends BaseObservable implements Serializable{
    private String id;

    Outlet outlet;
    List<OrderItem> items = new ArrayList<>();
    private List<OrderItem> removedItems = new ArrayList<>();
    private String orderNumber;
    private String status;
    private String orderType;

    //Pick up details
    private String pickUpLocation;
    private double latitude;
    private double longitude;

    private double orderCharge;
    private double deliveryCharge;
    private double grandTotal;
    private double discountAmount;
    private String orderInstruction;
    private String comment;
    private float rating;
    private String phoneNumber;
    private String paymentType;
    private String jobPickupDatetime;
    private String jobDeliveryDatetime;
    private UserLoginDetails user;
    private Coupons coupon;
    private Address shippingAddress;

    @SerializedName("orderCreatedDateTime")
    private CreateDate createdDateTime;
    private String pickupTrackingLink;
    private String deliveryTracingLink;

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public String getDeliveryTracingLink() {
        return deliveryTracingLink;
    }

    public void setDeliveryTracingLink(String deliveryTracingLink) {
        this.deliveryTracingLink = deliveryTracingLink;
    }

    public String getPickupTrackingLink() {
        return pickupTrackingLink;
    }

    public void setPickupTrackingLink(String pickupTrackingLink) {
        this.pickupTrackingLink = pickupTrackingLink;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public CreateDate getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(CreateDate createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Outlet getOutlet() {
        return outlet;
    }

    public void setOutlet(Outlet outlet) {
        this.outlet = outlet;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }


    public List<OrderItem> getRemovedItems() {
        return removedItems;
    }

    public void setRemovedItems(List<OrderItem> removedItems) {
        this.removedItems = removedItems;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Bindable
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        notifyPropertyChanged(BR.status);
        notifyPropertyChanged(BR.orderTrackingOrCompleted);
        notifyPropertyChanged(BR.orderOnTheWay);
        notifyPropertyChanged(BR.orderCompleted);
        notifyPropertyChanged(BR.orderConfirmed);
    }

    public double getOrderCharge() {
        return orderCharge;
    }

    public void setOrderCharge(double orderCharge) {
        this.orderCharge = orderCharge;
    }

    public double getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(double deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getOrderInstruction() {
        return orderInstruction;
    }

    public void setOrderInstruction(String orderInstruction) {
        this.orderInstruction = orderInstruction;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getJobPickupDatetime() {
        return jobPickupDatetime;
    }

    public void setJobPickupDatetime(String jobPickupDatetime) {
        this.jobPickupDatetime = jobPickupDatetime;
    }

    public String getJobDeliveryDatetime() {
        return jobDeliveryDatetime;
    }

    public void setJobDeliveryDatetime(String jobDeliveryDatetime) {
        this.jobDeliveryDatetime = jobDeliveryDatetime;
    }

    public UserLoginDetails getUser() {
        return user;
    }

    public void setUser(UserLoginDetails user) {
        this.user = user;
    }

    public Coupons getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupons coupon) {
        this.coupon = coupon;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public int getItemCount() {
        int itemCount = 0;
        for(OrderItem item : items){
            itemCount += item.getQuantity();
        }
        return itemCount;
    }

    //Used to set visibility status of map
    @Bindable
    public boolean isOrderTrackingOrCompleted(){
        if(OrderConstants.ORDER_STATUS_COMPLETED.equalsIgnoreCase(getStatus()) ||
                OrderConstants.ORDER_STATUS_FAILED.equalsIgnoreCase(getStatus()) ||
                OrderConstants.ORDER_STATUS_ON_THE_WAY.equalsIgnoreCase(getStatus())||
                OrderConstants.ORDER_STATUS_PREPARING.equalsIgnoreCase(getStatus())||
                OrderConstants.ORDER_STATUS_CONFIRM.equalsIgnoreCase(getStatus())||
                OrderConstants.ORDER_STATUS_DRIVER_ACCEPT.equalsIgnoreCase(getStatus())){
            return true;
        }else {
            return false;
        }
    }

    //Used to update the map link, based on order status
    @Bindable
    public boolean isOrderOnTheWay(){
        if(OrderConstants.ORDER_STATUS_PREPARING.equalsIgnoreCase(getStatus()) ||
                OrderConstants.ORDER_STATUS_ON_THE_WAY.equalsIgnoreCase(getStatus())){
            return true;
        }else {
            return false;
        }
    }

    @Bindable
    public boolean isOrderCompleted(){
        if(OrderConstants.ORDER_STATUS_COMPLETED.equalsIgnoreCase(getStatus()) ||
                OrderConstants.ORDER_STATUS_FAILED.equalsIgnoreCase(getStatus())){
            return true;
        }else {
            return false;
        }
    }

    @Bindable
    public boolean isOrderConfirmed() {
        if(OrderConstants.ORDER_STATUS_CONFIRM.equalsIgnoreCase(getStatus()))
        {
            return true;
        }
        return false;
    }

    public void convertToCart(Cart cart){

        cart.setUser(LoginCredentialsPreference.getLoginPreference().getUserCredentials());

        cart.setOutlet(getOutlet());

        cart.setOrderType(getOrderType());
        cart.setDeliveryCharge(getDeliveryCharge());

        cart.setPickUpLocation(getPickUpLocation());
        cart.setLatitude(getLatitude());
        cart.setLongitude(getLongitude());

        cart.setShippingAddress(getShippingAddress());
        cart.setOrderInstruction(getOrderInstruction());

        //Fetching previous order items details
        List<OrderItem> orderItems = getItems();
        List<OutletItem> outletItems = new ArrayList<>();
        if(getOutlet()==null){
            for (OrderItem item : orderItems){
                OutletItem outletItem = item.convertToOutletItem();
                outletItems.add(outletItem);
            }
        }else {
            List<OutletItemDetails> outletPriceItems = getOutlet().getOutletItems();
            for (OrderItem item : orderItems){
                boolean isAvailable = false;
                OutletItem outletItem = item.convertToOutletItem();
                //Update previous order price info
                for(OutletItemDetails outletItemDetails: outletPriceItems){
                    if(outletItem.getId().equalsIgnoreCase(outletItemDetails.getId())){
                        isAvailable = true;
                        outletItem.setItemDescription(outletItemDetails.getDescription());
                        outletItem.setPrice(outletItemDetails.getPrice());
                        outletItem.setImageUrl(outletItemDetails.getImageUrl());
                        outletItem.setCustomizedItems(outletItemDetails.getCustomizationItems());

                        //update customize details quantity & price
                        outletItem.setCustomized(true);
                        List<OrderCustomItem> customItems = item.getCustomItems();

                        //coping custom data
                        ArrayList<OutletItemCustomization> outletCustomItemsCopy = new ArrayList<>();
                        ArrayList<OutletItemCustomization> outletCustomizedItems = outletItemDetails.getCustomizationItems();
                        for(OutletItemCustomization outletCustomItem : outletCustomizedItems){
                        /*OutletItemCustomization customItemCopy = new OutletItemCustomization();
                        customItemCopy.setId(outletCustomItem.getId());
                        customItemCopy.setQuantity(outletCustomItem.getQuantity());
                        customItemCopy.setName(outletCustomItem.getName());
                        customItemCopy.setPrice(outletCustomItem.getPrice());
                        customItemCopy.setType(outletCustomItem.getType());
                        customItemCopy.setCategory(outletCustomItem.getCategory());*/
                            for(OrderCustomItem orderCustomItem: customItems){
                                //Checking for previous order custom item quantity
                                if(orderCustomItem.getCustomItemId() == outletCustomItem.getId()){
                                    outletCustomItem.setQuantity(orderCustomItem.getQuantity());
                                    break;
                                }
                            }
                        /*outletCustomItemsCopy.add(customItemCopy);*/
                        }
                    /*outletItem.setCustomizedItems(outletCustomItemsCopy);*/
                        outletItem.setOutletItemCustomFields(outletItemDetails.getOutletItemCustomFields());
                        break;
                    }
                }
                Loggers.temp("isAvailable="+isAvailable);
                if(isAvailable) {
                    outletItems.add(outletItem);
                }
            }
        }
        cart.addItem(outletItems);
    }

    //Update the order item description & price with previous order price &
    public void updateOrderItemDetails(){
        if(getOutlet()==null){
            //Special order case
            return;
        }
        //Fetching previous order items details
        List<OrderItem> orderItems = getItems();
        List<OutletItemDetails> outletPriceItems = getOutlet().getOutletItems();
        for (OrderItem item : orderItems){
            //Update previous order price info
            for(OutletItemDetails outletItemDetails: outletPriceItems){
                if(item.getItemId().equalsIgnoreCase(outletItemDetails.getId())){
                    item.setDescription(outletItemDetails.getDescription());
                    item.setOutletItemCustomFields(outletItemDetails.getOutletItemCustomFields());

                    double customPrice = 0f;
                    //update customize details quantity & price
                    List<OrderCustomItem> customItems = item.getCustomItems();

                    //coping custom data
                    ArrayList<OutletItemCustomization> outletCustomizedItems = outletItemDetails.getCustomizationItems();
                    for(OutletItemCustomization outletCustomItem : outletCustomizedItems){
                        for(OrderCustomItem orderCustomItem: customItems){
                            //Checking for previous order custom item quantity
                            if(orderCustomItem.getCustomItemId() == outletCustomItem.getId()){
                                customPrice = customPrice + (orderCustomItem.getQuantity() * orderCustomItem.getPrice());
                                break;
                            }
                        }
                    }
                    item.setPrice((item.getPrice() + customPrice)*item.getQuantity());
                    break;
                }
            }
        }
    }

    public boolean isDriverAccepted() {
        if(OrderConstants.ORDER_STATUS_DRIVER_ACCEPT.equalsIgnoreCase(status)){
            return true;
        }
        return false;
    }
}
