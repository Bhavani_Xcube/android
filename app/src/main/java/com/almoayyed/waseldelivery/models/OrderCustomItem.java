package com.almoayyed.waseldelivery.models;

import java.io.Serializable;

/**
 * Created on 28 Nov 2016 4:47 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */


public class OrderCustomItem implements Serializable {
    private String customItemId;
    private int quantity;
    private String name;
    private double price;

    public String getCustomItemId() {
        return customItemId;
    }

    public void setCustomItemId(String customItemId) {
        this.customItemId = customItemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
