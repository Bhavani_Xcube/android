package com.almoayyed.waseldelivery.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.almoayyed.waseldelivery.BR;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OrderItem extends BaseObservable implements Serializable {

    private String itemId;
    private int quantity;
    private String name;
    private double price;
    private String description;
    private Boolean isSpicy;
    private Boolean isVeg;
    ArrayList<OutletItemCustomField> outletItemCustomFields; //Extra parameters set to menu item(veg, spicy)
    private List<OrderCustomItem> customItems = new ArrayList<>();

    public Boolean getSpicy() {
        return isSpicy;
    }

    public void setSpicy(Boolean spicy) {
        isSpicy = spicy;
    }

    public Boolean getVeg() {
        return isVeg;
    }

    public void setVeg(Boolean veg) {
        isVeg = veg;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<OrderCustomItem> getCustomItems() {
        return customItems;
    }

    public void setCustomItems(List<OrderCustomItem> customItems) {
        this.customItems = customItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<OutletItemCustomField> getOutletItemCustomFields() {
        return outletItemCustomFields;
    }

    public void setOutletItemCustomFields(ArrayList<OutletItemCustomField> outletItemCustomFields) {
        this.outletItemCustomFields = outletItemCustomFields;
    }

    public boolean isOutletItemCustomFieldExist(@NotNull String fieldName){
        if(outletItemCustomFields == null){
            return false;
        }
        for (OutletItemCustomField customField : outletItemCustomFields){
            if(fieldName.equalsIgnoreCase(customField.getField().field)){
                return true;
            }
        }
        return false;
    }

    public boolean getOutletItemCustomFieldValue(@NotNull String fieldName){
        if(outletItemCustomFields == null){
            return false;
        }
        for (OutletItemCustomField customField : outletItemCustomFields){
            if(fieldName.equalsIgnoreCase(customField.getField().field)){
                return customField.value;
            }
        }
        return false;
    }

    public OutletItem convertToOutletItem(){
        OutletItem outletItem = new OutletItem();
        outletItem.setId(itemId);
        outletItem.setCartQuantity(quantity);
        outletItem.setName(name);
        outletItem.setPrice(price);
        outletItem.setItemDescription(description);
        ArrayList<OutletItemCustomization> outletCustomItems = new ArrayList<>();
        for(OrderCustomItem customizationItem: customItems){
            OutletItemCustomization customItem = new OutletItemCustomization();
            customItem.setId(customizationItem.getCustomItemId());
            customItem.setQuantity(customizationItem.getQuantity());
            customItem.setName(customizationItem.getName());
            customItem.setPrice(customizationItem.getPrice());
            outletCustomItems.add(customItem);
        }
        outletItem.setCustomizedItems(outletCustomItems);
        return outletItem;
    }
}
