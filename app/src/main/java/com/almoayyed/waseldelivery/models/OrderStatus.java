package com.almoayyed.waseldelivery.models;

/**
 * Created on 28 Nov 2016 4:35 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OrderStatus {

    private String PENDING;
    private String CONFIRM;

    public String getPENDING() {
        return PENDING;
    }

    public void setPENDING(String PENDING) {
        this.PENDING = PENDING;
    }

    public String getCONFIRM() {
        return CONFIRM;
    }

    public void setCONFIRM(String CONFIRM) {
        this.CONFIRM = CONFIRM;
    }
}
