package com.almoayyed.waseldelivery.models;

import android.text.TextUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletItemDetails;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class Outlet implements Serializable, Comparable<Outlet>{

    private String id;
    private String name;
    private String budget;
    private float rating;
    private String imageUrl;
    private float distance;
    private String description;
    private float deliveryCharge;
    private List<OutletItemDetails> outletItems; //Holds updated order item information
    private Address address;
    private boolean canAcessImages;
    private String opentime;

    private int closeBefore = 30; //time in min

    public float getRating() {
        return new BigDecimal(rating)
                .setScale(1,BigDecimal.ROUND_FLOOR).floatValue();

    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String  getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getDistance() {
//        return Math.round(distance);
        return new BigDecimal(distance ).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /*
    * when delivery charge  is zero, returns 'freeDelivery'
    * otherwise returns 'BD +deliveryChargeValue'
    * */
    public String getFormattedDeliveryCharge() {
        if(getDeliveryCharge() == 0){
            return WaselApplication.getContext().getString(R.string.free_delivery);
        }else {
            return String.format(WaselApplication.getContext().getString(R.string.price_format),getDeliveryCharge());
        }
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getDeliveryCharge() {
        return deliveryCharge;
    }

    public void setDeliveryCharge(float deliveryCharge) {
        this.deliveryCharge = deliveryCharge;
    }

    public List<OutletItemDetails> getOutletItems() {
        return outletItems;
    }

    public void setOutletItems(List<OutletItemDetails> outletItems) {
        this.outletItems = outletItems;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isCanAcessImages() {
        return canAcessImages;
    }

    public void setCanAcessImages(boolean canAcessImages) {
        this.canAcessImages = canAcessImages;
    }

    public String getOpentime() {
        if(TextUtils.isEmpty(opentime==null? "" : opentime.trim())){
            return WaselApplication.getContext().getString(R.string.outlet_not_available);
        }

        String outletShiftSeparator = "-";
        String outletTimeSeparator = ":";
        if(opentime.contains(outletShiftSeparator)){
            String shift[] =TextUtils.split(opentime,outletShiftSeparator);

            String shiftFrom[] = TextUtils.split(shift[0],outletTimeSeparator);
            String shiftTo[] = TextUtils.split(shift[1],outletTimeSeparator);

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            int now = calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE);

            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(shiftFrom[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(shiftFrom[1]));

            SimpleDateFormat formatter = new SimpleDateFormat("hh:mma");

            int from = Integer.parseInt(shiftFrom[0]) * 60 + Integer.parseInt(shiftFrom[1]);
            int toTimeAfterParse = Integer.parseInt(shiftTo[1]) - closeBefore;
            int to = Integer.parseInt(shiftTo[0]) * 60 + toTimeAfterParse;

            boolean isAvailable = now >= from && now<=to;
            StringBuffer fromStr = new StringBuffer(formatter.format(calendar.getTime()));

            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(shiftTo[0]));
            calendar.set(Calendar.MINUTE, toTimeAfterParse);
            StringBuffer toStr =  new StringBuffer(formatter.format(calendar.getTime()));

            return String.format(isAvailable?
                    WaselApplication.getContext().getString(R.string.outlet_available_shift_format):
                    WaselApplication.getContext().getString(R.string.outlet_next_available_shift_format),
                    fromStr, toStr);
        }

        return opentime;
    }

    public void setOpentime(String opentime) {
        this.opentime = opentime;
    }

    public boolean isOutletAvailable(){
        if(TextUtils.isEmpty(opentime==null? "" : opentime.trim())
                ||!outletCurrentlyAvailable()){
            return false;
        }
        return true;
    }

    //Check whether outlet is currently available or not
    private boolean outletCurrentlyAvailable() {
        String outletShiftSeparator = "-";
        String outletTimeSeparator = ":";
        if(opentime.contains(outletShiftSeparator)) {
            String shift[] = TextUtils.split(opentime, outletShiftSeparator);

            String shiftFrom[] = TextUtils.split(shift[0], outletTimeSeparator);
            String shiftTo[] = TextUtils.split(shift[1], outletTimeSeparator);
            Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone("UTC"));
            int now = c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE);
            int from = Integer.parseInt(shiftFrom[0]) * 60 + Integer.parseInt(shiftFrom[1]);
            int to = Integer.parseInt(shiftTo[0]) * 60 + Integer.parseInt(shiftTo[1]) - closeBefore;
            return (now >= from && now<=to);
        }else if(opentime.contains("24")){
            return true;
        }
        return false;
    }

    @Override
    public int compareTo(Outlet obj) {
        if (rating < obj.getRating()) {
            return 1;
        }
        else if (rating > obj.getRating()) {
            return -1;
        }
        else {
            return 0;
        }

    }
}
