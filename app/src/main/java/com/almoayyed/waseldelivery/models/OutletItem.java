package com.almoayyed.waseldelivery.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletItemDetails;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OutletItem extends BaseObservable implements Serializable {

    String id;
    String name;
    String itemDescription;
    boolean customized;
    Integer isCustomizable;
    double price;
    String imageUrl;
    int cartQuantity;
    ArrayList<OutletItemCustomField> outletItemCustomFields;
    ArrayList<OutletItemCustomization> customizedItems;
    ArrayList<OutletItemCustomization> prevCustomizedItems;
    boolean active;
    boolean delete;

    public boolean isHideImage() {
        return hideImage;
    }

    public void setHideImage(boolean hideImage) {
        this.hideImage = hideImage;
    }

    boolean hideImage;


    public OutletItem() {
        customizedItems = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    @Bindable
    public boolean isCustomized() {
        return customized;
    }

    public void setCustomized(boolean customized) {
        this.customized = customized;
        notifyPropertyChanged(BR.customized);
    }

    public boolean isCustomizable() {
        if(isCustomizable==1){
            return true;
        }else {
            return false;
        }
    }

    public void setCustomizable(Integer customizable) {
        isCustomizable = customizable;
    }

    public double getPrice() {
        return price;
    }

    @Bindable
    public double getTotalPrice() {
        return (price + getCustomizationPrice())* cartQuantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Bindable
    public int getCartQuantity() {
        return cartQuantity;
    }

    @Bindable
    public String getCartQuantityInString() {
        if(getCartQuantity() <= 0){
            return "   ";
        }
        return String.valueOf(getCartQuantity());
    }


    public void setCartQuantity(int cartQuantity) {
        this.cartQuantity = cartQuantity;
        notifyPropertyChanged(BR.cartQuantity);
    }

    public void incCartQuantity() {
        this.cartQuantity++;
        notifyPropertyChanged(BR.cartQuantity);
    }

    public void decCartQuantity() {
        this.cartQuantity--;
        notifyPropertyChanged(BR.cartQuantity);
    }

    @Bindable
    public float getCustomizationPrice(){
        float cusPrice = 0;
        for(OutletItemCustomization itemCustomization : customizedItems){
            cusPrice += itemCustomization.getPrice() * itemCustomization.getQuantity();
        }
        return cusPrice;
    }
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<OutletItemCustomization> getCustomizedItems() {
        return customizedItems;
    }

    public void setCustomizedItems(ArrayList<OutletItemCustomization> customizedItems) {
        this.customizedItems = customizedItems;
    }

    public void notifyCustomPrice(){
        notifyPropertyChanged(BR.customizationPrice);
    }

    //Used to revert previous customization changes
    public void revertCustomization() {
        this.customizedItems = prevCustomizedItems;
    }

    public void setCustomizedItemsCopy() {
        prevCustomizedItems = new ArrayList<>();
        for (OutletItemCustomization customizationItem : customizedItems){
            prevCustomizedItems.add(customizationItem.getCopy());
        }
    }

    public OrderItem convertToOrderItem(){
        OrderItem orderItem = new OrderItem();
        orderItem.setItemId(id);
        orderItem.setQuantity(cartQuantity);
        orderItem.setName(name);
        orderItem.setPrice(price);
        orderItem.setOutletItemCustomFields(outletItemCustomFields);
        List<OrderCustomItem> customItems = new ArrayList<>();
        for(OutletItemCustomization customizationItem: customizedItems){
            if (customizationItem.getQuantity()>0){
                //Sending only selected customization data
                OrderCustomItem customItem = new OrderCustomItem();
                customItem.setCustomItemId(customizationItem.getId());
                customItem.setQuantity(customizationItem.getQuantity());
                customItem.setName(customizationItem.getName());
                customItem.setPrice(customizationItem.getPrice());
                customItems.add(customItem);
            }
        }
        orderItem.setCustomItems(customItems);
        return orderItem;
    }

    public void clearCustomization() {
        if(customizedItems!=null){
            for(OutletItemCustomization item : customizedItems){
                item.setQuantity(0);
            }
        }
        setCustomized(false);
    }

    public ArrayList<OutletItemCustomField> getOutletItemCustomFields() {
        return outletItemCustomFields;
    }

    public void setOutletItemCustomFields(ArrayList<OutletItemCustomField> outletItemCustomFields) {
        this.outletItemCustomFields = outletItemCustomFields;
    }

    public boolean isOutletItemCustomFieldExist(@NotNull String fieldName){
        if(outletItemCustomFields == null){
            return false;
        }
        for (OutletItemCustomField customField : outletItemCustomFields){
            if(fieldName.equalsIgnoreCase(customField.getField().field)){
                return true;
            }
        }
        return false;
    }

    public boolean getOutletItemCustomFieldValue(@NotNull String fieldName){
        if(outletItemCustomFields == null){
            return false;
        }
        for (OutletItemCustomField customField : outletItemCustomFields){
            if(fieldName.equalsIgnoreCase(customField.getField().field)){
                return customField.value;
            }
        }
        return false;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }
}
