package com.almoayyed.waseldelivery.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.almoayyed.waseldelivery.BR;

import java.io.Serializable;

public class OutletItemCustomField extends BaseObservable implements Serializable{

    OutletItemCustomFieldParent field;
    boolean value;

    public OutletItemCustomFieldParent getField() {
        return field;
    }

    public void setField(OutletItemCustomFieldParent field) {
        this.field = field;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    /*Holds customization item tab information send by server*/
    public class OutletItemCustomFieldParent implements Serializable{
        Amenity amenity;
        String field;

        public Amenity getAmenity() {
            return amenity;
        }

        public void setAmenity(Amenity amenity) {
            this.amenity = amenity;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }
    }
}