package com.almoayyed.waseldelivery.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import com.almoayyed.waseldelivery.BR;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OutletItemCustomization extends BaseObservable implements Serializable{
    private String id;
    private String name;
    private double price;
    private int quantity;

    private String type; //Currently type info is not send by server

    private Parent category;
    private boolean active;
    private boolean delete;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Bindable
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
        notifyPropertyChanged(BR.quantity);
    }

    public void incQuantity() {
        this.quantity++;
        notifyPropertyChanged(BR.quantity);
    }

    public void decQuantity() {
        this.quantity--;
        notifyPropertyChanged(BR.quantity);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        type = category.getSelectionType();
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Parent getCategory() {
        return category;
    }

    public void setCategory(Parent category) {
        this.category = category;
    }

    public OutletItemCustomization getCopy(){
        OutletItemCustomization item = new OutletItemCustomization();
        item.setId(id);
        item.setName(name);
        /*item.setCustomItemId(customItemId);*/
        item.setPrice(price);
        item.setQuantity(quantity);
        item.setType(type);
        item.setCategory(category);
        return item;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    /*Holds customization item tab information send by server*/
    public class Parent implements Serializable{
        int id;
        String name;
        String selectionType;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSelectionType() {
            return selectionType;
        }

        public void setSelectionType(String selectionType) {
            this.selectionType = selectionType;
        }
    }
}