package com.almoayyed.waseldelivery.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * This object hold the information required for registration process
 */
public class Registration implements Serializable{

    //Holds the social media account id
    String facebookUID;
    String googleUID;

    @SerializedName("name")
    String name;

    @SerializedName("password")
    String password;
    String confirmPassword;

    @SerializedName("email")
    String emailId;

    @SerializedName("mobile")
    String mobile;

    @SerializedName("accountType")
    int accountType;

    ArrayList<DeviceIds> device;

    boolean shouldReadEmailId = false; //Flag used to show the email field in "AddPhoneNo" screen

    public Registration(){

    }

    public Registration(Registration registration){
        facebookUID = registration.getFacebookUID();
        googleUID = registration.getGoogleUID();
        name = registration.getName();
        password = registration.getPassword();
        confirmPassword = registration.getConfirmPassword();
        emailId = registration.getEmailId();
        mobile = registration.getMobile();
        accountType = registration.getAccountType();
        device = registration.getDevice();
    }

    public String getFacebookUID() {
        return facebookUID;
    }

    public void setFacebookUID(String facebookUID) {
        this.facebookUID = facebookUID;
    }

    public String getGoogleUID() {
        return googleUID;
    }

    public void setGoogleUID(String googleUID) {
        this.googleUID = googleUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public ArrayList<DeviceIds> getDevice() {
        return device;
    }

    public void setDevice(ArrayList<DeviceIds> device) {
        this.device = device;
    }

    public boolean isShouldReadEmailId() {
        return shouldReadEmailId;
    }

    public void setShouldReadEmailId(boolean shouldReadEmailId) {
        this.shouldReadEmailId = shouldReadEmailId;
    }
}
