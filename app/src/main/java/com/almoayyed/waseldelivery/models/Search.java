package com.almoayyed.waseldelivery.models;

import java.io.Serializable;

/**
 * Created on 29 Nov 2016 6:40 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class Search implements Serializable {

    public String searchedName;
    public String searchedId;

    public String getSearchedName() {
        return searchedName;
    }

    public void setSearchedName(String searchedName) {
        this.searchedName = searchedName;
    }

    public String getSearchedId() {
        return searchedId;
    }

    public void setSearchedId(String searchedId) {
        this.searchedId = searchedId;
    }
}
