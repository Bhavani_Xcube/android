package com.almoayyed.waseldelivery.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.TextUtils;

import com.almoayyed.waseldelivery.BR;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 11 Nov 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Holds latitude, longitude & its corresponding address
 */
public class UserLocation extends BaseObservable implements Serializable {

    String subLocality3 = ""; //Indicates point of interest
    String subLocality2 = ""; //refers sublocality2 of google geo-code response data
    String subLocality1 = "";//refers sublocality1 of google geo-code response data
    String city = "";
    String state = "";
    String country = "";
    String zip = "";
    String locDescription = "";
    String formattedAddress = "";
    double latitude;
    double longitude;

    public String getSubLocality3() {
        return subLocality3;
    }

    public void setSubLocality3(String subLocality3) {
        this.subLocality3 = subLocality3;
    }

    public String getSubLocality2() {
        return subLocality2;
    }

    public void setSubLocality2(String subLocality2) {
        this.subLocality2 = subLocality2;
    }

    public String getSubLocality1() {
        return subLocality1;
    }

    public void setSubLocality1(String subLocality1) {
        this.subLocality1 = subLocality1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
        notifyPropertyChanged(BR.formattedAddress);
    }

    public void setLocDescription(String locDescription) {
        this.locDescription = locDescription;
    }

    public String getLocDescription() {
        return locDescription;
    }

    @Bindable
    public String getFormattedAddress(){
        if(formattedAddress.length()>0){
            return formattedAddress;
        }
        formattedAddress = getLocationName();
        formattedAddress = formattedAddress + " ▼";
        return formattedAddress;
    }

    @Bindable
    public String getCompleteAddress(){
        List<String> filterStr = filterEmpty(subLocality3,subLocality2,subLocality1,city,state,country);
        return TextUtils.join(", ",filterStr);
    }

    public String getLocationName(){
        if(!TextUtils.isEmpty(subLocality3)){
            return subLocality3;
        }else if(!TextUtils.isEmpty(subLocality2)){
            return subLocality2;
        }else if(!TextUtils.isEmpty(subLocality1)){
            return subLocality1;
        } else if(!TextUtils.isEmpty(city)){
            return city;
        } else if(!TextUtils.isEmpty(state)){
            return state;
        }else if(!TextUtils.isEmpty(country)){
            return country;
        }
        return "Location";
    }

    public String getLocationArea(){
        List<String> filterStr = filterEmpty(subLocality3,subLocality2,subLocality1,city,state,country);

        if(filterStr.size() > 1){
            return TextUtils.join(", ",filterStr.subList(1,filterStr.size()).toArray()).toString();
        }else if(filterStr.size() == 1){
            return filterStr.get(0);
        }
        return "";
    }

    static List<String> filterEmpty(String... args) {
        List<String> list = new ArrayList<String>();
        for (String s : args) {
            if (!TextUtils.isEmpty(s))
                list.add(s);
        }
        return list;
    }

    public void clear(){
        subLocality3 = "";
        subLocality2 = "";
        subLocality1 = "";
        city = "";
        state = "";
        country = "";
        zip = "";
        formattedAddress = "";
        latitude = 0;
        longitude = 0;
    }

    public Address convertToAddress(){
        Address address = new Address();
        address.setLatitude(latitude);
        address.setLongitude(longitude);
        address.setLocation(getLocDescription());
        address.setCountry(country);
        return  address;
    }

    public void setDescription(String selectedPlace) {
        subLocality2 = "";
        subLocality1 = "";
        city = "";
        state = "";
        country = "";
        zip = "";
        formattedAddress = "";
        subLocality3 = selectedPlace;
    }
}
