
package com.almoayyed.waseldelivery.models;


/**
 * Created on 13 Jan 2016 4:50 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class VersionResponse {

    private String version;
    private String s3BaseUrl;


    public String getS3BaseUrl() {
        return s3BaseUrl;
    }

    public void setS3BaseUrl(String s3BaseUrl) {
        this.s3BaseUrl = s3BaseUrl;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
