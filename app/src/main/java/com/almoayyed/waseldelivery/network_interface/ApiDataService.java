package com.almoayyed.waseldelivery.network_interface;

import android.support.annotation.NonNull;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.models.Coupons;
import com.almoayyed.waseldelivery.models.Cuisine;
import com.almoayyed.waseldelivery.models.DeliveryChargeInput;
import com.almoayyed.waseldelivery.models.Feedback;
import com.almoayyed.waseldelivery.models.HelpAndSupport;
import com.almoayyed.waseldelivery.models.LogoutReq;
import com.almoayyed.waseldelivery.models.Notifications;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.models.Amenity;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.models.VersionResponse;
import com.almoayyed.waseldelivery.network_interface.wasel_api.CheckUserExistReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.GenerateOtpReqBody;
import com.almoayyed.waseldelivery.models.Registration;
import com.almoayyed.waseldelivery.network_interface.wasel_api.WaselInterface;
import com.almoayyed.waseldelivery.network_interface.wasel_api.VerifyOtpReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.ApiResponse;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.wasel_api.UserReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletDetailsResponse;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletsReqBody;
import com.almoayyed.waseldelivery.utils.NetworkConnection;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 14 Nov 2016 3:06 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Makes actual calls to the Wasel Server & returns their response.
 */
public class ApiDataService extends WaselDeliveryService {

    private final WaselInterface waselService;

    public ApiDataService(){
        waselService = RetrofitAdapter.getRetrofitInstance().create(WaselInterface.class);
    }

    public<T> Observable<T> checkNetwork() {
        //Returns NetworkStatusException when there is no network
        //Otherwise return nothing
        if(NetworkConnection.isNetworkAvailable()){
            return Observable.never();
        } else{
            return Observable.error(new NetworkStatusException(WaselApplication.getContext().getString(R.string.check_internet)));
        }
    }
    /*----------------------------------Notification module API calls started------------------------*/

    @Override
    public Observable<Response> getNotification(Notifications notifications) {

        Observable<Response> network = checkNetwork();
        return network.ambWith(waselService.getNotification(notifications)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));


    }
/*----------------------------------Login Notification API calls started------------------------*/

    /*----------------------------------Login module API calls started------------------------*/

    @Override
    public Observable<Response> generateOTP(GenerateOtpReqBody otpReqBody) {

        Observable<Response> network = checkNetwork();
        return network.ambWith(waselService.generateOTP(otpReqBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));


    }

    @Override
    public Observable<ApiResponse> verifyOTP(@NonNull VerifyOtpReqBody verifyOtpReqBody) {
        //Check internet and parallelly makes network call.
        //If internet not exist then immediately NetworkStatusException is thrown
        //Otherwise api response is returned
        Observable<ApiResponse> network = checkNetwork();
        return network.ambWith(waselService.verifyOTP(verifyOtpReqBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));


    }

    @Override
    public Observable<UserLoginDetails> registerUser(@NonNull Registration user) {
        Observable<UserLoginDetails> network = checkNetwork();
        return network.ambWith(waselService.registerUser(user)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));

    }

    @Override
    public Observable<UserLoginDetails> getLoginDetails(@NonNull UserReqBody reqBody) {
        Observable<UserLoginDetails> network = checkNetwork();
        return network.ambWith(waselService.userLoginDetails(reqBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<UserLoginDetails> checkForExistingUser(@NonNull CheckUserExistReqBody reqBody) {
        Observable<UserLoginDetails> network = checkNetwork();
        return network.ambWith(waselService.checkUserExistence(reqBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<ApiResponse> changePassword(@NonNull UserReqBody reqBody) {
        Observable<ApiResponse> network = checkNetwork();
        return network.ambWith(waselService.resetPassword(reqBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<UserLoginDetails> updateProfile(UserLoginDetails details) {
        Observable<UserLoginDetails> network = checkNetwork();
        return network.ambWith(waselService.updateUserProfile(details)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<UserLoginDetails> saveProfilePic(RequestBody userName, MultipartBody.Part path) {
        Observable<UserLoginDetails> network = checkNetwork();
        return network.ambWith(waselService.saveProfilePic(userName,path)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<UserLoginDetails> getProfile(String id) {
        Observable<UserLoginDetails> network = checkNetwork();
        return network.ambWith(waselService.getUserProfile(id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<Response> logOut(@NonNull LogoutReq regData) {
        Observable<Response> network = checkNetwork();
        return network.ambWith(waselService.logOut(regData)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<VersionResponse> appVersion() {
        Observable<VersionResponse> network = checkNetwork();
        return network.ambWith(waselService.appVersion()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<HelpAndSupport> getHelpAndSupportDetails() {
        Observable<HelpAndSupport> network = checkNetwork();
        return network.ambWith(waselService.getHelpAndSupportDetails()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }
/*----------------------------------Login module API calls ended-------------------------*/

    /*----------------------------------Homepage API calls started------------------------*/
    @Override
    public Observable<List<Amenity>> getAmenities() {
        Observable<List<Amenity>> network = checkNetwork();
        return network.ambWith(waselService.loadAmenities()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<List<Outlet>> getOutlets(@NonNull final OutletsReqBody requestBody) {
        Observable<List<Outlet>> network = checkNetwork();
        return network.ambWith(waselService.loadOutlets(requestBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<List<Outlet>> getSearchedOutlets(@NonNull final OutletsReqBody requestBody) {
        Observable<List<Outlet>> network = checkNetwork();
        return network.ambWith(waselService.loadSearchedOutlets(requestBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<OutletDetailsResponse> getOutletDetails(@NonNull final String outletId) {
        Observable<OutletDetailsResponse> network = checkNetwork();
        return network.ambWith(waselService.loadOutletDetails(Integer.parseInt(outletId))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<List<Cuisine>> getCuisines() {
        Observable<List<Cuisine>> network = checkNetwork();
        return network.ambWith(waselService.getCusines()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<List<Outlet>> searchOutlets(final String restaurant) {
        //TODO: need to include search restaurant API
        return Observable.empty();
    }

    /*----------------------------------Homepage API calls ended------------------------*/

    /************-----------Checkout API calls started--------------********/
    @Override
    public Observable<Order> confirmOrder(Order order) {
        Observable<Order> network = checkNetwork();
        return network.ambWith(waselService.confirmOrder(order)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<List<Coupons>> getCoupons() {
        Observable<List<Coupons>> network = checkNetwork();
        return network.ambWith(waselService.getCoupons()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<List<Order>> getOrderHistory(String userId, int start, int limit) {
        Observable<List<Order>> network = checkNetwork();
        return network.ambWith(waselService.getOrderHistory(userId, start, limit)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<Order> getOrderDetails(String orderId) {
        Observable<Order> network = checkNetwork();
        return network.ambWith(waselService.getOrderDetails(orderId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<Response> setOrderFeedback(@NonNull List<Feedback> feedbackBody) {
        Observable<Response> network = checkNetwork();
        return network.ambWith(waselService.orderFeedback(feedbackBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<List<Order>> getPendingFeedbackOrders(String userId) {
        Observable<List<Order>> network = checkNetwork();
        return network.ambWith(waselService.getPendingFeedbackOrders(userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    public  Observable<Response> verifyCoupon(String userId, long couponId)  {
        Observable<Response> network = checkNetwork();
        return network.ambWith(waselService.verifyCoupon(userId, couponId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    @Override
    public Observable<List<Address>> getDeliveryCharges(DeliveryChargeInput data) {
        Observable<List<Address>> network = checkNetwork();
        return network.ambWith(waselService.getDeliveryCharges(data)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()));
    }

    /************-----------Checkout API calls ended--------------********/
}
