package com.almoayyed.waseldelivery.network_interface;

/**
 * Created on 28 Nov 2016 11:19 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * NetworkStatusException is created to pass internal network related message
 * Eg: no internet connection
 */
public class NetworkStatusException extends Exception {
    String message;

    public NetworkStatusException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
