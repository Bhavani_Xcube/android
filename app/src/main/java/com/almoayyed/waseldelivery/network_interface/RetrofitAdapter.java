package com.almoayyed.waseldelivery.network_interface;


import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import android.util.Log;

import com.almoayyed.waseldelivery.BuildConfig;
import com.almoayyed.waseldelivery.constants.ServerConstants;
import com.almoayyed.waseldelivery.models.CustomizationItem;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/*
* Retrofit instance is a singleTon.
* As same instance is used for creating different instancfes
* */
public class RetrofitAdapter {
    static Retrofit retrofit;

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL + "api/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(getClient())
                    .addConverterFactory(GsonConverterFactory.create(getLineUpGson()))
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient getClient() {
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addNetworkInterceptor(new SecurityInterceptor())
                .build();

        return okHttpClient;
    }

    public static Gson getLineUpGson() {

        return new GsonBuilder().registerTypeAdapter(CustomizationItem.class, new JsonDeserializer<CustomizationItem>() {

            @Override
            public CustomizationItem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                Log.e("CustomizationItem", "CustomizationItem==" + json);
                CustomizationItem customizationItem = new CustomizationItem();
                try {
                    JsonObject elemsObj = json.getAsJsonObject();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                return customizationItem;
            }
        }).create();
    }

    public static final class SecurityInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            Request.Builder builder = request.newBuilder();
            if(LoginCredentialsPreference.getLoginPreference().isUserLoggedIn()){
                List<String> customAnnotations = request.headers().values("Authorization");
                if(customAnnotations.size()>0) {
                    builder.addHeader("Authorization", "Bearer "+LoginCredentialsPreference.getLoginPreference().getAccessToken());
                }
            }else{
                builder.addHeader("Content-Type", "application/json");
                builder.addHeader("Accept", "application/json");
            }

            return chain.proceed(builder.build());
        }
    }
}