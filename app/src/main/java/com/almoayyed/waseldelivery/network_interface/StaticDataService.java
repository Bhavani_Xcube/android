package com.almoayyed.waseldelivery.network_interface;

import android.support.annotation.NonNull;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.models.Coupons;
import com.almoayyed.waseldelivery.models.Cuisine;
import com.almoayyed.waseldelivery.models.DeliveryChargeInput;
import com.almoayyed.waseldelivery.models.Feedback;
import com.almoayyed.waseldelivery.models.HelpAndSupport;
import com.almoayyed.waseldelivery.models.LogoutReq;
import com.almoayyed.waseldelivery.models.Notifications;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.models.OrderItem;
import com.almoayyed.waseldelivery.models.OutletItem;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.models.Amenity;
import com.almoayyed.waseldelivery.models.OutletItemCustomization;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.models.VersionResponse;
import com.almoayyed.waseldelivery.network_interface.wasel_api.CheckUserExistReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.GenerateOtpReqBody;
import com.almoayyed.waseldelivery.models.Registration;
import com.almoayyed.waseldelivery.network_interface.wasel_api.VerifyOtpReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.ApiResponse;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.wasel_api.UserReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletDetailsResponse;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletItemDetails;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletsReqBody;
import com.almoayyed.waseldelivery.utils.CheckAppVersion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created on 14 Nov 2016 3:06 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Returns the static data to the api calls
 */
public class StaticDataService extends WaselDeliveryService
{
    @Override
    public Observable<List<Amenity>> getAmenities() {
        List<Amenity> amenities = new ArrayList<>();
        String[] array = WaselApplication.getContext().getResources().getStringArray(R.array.categories_array);
        Map<String, String> categories = new HashMap<>();
        for (int index = 0; index<array.length;index++) {
            String[] splittedItem = array[index].split("-");
            categories.put(splittedItem[0], splittedItem[1]);
            Amenity amenity = new Amenity();
            amenity.setId(Integer.parseInt(splittedItem[0]));
            amenity.setName(splittedItem[1]);
            amenities.add(amenity);
        }
        Observable returnObservable = Observable
                .just(amenities)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        return returnObservable;
    }

    public Observable<Response> getNotification(@NonNull Notifications notifications) {
        return Observable.<Response>just(null)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Response> generateOTP(@NonNull GenerateOtpReqBody otpReqBody) {
        return Observable.<Response>just(null)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ApiResponse> verifyOTP(@NonNull VerifyOtpReqBody otpReqBody) {
        ApiResponse response = new ApiResponse();
        response.setMessage("otp verified");
        return Observable.just(response)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response> logOut(LogoutReq regData) {
        //TODO: update order info
        return Observable
                .fromCallable(new Callable<Response>() {
                    @Override
                    public Response call() throws Exception {
                        return new Response();
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<VersionResponse> appVersion() {
        //TODO: update order info
        return Observable
                .fromCallable(new Callable<VersionResponse>() {
                    @Override
                    public VersionResponse call() throws Exception {
                        VersionResponse response = new VersionResponse();
                        response.setVersion(CheckAppVersion.getAppVersion(WaselApplication.getContext()));

                        return response;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }


    @Override
    public Observable<UserLoginDetails> registerUser(@NonNull Registration user) {
        UserLoginDetails registrationResponse = new UserLoginDetails();
        registrationResponse.setName(user.getName());
        registrationResponse.setMobile(user.getMobile());
        registrationResponse.setAccountType(user.getAccountType());
        registrationResponse.setEmail(user.getEmailId());
        return Observable.just(registrationResponse)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserLoginDetails> getLoginDetails(UserReqBody reqBody) {
        return Observable
                .fromCallable(new Callable<UserLoginDetails>() {
                    @Override
                    public UserLoginDetails call() throws Exception {
                        return getStaticUserLoginDetails();
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }


    @Override
    public Observable<UserLoginDetails> checkForExistingUser(@NonNull CheckUserExistReqBody reqBody) {
        return Observable
                .fromCallable(new Callable<UserLoginDetails>() {
                    @Override
                    public UserLoginDetails call() throws Exception {
                        return getStaticUserLoginDetails();
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ApiResponse> changePassword(@NonNull UserReqBody reqBody) {
        ApiResponse response = new ApiResponse();
        response.setMessage("password changed");
        return Observable.just(response)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Outlet>> getOutlets(final OutletsReqBody requestBody) {
        /*final OutletsReqBody.Filter filter = requestBody.getFilter();*/
        Observable returnObsr = Observable
                .fromCallable(new Callable<List<Outlet>>() {
                    @Override
                    public List<Outlet> call() throws Exception {
                        List<Outlet> restaurants = getStaticRestaurants();

                        /*//sort the data
                        if(filter.isRating()){
                            Collections.sort(restaurants);
                        }
                        if(filter.getBudgets()!=null && !filter.getBudgets().isEmpty()){
                            List<Outlet> tmpRestaurants = new ArrayList<Outlet>();
                            List<Integer> budgetList = filter.getBudgets();
                            for(int i=0 ; i<restaurants.size();i++){
                                if(budgetList.contains(restaurants.get(i).getBudget())){
                                    tmpRestaurants.add(restaurants.get(i));
                                }
                            }
                            restaurants = tmpRestaurants;
                        }
                        if(filter.getCuisines()!=null && !filter.getCuisines().isEmpty()){
                            List<Outlet> tmpRestaurants = new ArrayList<Outlet>();
                            List<Integer> selCuisines = filter.getCuisines();
                            for(int i=0 ; i<restaurants.size();i++){
                                List<Cuisine> cuisines = restaurants.get(i).getCuisines();
                                for(int j=0; j<cuisines.size();j++){
                                    if(selCuisines.contains(Integer.parseInt(cuisines.get(j).getId()))){
                                        tmpRestaurants.add(restaurants.get(i));
                                        break;
                                    }
                                }
                            }
                            restaurants = tmpRestaurants;
                        }*/
                        return restaurants;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        return returnObsr;
    }

    @Override
    public Observable<List<Outlet>> getSearchedOutlets(@NonNull final OutletsReqBody reqBody) {
        Observable returnObsr = Observable
                .fromCallable(new Callable<List<Outlet>>() {
                    @Override
                    public List<Outlet> call() throws Exception {
                        List<Outlet> restaurants = getStaticRestaurants();
                        return restaurants;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        return returnObsr;
    }

    @Override
    public Observable<OutletDetailsResponse> getOutletDetails(String outletId) {
        return  Observable
                .fromCallable(new Callable<OutletDetailsResponse>() {
                    @Override
                    public OutletDetailsResponse call() throws Exception {
                        return getStaticRestaurantDetails();
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Cuisine>> getCuisines() {
        return Observable
                .fromCallable(new Callable<List<Cuisine>>() {
                    @Override
                    public List<Cuisine> call() throws Exception {
                        return getStaticCuisines();
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Outlet>> searchOutlets(@NonNull final String restaurant) {
        return Observable
                .fromCallable(new Callable<List<Outlet>>() {
                    @Override
                    public List<Outlet> call() throws Exception {
                        return searchStaticRestaurants(restaurant);
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Order> confirmOrder(@NonNull final Order order) {
        return Observable
                .fromCallable(new Callable<Order>() {
                    @Override
                    public Order call() throws Exception {
                        order.setId("1");
                        return order;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response> setOrderFeedback(@NonNull List<Feedback> feedbackBody) {
        return Observable
                .fromCallable(new Callable<Response>() {
                    @Override
                    public Response call() throws Exception {
                        Response response = new Response();
                        response.setMessage("success");
                        return response;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserLoginDetails> updateProfile(final UserLoginDetails details) {
        Address address = new Address();
        address.setAddressType("Home ");
        address.setLocation("Shaikh Salman Avenue - 9 Shaikh Salman Hwy Block 428, Seef Area, Kingdom of Bahrain ");
        ArrayList<Address> addressList = new ArrayList<>();
        addressList.add(address);
        details.setAddress(addressList);
        return Observable
                .fromCallable(new Callable<UserLoginDetails>() {
                    @Override
                    public UserLoginDetails call() throws Exception {
                        return details;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserLoginDetails> saveProfilePic(final RequestBody userName, final MultipartBody.Part path) {
      final UserLoginDetails userLoginDetails = new UserLoginDetails();
        userLoginDetails.setName("Sample");
        userLoginDetails.setImageUrl("home/wasel/1.png");
        return Observable
                .fromCallable(new Callable<UserLoginDetails>() {
                    @Override
                    public UserLoginDetails call() throws Exception {
                        return userLoginDetails;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserLoginDetails> getProfile(String id) {
        final UserLoginDetails details = new UserLoginDetails();
        details.setId("1");
        return Observable
                .fromCallable(new Callable<UserLoginDetails>() {
                    @Override
                    public UserLoginDetails call() throws Exception {
                        return details;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Coupons>> getCoupons() {
        return Observable.fromCallable(new Callable<List<Coupons>>() {
            @Override
            public List<Coupons> call() throws Exception {
                return new ArrayList<Coupons>();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Order>> getOrderHistory(String userId, int start, int limit) {
        return Observable
                .fromCallable(new Callable<List<Order>>() {
                    @Override
                    public List<Order> call() throws Exception {
                        return new ArrayList<Order>();
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Order> getOrderDetails(String orderId) {
        return Observable
                .fromCallable(new Callable<Order>() {
                    @Override
                    public Order call() throws Exception {
                        return new Order();
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Order>> getPendingFeedbackOrders(String userId) {
        return Observable
                .fromCallable(new Callable<List<Order>>() {
                    @Override
                    public List<Order> call() throws Exception {
                        /*Outlet outlet = new Outlet();
                        outlet.setId("1");
                        outlet.setName("outlet");

                        ArrayList<Order> orders = new ArrayList<>();
                        Order e1 = new Order();
                        e1.setId("1");
                        e1.setOutlet(outlet);
                        e1.setItems(new ArrayList<OrderItem>());
                        orders.add(e1);
                        Order e2 = new Order();
                        e2.setId("2");
                        e2.setOutlet(outlet);
                        e2.setItems(new ArrayList<OrderItem>());
                        orders.add(e2);
                        Order e3 = new Order();
                        e3.setId("3");
                        e3.setOutlet(outlet);
                        e3.setItems(new ArrayList<OrderItem>());
                        orders.add(e3);
                        Order e4 = new Order();
                        e4.setId("4");
                        e4.setOutlet(outlet);
                        e4.setItems(new ArrayList<OrderItem>());
                        orders.add(e4);
                        Order e5 = new Order();
                        e5.setId("5");
                        e5.setOutlet(outlet);
                        e5.setItems(new ArrayList<OrderItem>());
                        orders.add(e5);
                        Order e6 = new Order();
                        e6.setId("6");
                        e6.setOutlet(outlet);
                        e6.setItems(new ArrayList<OrderItem>());
                        orders.add(e6);
                        return orders;*/
                        return null;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response> verifyCoupon(String userId, long couponId) {
        return Observable
                .fromCallable(new Callable<Response>() {
                    @Override
                    public Response call() throws Exception {
                        return new Response();
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Address>> getDeliveryCharges(DeliveryChargeInput data) {
        return Observable
                .fromCallable(new Callable<List<Address>>() {
                    @Override
                    public List<Address> call() throws Exception {
                        return new ArrayList<>();
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<HelpAndSupport> getHelpAndSupportDetails() {
        return Observable
                .fromCallable(new Callable<HelpAndSupport>() {
                    @Override
                    public HelpAndSupport call() throws Exception {
                        HelpAndSupport helpAndSupport = new HelpAndSupport();
                        helpAndSupport.setMobile("+973-4998585262");
                        helpAndSupport.setEmail("Ahmed@xyz.com");
                        return helpAndSupport;
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /*******************start: Static data methods*******************************/
    public String loadJSONFromAsset(String filename) {
        String json = null;
        try {
            InputStream is = WaselApplication.getContext().getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private UserLoginDetails getStaticUserLoginDetails() {
        UserLoginDetails json = new UserLoginDetails();
        json.setMobile("67890070");
        json.setAccountType(1);
        json.setActive(true);
        json.setEmail("abc@gmail.com");
        json.setId("1");
        json.setName("Wasel");
        return json;
    }


    @NonNull
    private List<Outlet> getStaticRestaurants() {
        List<Outlet> resList = new ArrayList<>();

        String restaurantsData = loadJSONFromAsset(WaselApplication.getContext().getResources().getString(R.string.static_data_restaurants));
        try {
            JSONArray array = new JSONArray(restaurantsData);
            for (int i=0; i<array.length(); i++){
                JSONObject resJsonObj = array.getJSONObject(i);
                Outlet restaurant = new Outlet();
                restaurant.setId(resJsonObj.getString("id"));
                restaurant.setName(resJsonObj.getString("name"));
                restaurant.setImageUrl(resJsonObj.getString("imageUrl"));
                restaurant.setBudget("low");//resJsonObj.getInt("budget")
                /*List<Cuisine> cuisines = new ArrayList<>();
                JSONArray cuisineArray = resJsonObj.getJSONArray("cuisines");
                for (int j=0; j<cuisineArray.length(); j++){
                    Cuisine tmpCusine = new Cuisine();
                    JSONObject cuiJsonObj =cuisineArray.getJSONObject(j);
                    tmpCusine.setId(cuiJsonObj.getString("cuisine_id"));
                    tmpCusine.setName(cuiJsonObj.getString("name"));
                    cuisines.add(tmpCusine);
                }
                restaurant.setCuisines(cuisines);*/
                restaurant.setDescription("Japanese, Sushi, Asian");
                restaurant.setDistance(3);
                restaurant.setRating((float)resJsonObj.getDouble("rating"));
                resList.add(restaurant);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return resList;
    }

    @NonNull
    private List<Outlet> searchStaticRestaurants(String text) {

        if(text==null || text.length()==0){
            return  null;
        }
        List<Outlet> restaurants = getStaticRestaurants();
        List<Outlet> searchRestaurants = new ArrayList<>();
        for(int i=0; i<restaurants.size(); i++){
            if(restaurants.get(i).getName().toLowerCase().contains(text.toLowerCase())
                    || (restaurants.get(i).getDescription()!=null
                    && restaurants.get(i).getDescription().toLowerCase().contains(text.toLowerCase()))){
                searchRestaurants.add(restaurants.get(i));
            }
        }
        return searchRestaurants;
    }

    public OutletDetailsResponse getStaticRestaurantDetails() {
        String menuListJson = loadJSONFromAsset("staticdata/fooditems.json");

        OutletDetailsResponse response = new OutletDetailsResponse();

        HashMap<String, Map<String,List<OutletItem>>> category = null;
        try {
            JSONObject json= (JSONObject) new JSONTokener(menuListJson).nextValue();
            response.setCanAccessImages(json.getBoolean("canAcessImages"));

            response.setOutletItems(new ArrayList<OutletItemDetails>());
            JSONArray outletJsonItems = json.getJSONArray("outletItems");
            for(int i=0; i<outletJsonItems.length(); i++){
                OutletItemDetails outletItemDetails = new OutletItemDetails();
                JSONObject jsonObject = outletJsonItems.getJSONObject(i);
                outletItemDetails.setId(jsonObject.getString("id"));
                outletItemDetails.setName(jsonObject.getString("name"));
                outletItemDetails.setDescription(jsonObject.getString("description"));
                outletItemDetails.setPrice(Float.parseFloat(jsonObject.getString("price")));
                outletItemDetails.setImageUrl(jsonObject.getString("imageUrl"));
                outletItemDetails.setRecommend(jsonObject.getBoolean("recommend"));

                //adding accordion
                JSONObject accordionJson = jsonObject.getJSONObject("itemCategory");
                outletItemDetails.setItemCategory(outletItemDetails.new Parent());
                outletItemDetails.getItemCategory().setId(accordionJson.getInt("id"));
                outletItemDetails.getItemCategory().setName(accordionJson.getString("name"));

                //adding tab
                JSONObject tabJson = accordionJson.getJSONObject("parent");
                outletItemDetails.getItemCategory().setParent(outletItemDetails.new Parent());
                outletItemDetails.getItemCategory().getParent().setId(tabJson.getInt("id"));
                outletItemDetails.getItemCategory().getParent().setName(tabJson.getString("name"));

                //Customization item
                JSONArray customizationJsonArray = jsonObject.getJSONArray("customizationItems");
                ArrayList<OutletItemCustomization> customizationItems = new ArrayList<>();
                for(int index = 0;index<customizationJsonArray.length();index++){
                    JSONObject customizationJson = customizationJsonArray.getJSONObject(index);
                    OutletItemCustomization customizationItem = new OutletItemCustomization();
                    customizationItem.setId(customizationJson.getString("id"));
                    customizationItem.setName(customizationJson.getString("name"));
                    customizationItem.setPrice(customizationJson.getDouble("price"));
                    customizationItem.setType(customizationJson.getString("type"));

                    //Setting customization tab
                    JSONObject customizeParentJson = customizationJson.getJSONObject("category");
                    OutletItemCustomization.Parent customizeParent = customizationItem.new Parent();
                    customizeParent.setId(customizeParentJson.getInt("id"));
                    customizeParent.setName(customizeParentJson.getString("name"));
                    customizationItem.setCategory(customizeParent);

                    customizationItems.add(customizationItem);
                }
                outletItemDetails.setCustomizationItems(customizationItems);

                response.getOutletItems().add(outletItemDetails);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }

    private List<Cuisine> getStaticCuisines(){
        String cuisinesData = loadJSONFromAsset(WaselApplication.getContext().getResources().getString(R.string.static_data_cuisines));
        List<Cuisine> cuisines = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(cuisinesData);
            for (int i=0; i<array.length(); i++){
                JSONObject cuiJsonObj = array.getJSONObject(i);
                Cuisine tmpCusine = new Cuisine();
                tmpCusine.setId(cuiJsonObj.getString("cuisine_id"));
                tmpCusine.setName(cuiJsonObj.getString("name"));
                cuisines.add(tmpCusine);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cuisines;
    }

    /*******************end: Static data methods*******************************/
}
