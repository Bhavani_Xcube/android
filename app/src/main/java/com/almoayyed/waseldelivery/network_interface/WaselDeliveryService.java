package com.almoayyed.waseldelivery.network_interface;

import android.support.annotation.NonNull;

import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.models.Coupons;
import com.almoayyed.waseldelivery.models.Cuisine;
import com.almoayyed.waseldelivery.models.DeliveryChargeInput;
import com.almoayyed.waseldelivery.models.Feedback;
import com.almoayyed.waseldelivery.models.HelpAndSupport;
import com.almoayyed.waseldelivery.models.LogoutReq;
import com.almoayyed.waseldelivery.models.Notifications;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.models.Amenity;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.models.VersionResponse;
import com.almoayyed.waseldelivery.network_interface.wasel_api.CheckUserExistReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.GenerateOtpReqBody;
import com.almoayyed.waseldelivery.models.Registration;
import com.almoayyed.waseldelivery.network_interface.wasel_api.VerifyOtpReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.ApiResponse;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.wasel_api.UserReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletDetailsResponse;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletsReqBody;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;

/**
 * Created on 14 Nov 2016 3:02 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Contains all the server calls
 */
public abstract class WaselDeliveryService {
    public static final int SERVICE_OFFLINE = 0;
    public static final int SERVICE_ONLINE = 1;
    static int serviceType = SERVICE_OFFLINE;

    private static WaselDeliveryService service;

    public static int getServiceType(){
        return serviceType;
    }

    public static void setServiceType(int typeOfService) {
        serviceType = typeOfService;
        switch (serviceType){
            case SERVICE_OFFLINE:
                service = new StaticDataService();
                break;
            case SERVICE_ONLINE:
                service = new ApiDataService();
                break;
        }
    }

    public static WaselDeliveryService getService() {
        return service;
    }

    /************-----------Notification module API calls started--------------********/

    public abstract Observable<Response> getNotification(@NonNull Notifications notifications);

    /************-----------Notification module API calls started--------------********/

    /************-----------Login module API calls started--------------********/
    public abstract Observable<Response> generateOTP(@NonNull GenerateOtpReqBody otpReqBody);

    public abstract Observable<ApiResponse> verifyOTP(@NonNull VerifyOtpReqBody verifyOtpReqBody);

    public abstract Observable<UserLoginDetails> registerUser(@NonNull Registration user);

    public abstract Observable<UserLoginDetails> getLoginDetails(@NonNull UserReqBody reqBody);

    public abstract Observable<UserLoginDetails> checkForExistingUser(@NonNull CheckUserExistReqBody reqBody);

    public abstract Observable<ApiResponse> changePassword(@NonNull UserReqBody reqBody);

    public abstract Observable<Response> logOut(LogoutReq regData);

    public abstract Observable<VersionResponse> appVersion();

    public abstract Observable<HelpAndSupport> getHelpAndSupportDetails();
    /************-----------Login module API calls ended--------------********/


    /************-----------Outlet API calls started--------------********/
    public abstract Observable<List<Amenity>> getAmenities();

    public abstract Observable<List<Outlet>> getOutlets(@NonNull OutletsReqBody requestBody);

    public abstract Observable<OutletDetailsResponse> getOutletDetails(String outletId);

    public abstract Observable<List<Cuisine>> getCuisines();

    public abstract Observable<List<Outlet>> searchOutlets(@NonNull String restaurant);

    public abstract Observable<List<Outlet>> getSearchedOutlets(@NonNull OutletsReqBody requestBody);

    public abstract Observable<Response> setOrderFeedback(@NonNull List<Feedback> feedbackBody);

    public abstract Observable<List<Address>> getDeliveryCharges(DeliveryChargeInput data);
    /************-----------Outlet API calls ended--------------********/

    /************-----------Checkout API calls started--------------********/
    public abstract Observable<Order> confirmOrder(@NonNull Order order);

    public abstract Observable<UserLoginDetails> updateProfile(UserLoginDetails details);

    public abstract Observable<UserLoginDetails> getProfile(String id);

    public abstract Observable<List<Coupons>> getCoupons();

    public abstract Observable<List<Order>> getOrderHistory(String userId, int start, int limit);

    public abstract Observable<Order> getOrderDetails(String orderId);

    public abstract Observable<UserLoginDetails> saveProfilePic(RequestBody name, MultipartBody.Part path);

    public abstract Observable<Response> verifyCoupon(String userId, long couponId);

    public abstract Observable<List<Order>> getPendingFeedbackOrders(String userId);
    /************-----------Checkout API calls ended--------------*********/

}
