package com.almoayyed.waseldelivery.network_interface.location;

/**
 * Created on 06 Oct 2016 5:47 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

import com.almoayyed.waseldelivery.BuildConfig;

/**
 * Contains the constants for location search & details
 */
public class LocationConstants {
    public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    public static final String OUT_JSON = "/json";
    //prev = AIzaSyB0GD7EyWp7D5kPJZ_AWPbOudMVzrTKDYs //AIzaSyBkK4fK_1HrgZI6wNZs1ltHK-aO7McrJ1s
    public static final String API_KEY = BuildConfig.PLAY_SERVICE_KEY;

    public static final String PLACES_DETAILS_API_BASE = "https://maps.googleapis.com/maps/api/geocode";
}
