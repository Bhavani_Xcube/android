package com.almoayyed.waseldelivery.network_interface.location;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.utils.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import rx.subjects.PublishSubject;

/**
 * Created on 11 Nov 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Input: lat, lng values
 * Returns: address corresponding to lat, lng
 * Rx is used for sending reverse geo coding info
 */
public class ReverseGeoCode extends Thread {
    private final PublishSubject<Object> revGeoCodeSubject;
    private final Lock inputWaitLock;
    private final Condition inputTextExist;
    boolean isAlive = true, isDataExist = false;

    static final int SEARCH_BY_PLACE_ID = 1;
    static final int SEARCH_BY_LAT_LNG = 2;
    int searchBy = 0;

    //Search data
    String placeId;

    //Output data
    private UserLocation location = new UserLocation();


    public ReverseGeoCode(PublishSubject<Object> revGeoCodeSubject) {
        inputWaitLock = new ReentrantLock();
        inputTextExist  = inputWaitLock.newCondition();

        this.revGeoCodeSubject = revGeoCodeSubject;
    }

    public void clear(){
        isAlive = false;
    }

    @Override
    public void run() {
        super.run();
        //fetch the places records & pass data over otta bus
        while (isAlive){
            inputWaitLock.lock();
            try{
                if(isDataExist){
                    if(NetworkConnection.isNetworkAvailable()){
                        //get address details
                        getAddressDetails();
                        revGeoCodeSubject.onNext(location);
                    }else {
                        revGeoCodeSubject.onNext(WaselApplication.getContext().getString(R.string.check_internet));
                    }
                    isDataExist = false;
                }else {
                    try {
                        inputTextExist.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }finally {
                inputWaitLock.unlock();
            }
        }
        revGeoCodeSubject.onCompleted();
    }

    public void searchLocationByPlaceId(String placeId) {
        inputWaitLock.lock();
        try{
            isDataExist = true;
            searchBy = SEARCH_BY_PLACE_ID;
            this.placeId = placeId;
            inputTextExist.signalAll();
        }finally {
            inputWaitLock.unlock();
        }
    }

    public void searchLocationByLatLng(UserLocation userLocation) {
        inputWaitLock.lock();
        try{
            isDataExist = true;
            searchBy = SEARCH_BY_LAT_LNG;
            location.setLatitude(userLocation.getLatitude());
            location.setLongitude(userLocation.getLongitude());
            inputTextExist.signalAll();
        }finally {
            inputWaitLock.unlock();
        }
    }

    public boolean getAddressDetails() {

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(LocationConstants.PLACES_DETAILS_API_BASE+LocationConstants.OUT_JSON);
            sb.append("?key=" + LocationConstants.API_KEY);
            switch (searchBy){
                case SEARCH_BY_PLACE_ID:
                    sb.append("&place_id=" + URLEncoder.encode(placeId, "utf8"));
                    break;
                case SEARCH_BY_LAT_LNG:
                    sb.append("&latlng=" + URLEncoder.encode(location.getLatitude()+","+location.getLongitude(), "utf8"));
                    break;
            }

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            return false;
        } catch (IOException e) {
            return false;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            parseResponse(jsonResults);

        }
        catch (JSONException e) {
        }

        return true;
    }

    private void parseResponse(StringBuilder jsonResults) throws JSONException {
        final String point_of_interest ="point_of_interest";
        final String sub_loc_1 = "sublocality_level_1";
        final String neighborhood_1 = "neighborhood";
        final String route_2 = "route";
        final String sub_loc_2 ="sublocality_level_2";
        final String locality = "locality";
        final String addmin_level_1 = "administrative_area_level_1";
        final String country = "country";

        // Create a JSON object hierarchy from the results
        JSONObject jsonObj = new JSONObject(jsonResults.toString());
        JSONArray resJsonArray = jsonObj.getJSONArray("results");

        switch (searchBy){
            case SEARCH_BY_PLACE_ID:
                // Extract the location lat lng from the results
                JSONObject geometryJson = resJsonArray.getJSONObject(0).getJSONObject("geometry");
                JSONObject locJson = geometryJson.getJSONObject("location");
                location.setLatitude(Double.parseDouble(locJson.getString("lat")));
                location.setLongitude(Double.parseDouble(locJson.getString("lng")));
                break;
            case SEARCH_BY_LAT_LNG:
//                location.setId(resJsonArray.getJSONObject(0).getString("place_id"));
                break;
        }



        // Extract the Place descriptions from the results
        JSONArray addrJsonArray = resJsonArray.getJSONObject(0).getJSONArray("address_components");
        location.setLocDescription(resJsonArray.getJSONObject(0).getString("formatted_address"));
        for (int i=0; i<addrJsonArray.length() ; i++){
            JSONArray jsonList = (JSONArray)addrJsonArray.getJSONObject(i).getJSONArray("types");
            ArrayList<String> list = new ArrayList<>();//"formatted_address" -> "Block 324, Manama, Bahrain"
            for (int j=0;j<jsonList.length();j++){
                list.add(jsonList.getString(j));
            }
            if(list.contains(point_of_interest)){
                location.setSubLocality3(addrJsonArray.getJSONObject(i).getString("long_name").trim());
            } else if(list.contains(sub_loc_2) || list.contains(route_2)){
                location.setSubLocality2(addrJsonArray.getJSONObject(i).getString("long_name").trim());
            }else if(list.contains(sub_loc_1) || list.contains(neighborhood_1)){
                location.setSubLocality1(addrJsonArray.getJSONObject(i).getString("long_name").trim());
            }else if(list.contains(locality)){
                location.setCity(addrJsonArray.getJSONObject(i).getString("long_name").trim());
            } else if(list.contains(addmin_level_1)){
                location.setState(addrJsonArray.getJSONObject(i).getString("long_name").trim());
            }else if(list.contains(country)){
                location.setCountry(addrJsonArray.getJSONObject(i).getString("long_name").trim());
            }
        }

    }

}
