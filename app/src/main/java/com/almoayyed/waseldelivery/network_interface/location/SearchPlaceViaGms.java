package com.almoayyed.waseldelivery.network_interface.location;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.models.LocationSearchResult;
import com.almoayyed.waseldelivery.utils.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import rx.subjects.PublishSubject;

/**
 * Created on 28 Nov 2016 6:46 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * A separate thread is created for fetching places from google server
 * Rx is used for send back auto search places information
 */
public class SearchPlaceViaGms extends Thread {
    private final PublishSubject<Object> revGeoCodeSubject;
    private final SearchPlaceInput input;
    private final SearchPlaceOutput output;
    private final Lock inputWaitLock;
    private final Condition inputTextExist;
    boolean isAlive = true;

    String prevSearchStr = "";

    public SearchPlaceViaGms(PublishSubject<Object> revGeoCodeSubject) {
        inputWaitLock = new ReentrantLock();
        inputTextExist  = inputWaitLock.newCondition();

        this.revGeoCodeSubject = revGeoCodeSubject;

        input = new SearchPlaceInput();
        output = new SearchPlaceOutput();
    }

    /*Forcefully terminates the thread by resetting isAlive flag*/
    public void clear(){
        isAlive = false;
    }



    @Override
    public void run() {
        super.run();
        //fetch the places records & pass data over otta bus
        while (isAlive){
            inputWaitLock.lock();
            try{
                if(NetworkConnection.isNetworkAvailable()){
                    if(input.searchString.length()==0 || input.searchString.equalsIgnoreCase(prevSearchStr)){
                        //if text is updated
                        try {
                            //Delay is kept to display loading animation when user click on keyboard search repeatedly
                            //Here previous search result is returned instead on hitting another place search api
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        revGeoCodeSubject.onNext(output);
                    }else {
                        prevSearchStr = input.searchString;

                        output.results.clear();
                        ArrayList<LocationSearchResult> results = autocomplete(input.searchString);
                        if(results!=null){
                            output.results.addAll(results);
                        }
                        revGeoCodeSubject.onNext(output);
                    }
                }else {
                    revGeoCodeSubject.onNext(WaselApplication.getContext().getString(R.string.check_internet));
                }
                try {
                    inputTextExist.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }finally {
                inputWaitLock.unlock();
            }
        }
        revGeoCodeSubject.onCompleted();
    }

    public void onSearchTextChange(SearchPlaceInput inputData) {
        inputWaitLock.lock();
        try{
            input.searchString = inputData.searchString;
            inputTextExist.signalAll();
        }finally {
            inputWaitLock.unlock();
        }
    }

    public void onSearchTextChange(String test) {
        inputWaitLock.lock();
        try{
            input.searchString = test;
            inputTextExist.signalAll();
        }finally {
            inputWaitLock.unlock();
        }
    }

    /*
    * Fetches search places from google server
    * */
    public ArrayList<LocationSearchResult> autocomplete(String input) {
        ArrayList<LocationSearchResult> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(LocationConstants.PLACES_API_BASE + LocationConstants.TYPE_AUTOCOMPLETE + LocationConstants.OUT_JSON);
            sb.append("?key=" + LocationConstants.API_KEY);
            sb.append("&components=country:bh");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            return resultList;
        } catch (IOException e) {
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                LocationSearchResult result = new LocationSearchResult();
                result.setDescription(predsJsonArray.getJSONObject(i).getString("description"));
                result.setPlace_id(predsJsonArray.getJSONObject(i).getString("place_id"));
                resultList.add(result);
            }
        } catch (JSONException e) {
        }

        return resultList;
    }

    /*
    * Objects holds the input text for search
    * */
    public static class SearchPlaceInput{
        public String searchString = "";
    }

    /*
   * Objects holds the input text for search
   * */
    public static class SearchPlaceOutput{
        //Need to send req place details
        //Currently only place name is send
        public ArrayList<LocationSearchResult>  results = new ArrayList<>();
    }
}
