package com.almoayyed.waseldelivery.network_interface.wasel_api;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

import com.almoayyed.waseldelivery.models.DeviceIds;

/**
 * Holds data which contains info of social media login user info
 * checking for already registered user
 */

public class CheckUserExistReqBody {
    private String uid;
    private String token;
    private int accountType;
    public DeviceIds device;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public DeviceIds getDevice() {
        return device;
    }

    public void setDevice(DeviceIds device) {
        this.device = device;
    }
}
