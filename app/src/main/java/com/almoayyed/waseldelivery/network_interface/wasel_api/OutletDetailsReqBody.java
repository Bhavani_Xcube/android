package com.almoayyed.waseldelivery.network_interface.wasel_api;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 10 Nov 2016 7:03 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Holds the request body of restaurant details API call
 */
public class OutletDetailsReqBody {
    //TODO: need to update acc to API call
    @SerializedName("restaurant_id")
    public String restaurantId;

}
