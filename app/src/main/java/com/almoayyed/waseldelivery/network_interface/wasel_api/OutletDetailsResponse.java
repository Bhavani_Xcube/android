package com.almoayyed.waseldelivery.network_interface.wasel_api;

import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletItemDetails;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Holds the response data of outlet details
 */
public class OutletDetailsResponse {

    @SerializedName("canAcessImages")
    public boolean canAccessImages;

    @SerializedName("outletItems")
    List<OutletItemDetails> outletItems;

    Address address;

    public boolean isCanAccessImages() {
        return canAccessImages;
    }

    public void setCanAccessImages(boolean canAccessImages) {
        this.canAccessImages = canAccessImages;
    }

    public List<OutletItemDetails> getOutletItems() {
        return outletItems;
    }

    public void setOutletItems(List<OutletItemDetails> outletItems) {
        this.outletItems = outletItems;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
