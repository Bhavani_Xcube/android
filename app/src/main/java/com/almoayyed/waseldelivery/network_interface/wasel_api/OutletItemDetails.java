package com.almoayyed.waseldelivery.network_interface.wasel_api;

import android.text.TextUtils;

import com.almoayyed.waseldelivery.constants.AppLimitations;
import com.almoayyed.waseldelivery.models.OutletItemCustomField;
import com.almoayyed.waseldelivery.models.OutletItemCustomization;
import com.almoayyed.waseldelivery.utils.log.Loggers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/*Holds outlet item information send by server*/
public class OutletItemDetails implements Serializable {
    String id;
    String name;
    float price;
    String description;
    String imageUrl;
    boolean recommend;
    Parent itemCategory;
    boolean active; // indicates whether category active or not
    ArrayList<OutletItemCustomization> customizationItems;
    ArrayList<OutletItemCustomField> outletItemCustomFields;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isRecommend() {
        return recommend;
    }

    public void setRecommend(boolean recommend) {
        this.recommend = recommend;
    }

    public ArrayList<OutletItemCustomization> getCustomizationItems() {
        return customizationItems;
    }

    public void setCustomizationItems(ArrayList<OutletItemCustomization> customizationItems) {
        this.customizationItems = customizationItems;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Parent getItemCategory() {
        if(TextUtils.isEmpty(itemCategory.getName()) ||
                itemCategory.getName().trim().length() == 0){
            itemCategory.setName(AppLimitations.EMPTY_SUB_CATEGORY_HEADING);
        }
        return itemCategory;
    }

    public void setItemCategory(Parent itemCategory) {
        if(TextUtils.isEmpty(itemCategory.getName()) ||
                itemCategory.getName().trim().length() == 0){
            itemCategory.setName(AppLimitations.EMPTY_SUB_CATEGORY_HEADING);
        }
        this.itemCategory = itemCategory;
    }

    public com.almoayyed.waseldelivery.models.OutletItem getOutletItem(){
        com.almoayyed.waseldelivery.models.OutletItem outletItem = new com.almoayyed.waseldelivery.models.OutletItem();
        outletItem.setId(id);
        outletItem.setName(name);
        outletItem.setImageUrl(imageUrl);
        outletItem.setPrice(price);
        outletItem.setItemDescription(description);
        outletItem.setCustomizedItems(customizationItems);
        outletItem.setOutletItemCustomFields(outletItemCustomFields);
        outletItem.setHideImage(itemCategory.isHideImage());
        Loggers.temp("isHideImage="+itemCategory.isHideImage());
        return outletItem;
    }

    public ArrayList<OutletItemCustomField> getOutletItemCustomFields() {
        return outletItemCustomFields;
    }

    public void setOutletItemCustomFields(ArrayList<OutletItemCustomField> outletItemCustomFields) {
        this.outletItemCustomFields = outletItemCustomFields;
    }

    /*Holds outlet item parent information send by server*/
    public class Parent implements Serializable{
        int id;
        Parent parent;
        String name;
        String description;
        boolean disable; // indicates whether category disabled or not
         boolean delete;
        boolean hideImage;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Parent getParent() {
            return parent;
        }

        public void setParent(Parent parent) {
            this.parent = parent;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isDisable() {
            return disable;
        }

        public void setDisable(boolean disable) {
            this.disable = disable;
        }

        public boolean isHideImage() {
            return hideImage;
        }

        public void setHideImage(boolean hideImage) {
            this.hideImage = hideImage;
        }
    }
}