package com.almoayyed.waseldelivery.network_interface.wasel_api;

import com.google.gson.annotations.SerializedName;


import java.util.List;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OutletsReqBody {
    private double latitude;
    private double longitude;
    private int amenityId;
    private int start;
    private int maxResult;
    private String search;


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getAmenityId() {
        return amenityId;
    }

    public void setAmenityId(int amenityId) {
        this.amenityId = amenityId;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getMaxResult() {
        return maxResult;
    }

    public void setMaxResult(int maxResult) {
        this.maxResult = maxResult;
    }

    public class Filter {
        @SerializedName("rating")
        boolean rating;
        List<Integer> budgets;
        List<Integer> cuisines;

        public boolean isRating() {
            return rating;
        }

        public void setRating(boolean rating) {
            this.rating = rating;
        }

        public List<Integer> getBudgets() {
            return budgets;
        }

        public void setBudgets(List<Integer> budgets) {
            this.budgets = budgets;
        }

        public List<Integer> getCuisines() {
            return cuisines;
        }

        public void setCuisines(List<Integer> cuisines) {
            this.cuisines = cuisines;
        }

    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
