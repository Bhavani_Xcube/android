package com.almoayyed.waseldelivery.network_interface.wasel_api;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * This object hold the information required for registration process
 */
public class RegistrationResponse implements Serializable{
    public static final int ACC_TYPE_WASEL = 1;
    public static final int ACC_TYPE_FACEBOOK = 2;
    public static final int ACC_TYPE_GOOGLE = 3;

    @SerializedName("name")
    String name;

    @SerializedName("password")
    String password;
    String confirmPassword;

    @SerializedName("email")
    String emailId;

    @SerializedName("mobile")
    String phoneNo;

    @SerializedName("accountType")
    int accountType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }
}
