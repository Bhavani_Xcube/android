package com.almoayyed.waseldelivery.network_interface.wasel_api;

import android.databinding.BaseObservable;

/**
 * Created on 11 Nov 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Login Api body structure
 */

public class UserReqBody extends BaseObservable{
    public String mobile;
    public String password;
    public boolean isAndroid = true;
    public String deviceToken;

    public UserReqBody(){
    }

    public UserReqBody(UserReqBody userReqBody){
        mobile = userReqBody.getMobile();
        password = userReqBody.getPassword();
        isAndroid = userReqBody.isAndroid();
        deviceToken = userReqBody.getDeviceToken();
    }
    public String getMobile(){
        return mobile;
    }
    public void setMobile(String mobile){
        this.mobile = mobile;
    }

    public String getPassword(){
        return password;
    }
    public void setPassword(String password){
        this.password = password;
    }

    public boolean isAndroid() {
        return isAndroid;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}
