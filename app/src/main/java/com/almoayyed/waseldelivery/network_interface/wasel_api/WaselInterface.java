package com.almoayyed.waseldelivery.network_interface.wasel_api;

import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.models.Amenity;
import com.almoayyed.waseldelivery.models.Coupons;
import com.almoayyed.waseldelivery.models.Cuisine;
import com.almoayyed.waseldelivery.models.DeliveryChargeInput;
import com.almoayyed.waseldelivery.models.Feedback;
import com.almoayyed.waseldelivery.models.HelpAndSupport;
import com.almoayyed.waseldelivery.models.LogoutReq;
import com.almoayyed.waseldelivery.models.Notifications;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.models.Registration;
import com.almoayyed.waseldelivery.models.VersionResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import rx.Observable;
/**
 * Created on 1 Dec 2016.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Created on 11 Nov 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/*
* LoginInterface class contains following API calls
* 1. Register new user
* 2. Sign up
* 3. Forget password
* 4. OTP generation
* 5. OTP verification
* 6. updating profile
* */
public interface WaselInterface {

    /*--------------------------start: Notification api------------------------------*/
    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @POST("security/user/allow/notification")
    Observable<Response> getNotification(@Body Notifications notifications);
    /*--------------------------end: Notification api------------------------------*/
    /*--------------------------start: login api------------------------------*/
    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @POST("public/user/generate/otp")
    Observable<Response> generateOTP(@Body GenerateOtpReqBody generateOtpReqBody);

    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @POST("public/user/verifyotp")
    Observable<ApiResponse> verifyOTP(@Body VerifyOtpReqBody verifyOtpReqBody);

    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @POST("public/users/create/new/user")
    Observable<UserLoginDetails> registerUser(@Body Registration user);

    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @POST("public/user/login")
    Observable <UserLoginDetails> userLoginDetails(@Body UserReqBody user);

    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @POST("public/user/password/save")
    Observable <ApiResponse> resetPassword(@Body UserReqBody user);

    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @POST("public/users/checkUserExist")
    Observable <UserLoginDetails> checkUserExistence(@Body CheckUserExistReqBody user);

    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @POST("security/users/update")
    Observable <UserLoginDetails> updateUserProfile(@Body UserLoginDetails profileDetails);

    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @GET("security/users/get/{userId}" )
    Observable <UserLoginDetails> getUserProfile(@Path("userId") String id);

    @Multipart
    @Headers({"Authorization: @"})
    @POST("security/users/add/profile/image" )
    Observable<UserLoginDetails> saveProfilePic(@Part("userId") RequestBody userId, @Part MultipartBody.Part file);

    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @POST("security/users/logout")
    Observable<Response> logOut(@Body LogoutReq regData);

    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @GET("public/app/detail")//version
    Observable<VersionResponse> appVersion();

    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @GET("public/app/help")//version
    Observable<HelpAndSupport> getHelpAndSupportDetails();

    /*--------------------------end: login api------------------------------*/

    /*--------------------------start: Outlet api-------------------------*/
    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @GET("public/amenities/list")
    Observable<List<Amenity>> loadAmenities();

    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @POST("public/outlets/list")
    Observable<List<Outlet>> loadOutlets(@Body OutletsReqBody user);


    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @GET("public/outlets/get/{outletId}")
    Observable<OutletDetailsResponse> loadOutletDetails(@Path("outletId") int outletId);


    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @GET("restaurants/get-cuisines")
    Observable<List<Cuisine>> getCusines();

    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @POST("public/outlets/search")
    Observable<List<Outlet>> loadSearchedOutlets(@Body OutletsReqBody user);

    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @POST("security/deliverycharge/")
    Observable<List<Address>> getDeliveryCharges(@Body DeliveryChargeInput data);
    /*--------------------------end: Outlet api ---------------------------*/

    /*--------------------------start: Checkout api----------------------*/
    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @POST("security/order/disposal")
    Observable<Order> confirmOrder(@Body Order order);

    @Headers({"Content-Type: application/json",
            "Accept: application/json"})
    @GET("public/order/coupons")
    Observable<List<Coupons>> getCoupons();

    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @GET("security/order/get/histories/{id}/{start}/{limit}")
    Observable<List<Order>> getOrderHistory(@Path("id") String userId,
                                            @Path("start") int start,
                                            @Path("limit") int limit);

    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @GET("security/order/get/{orderId}")
    Observable<Order> getOrderDetails(@Path("orderId") String orderId);

    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @POST("security/order/feedback")
    Observable<Response> orderFeedback(@Body List<Feedback> feedback);

    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @GET("security/order/get/completed/order/{userId}")
    Observable<List<Order>> getPendingFeedbackOrders(@Path("userId") String userId);

    @Headers({"Content-Type: application/json",
            "Accept: application/json",
            "Authorization: @"})
    @GET("security/order/verify/coupon/{id}/{couponId}")
    Observable<Response> verifyCoupon(@Path("id") String userId,
                                      @Path("couponId") long couponId);
    /*-----------------------end: Checkout api------------------------*/

}
