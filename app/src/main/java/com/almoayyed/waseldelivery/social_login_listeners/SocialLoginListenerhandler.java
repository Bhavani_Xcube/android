package com.almoayyed.waseldelivery.social_login_listeners;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RelativeLayout;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.models.DeviceIds;
import com.almoayyed.waseldelivery.models.FacebookResponse;
import com.almoayyed.waseldelivery.models.Registration;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.network_interface.wasel_api.CheckUserExistReqBody;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.checkout.CheckoutActivity;
import com.almoayyed.waseldelivery.ui.location.LocationActivity;
import com.almoayyed.waseldelivery.ui.login.AddPhoneNumberActivity;
import com.almoayyed.waseldelivery.utils.Facebook.Facebook;
import com.almoayyed.waseldelivery.utils.Facebook.FacebookLoginCallback;
import com.almoayyed.waseldelivery.utils.Facebook.LogoutFromFacebookListener;
import com.almoayyed.waseldelivery.utils.GooglePlus.GoogleConnectionListener;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONObject;

import java.util.ArrayList;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 13 Dec 2016 3:18 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class SocialLoginListenerhandler {
    private final int loginFrom;
    private Context context;
    public ProgressBarHandler mProgressBarHandler;
    private Observable<UserLoginDetails> checkForExistingUserObservable;
    public Subscription subscribe;
    public CompositeSubscription startStopCompositeSubscription = new CompositeSubscription();
    private Registration registrationData;
    private FacebookResponse fbResp;
    private LoginCredentialsPreference loginPreference;
    private RelativeLayout layout;

    public SocialLoginListenerhandler(Context context, RelativeLayout layout, int loginFrom) {
        this.context = context;
        this.layout = layout;
        loginPreference = LoginCredentialsPreference.getLoginPreference();
        mProgressBarHandler = new ProgressBarHandler(context, layout);
        this.loginFrom = loginFrom;
    }

    public ArrayList<DeviceIds> getRegId() {
        ArrayList<DeviceIds> regIds = new ArrayList<>();
        String regId = loginPreference.getRegId();
        DeviceIds ids = new DeviceIds();
        ids.setDeviceToken(regId);
        regIds.add(ids);
        return regIds;
    }

    public DeviceIds getRegsId() {
        String regId = loginPreference.getRegId();
        DeviceIds ids = new DeviceIds();
        ids.setDeviceToken(regId);
        return ids;
    }


    //Call back listener for facebook sign-out process
    public LogoutFromFacebookListener fbSignoutListener = new LogoutFromFacebookListener() {
        @Override
        public void onLoggedOutFromFacebook() {
            mProgressBarHandler.hide();
        }
    };

    //Call back listener for facebook sign-in process
    public FacebookLoginCallback fbSigninCallback = new FacebookLoginCallback() {
        @Override
        public void onSuccess(JSONObject fbObject) {

            Log.i("fb", "fbObject +++++++ " + fbObject);
            if (fbObject != null) {
                try {
                    fbResp = new FacebookResponse();
                    fbResp.setId(fbObject.has("id") ? fbObject.getString("id") : null);
                    fbResp.setName(fbObject.has("name") ? fbObject.getString("name") : null);
                    fbResp.setEmail(fbObject.has("email") ? fbObject.getString("email") : null);

                    ArrayList<DeviceIds> regIds = getRegId();
                    DeviceIds regId = getRegsId();

                    registrationData = new Registration();
                    registrationData.setFacebookUID(fbResp.getId());
                    registrationData.setName(fbResp.getName());
                    registrationData.setEmailId(fbResp.getEmail());
                    registrationData.setDevice(regIds);
                    registrationData.setAccountType(RegistrationBundleKeyConstants.ACC_TYPE_FACEBOOK);

                    if (TextUtils.isEmpty(fbResp.getEmail())) {
                        registrationData.setShouldReadEmailId(true);
                    }

                    CheckUserExistReqBody checkUserExistReqBody = new CheckUserExistReqBody();
                    checkUserExistReqBody.setUid(fbResp.getId());
                    checkUserExistReqBody.setToken(Facebook.getAccessToken().getToken());
                    checkUserExistReqBody.setAccountType(RegistrationBundleKeyConstants.ACC_TYPE_FACEBOOK);
                    checkUserExistReqBody.setDevice(regId);

                    //check whether this user already registered or not
                    checkForExistingUser(checkUserExistReqBody);
                } catch (Exception e) {
                    e.printStackTrace();
                    mProgressBarHandler.hide();
                }
            }
        }

        @Override
        public void onFailed(String reason) {
            mProgressBarHandler.hide();
            Log.i("fb", "reason ::::::::: " + reason);
        }
    };


    //Call back listener for Google sign-out process
    public ResultCallback<Status> googleSignOutCallback = new ResultCallback<Status>() {
        @Override
        public void onResult(Status status) {
            mProgressBarHandler.hide();
        }
    };

    //Call back listener for google sign-in process
    public GoogleConnectionListener googleSigninListener = new GoogleConnectionListener() {
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            // An unresolvable error has occurred and Google APIs (including Sign-In) will not
            // be available.
            Log.d("Google con", "onConnectionFailed:" + connectionResult);
            mProgressBarHandler.hide();
        }
    };


    public void onGoogleLoginSuccess(Intent data) {
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

        ArrayList<DeviceIds> regIds = getRegId();
        DeviceIds regId = getRegsId();

        //Save current user information in registration instance & navigate to phone no. verification
        GoogleSignInAccount account = result.getSignInAccount();
        registrationData = new Registration();
        registrationData.setGoogleUID(account.getId());
        registrationData.setName(account.getDisplayName());
        registrationData.setEmailId(account.getEmail());
        registrationData.setPassword("");
        registrationData.setConfirmPassword("");
        registrationData.setDevice(regIds);
        registrationData.setAccountType(RegistrationBundleKeyConstants.ACC_TYPE_GOOGLE);

        CheckUserExistReqBody checkUserExistReqBody = new CheckUserExistReqBody();
        checkUserExistReqBody.setUid(account.getId());
        checkUserExistReqBody.setToken(account.getIdToken());
        checkUserExistReqBody.setAccountType(RegistrationBundleKeyConstants.ACC_TYPE_GOOGLE);
        checkUserExistReqBody.setDevice(regId);
        mProgressBarHandler.hide();
        //check whether this user already registered or not
        checkForExistingUser(checkUserExistReqBody);
    }


    public void checkForExistingUser(CheckUserExistReqBody checkUserExistReqBody) {
        mProgressBarHandler.show();

        checkForExistingUserObservable = WaselDeliveryService.getService().checkForExistingUser(checkUserExistReqBody);
        subscribe = checkForExistingUserObservable.subscribe(checkForExistingUserObserver);
        startStopCompositeSubscription.clear();
        startStopCompositeSubscription.add(subscribe);
    }


    /*
    * Observer for checking already existing user
    * */
    Observer<UserLoginDetails> checkForExistingUserObserver = new Observer<UserLoginDetails>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
//            Toast.makeText(SplashScreenActivity.this,getServerErrorMessage(e),Toast.LENGTH_SHORT).show();
            WaselToast.showToast(getServerErrorMessage(e));
            mProgressBarHandler.hide();
        }

        @Override
        public void onNext(UserLoginDetails loginDetails) {
            if (TextUtils.isEmpty(loginDetails.getId()) || loginDetails.getId().equalsIgnoreCase("0")) {
                //User not registered.
                Intent intent = new Intent(context, AddPhoneNumberActivity.class);
                intent.putExtra(RegistrationBundleKeyConstants.KEY_REGISTRATION_DATA, registrationData);
                intent.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM, loginFrom);
                context.startActivity(intent);

            } else {
                //User already registered.
                //Persisting user information
                loginPreference.saveUserCredentials(loginDetails);

                //Registration data successfully send to user.
                //OTP will be send to phone via server.
                mProgressBarHandler.hide();

                //Navigate to corresponding screen(location, landing or login page)
                openActivity();
            }
        }
    };


    /*
   * Observer for Login Details downloaded from server.
   * */
    public Observer<UserLoginDetails> loginDetailsObserver = new Observer<UserLoginDetails>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
//            Toast.makeText(SignInActivity.this,getServerErrorMessage(e),Toast.LENGTH_SHORT).show();
            WaselToast.showToast(getServerErrorMessage(e));
            mProgressBarHandler.hide();
        }

        @Override
        public void onNext(UserLoginDetails userLoginDetails) {
            if (userLoginDetails != null) {
                Log.e("TAG", "Name : " + userLoginDetails.getName() + "\n id : " + userLoginDetails.getId() + " \n Mobile : " + userLoginDetails.getMobile() + " \n email : " + userLoginDetails.getEmail());
                loginPreference.saveUserCredentials(userLoginDetails);
                openActivity();
            }
        }
    };

    public void openActivity() {
        Intent i;
        //Check whether the location was updated or not
        if (LocationPreference.hasLocation()) {
            switch (loginFrom) {
                case RegistrationBundleKeyConstants.LOGIN_FROM_CHECKOUT_NORMAL:
                    i = new Intent(context, CheckoutActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    break;
                case RegistrationBundleKeyConstants.LOGIN_FROM_CHECKOUT_SPECIAL:
                case RegistrationBundleKeyConstants.LOGIN_FROM_ORDER_HISTORY:
                case RegistrationBundleKeyConstants.LOGIN_FROM_PROFILE_TAB:
                    i = new Intent(context, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    break;
                case RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH:
                default:
                    i = new Intent(context, MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    break;
            }
            i.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM, loginFrom);

        } else {
            //Location not updated
            i = new Intent(context, LocationActivity.class);
            i.putExtra(LocationActivity.DISPLAY_LYT, LocationActivity.DISPLAY_GPS_LYT);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }

        context.startActivity(i);
        mProgressBarHandler.hide();
        ((Activity) context).finish();
    }


}
