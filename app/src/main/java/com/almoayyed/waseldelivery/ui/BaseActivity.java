package com.almoayyed.waseldelivery.ui;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.almoayyed.waseldelivery.utils.ProgressBarHandler;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created on 16 Dec 2016 2:53 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public class BaseActivity extends AppCompatActivity {

    private CompositeSubscription compositeSubscription;
    public ProgressBarHandler mProgressBarHandler;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        compositeSubscription = new CompositeSubscription();

    }

    public<T> Subscription addObserver(Observable<T> observable, Observer<T> observer){
        if(mProgressBarHandler != null){
            mProgressBarHandler.show();
        }
        Subscription subscription = observable.subscribe(observer);
        if(compositeSubscription == null) {
            compositeSubscription = new CompositeSubscription();
        }
        compositeSubscription.add(subscription);
        return subscription;
    }

    public void addToSubscription(Subscription subscription)
    {
        if(compositeSubscription == null) {
            compositeSubscription = new CompositeSubscription();
        }
        compositeSubscription.add(subscription);

        if(mProgressBarHandler != null){
            mProgressBarHandler.show();
        }
    }

    public void removeFromSubscription(Subscription subscription)
    {
        if(compositeSubscription != null) {
            compositeSubscription.remove(subscription);
        }

        if(mProgressBarHandler != null){
            mProgressBarHandler.hide();
        }
    }

    public void clearSubscriptions()
    {
        if(compositeSubscription != null) {
            compositeSubscription.clear();
            compositeSubscription = null;
        }

        if(mProgressBarHandler != null){
            mProgressBarHandler.hide();
        }
    }


    @Override
    protected void onDestroy() {
        clearSubscriptions();
        super.onDestroy();
    }
}
