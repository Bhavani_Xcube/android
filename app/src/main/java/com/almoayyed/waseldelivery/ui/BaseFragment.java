package com.almoayyed.waseldelivery.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.utils.ProgressBarHandler;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Main fragment which consist of all common functions
 * Note: not in use for now, added to use in future
 */
public class BaseFragment extends Fragment{
    public View fragView;
    private CompositeSubscription compositeSubscription;
    public ProgressBarHandler mProgressBarHandler;

    public BaseFragment() {
        //Handles illegal state exception
        //Fragment is active while setting arguments
        setArguments(new Bundle());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    public<T> Subscription addObserver(Observable<T> observable, Observer<T> observer){
        if(mProgressBarHandler != null){
            mProgressBarHandler.show();
        }
        Subscription subscription = observable.subscribe(observer);
        if(compositeSubscription == null) {
            compositeSubscription = new CompositeSubscription();
        }
        compositeSubscription.add(subscription);
        return subscription;
    }

    public<T> Subscription addObserver(Observable<T> observable, Observer<T> observer, boolean showProgress){
        if(mProgressBarHandler != null && showProgress){
            mProgressBarHandler.show();
        }
        Subscription subscription = observable.subscribe(observer);
        if(compositeSubscription == null) {
            compositeSubscription = new CompositeSubscription();
        }
        compositeSubscription.add(subscription);
        return subscription;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compositeSubscription = new CompositeSubscription();
    }

    public void addToSubscription(Subscription subscription)
    {
        if(compositeSubscription == null) {
            compositeSubscription = new CompositeSubscription();
        }
        compositeSubscription.add(subscription);

        //Displaying progress when ui entity is waiting for server (network) response
        if(mProgressBarHandler!=null){
            mProgressBarHandler.show();
        }
    }

    public void removeFromSubscription(Subscription subscription)
    {
        if(compositeSubscription != null) {
            compositeSubscription.remove(subscription);
        }

        if(mProgressBarHandler!=null){
            mProgressBarHandler.hide();
        }
    }

    public void clearSubscriptions()
    {
        if(compositeSubscription != null) {
            compositeSubscription.clear();
        }
        if(mProgressBarHandler!=null){
            mProgressBarHandler.hide();
        }
    }

    /*Handle device back button is pressed*/
    public  boolean onPressDeviceBackButton(){
        return true;
    }

    public boolean isFragmentVisible() {
        return (getUserVisibleHint() && isVisible());

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        clearSubscriptions();
        compositeSubscription = null;
    }
}
