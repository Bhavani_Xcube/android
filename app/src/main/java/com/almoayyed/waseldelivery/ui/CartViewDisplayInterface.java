package com.almoayyed.waseldelivery.ui;

import android.support.annotation.NonNull;

import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.Outlet;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Declares method to show & hide the cart view
 */

public interface CartViewDisplayInterface {
    Cart getCart();
    void setOutlet(int amenityId, @NonNull Outlet outlet);
    void displayCartView(boolean show);
}
