package com.almoayyed.waseldelivery.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.FragmentsIndex;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.databinding.ActivityLoadFragmentBinding;
import com.almoayyed.waseldelivery.databinding.CartLayoutBinding;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.ui.checkout.CheckoutActivity;
import com.almoayyed.waseldelivery.ui.home.OutletDetails.OutletDetailsFragment;
import com.almoayyed.waseldelivery.ui.profile.AboutWaselFragment;
import com.almoayyed.waseldelivery.ui.profile.CouponsListFragment;
import com.almoayyed.waseldelivery.ui.profile.EditProfileFragment;
import com.almoayyed.waseldelivery.ui.profile.HelpAndSupportFragment;
import com.almoayyed.waseldelivery.ui.profile.LegalPolicyFragment;
import com.almoayyed.waseldelivery.ui.profile.ManageAddressFragment;
import com.almoayyed.waseldelivery.utils.Converters;

import java.util.List;
import java.util.Stack;

import static com.almoayyed.waseldelivery.ui.home.Outlets.OutletsFragment.AMENITY_ID;

public class LoadFragmentActivity extends BaseActivity implements CartViewDisplayInterface{
    ActivityLoadFragmentBinding binding;
    private ViewStub cartViewStub;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_load_fragment);
        cartViewStub = this.binding.cartView.getViewStub();

        Bundle bundle = getIntent().getBundleExtra(FragmentsIndex.FRAGMENT_DATA);
        int fragmentType = bundle.containsKey(FragmentsIndex.FRAGMENT_TYPE) ? bundle.getInt(FragmentsIndex.FRAGMENT_TYPE) : 0;

        if(fragmentType == 0){
            //No fragment to load
            finish();
        }else {
            setFrameBottomMargin(0);
            loadFragment(fragmentType);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getCart().isEmpty()){
            displayCartView(false);
        }
    }

    private void loadFragment(int fragmentType) {
        BaseFragment fragment = null;
        if(fragmentType == FragmentsIndex.FRAGMENT_OUTLET_DETAILS){
            //Loads outlets details fragment
            fragment = new OutletDetailsFragment();
        }else if(fragmentType == FragmentsIndex.FRAGMENT_EDIT_PROFILE){
            //Edit profile fragment
            fragment = new EditProfileFragment();
        }else if(fragmentType == FragmentsIndex.FRAGMENT_MANAGE_ADDRESS){
            //Manage address fragment
            fragment = new ManageAddressFragment();
        }else if(fragmentType == FragmentsIndex.FRAGMENT_COUPONS){
            //Coupons list fragment
            fragment = new CouponsListFragment();
        }else if(fragmentType == FragmentsIndex.FRAGMENT_HELP_SUPPORT){
            //Help & support fragment
            fragment = new HelpAndSupportFragment();
        }else if(fragmentType == FragmentsIndex.FRAGMENT_ABOUT_WASEL){
            //Coupons list fragment
            fragment = new AboutWaselFragment();
        }else if(fragmentType == FragmentsIndex.FRAGMENT_LEGAL){
            //Help & support fragment
            fragment = new LegalPolicyFragment();
        }

        if(fragment != null){
            addFragment(fragment);
        }
    }

    public void addFragment(BaseFragment fragment) {
        fragment.setArguments(getIntent().getBundleExtra(FragmentsIndex.FRAGMENT_DATA));
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft =  fm.beginTransaction();
        ft.replace(R.id.fragment_container, fragment, "fragment");
        /*ft.addToBackStack("fragment");*/
        ft.commit();
    }

    //Cart related UI
    @Override
    public Cart getCart() {
        return WaselApplication.getCart();
    }

    @Override
    public void displayCartView(boolean show) {
        final ViewTreeObserver vto = cartViewStub.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(WaselApplication.getCart().getItemCount()==0){
                    /*binding.cartView.getRoot().getViewTreeObserver().removeOnGlobalLayoutListener(this);*/
                    setFrameBottomMargin(0);
                }else {
                    setFrameBottomMargin(binding.cartView.getRoot()== null? (int)Converters.dpToPx(LoadFragmentActivity.this,80)
                            : binding.cartView.getRoot().getHeight());
                }
            }
        });
        if(show){
            TextView cartCount = (TextView) findViewById(R.id.cart_count);
            //Display cart view at bottom & bind the cart instance to it.
            binding.cartView.setOnInflateListener(new ViewStub.OnInflateListener() {
                @Override
                public void onInflate(ViewStub stub, View inflated) {
                    CartLayoutBinding cartLayoutBinding = DataBindingUtil.bind(inflated);
                    cartLayoutBinding.setCart(WaselApplication.getCart());
                    cartLayoutBinding.setBottomTabHandler(onCartClickListener);
                }
            });

            if (!this.binding.cartView.isInflated()) {
                cartViewStub.inflate();
            }else {
                cartViewStub.setVisibility(View.VISIBLE);
            }

            GradientDrawable background = (GradientDrawable)((CartLayoutBinding) binding.cartView.getBinding()).cartCount.getBackground();
            background.setColor(getResources().getColor(android.R.color.white));
        }else {
            cartViewStub.setVisibility(View.GONE);
        }
    }

    private void setFrameBottomMargin(int bottomMargin) {
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) binding.fragmentContainer.getLayoutParams();
        params.setMargins(0, 0, 0, bottomMargin);
        binding.fragmentContainer.setLayoutParams(params);
    }


    @Override
    public void setOutlet(int amenityId, @NonNull Outlet outlet) {
        WaselApplication.getCart().setAmenityId(amenityId);
        WaselApplication.getCart().setOutlet(outlet);
    }

    //Navigates to checkout screen, on tapping bottom cart lyt from menu page
    public View.OnClickListener onCartClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(LoadFragmentActivity.this, CheckoutActivity.class);
            i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,CheckoutConstants.KEY_CHECKOUT_CART_ITEMS);
            startActivity(i);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft =  fm.beginTransaction();
        List<Fragment> fragments = fm.getFragments();
        BaseFragment fragment = (BaseFragment) fm.findFragmentByTag("fragment");
        if(fragments!=null && fragments.size() >0){
            fragment.onActivityResult(requestCode,resultCode, data);
        }

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft =  fm.beginTransaction();
        List<Fragment> fragments = fm.getFragments();
        BaseFragment fragment = (BaseFragment) fm.findFragmentByTag("fragment");
        if(fragments!=null && fragments.size() >0){
            /*BaseFragment fragment = (BaseFragment) fragments.get(fragments.size()-1);*/
            if(fragment.onPressDeviceBackButton()){
                finish();
            }
        }else {
            finish();
        }
    }
}
