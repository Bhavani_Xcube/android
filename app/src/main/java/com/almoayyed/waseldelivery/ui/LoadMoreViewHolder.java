package com.almoayyed.waseldelivery.ui;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;

/**
 * Created on 24 Jan 2016 11:23 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

//Holds load more view holder
public class LoadMoreViewHolder extends RecyclerView.ViewHolder {

    public LoadMoreViewHolder(View view) {
        super(view);
    }
}