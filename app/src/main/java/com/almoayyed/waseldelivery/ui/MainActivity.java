package com.almoayyed.waseldelivery.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.*;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.ActivityMainBinding;
import com.almoayyed.waseldelivery.databinding.CartLayoutBinding;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.checkout.CheckoutActivity;
import com.almoayyed.waseldelivery.ui.feedback.FeedbackActivity;
import com.almoayyed.waseldelivery.ui.history.OrderHistoryFragment;
import com.almoayyed.waseldelivery.ui.home.Outlets.HomeFragment;
import com.almoayyed.waseldelivery.ui.home.Outlets.OutletsFragment;
import com.almoayyed.waseldelivery.ui.login.ProfileFragment;
import com.almoayyed.waseldelivery.ui.search.SearchFragment;
import com.almoayyed.waseldelivery.ui.special_order.OrderAnythingFragment;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 11 Nov 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class MainActivity extends BaseActivity{

    private static final int TAB_TAG_HOME = 0;
    private static final int TAB_TAG_SEARCH = 1;
    private static final int TAB_TAG_SPECIAL_ORDER = 2;
    private static final int TAB_TAG_HISTORY = 3;
    private static final int TAB_TAG_PROFILE = 4;

    private int currentTab = TAB_TAG_HOME;
    private ActivityMainBinding binding;

    FragmentManager fm ;
    private static final String CURRENT_TAB = "current_tab";

    /*//Since user is restricted to order from only one outlet. So, it was taken as static.
    public static Cart cart = new Cart();*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        fm = getSupportFragmentManager();
        //Setting the bottom bar tab click listener
        binding.setBottomTabHandler(onTabClickListener);

        //Loading the first tab i.e home
        //currentTab = TAB_TAG_HOME;
        if(savedInstanceState==null){
            currentTab = TAB_TAG_HOME;
        }else {
            currentTab = savedInstanceState.getInt(CURRENT_TAB);
        }
        navigateToCurrentTabHomePage(currentTab);

    }

    @Override
    protected void onStart() {
        super.onStart();
        //Check for pending feedback orders
        if(LoginCredentialsPreference.getLoginPreference().isUserLoggedIn()){
            //check for pending feedback orders
            Observable<List<Order>> pendingFeedbackOrdersObservable = WaselDeliveryService.getService().getPendingFeedbackOrders(LoginCredentialsPreference.getLoginPreference().getId());
            Subscription pendingFeedbackOrdersSubscribe = addObserver(pendingFeedbackOrdersObservable, setOrderFeedbackObserver);
        }
    }

    //Persisting cart data while app going background
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_TAB, currentTab);
    }

    //Restoring cart data when app is restoring from background
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState!=null){
            currentTab = savedInstanceState.getInt(CURRENT_TAB);
            navigateToCurrentTabHomePage(currentTab);
        }
    }

    public View.OnClickListener onTabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getId() == binding.btnHome.getId()){
                gotoHomeTab(view);
            }else if(view.getId() == binding.btnSearch.getId()){
                gotoSearchTab(view);
            }if(view.getId() == binding.btnSepcialOrder.getId() ||
                    view.getId() == binding.btnCustomSepcialOrder.getId()){
                gotoSpecialOrderTab(view);
            }else if(view.getId() == binding.btnHistory.getId()){
                gotoOrderHistoryTab(view);
            }else if(view.getId() == binding.btnProfile.getId()){
                gotoProfileTab(view);
            }
        }
    };


    //---------------------------------start: Tab navigation methods------------------//

    private void gotoHomeTab(View view) {
        if(currentTab == TAB_TAG_SPECIAL_ORDER) {
            if (checkSpecialOrderDetails(view,TAB_TAG_HOME)) {
                return;
            }
        }
        binding.btnCustomSepcialOrder.setChecked(false);
        binding.btnHome.setChecked(true);
        showCurrentFragment(TAB_TAG_HOME);
    }

    private void gotoSearchTab(View view) {
        if(currentTab == TAB_TAG_SPECIAL_ORDER) {
            if (checkSpecialOrderDetails(view,TAB_TAG_SEARCH)) {
                return;
            }
        }
        binding.btnCustomSepcialOrder.setChecked(false);
        showCurrentFragment(TAB_TAG_SEARCH);
    }

    private void gotoSpecialOrderTab(View view) {
        binding.bottomBar.clearCheck();
        binding.btnCustomSepcialOrder.setChecked(true);
        showCurrentFragment(TAB_TAG_SPECIAL_ORDER);
    }

    private void gotoOrderHistoryTab(View view) {
        if(currentTab == TAB_TAG_SPECIAL_ORDER) {
            if (checkSpecialOrderDetails(view,TAB_TAG_HISTORY)) {
                return;
            }
        }
        binding.btnCustomSepcialOrder.setChecked(false);
        binding.btnHistory.setChecked(true);
        showCurrentFragment(TAB_TAG_HISTORY);
    }

    private void gotoProfileTab(View view) {
        if(currentTab == TAB_TAG_SPECIAL_ORDER) {
            if (checkSpecialOrderDetails(view,TAB_TAG_PROFILE)) {
                return;
            }
        }
        binding.btnCustomSepcialOrder.setChecked(false);
        binding.btnProfile.setChecked(true);
        showCurrentFragment(TAB_TAG_PROFILE);
    }
    //---------------------------------end: Tab navigation methods------------------//

    //------------------------------start: special order clear functionality---------------------//
    //Check whether special order is empty or not
    private boolean checkSpecialOrderDetails(View view, int toTab) {
        /*Stack<BaseFragment> fragments = fragmentStack.get(currentTab);*/
        BaseFragment fragment = (BaseFragment) fm.findFragmentByTag(TAB_TAG_SPECIAL_ORDER+"");
        ((OrderAnythingFragment)fragment).updateSpecialOrderDetailsToCart();
        if(!(TextUtils.isEmpty(WaselApplication.getCart().getPickUpLocation()) && WaselApplication.getCart().isEmpty()) ){
            showClearCartPopup(view, toTab);
            return true;
        }
        return false;
    }

    //Pop up is displayed while trying to move out of partially filled special order screen
    private void showClearCartPopup(final View toTabView, final int toTab) {
        final WaselDialog dialog =  new WaselDialog(this);
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setMessage(getResources().getString(R.string.clear_cart_data));
        dialog.setPositiveButton(getResources().getString(R.string.clear),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        currentTab = toTab;
                        //Clear special order data
                        clearSpecialOrderDetails();

                        //Navigate to particular tab
                        toTabView.callOnClick();
                        dialog.cancel();
                    }
                });
        dialog.setNegativeButton(getResources().getString(R.string.cancel),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        binding.btnCustomSepcialOrder.callOnClick();
                        dialog.cancel();
                    }
                });
        dialog.setPositiveBtnColor(R.color.ok_btn);
        dialog.setNegetiveBtnColor(android.R.color.white);
        dialog.show();
    }

    //Clear special order details from cart
    private void clearSpecialOrderDetails() {
        WaselApplication.getCart().clear();
        WaselApplication.getCart().setPickUpLocation("");

        ((OrderAnythingFragment)getCurrentFragment(TAB_TAG_SPECIAL_ORDER)).clearSpecialOrderCartItems();
    }
    //------------------------------end: special order clear functionality---------------------//


    //------------------------------start: Handle main activity fragments---------------------//
    public int getCurrentTab()
    {
        return currentTab;
    }

    private BaseFragment getCurrentFragment(int currentTab){
        this.currentTab = currentTab;
        /*Stack<BaseFragment> fragments = fragmentStack.get(currentTab);
        //Returns the top most fragment. if and only if corresponding stack is not empty.
        if(fragments.size()>=1){
            return fragments.peek();
        }*/

        BaseFragment fragment = (BaseFragment) fm.findFragmentByTag(currentTab+"");
        if(fragment == null){
            //If there is no fragment at corresponding stack then the respective first fragment is created, pushed & returned.
            switch (currentTab){

                case TAB_TAG_SPECIAL_ORDER:
                    fragment = new OrderAnythingFragment();
                    break;

                case TAB_TAG_HISTORY:
                    fragment =  new OrderHistoryFragment();
                    break;

                case TAB_TAG_SEARCH:
                    fragment =  new SearchFragment();
                    break;

                case TAB_TAG_PROFILE:
                    fragment = new ProfileFragment();
                    break;

                case TAB_TAG_HOME:
                default:
                    fragment =  new HomeFragment();
                    break;
            }
        }
        return fragment;
    }

    public void navigateToCurrentTabHomePage(int tab){
        navigateToCurrentTabHomePage(tab, null);
    }

    public void navigateToCurrentTabHomePage(int tab, Bundle bundle){
        currentTab = tab;

        //Highlight current tab
        highlightCurrentTab();

        //fragments.peek()
        BaseFragment fragment = getCurrentFragment(tab);
        FragmentTransaction ft =  fm.beginTransaction();
        if(bundle != null)
        {
            fragment.getArguments().putAll(bundle);
        }
        ft.replace(R.id.fragment_container, fragment, currentTab+"");
        ft.addToBackStack(null);
        ft.commit();
    }

    public void navigateToCurrentTab(int tab, Bundle bundle){
        currentTab = tab;
        highlightCurrentTab();

        FragmentTransaction ft =  fm.beginTransaction();
        BaseFragment fragment = getCurrentFragment(tab);
        if(bundle != null)
        {
            fragment.getArguments().putAll(bundle);
        }
        ft.replace(R.id.fragment_container, fragment, tab+"");
        ft.addToBackStack(null);
        ft.commit();
    }

    public void navigateToHomeFragment(int amenityId){
        Bundle bundle = new Bundle();
        bundle.putInt(OutletsFragment.AMENITY_ID, amenityId);

        binding.bottomBar.check(R.id.btnHome);
        navigateToCurrentTabHomePage(TAB_TAG_HOME, bundle);

    }

    public void showCurrentFragment(int tab){

        Fragment fragment = getCurrentFragment(tab);
        Bundle arguments = fragment.getArguments();
        if(arguments != null) {
            arguments.clear();
        }

        FragmentTransaction ft =  fm.beginTransaction();
        ft.replace(R.id.fragment_container, fragment, tab+"");
        /*ft.addToBackStack(null);*/
        ft.commit();
    }

    //Checks & executes the potential action to done on press back button
    //& then moves to previous fragment
    @Override
    public void onBackPressed() {
        showAppQuitPopUp();
    }

    private void highlightCurrentTab() {
        switch (currentTab){
            case TAB_TAG_HOME:
                binding.btnCustomSepcialOrder.setChecked(false);
                binding.btnHome.setChecked(true);
                break;
            case TAB_TAG_SEARCH:
                binding.btnCustomSepcialOrder.setChecked(false);
                binding.btnSearch.setChecked(true);
                break;
            case TAB_TAG_SPECIAL_ORDER:
                binding.btnCustomSepcialOrder.setChecked(true);
                binding.bottomBar.clearCheck();
                break;
            case TAB_TAG_HISTORY:
                binding.btnCustomSepcialOrder.setChecked(false);
                binding.btnHistory.setChecked(true);
                break;
            case TAB_TAG_PROFILE:
                binding.btnCustomSepcialOrder.setChecked(false);
                binding.btnProfile.setChecked(true);
                break;
        }
    }
    //------------------------------end: Handle main activity fragments---------------------//

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(intent == null || intent.getExtras()==null){
            return;
        }

        //Navigation after successfully logged in
        if(intent.getExtras().containsKey(RegistrationBundleKeyConstants.KEY_LOGIN_FROM)){
            switch (intent.getIntExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM,
                    RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH)){
                case RegistrationBundleKeyConstants.LOGIN_FROM_PROFILE_TAB:
                    navigateToCurrentTabHomePage(TAB_TAG_PROFILE);
                    break;
                case RegistrationBundleKeyConstants.LOGIN_FROM_CHECKOUT_SPECIAL:
                    navigateToCurrentTabHomePage(TAB_TAG_SPECIAL_ORDER,intent.getExtras());
                    break;
                case RegistrationBundleKeyConstants.LOGIN_FROM_ORDER_HISTORY:
                    navigateToCurrentTabHomePage(TAB_TAG_HISTORY,intent.getExtras());
                    break;
                case RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH:
                default:
                    navigateToCurrentTabHomePage(TAB_TAG_HOME);
                    break;
            }
        }

        //Navigation handled after successfully placing an order
        if(intent.getExtras().containsKey(CheckoutConstants.KEY_CHECKOUT_FLOW_DONE)){
            int checkoutFlow = intent.getExtras().getInt(CheckoutConstants.KEY_CHECKOUT_FLOW);
            //Clear cart items
            WaselApplication.getCart().clear();
            switch (checkoutFlow){
                case CheckoutConstants.KEY_CHECKOUT_ORDER_ANY_THING:
                    navigateToCurrentTabHomePage(TAB_TAG_SPECIAL_ORDER);
                    binding.btnCustomSepcialOrder.setChecked(true);
                    clearSpecialOrderDetails();
                    return;
                case CheckoutConstants.KEY_CHECKOUT_REPEAT_NORMAL_ORDER:
                case CheckoutConstants.KEY_CHECKOUT_REPEAT_SPECIAL_ORDER:
                case CheckoutConstants.KEY_CHECKOUT_HISTORY_DETAILS:
                    navigateToCurrentTabHomePage(TAB_TAG_HISTORY);
                    break;
                case CheckoutConstants.KEY_CHECKOUT_CART_ITEMS:
                default:
                    /*displayCartView(false);*/
                    navigateToCurrentTabHomePage(TAB_TAG_HOME);
                    return;
            }
        }

        //Navigation handled when Items are Updated before placing order
        if(intent.getExtras().containsKey(CheckoutConstants.KEY_ITEMS_UPDATED_FROM_SERVER)){
            //Clear cart items
            WaselApplication.getCart().clear();
            /*displayCartView(false);*/
            navigateToCurrentTabHomePage(TAB_TAG_HOME);
        }

        //Navigation handled from address screen
        if(intent.getExtras().containsKey(CheckoutConstants.KEY_ADD_NEW_ADDRESS_FLOW)){
            navigateToCurrentTab(TAB_TAG_PROFILE, intent.getExtras());
        }
    }

    private void showAppQuitPopUp() {
        final WaselDialog dialog =  new WaselDialog(this);
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setMessage(getResources().getString(R.string.quit_app_msg));
        dialog.setPositiveButton(getResources().getString(R.string.quit),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        finish();
                    }
                });
        dialog.setNegativeButton(getResources().getString(R.string.cancel),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });
        dialog.setPositiveBtnColor(R.color.ok_btn);
        dialog.setNegetiveBtnColor(android.R.color.white);
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        getCurrentFragment(getCurrentTab()).onActivityResult(requestCode,resultCode, data);
    }

    /*
    * Observer for listening the pending feedback orders
    * */
    public Observer<List<Order>> setOrderFeedbackObserver = new Observer<List<Order>>() {

        @Override
        public void onCompleted() {
            clearSubscriptions();
        }

        @Override
        public void onError(Throwable e) {
            /*WaselToast.showToast(getServerErrorMessage(e));*/
            clearSubscriptions();
        }

        @Override
        public void onNext(List<Order> orders) {
            if(orders!=null && orders.size()>0 ) {
                //If there are pending feedback orders
                Intent feedbackIntent = new Intent(MainActivity.this, FeedbackActivity.class);
                feedbackIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                feedbackIntent.putExtra(CheckoutConstants.KEY_FEEDBACK_DATA,(ArrayList<Order>) orders);
                startActivity(feedbackIntent);
            }
            clearSubscriptions();
        }
    };

}
