package com.almoayyed.waseldelivery.ui;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;

import com.almoayyed.waseldelivery.BuildConfig;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.ImageUrlPrefixPreference;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.ActivitySplashScreenBinding;
import com.almoayyed.waseldelivery.models.Notifications;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.models.VersionResponse;
import com.almoayyed.waseldelivery.network_interface.NetworkStatusException;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.social_login_listeners.SocialLoginListenerhandler;
import com.almoayyed.waseldelivery.ui.login.RegisterActivity;
import com.almoayyed.waseldelivery.ui.login.SignInActivity;
import com.almoayyed.waseldelivery.utils.PlayServiceVersion;
import com.almoayyed.waseldelivery.utils.Facebook.Facebook;
import com.almoayyed.waseldelivery.utils.GooglePlus.Google;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.android.databinding.library.baseAdapters.BR;
import com.google.firebase.iid.FirebaseInstanceId;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import rx.Observable;
import rx.Observer;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 14 Nov 2016 6:16 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class SplashScreenActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1500;

    private ActivitySplashScreenBinding binding;
    private SocialLoginListenerhandler socialLoginListenerhandler;
    WaselDialog dialog;
    private ViewStub splashView;
    private Resources resources;
    private static final String INFLATE_STATE = "inflate_state";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_screen);
        binding.setLoginHandler(this);

        resources = getResources();

        socialLoginListenerhandler = new SocialLoginListenerhandler(this,binding.splashMainLyt,
                RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH);
        splashView = this.binding.splashStub.getViewStub();

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        if(!TextUtils.isEmpty(refreshedToken)){
            LoginCredentialsPreference.getLoginPreference().saveRegId(refreshedToken);
            triggerNotificationApi(refreshedToken);
        }

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.almoayyed.waseldelivery",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void triggerNotificationApi(String refreshedToken) {
        if(LoginCredentialsPreference.getLoginPreference().isUserLoggedIn()){
            //Triggering Notification Api to send DeviceToken to Server
            Notifications notifications = new Notifications();
            notifications.setAndroid(true);
            notifications.setDeviceToken(refreshedToken);
            notifications.setUserId(LoginCredentialsPreference.getLoginPreference().getId());

            Observable<Response> notificationObservable = WaselDeliveryService.getService().getNotification(notifications);
            socialLoginListenerhandler.subscribe = notificationObservable.subscribe(notificationObserver);
            socialLoginListenerhandler.startStopCompositeSubscription.add(socialLoginListenerhandler.subscribe);
        }
    }

    /*
 * Observer for Notification check api.
 * */
    Observer<Response> notificationObserver = new Observer<Response>() {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            WaselToast.showToast(getServerErrorMessage(e));
        }

        @Override
        public void onNext(Response response) {
            Loggers.error("response :"+response);
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(INFLATE_STATE,binding.splashStub.isInflated());
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Boolean state = savedInstanceState.getBoolean(INFLATE_STATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!PlayServiceVersion.checkPlayServices(this)){

            // getPackageName() from Context or Activity object
            /*try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
            }*/
        }else{
            if(!binding.splashStub.isInflated()){
                //Removed app version, as it is not considered
            /*checkAppVersion();*/
                navigateFromSplash();
            }
        }

    }

    private void checkAppVersion() {
        Observable<VersionResponse> appVersionObservable = WaselDeliveryService.getService().appVersion();
        socialLoginListenerhandler.subscribe = appVersionObservable.subscribe(appVersionObserver);
        socialLoginListenerhandler.startStopCompositeSubscription.add(socialLoginListenerhandler.subscribe);
    }

    /*
  * Observer for App Version Check api.
  * */
    Observer<VersionResponse> appVersionObserver = new Observer<VersionResponse>() {

        @Override
        public void onCompleted() {
            socialLoginListenerhandler.mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            socialLoginListenerhandler.mProgressBarHandler.hide();
            if(e instanceof NetworkStatusException){
                dialog =  new WaselDialog(SplashScreenActivity.this);
                dialog.setTitle(resources.getString(R.string.app_name));
                dialog.setMessage(resources.getString(R.string.check_internet));
                dialog.setNeutralButton(resources.getString(R.string.ok),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        });
                dialog.setNeutralBtnColor(android.R.color.white);
                dialog.show();
            }else {
                WaselToast.showToast(getServerErrorMessage(e));
            }

        }

        @Override
        public void onNext(VersionResponse response) {
            socialLoginListenerhandler.mProgressBarHandler.hide();
            String serverVersion = response.getVersion().replaceAll("_",".").toString();
            ImageUrlPrefixPreference.setImagePrefix(response.getS3BaseUrl());

            /*//Removed as force update is temporarily paused
            if(serverVersion.equalsIgnoreCase(CheckAppVersion.getAppVersion(SplashScreenActivity.this))){
                navigateFromSplash();
            }else{
                dialog =  new WaselDialog(SplashScreenActivity.this);
                dialog.setTitle(getResources().getString(R.string.app_name));
                dialog.setMessage(getResources().getString(R.string.version_not_match));
                dialog.setNeutralButton(getResources().getString(R.string.ok),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        });
                dialog.setNeutralBtnColor(android.R.color.white);
                dialog.show();
            }*/

            navigateFromSplash();
        }
    };

    private void navigateFromSplash() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                // Verifying if user had already Logged in or not
                if (LoginCredentialsPreference.getLoginPreference().isUserLoggedIn()) {
                    socialLoginListenerhandler.openActivity();
                } else {
                    showLoginLayout();
                }
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(runnable, SPLASH_TIME_OUT);
    }


    @Override
    public void onDestroy() {
        socialLoginListenerhandler.startStopCompositeSubscription.clear();
        super.onDestroy();
    }

    private void showLoginLayout() {

        binding.splashImg.setImageDrawable(resources.getDrawable(R.drawable.login_logo));
        binding.splashStub.setOnInflateListener(new ViewStub.OnInflateListener() {
            @Override
            public void onInflate(ViewStub viewStub, View view) {
                DataBindingUtil.bind(view).setVariable(BR.stubLoginHandler,SplashScreenActivity.this);
            }
        });
        if (!binding.splashStub.isInflated()) {
            splashView.inflate();
        }else {
            splashView.setVisibility(View.VISIBLE);
        }
        final ViewTreeObserver viewTreeObserver = splashView.getViewTreeObserver();
        viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                /*
                //Sliding animation of bottom login panel
                int height = loginStub.getHeight();
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) loginStub.getLayoutParams();
                layoutParams.bottomMargin = -height;
                loginStub.setLayoutParams(layoutParams);

                ObjectAnimator imageTranslateAnimator = ObjectAnimator.ofFloat(binding.splashImg, "translationY", -height / 2);
                ObjectAnimator splashLoginStubAnimator = ObjectAnimator.ofFloat(loginStub, "translationY", -height);

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.setDuration(500);
                animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
                animatorSet.playTogether(imageTranslateAnimator, splashLoginStubAnimator);
                animatorSet.start();

                if (viewTreeObserver.isAlive()) {
                    viewTreeObserver.removeOnPreDrawListener(this);
                } else {
                    ViewTreeObserver viewTreeObserver = loginStub.getViewTreeObserver();
                    viewTreeObserver.removeOnPreDrawListener(this);
                }*/

                //Fade in animation of bottom login panel
                Animation fadeIn = new AlphaAnimation(0, 1);
                fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
                fadeIn.setDuration(500);

                AnimationSet animation = new AnimationSet(false); //change to false
                animation.addAnimation(fadeIn);
                binding.splashStub.getRoot().setAnimation(animation);

                if (viewTreeObserver.isAlive()) {
                    viewTreeObserver.removeOnPreDrawListener(this);
                } else {
                    ViewTreeObserver viewTreeObserver = splashView.getViewTreeObserver();
                    viewTreeObserver.removeOnPreDrawListener(this);
                }
                return false;
            }
        });

    }
    public View.OnClickListener setOnSignInClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(SplashScreenActivity.this, SignInActivity.class);
            startActivity(i);
        }
    };

    public View.OnClickListener onRegisterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(SplashScreenActivity.this, RegisterActivity.class);
            startActivity(i);
        }
    };

    public View.OnClickListener onFBClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            socialLoginListenerhandler.mProgressBarHandler.show();
            Facebook.signUp(SplashScreenActivity.this, socialLoginListenerhandler.fbSigninCallback);
        }
    };

    public View.OnClickListener onGoogleClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            socialLoginListenerhandler.mProgressBarHandler.show();
            Google.signUp(SplashScreenActivity.this, socialLoginListenerhandler.googleSigninListener);
        }
    };

    public View.OnClickListener onSkipClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            socialLoginListenerhandler.openActivity();
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode != RESULT_OK)
        {
            socialLoginListenerhandler.mProgressBarHandler.hide();
            return;
        }
        switch (requestCode) {
            case Facebook.RC_FACEBOOK:
                Facebook.onFacebookActivityResult(requestCode, resultCode, data);
                //TODO: log current user information & further navigations
                /*socialLoginListenerhandler.mProgressBarHandler.hide();*/
                break;
            case Google.RC_SIGN_IN:
                socialLoginListenerhandler.onGoogleLoginSuccess(data);
                break;
            default:
                socialLoginListenerhandler.mProgressBarHandler.hide();
                break;
        }

    }




}
