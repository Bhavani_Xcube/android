package com.almoayyed.waseldelivery.ui.address;

import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.utils.ScreenUtils;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.databinding.ActivityAddAddressBinding;
import com.almoayyed.waseldelivery.models.LocationSearchResult;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.network_interface.location.ReverseGeoCode;
import com.almoayyed.waseldelivery.network_interface.location.SearchPlaceViaGms;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.viewlisteners.onRecyclerItemClickListener;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

/** Created on 28 Nov 2016 6:47 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.ViewTextWatcherListener
 */

public class AddAddressActivity extends FragmentActivity implements OnMapReadyCallback, onRecyclerItemClickListener {

    private GoogleMap mMap = null;
    private static String TAG = "MAP LOCATION";
    private LatLng mCenterLatLong = null;
    private UserLocation mLocation = null;
    private final int inputThresholdLmt = 1;
    private ReverseGeoCode reverseGeoCodeThread;
    //revGeoCodeSubject: Creates an "Observable" to send reverse geo code data
    private PublishSubject<Object> revGeoCodeSubject = PublishSubject.create();
    private ProgressBarHandler mProgressBarHandler;
    private String selectedPlace = "";
    private PlacesListAdapter placesListAdapter;
    private SearchPlaceViaGms searchPlaceThread = null;
    private PublishSubject<Object> searchPlaceSubject = PublishSubject.create();
    private ActivityAddAddressBinding binding;
    private String from;
    private CompositeSubscription compositeSubscription;
    private boolean searchFromDropDown = false; //enabled when user click on search drop down list.


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_address);
        binding.setVariable(BR.confirmOrderHandler,this);

        initViews();
    }

    private void initViews() {

        //Loading data
        if(getIntent().hasExtra(CheckoutConstants.KEY_ADDRESS)){
            mLocation = (UserLocation)getIntent().getSerializableExtra(CheckoutConstants.KEY_ADDRESS);
        }else {
            mLocation = new UserLocation();
            mLocation.setLatitude(LocationPreference.getCurrentLat());
            mLocation.setLongitude(LocationPreference.getCurrentLng());
            StringBuffer currentLocName = new StringBuffer(LocationPreference.getCurrentLocName());
            currentLocName.replace(currentLocName.length()-2,currentLocName.length(),"");
            mLocation.setSubLocality3(currentLocName.toString());
        }
        from = getIntent().getStringExtra(ProfileConstants.FROM);

        binding.setVariable(BR.navFrom,from);
        binding.backBtn.getDrawable().setAutoMirrored(true);

        mProgressBarHandler = new ProgressBarHandler(this, binding.addAddressLyt);
        compositeSubscription = new CompositeSubscription();

        //search location recycler view & adapter
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        placesListAdapter = new PlacesListAdapter(this);
        placesListAdapter.setOnItemClickListener(this);
        binding.placesListview.setLayoutManager(layoutManager);
        binding.placesListview.setAdapter(placesListAdapter);
        //Observer that listens the search places list.
        Subscription searchSubscription = searchPlaceSubject
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object data) {
                        if (data instanceof SearchPlaceViaGms.SearchPlaceOutput) {
                            onReceiveSearchPlaces((SearchPlaceViaGms.SearchPlaceOutput) data);
                        } else if (data instanceof String) {
                            onReceiveError((String) data);
                        }
                    }
                });
        //Thread that returns the places of search text
        searchPlaceThread = new SearchPlaceViaGms(searchPlaceSubject);
        searchPlaceThread.start();


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Subscription subscription = revGeoCodeSubject
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object data) {
                        if (data instanceof UserLocation) {
                            onReceiveLocationDetails((UserLocation) data);
                        } else if (data instanceof String) {
                            onReceiveError((String) data);
                        }
                    }
                });

        //Setting click listeners
        binding.setBtnHandler(this);

        //Search from keyboard
        binding.findManualLocEntry.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch(binding.findManualLocEntry.getText().toString());
                    return true;
                }
                return false;
            }
        });

//        binding.findManualLocEntry.setTranslationZ(10f);

        KeyboardHandler.setKeyboardHandler(binding.getRoot());
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "OnMapReady");
        if(mMap!=null){
            mMap.clear();
        }
        mMap = googleMap;

        updateLocationDetailsOnUI(mLocation);

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                Log.e("Camera postion change" + "", cameraPosition + "");
                mCenterLatLong = cameraPosition.target;

                mMap.clear();

                try {
                    if(searchFromDropDown){
                        mLocation.setLatitude(mCenterLatLong.latitude);
                        mLocation.setLongitude(mCenterLatLong.longitude);
                        searchFromDropDown = false;
                        return;
                    }
                    mLocation.clear();
                    mLocation.setLatitude(mCenterLatLong.latitude);
                    mLocation.setLongitude(mCenterLatLong.longitude);

                    //Find current location by lat lng
                    if(reverseGeoCodeThread==null){
                        reverseGeoCodeThread = new ReverseGeoCode(revGeoCodeSubject);
                        reverseGeoCodeThread.start();
                    }
                    reverseGeoCodeThread.searchLocationByLatLng(mLocation);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

    }

    //Closes the activity
    public View.OnClickListener backPressListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    //Clear the location text field
    public View.OnClickListener clearFieldListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            selectedPlace = "";
            binding.findManualLocEntry.setText("");
            placesListAdapter.clearData();
        }
    };

    //Navigates to save address screen by confirming address
    public View.OnClickListener onClickConfirmLocation = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            if(TextUtils.isEmpty(mLocation.getLocDescription())){
                WaselToast.showToast(getString(R.string.empty_address));
                return;
            }

            if(ProfileConstants.FROM_SP_ORDER.equalsIgnoreCase(from)){
                //Passing pickup address
                Intent i = new Intent();
                i.putExtra(CheckoutConstants.KEY_ADDRESS,mLocation);
                i.putExtra(ProfileConstants.FROM,from);
                setResult(Activity.RESULT_OK,i);
                finish();
                return;
            }
            //reverse geo code the selected lat lng
            //Creates thread for reverse geo-code location
            Intent i = new Intent(getApplicationContext(),SaveAddressActivity.class);
            i.putExtra(CheckoutConstants.KEY_ADDRESS,mLocation);
            i.putExtra(ProfileConstants.FROM,from);
            startActivity(i);
            finish();
        }
    };

    //Updates location text & map
    private void updateLocationDetailsOnUI(UserLocation revGeoCode) {

        //move map camera to current location
        mLocation = revGeoCode;
        LatLng currentMapPosition = mMap.getCameraPosition().target;
        if(currentMapPosition.latitude != mLocation.getLatitude() && currentMapPosition.longitude !=mLocation.getLongitude()) {
            float camZoom = Math.max(mMap.getCameraPosition().zoom, 14.0f);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), camZoom));
        }

        //Displaying current location text
        String currentLocText;
        if(searchFromDropDown){
            currentLocText = selectedPlace;
            mLocation.setLocDescription(selectedPlace);
        }else {
            currentLocText = revGeoCode.getLocDescription();
        }
        if(!currentLocText.equalsIgnoreCase(selectedPlace)){
            selectedPlace = currentLocText;
            binding.findManualLocEntry.setText(selectedPlace);
            binding.findManualLocEntry.setSelection(binding.findManualLocEntry.getText().length());
        }
    }

    public void getLocDetailsByPlaceId(String placeId) {
        //Creates thread for reverse geo-code location
        if(reverseGeoCodeThread==null){
            reverseGeoCodeThread = new ReverseGeoCode(revGeoCodeSubject);
            reverseGeoCodeThread.start();
        }
        reverseGeoCodeThread.searchLocationByPlaceId(placeId);
        mProgressBarHandler.show();
    }


    /*
    * Input: UserLocation details
    * Desc: Updates the location details on screen
    * */
    public void onReceiveLocationDetails(final UserLocation revGeoCode){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(revGeoCode!=null && revGeoCode.getLocDescription() !=  null) {
                    //reverse geo code location details found successfully
                    updateLocationDetailsOnUI(revGeoCode);
                }
                mProgressBarHandler.hide();
            }
        });
    }

    public void onReceiveError(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                WaselToast.showToast(message);
                mProgressBarHandler.hide();
            }
        });
    }

    /*
    * Method: onReceiveSearchPlaces
    * Desc: Receives the auto search places result & display them.
    * */
    public void onReceiveSearchPlaces(SearchPlaceViaGms.SearchPlaceOutput output){
        Log.i("received","places received");
        final ArrayList<LocationSearchResult> results = output.results;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressBarHandler.hide();
                if(results.size()>0){
                    placesListAdapter.setData(results);
                }else {
                    placesListAdapter.clearData();
                }
            }
        });
    }
    @Override
    public void onStart() {
        super.onStart();
        registerLocSearchTextWatcher();
    }
    /*
  * Check for changes at input text
  * Pass the changed text to search-places thread
  * */
    private void registerLocSearchTextWatcher() {

        Subscriber<CharSequence> editTextSubscription = new Subscriber<CharSequence>() {
            @Override
            public void onCompleted() {
                Loggers.temp("TextViewAfterTextChangeEvent = onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Loggers.temp("TextViewAfterTextChangeEvent = " + e.getMessage());
            }


            public void onNext(CharSequence s) {
                //Enabling close img
                binding.findManualLocEntry.setCompoundDrawablesWithIntrinsicBounds(R.drawable.location_small,0,
                        s.length()>0?R.drawable.close:0,0);

                performSearchWithLimitation(s);
            }
        };

        Subscription searchSubscribe = RxTextView.textChanges(binding.findManualLocEntry)
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(editTextSubscription);
        compositeSubscription.add(searchSubscribe);
    }

    private void performSearchWithLimitation(CharSequence s) {
        if (s.length() >= inputThresholdLmt) {
            if(selectedPlace.equalsIgnoreCase(s.toString())){
                return;
            }
            performSearch(s.toString());
        }else {
            placesListAdapter.clearData();
        }
    }

    private void performSearch(String text) {
        if(TextUtils.isEmpty(text)){
            return;
        }
        //Start searching for places
        mProgressBarHandler.show();
        searchFromDropDown = true;
        searchPlaceThread.onSearchTextChange(text);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("loc",mLocation);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState!=null){
            mLocation = (UserLocation)savedInstanceState.getSerializable("loc");
        }
    }

    /*
            ** Closes the search-places thread.
            **/
    @Override
    public void onDestroy() {
        if(reverseGeoCodeThread!=null){
            reverseGeoCodeThread.clear();
            reverseGeoCodeThread = null;
        }
        if(searchPlaceThread!=null){
            searchPlaceThread.clear();
            searchPlaceThread = null;
        }
        compositeSubscription.clear();
        if(mMap!=null){
            mMap.clear();
        }
        super.onDestroy();
    }


    @Override
    public void onItemClick(View v, int pos) {
        if(v.getTag() != null){

            LocationSearchResult userLocation = placesListAdapter.getItemAtPosition(pos);
            getLocDetailsByPlaceId(userLocation.getPlace_id());
            selectedPlace = userLocation.getDescription();
            binding.findManualLocEntry.setText(selectedPlace);
            binding.findManualLocEntry.setSelection(binding.findManualLocEntry.getText().length());
            binding.findManualLocEntry.clearFocus();

            //Clear adapter data when user selects one place
            placesListAdapter.clearData();
        }
    }

}
