package com.almoayyed.waseldelivery.ui.address;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.models.LocationSearchResult;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.viewlisteners.onRecyclerItemClickListener;

import java.util.ArrayList;

/**
 * Created on 15 Dec 2016 6:16 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/** Updates the search places list on to the recycler view
 */
public class PlacesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final LayoutInflater mInflater;
    private ArrayList<LocationSearchResult> resultList;
    private onRecyclerItemClickListener recyclerItemClickListener;

    public PlacesListAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        resultList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.manual_place_item, parent, false);
        KeyboardHandler.setKeyboardHandler(view);
        return new PlacesListAdapter.PlaceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        PlacesListAdapter.PlaceViewHolder viewHolder = (PlacesListAdapter.PlaceViewHolder) holder;
        viewHolder.itemView.setOnClickListener(getLocationDetailsByPlaceId());
        viewHolder.itemView.setTag(position);
        viewHolder.autoText.setText(resultList.get(position).getDescription());
    }

    @NonNull
    private View.OnClickListener getLocationDetailsByPlaceId() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(recyclerItemClickListener != null)
               {
                   recyclerItemClickListener.onItemClick(v, (Integer) v.getTag());
               }
            }
        };
    }

    public void setOnItemClickListener(onRecyclerItemClickListener recyclerItemClickListener)
    {
        this.recyclerItemClickListener =recyclerItemClickListener;
    }


    @Override
    public int getItemCount() {
        return resultList.size();
    }

    public void clearData(){
        resultList.clear();
        notifyDataSetChanged();
    }

    public void setData(ArrayList searchData) {
        resultList.clear();
        resultList.addAll(searchData);
        notifyDataSetChanged();
    }

    public LocationSearchResult getItemAtPosition(int pos)
    {
        return resultList.get(pos);
    }

    class PlaceViewHolder extends RecyclerView.ViewHolder {
        TextView autoText;
        public PlaceViewHolder(View view){
            super(view);
            autoText = (TextView) view.findViewById(R.id.search_loc_text);
        }
    }
}