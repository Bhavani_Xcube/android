package com.almoayyed.waseldelivery.ui.address;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.FragmentsIndex;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.ActivitySaveAddressBinding;
import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseActivity;
import com.almoayyed.waseldelivery.ui.LoadFragmentActivity;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.checkout.ConfirmOrderActivity;
import com.almoayyed.waseldelivery.ui.special_order.ApplyCouponCodeActivity;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;


/**
 * Created on 28 Nov 2016 6:47 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class SaveAddressActivity extends BaseActivity{
    private UserLocation userLocation;
    private ActivitySaveAddressBinding binding;
    private Address address = null;
    private String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_save_address);
        if(savedInstanceState!=null){
            address = (Address) savedInstanceState.getSerializable("address");
        }
        initViews();
    }

    private void initViews() {
        mProgressBarHandler = new ProgressBarHandler(this, binding.saveAddressLyt);
        userLocation = getIntent().hasExtra(CheckoutConstants.KEY_ADDRESS)?
                (UserLocation) getIntent().getSerializableExtra(CheckoutConstants.KEY_ADDRESS) : new UserLocation();
        from = getIntent().getStringExtra(ProfileConstants.FROM);
        if(address==null){
            address = userLocation.convertToAddress();
        }
        binding.setAddress(address);
        binding.setBtnHandler(this);
        binding.backBtn.getDrawable().setAutoMirrored(true);
        binding.addressCategory.setOnCheckedChangeListener(addressCategoryChangeListener);

        //Setting addressCategory as home
        // addressCategory.check(R.id.btnHome);
        KeyboardHandler.setKeyboardHandler(binding.getRoot());
    }

    public View.OnClickListener saveAddressListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            /*if(TextUtils.isEmpty(address.getDoorNumber())){
                WaselToast.showToast(getString(R.string.empty_flat_no));
            }else if(TextUtils.isEmpty(address.getLandmark())){
                WaselToast.showToast(getString(R.string.empty_land_mark));
            }else */
            if(binding.addressCategory.getCheckedRadioButtonId()==-1){
                WaselToast.showToast(getString(R.string.empty_address_type));
            }
            else if(TextUtils.isEmpty(address.getAddressType())){
                WaselToast.showToast(getString(R.string.empty_other_address_name));
            }
            else{
                //TODO: save userLocation by passing to server
                UserLoginDetails addNewAddress = new UserLoginDetails();
                addNewAddress.setId(LoginCredentialsPreference.getLoginPreference().getId());
                ArrayList<Address> addresses = new ArrayList<>();
                addresses.add(address);
                addNewAddress.setAddress(addresses);
                Observable<UserLoginDetails> addAddressObservable = WaselDeliveryService.getService().updateProfile(addNewAddress);
                addObserver(addAddressObservable, addAddressObserver);
            }
        }
    };
    public View.OnClickListener backPressListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(SaveAddressActivity.this, AddAddressActivity.class);
            i.putExtra(CheckoutConstants.KEY_ADDRESS, userLocation);
            i.putExtra(ProfileConstants.FROM,from);
            startActivity(i);
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        binding.backBtn.callOnClick();
    }

    RadioGroup.OnCheckedChangeListener addressCategoryChangeListener = new RadioGroup.OnCheckedChangeListener(){

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId){
                case R.id.btnOther:
                    /*address.setAddressType(Address.ADDRESS_TYPE_OTHER);*/
                    address.setAddressType(binding.addressType.getText().toString());
                    binding.addressType.setVisibility(View.VISIBLE);
                    break;
                case R.id.btnOffice:
                    address.setAddressType(Address.ADDRESS_TYPE_OFFICE);
                    binding.addressType.setVisibility(View.INVISIBLE);
                    break;
                default:
                    address.setAddressType(Address.ADDRESS_TYPE_HOME);
                    binding.addressType.setVisibility(View.INVISIBLE);
                    break;
            }
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("address",address);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState!=null){
            address = (Address) savedInstanceState.getSerializable("address");
        }
    }

    /*
  * Observer for updating new userLocation to current user
  * */
    public Observer<UserLoginDetails> addAddressObserver = new Observer<UserLoginDetails>() {

        @Override
        public void onCompleted() {
            clearSubscriptions();
        }

        @Override
        public void onError(Throwable e) {
            String msg = getServerErrorMessage(e);
            if(TextUtils.isEmpty(msg)){
                return;
            }
            final WaselDialog dialog =  new WaselDialog(SaveAddressActivity.this);
            dialog.setTitle(getResources().getString(R.string.app_name));
            dialog.setMessage(msg);
            dialog.setNeutralButton(getResources().getString(R.string.ok),
                    new View.OnClickListener() {
                        public void onClick(View view) {
                            dialog.cancel();
                        }
                    });
            dialog.setNeutralBtnColor(android.R.color.white);
            dialog.show();
            clearSubscriptions();
        }

        @Override
        public void onNext(UserLoginDetails userLoginDetails) {
            if(userLoginDetails!=null) {
                //Address successfully saved
                if(ProfileConstants.FROM_PROFILE.equalsIgnoreCase(from)){
                    /*ManageAddressFragment manageAddressFragment = new ManageAddressFragment();
                    Bundle b = new Bundle();
                    b.putSerializable(manageAddressFragment.LOGIN_DETAILS, userLoginDetails);
                    manageAddressFragment.setArguments(b);
                    addFragment(manageAddressFragment);*/
                    Intent i = new Intent(SaveAddressActivity.this, LoadFragmentActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    i.putExtra(CheckoutConstants.KEY_ADD_NEW_ADDRESS_FLOW,1);
                    i.putExtra(CheckoutConstants.KEY_ADDRESS, userLoginDetails);
                    Bundle bundle = new Bundle();
                    bundle.putInt(FragmentsIndex.FRAGMENT_TYPE, FragmentsIndex.FRAGMENT_MANAGE_ADDRESS);
                    i.putExtra(FragmentsIndex.FRAGMENT_DATA, bundle);
                    startActivity(i);
                    Log.e("TAG","inside profile");
                }else {
                    //Pass the userLocation info to order-posting screen
                    Intent i = new Intent(SaveAddressActivity.this, ConfirmOrderActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    i.putExtra(CheckoutConstants.KEY_ADDRESS, userLoginDetails);
                    startActivity(i);
                }
                finish();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
