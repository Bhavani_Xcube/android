package com.almoayyed.waseldelivery.ui.checkout;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.models.Cart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 2 Jan 2017 07:23 PM PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public class BillDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private Cart cart;
    private View view;
    private static final int DETAILS_VIEW= 1;
    private static final int GRAND_TOTAL_VIEW = 2;
    private Resources resources;

    public BillDetailsAdapter(Context context, Cart cart) {
        if(cart == null){
            this.cart = WaselApplication.getCart();
        }else{
            this.cart = cart;
        }
        this.context = context;
        resources = context.getResources();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == DETAILS_VIEW){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_details_item_list, parent, false);
            return new BillDetailsViewHolder(view);
        }else if(viewType == GRAND_TOTAL_VIEW){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grand_total_item, parent, false);
            return new GrandTotalViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getItemViewType() == DETAILS_VIEW){
            BillDetailsViewHolder billDetailsViewHolder = (BillDetailsViewHolder) holder;

            if(position == 0){
                billDetailsViewHolder.hiphan.setVisibility(View.GONE);
                billDetailsViewHolder.billType.setText(resources.getString(R.string.bill_order_charge));
                billDetailsViewHolder.billCost.setText(String.format(WaselApplication.getContext()
                        .getString(R.string.price_format),cart.getOrderCharge()));
            }else if(position == 1){
                billDetailsViewHolder.hiphan.setVisibility(View.GONE);
                billDetailsViewHolder.billType.setText(resources.getString(R.string.bill_delivery_charge));
                billDetailsViewHolder.billCost.setText(String.format(WaselApplication.getContext()
                        .getString(R.string.price_format),cart.getDeliveryCharge()));
            }else if(position == 2){
                billDetailsViewHolder.hiphan.setVisibility(View.VISIBLE);
                billDetailsViewHolder.billType.setText(resources.getString(R.string.bill_discount));
                billDetailsViewHolder.billCost.setText(String.format(WaselApplication.getContext()
                        .getString(R.string.price_format),cart.getDiscountAmount()));
            }

            billDetailsViewHolder.billType.setTag(position);
            billDetailsViewHolder.billCost.setTag(position);

        }else if(holder.getItemViewType() == GRAND_TOTAL_VIEW){
            GrandTotalViewHolder grandTotalViewHolder = (GrandTotalViewHolder) holder;
            grandTotalViewHolder.grandTotalBillType.setText(resources.getString(R.string.grand_total1));
            grandTotalViewHolder.grandTotalBillCost.setText(String.format(WaselApplication.getContext()
                    .getString(R.string.price_format),cart.getGrandTotal()));

            grandTotalViewHolder.grandTotalBillType.setTag(position);
            grandTotalViewHolder.grandTotalBillCost.setTag(position);

        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 3) {
            return GRAND_TOTAL_VIEW;
        } else {
            return DETAILS_VIEW;
        }
    }

    private class BillDetailsViewHolder extends RecyclerView.ViewHolder {
        private TextView billType;
        private TextView billCost;
        private TextView hiphan;

        public BillDetailsViewHolder(View view) {
            super(view);
            billType = (TextView) view.findViewById(R.id.name);
            billCost = (TextView) view.findViewById(R.id.cost);
            hiphan = (TextView) view.findViewById(R.id.hiphan);
        }
    }

    private class GrandTotalViewHolder extends RecyclerView.ViewHolder {
        private TextView grandTotalBillType;
        private TextView grandTotalBillCost;

        public GrandTotalViewHolder(View view) {
            super(view);
            grandTotalBillType = (TextView) view.findViewById(R.id.grand_total_name);
            grandTotalBillCost = (TextView) view.findViewById(R.id.grand_total_cost);
        }
    }
}
