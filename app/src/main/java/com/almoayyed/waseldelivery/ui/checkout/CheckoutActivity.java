package com.almoayyed.waseldelivery.ui.checkout;

import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.AppLimitations;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.ActivityCheckoutBinding;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.models.OrderItem;
import com.almoayyed.waseldelivery.models.OutletItem;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseActivity;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.home.OutletDetails.CustomizeOutletItemDialog;
import com.almoayyed.waseldelivery.ui.login.LoginActivity;
import com.almoayyed.waseldelivery.utils.NetworkConnection;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselToast;
import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created on 28 Nov 2016 11:19 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CheckoutActivity extends BaseActivity{

    private Cart cart;
    private CheckoutAdapter checkoutAdapter;
    private ActivityCheckoutBinding binding;
    private int checkoutFlow;
    private Resources resources;
    private static final String CART = "cart";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =DataBindingUtil.setContentView(this,R.layout.activity_checkout);
        binding.setBtnHandler(this);
        initViews();
    }

    private void initViews() {
        mProgressBarHandler = new ProgressBarHandler(this,binding.checkoutLayout);
        resources = getResources();

        checkoutFlow = getIntent().getExtras().getInt(CheckoutConstants.KEY_CHECKOUT_FLOW);
        cart = WaselApplication.getCart();
        if(checkoutFlow == CheckoutConstants.KEY_CHECKOUT_CART_ITEMS){
            binding.hideText.setText(R.string.hide);
        }else {
            //fetch the order information
//            cart = new Cart();
            Order order = (Order) getIntent().getExtras().getSerializable(CheckoutConstants.KEY_DATA);
            Observable<Order> orderDetailsObservable = WaselDeliveryService.getService().getOrderDetails(order.getId());
            addObserver(orderDetailsObservable, orderDetailsObserver);

            //Update UI
            binding.hideText.setText(R.string.cancel);
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.checkoutList.setLayoutManager(linearLayoutManager);

        checkoutAdapter = new CheckoutAdapter(this, cart);
        checkoutAdapter.setIncQuantityListener(incItemQuantityListener);
        checkoutAdapter.setDecQuantityListener(decItemQuantityListener);
        checkoutAdapter.setShowCustomizeDlgListener(showCustomDlgListener);
        binding.checkoutList.setAdapter(checkoutAdapter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //Navigate posting order screen when user successfully logged in
        if(intent.getExtras()!= null && intent.getExtras().containsKey(RegistrationBundleKeyConstants.KEY_LOGIN_FROM)){
            if(LoginCredentialsPreference.getLoginPreference().isUserLoggedIn()){
                Intent i = new Intent(CheckoutActivity.this, ConfirmOrderActivity.class);
                i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,checkoutFlow);
                if(checkoutFlow == CheckoutConstants.KEY_CHECKOUT_REPEAT_NORMAL_ORDER)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             {
                    i.putExtra(CheckoutConstants.KEY_DATA,cart);
                }
                startActivity(i);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(CART,cart);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        cart = (Cart)savedInstanceState.getSerializable(CART);
    }

    /*Listeners to inc the individual item quantity*/
    View.OnClickListener incItemQuantityListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //inc item quantity
            int pos = (Integer) v.getTag();
            OutletItem outletItem = checkoutAdapter.getOutletItemAtPos(pos);
            if(cart.getItemCount() >= AppLimitations.MAX_CART_ITEMS){
                WaselToast.showToast(getString(R.string.max_item_limit));
                return;
            }
            cart.addItem(outletItem);
        }
    };

    /*Listeners to dec the individual item quantity*/
    View.OnClickListener decItemQuantityListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //dec item quantity
            int pos = (Integer) v.getTag();
            OutletItem outletItem = checkoutAdapter.getOutletItemAtPos(pos);
            if(outletItem.getCartQuantity()>1){
                cart.removeItem(pos);
                checkoutAdapter.notifyDataSetChanged();
            }else if(outletItem.getCartQuantity() == 1){
                //prompt asking to remove item from cart
                showItemRemoveFromCartDialog(pos);
            }
        }
    };

    View.OnClickListener showCustomDlgListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //add item to cart & inc its quantity
            OutletItem outletItem = (OutletItem) v.getTag();
            showCustomizationDialog(outletItem);
        }
    };

    //Displays customization dialog
    private void showCustomizationDialog(OutletItem outletItem) {

        Bundle bundle = new Bundle();
        bundle.putSerializable(CheckoutConstants.KEY_CUSTOMIZATION_ITEM,outletItem);

        //Creating customization dialog
        final CustomizeOutletItemDialog dlg = new CustomizeOutletItemDialog();
        dlg.setArguments(bundle);

        View.OnClickListener doneClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Updating customization is updated
                OutletItem outletItem = (OutletItem) v.getTag();
                outletItem.setCustomized(true);

                //Update cart values on UI
                cart.notifyTotalPriceChanged();
                dlg.dismiss();
            }
        };

        View.OnClickListener cancelClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: display prompt for clear the content
                OutletItem outletItem = (OutletItem) v.getTag();
                //Set the previous data
                outletItem.revertCustomization();
                dlg.dismiss();
            }
        };

        dlg.setDoneClickListener(doneClickListener);
        dlg.setCancelClickListener(cancelClickListener);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(dlg,"fragment_dialog");
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        binding.hideText.callOnClick();
    }

    //Listener to close the checkout screen
    public View.OnClickListener hideListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v instanceof TextView &&
                    getString(R.string.cancel).equalsIgnoreCase(((TextView)v).getText().toString())){
                //Show prompt to clear the cart
                showCartClearDialog();
            }else{
                finish();
            }
        }
    };

    //Prompt is displayed before remove the last item of each type from the cart.
    //Close this screen if cart is empty
    private void showItemRemoveFromCartDialog(final int pos) {
        final WaselDialog dialog =  new WaselDialog(this);
        dialog.setTitle(resources.getString(R.string.app_name));
        if(cart.getItemCount()==1){
            dialog.setMessage(resources.getString(R.string.cart_clear_items_msg));
        }else{
            dialog.setMessage(resources.getString(R.string.delete_item_from_cart_msg));
        }
        dialog.setPositiveButton(resources.getString(R.string.yes),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Remove item from cart
                        OutletItem outletItem = checkoutAdapter.getOutletItemAtPos(pos);
                        if(outletItem.getCartQuantity()>0){
                            cart.removeItem(pos);
                            checkoutAdapter.notifyDataSetChanged();
                        }
                        dialog.cancel();

                        //Close checkout screen if cart is empty
                        if(cart.isEmpty()){
                            finish();
                        }
                    }
                });
        dialog.setNegativeButton(resources.getString(R.string.cancel),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });
        dialog.setPositiveBtnColor(R.color.ok_btn);
        dialog.setNegetiveBtnColor(android.R.color.white);
        dialog.show();
    }

    private void showCartClearDialog() {
        final WaselDialog dialog =  new WaselDialog(this);
        dialog.setTitle(resources.getString(R.string.app_name));
        dialog.setMessage(resources.getString(R.string.clear_cart_msg));
        dialog.setPositiveButton(resources.getString(R.string.clear),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        WaselApplication.getCart().clear();
                        finish();
                        dialog.cancel();
                    }
                });
        dialog.setNegativeButton(resources.getString(R.string.cancel),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });
        dialog.setPositiveBtnColor(R.color.ok_btn);
        dialog.setNegetiveBtnColor(android.R.color.white);
        dialog.show();
    }

    public View.OnClickListener checkoutListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //Check internet before proceeding to checkout an order
            if(!NetworkConnection.isNetworkAvailable()){
                WaselToast.showToast(getString(R.string.check_internet));
                return;
            }
            if(cart.getItemCount() == 0){
                WaselToast.showToast(getString(R.string.cart_empty));
                return;
            }
            if(LoginCredentialsPreference.getLoginPreference().isUserLoggedIn()){
                cart.setOrderInstruction(checkoutAdapter.getCheckoutInstructions());
                Intent i = new Intent(CheckoutActivity.this, ConfirmOrderActivity.class);
                i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,checkoutFlow);
                if(checkoutFlow == CheckoutConstants.KEY_CHECKOUT_REPEAT_NORMAL_ORDER)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             {
                    i.putExtra(CheckoutConstants.KEY_DATA,cart);
                }
                startActivity(i);
            }else {
                //TODO: go to login in flow
                WaselToast.showToast(getString(R.string.checkout_login));
                //go to login in flow
                Intent loginIntent = new Intent(CheckoutActivity.this, LoginActivity.class);
                loginIntent.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM,RegistrationBundleKeyConstants.LOGIN_FROM_CHECKOUT_NORMAL);
                startActivity(loginIntent);
            }
        }
    };

    /*
     * Observer for fetching Orders details
     * */
    Observer<Order> orderDetailsObserver = new Observer<Order>() {

        @Override
        public void onCompleted() {
            clearSubscriptions();
        }

        @Override
        public void onError(Throwable e) {
            clearSubscriptions();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
        }

        @Override
        public void onNext(Order order) {
            final WaselDialog dialog = new WaselDialog(CheckoutActivity.this);
            dialog.setTitle(resources.getString(R.string.app_name));
            // load order data to cart
            order.convertToCart(cart);

            if(order.getRemovedItems().size() == 1){
                if(checkExistenceOfRemoveItem(order.getRemovedItems().get(0).getItemId())){
                    dialog.setMessage(String.format(resources.getString(R.string.item_customization_not_available),order.getRemovedItems().get(0).getName()));
                }else {
                    dialog.setMessage(String.format(resources.getString(R.string.single_item_not_available),order.getRemovedItems().get(0).getName()));
                }
                dialog.setNeutralButton(resources.getString(R.string.ok),
                        new View.OnClickListener() {
                            public void onClick(View view) {
                                dialog.cancel();
                            }
                        });
                dialog.setNeutralBtnColor(android.R.color.white);
                dialog.show();
            }else if(order.getRemovedItems().size() >1 && order.getRemovedItems().size() < order.getItems().size()){
                dialog.setMessage(resources.getString(R.string.few_items_not_available));
                dialog.setNeutralButton(resources.getString(R.string.ok),
                        new View.OnClickListener() {
                            public void onClick(View view) {
                                dialog.cancel();
                            }
                        });
                dialog.setNeutralBtnColor(android.R.color.white);
                dialog.show();
            }else if(order.getRemovedItems().size() == order.getItems().size()){
                dialog.setMessage(resources.getString(R.string.all_items_not_available));
                dialog.setNeutralButton(resources.getString(R.string.ok),
                        new View.OnClickListener() {
                            public void onClick(View view) {
                                dialog.cancel();
                                finish();
                            }
                        });
                dialog.setNeutralBtnColor(android.R.color.white);
                dialog.show();
            }

            checkoutAdapter.notifyDataSetChanged();
            clearSubscriptions();
        }

        private boolean checkExistenceOfRemoveItem(String itemId) {
            for (OutletItem item: cart.getItems()){
                if(Integer.parseInt(item.getId()) == Integer.parseInt(itemId)){
                    return true;
                }
            }
            return false;
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
