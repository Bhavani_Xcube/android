package com.almoayyed.waseldelivery.ui.checkout;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.AmenityConstants;
import com.almoayyed.waseldelivery.constants.OutletItemCustomFieldConstants;
import com.almoayyed.waseldelivery.databinding.OutletItemForSelectionBinding;
import com.almoayyed.waseldelivery.databinding_eventhandler.LoadImage;
import com.almoayyed.waseldelivery.models.AddItem;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.OutletItem;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.views.CircleView;

import java.util.List;

import static com.almoayyed.waseldelivery.constants.AmenityConstants.setAppropriateCatagoryImage;
import static com.almoayyed.waseldelivery.databinding_eventhandler.LoadImage.loadImage;

/**
 * Created on 28 Nov 2016 11:19 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CheckoutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int FOOD_AMENITY_ID = 1;
    private final int CHECKOUT_TYPE_HEADER = 1;
    private final int CHECKOUT_TYPE_ITEMS = 2;
    private final int CHECKOUT_TYPE_INSTRUCTION = 3;

    private Cart cart;
    private AddItem instruction;
    private View.OnClickListener incQuantityListener;
    private View.OnClickListener decQuantityListener;
    private View.OnClickListener showCustomizeDlgListener;
    private AmenityConstants amenityConstants = new AmenityConstants();

    public CheckoutAdapter(Context context, Cart cart) {
        this.cart = cart;
        instruction = new AddItem();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case CHECKOUT_TYPE_HEADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_checkout_header, parent, false);
                return new CheckoutHeaderViewHolder(view);
            case CHECKOUT_TYPE_ITEMS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.outlet_item_for_selection, parent, false);
                return new CheckoutItemViewHolder(view);
            case CHECKOUT_TYPE_INSTRUCTION:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkout_instruction, parent, false);
                KeyboardHandler.hideKeyboardIfAvailable(view);
                return new CheckoutInstructionViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == CHECKOUT_TYPE_HEADER) {
            CheckoutHeaderViewHolder headerViewHolder = ((CheckoutHeaderViewHolder)holder);

            /*
            //Image loaded using data binding
            if(cart.getOutlet()!=null) {
                if (TextUtils.isEmpty(cart.getOutlet().getImageUrl())) {
                    headerViewHolder.headerImg.setImageResource(amenityConstants.setAppropriateCatagoryImage(cart.getAmenityId()));
                } else {
                    LoadImage.loadImage(headerViewHolder.headerImg, cart.getOutlet().getImageUrl());
                }
            }*/
            headerViewHolder.binding.setVariable(BR.cart,cart);
        }else if (holder.getItemViewType() == CHECKOUT_TYPE_ITEMS) {
            CheckoutItemViewHolder viewHolder = (CheckoutItemViewHolder) holder;
            CheckoutItemViewHolder checkoutItemViewHolder = viewHolder;
            OutletItem item = cart.getOutletItemAtPos(position-1);

            if(item.getOutletItemCustomFieldValue(OutletItemCustomFieldConstants.FIELD_SPICY)){
                viewHolder.foodItemName.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.spicy,0);
            }else {
                viewHolder.foodItemName.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
            }


            viewHolder.foodItemVegType.setVisibility(View.VISIBLE);
            if(item.isOutletItemCustomFieldExist(OutletItemCustomFieldConstants.FIELD_VEG)){
                ((CircleView)viewHolder.foodItemVegType).setVisibility(View.VISIBLE);
                if(item.getOutletItemCustomFieldValue(OutletItemCustomFieldConstants.FIELD_VEG)){
                    //Veg indication
                    ((CircleView)viewHolder.foodItemVegType).setCircleColor(WaselApplication.getContext().getResources().getColor(R.color.bg));
                }else {
                    ((CircleView)viewHolder.foodItemVegType).setCircleColor(WaselApplication.getContext().getResources().getColor(R.color.reddish_orange));
                }
            }else {
                ((CircleView)viewHolder.foodItemVegType).setVisibility(View.GONE);
            }

            checkoutItemViewHolder.addItemView.setOnClickListener(incQuantityListener);
            checkoutItemViewHolder.addItemView.setTag(position-1);
            checkoutItemViewHolder.removeItemView.setOnClickListener(decQuantityListener);
            checkoutItemViewHolder.removeItemView.setTag(position-1);
            checkoutItemViewHolder.getBinding().getRoot().findViewById(R.id.food_item_customize).setOnClickListener(showCustomizeDlgListener);
            checkoutItemViewHolder.getBinding().getRoot().findViewById(R.id.food_item_customize).setTag(item);
            checkoutItemViewHolder.binding.setVariable(BR.item,item);

            //Enabling customization text
            if((item.getCustomizedItems() != null && item.getCustomizedItems().size()>0)){
                checkoutItemViewHolder.getBinding().getRoot().findViewById(R.id.food_item_customize).setVisibility(View.VISIBLE);
            }else {
                checkoutItemViewHolder.getBinding().getRoot().findViewById(R.id.food_item_customize).setVisibility(View.INVISIBLE);
            }

            /*
            //Image loaded using data binding
            if(TextUtils.isEmpty(item.getImageUrl())){
                ((OutletItemForSelectionBinding) viewHolder.getBinding()).foodItemImage.
                        setImageResource(setAppropriateCatagoryImage(cart.getAmenityId()));
            }else{
                loadImage(((OutletItemForSelectionBinding) viewHolder.getBinding()).foodItemImage, item.getImageUrl());
            }*/
        }else if (holder.getItemViewType() == CHECKOUT_TYPE_INSTRUCTION) {
            ((CheckoutInstructionViewHolder)holder).binding.setVariable(BR.addItem,instruction);
        }
    }

    @Override
    public int getItemCount() {
        /*Displays checkout summary, cart items & instruction field
        * */
        return cart.getItems().size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0){
            return CHECKOUT_TYPE_HEADER;
        }else if(position>0 && position<=cart.getItems().size()){
            return CHECKOUT_TYPE_ITEMS;
        }else {
            return CHECKOUT_TYPE_INSTRUCTION;
        }
    }

    public void setIncQuantityListener(View.OnClickListener addItemToCartListener) {
        this.incQuantityListener = addItemToCartListener;
    }

    public void setDecQuantityListener(View.OnClickListener removeItemFromCartListener) {
        this.decQuantityListener = removeItemFromCartListener;
    }

    public void setShowCustomizeDlgListener(View.OnClickListener showCustomizeDlgListener) {
        this.showCustomizeDlgListener = showCustomizeDlgListener;
    }

    public String getCheckoutInstructions(){
        return instruction.getItemName();
    }

    public void addOrderItems(List<OutletItem> items){
        cart.addItem(items);
        notifyDataSetChanged();
    }

    public OutletItem getOutletItemAtPos(int pos){
        return cart.getItems().get(pos);
    }

    public class CheckoutHeaderViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private ViewDataBinding binding;
        private ImageView headerImg;

        public CheckoutHeaderViewHolder(View view) {
            super(view);
            mView = view;
            headerImg = (ImageView) view.findViewById(R.id.header_bg);
            binding = DataBindingUtil.bind(view);
            KeyboardHandler.setKeyboardHandler(view);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    public class CheckoutItemViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final View addItemView;
        public final View removeItemView;
        public final TextView foodItemName;
        public CircleView foodItemVegType;
        private ViewDataBinding binding;

        public CheckoutItemViewHolder(View view) {
            super(view);
            mView = view;
            foodItemName = (TextView)view.findViewById(R.id.food_item_name);
            foodItemVegType = (CircleView)view.findViewById(R.id.food_item_veg_type);
            addItemView = view.findViewById(R.id.food_item_add);
            removeItemView = view.findViewById(R.id.food_item_remove);
            binding = DataBindingUtil.bind(view);
            KeyboardHandler.setKeyboardHandler(view);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    public class CheckoutInstructionViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private ViewDataBinding binding;

        public CheckoutInstructionViewHolder(View view) {
            super(view);
            mView = view;
            binding = DataBindingUtil.bind(view);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }
}
