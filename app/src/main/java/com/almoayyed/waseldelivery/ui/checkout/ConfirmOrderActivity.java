package com.almoayyed.waseldelivery.ui.checkout;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.LocalPermissionConstants;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.ActivityConfirmOrderBinding;
import com.almoayyed.waseldelivery.databinding.BillDetailsBinding;
import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.Coupons;
import com.almoayyed.waseldelivery.models.DeliveryChargeInput;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseActivity;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.address.AddAddressActivity;
import com.almoayyed.waseldelivery.ui.special_order.ApplyCouponCodeActivity;
import com.almoayyed.waseldelivery.utils.Converters;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselStatusDialog;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.List;

import rx.Observable;
import rx.Observer;

/**
 * Created on 02 Jan 2017 11:19 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Displays confirmorder screen by allowing user to place order
 */

public class ConfirmOrderActivity extends BaseActivity{
    private ConfirmOrderAddressDisplayAdapter adapter;
    private int checkoutFlow;
    Cart cart;
    private int addressSelIndex = -1;
    private ActivityConfirmOrderBinding binding;
    private BillDetailsAdapter billDetailsAdapter;
    private ViewStub billDetailsViewStub;
    private static final String CART = "cart";

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_order);
        binding.setVariable(BR.confirmOrderHandler,clickListener);
        binding.backBtn.getDrawable().setAutoMirrored(true);

        checkoutFlow = getIntent().getExtras().getInt(CheckoutConstants.KEY_CHECKOUT_FLOW);
        binding.setCheckoutFlow(checkoutFlow);
        cart = WaselApplication.getCart();
        //Displaying corresponding layout
        switch (checkoutFlow){

            default:
            case CheckoutConstants.KEY_CHECKOUT_REPEAT_NORMAL_ORDER:
                cart.setOrderType(CheckoutConstants.CHECKOUT_NORMAL_ORDER);
                binding.setCart(cart);

                break;
            case CheckoutConstants.KEY_CHECKOUT_REPEAT_SPECIAL_ORDER:
            case CheckoutConstants.KEY_CHECKOUT_ORDER_ANY_THING:
                cart.setOrderType(CheckoutConstants.CHECKOUT_SPECIAL_ORDER);
                /*//Reading special order items  & displaying ordet anything header layout
                cart = (Cart)getIntent().getSerializableExtra(CheckoutConstants.KEY_DATA);
                binding.setCart(cart);*/
                break;
        }
        binding.setCart(cart);


        binding.viewpager.setPageMargin(Converters.pxToDp(this, 20f));

        binding.viewpager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override public void transformPage(View page, float position) {
                page.setAlpha(1.0f - Math.abs(position * 0.2f));
            }
        });
        mProgressBarHandler = new ProgressBarHandler(ConfirmOrderActivity.this, binding.confirmOrderLyt);

        billDetailsViewStub = this.binding.billDetails.getViewStub();

        //Loading address information from server
        /*fetchAddressFromServer();*/
        fetchDeliveryChargesFromServer();
    }

    //Update page background when page is selected by swiping/**
    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {

            if(adapter.getSelectAddressPos() != -1)
            {
                adapter.getChild(adapter.getSelectAddressPos()).updateSelectionState(false);
            }
            adapter.getChild(position).updateSelectionState(true);

            adapter.setSelectAddressPos(position);

            //Update delelivery charge on UI
            if(cart.getOrderType().equalsIgnoreCase(CheckoutConstants.CHECKOUT_NORMAL_ORDER)){
                cart.setDeliveryCharge(adapter.getSelectedAddress().getDeliveryCharge());
            }else {
                cart.setDeliveryCharge(2);
            }

            //Update delivery charge value on UI
            if(billDetailsAdapter!=null){
                billDetailsAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };


    @Override
    protected void onStart() {
        super.onStart();

        //Update the UI of billing details as databinding is not used
        updateBillingDetails();
    }

    private void addNewAddress() {
        Intent i = new Intent(ConfirmOrderActivity.this, AddAddressActivity.class);
        UserLocation mLocation = new UserLocation();
        mLocation.setLatitude(LocationPreference.getCurrentLat());
        mLocation.setLongitude(LocationPreference.getCurrentLng());
        StringBuffer currentLocName = new StringBuffer(LocationPreference.getCurrentLocName());
        currentLocName.replace(currentLocName.length()-2,currentLocName.length(),"");
        mLocation.setSubLocality3(currentLocName.toString());
        i.putExtra(CheckoutConstants.KEY_ADDRESS,mLocation);
        i.putExtra(ProfileConstants.FROM,ProfileConstants.FROM_ORDER);
        startActivity(i);
    }

    private void editCoupon() {
        switch (checkoutFlow){
            case CheckoutConstants.KEY_CHECKOUT_ORDER_ANY_THING:
            case CheckoutConstants.KEY_CHECKOUT_REPEAT_SPECIAL_ORDER:
                WaselToast.showToast(getString(R.string.coupon_not_applicable));
                break;
            case CheckoutConstants.KEY_CHECKOUT_CART_ITEMS:
            case CheckoutConstants.KEY_CHECKOUT_REPEAT_NORMAL_ORDER:
                Intent i = new Intent(ConfirmOrderActivity.this, ApplyCouponCodeActivity.class);
                i.putExtra(CheckoutConstants.KEY_COUPON_DATA, cart.getCoupon());
                startActivityForResult(i, LocalPermissionConstants.REQUEST_APPLY_COUPON);
                break;
        }
    }

    private void clickOnConfirmOrder() {
        if(adapter!=null && adapter.getCount() >0){
            if(adapter.getSelectAddressPos() == -1){
                WaselToast.showToast(getResources().getString(R.string.select_shipping_address));
                return;
            }
            //Setting shipping address for order delivery
            cart.setShippingAddress(adapter.getSelectedAddress());

            //Set payment mode & placing order
            BottomSheetDialogFragment bottomSheetDialogFragment = new PaymentModeDialogFragment();
            Bundle bundle =new Bundle();
            bundle.putSerializable(CheckoutConstants.KEY_DATA,cart);
            bundle.putInt(CheckoutConstants.KEY_CHECKOUT_FLOW,checkoutFlow);
            bottomSheetDialogFragment.setArguments(bundle);
            bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
        }else {
            WaselToast.showToast(getResources().getString(R.string.add_shipping_address));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK){
            switch (requestCode){
                case LocalPermissionConstants.REQUEST_APPLY_COUPON:
                    Coupons appliedCoupon = (Coupons) data.getSerializableExtra(CheckoutConstants.KEY_COUPON_DATA);
                    if(cart.setCoupon(appliedCoupon)){
                        WaselStatusDialog couponStatusDlg = new WaselStatusDialog(this);
                        couponStatusDlg.setStatusImage(R.drawable.tick_big);
                        couponStatusDlg.setStatusImageBackgroundColor(getResources().getColor(android.R.color.white));
                        couponStatusDlg.setStatusImageStroke(getResources().getColor(R.color.textColor7));
                        couponStatusDlg.setMessage(String.format(getResources().getString(R.string.coupon_applied_msg),
                                cart.getDiscountAmount()));
                        couponStatusDlg.showDialog();
                    }else {
                        final WaselDialog couponStatusDlg = new WaselDialog(this);
                        couponStatusDlg.setTitle(getResources().getString(R.string.app_name));
                        couponStatusDlg.setMessage(String.format(getResources().getString(R.string.unable_to_apply_coupon),
                                appliedCoupon.getMinOrderValue()));
                        couponStatusDlg.setNeutralButton(getResources().getString(R.string.ok),
                                new View.OnClickListener() {
                                    public void onClick(View view) {
                                        couponStatusDlg.cancel();
                                    }
                                });
                        couponStatusDlg.setNeutralBtnColor(android.R.color.white);
                        couponStatusDlg.show();
                    }
                    break;
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(intent.getExtras().containsKey(CheckoutConstants.KEY_ADDRESS)){
            /*UserLoginDetails userLoginDetails = (UserLoginDetails) intent.getExtras().getSerializable(CheckoutConstants.KEY_ADDRESS);
            updateAddressAdapter(userLoginDetails);*/
            //Recalculate the delivery charges
            fetchDeliveryChargesFromServer();
        }
    }

    private void fetchDeliveryChargesFromServer(){
        DeliveryChargeInput data = new DeliveryChargeInput();
        data.setUserId(LoginCredentialsPreference.getLoginPreference().getId());
        if(cart.getOrderType().equalsIgnoreCase(CheckoutConstants.CHECKOUT_NORMAL_ORDER)){
            data.setLatitude(cart.getOutlet().getAddress().getLatitude());
            data.setLongitude(cart.getOutlet().getAddress().getLongitude());
        }else {
            data.setLatitude(cart.getLatitude());
            data.setLongitude(cart.getLongitude());
        }
        Observable<List<Address>> getDeliveryChargeObservable = WaselDeliveryService.getService().getDeliveryCharges(data);
        addObserver(getDeliveryChargeObservable,getDeliveryChargeObserver);
    }

    //Makes api to fetch address info from server
    private void fetchAddressFromServer(){
        Observable<UserLoginDetails> getAddressObservable = WaselDeliveryService.getService().getProfile(LoginCredentialsPreference.getLoginPreference().getId());
        addObserver(getAddressObservable,getAddressesObserver);
    }

    /*
       * Observer for listening delivery charges list
       * */
    Observer<List<Address>> getDeliveryChargeObserver = new Observer<List<Address>>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
            clearSubscriptions();
        }

        @Override
        public void onError(Throwable e) {
            clearSubscriptions();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
        }

        @Override
        public void onNext(List<Address> addresses) {
            //Successfully received delivery charges.
            //Successfully received address list
            updateAddressAdapter(addresses);
        }
    };

    /*
       * Observer for listening address list
       * */
    Observer<UserLoginDetails> getAddressesObserver = new Observer<UserLoginDetails>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
            clearSubscriptions();
        }

        @Override
        public void onError(Throwable e) {
            clearSubscriptions();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
        }

        @Override
        public void onNext(UserLoginDetails profileDetails) {
            //Successfully received address list
            updateAddressAdapter(profileDetails.getAddress());
        }
    };

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null){
            addressSelIndex = savedInstanceState.getInt("selectAddressPos");
            cart = (Cart)savedInstanceState.getSerializable(CART);
            if(adapter != null){
                binding.viewpager.setCurrentItem(addressSelIndex);
                pageChangeListener.onPageSelected(addressSelIndex);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("selectAddressPos",adapter==null?-1:adapter.getSelectAddressPos());
        outState.putSerializable(CART,cart);
    }

    //Updates the addresses on UI
    private void updateAddressAdapter(List<Address> addresses) {
        binding.viewpager.removeAllViews();

        // Pass results to ViewPagerAdapter Class
        adapter = new ConfirmOrderAddressDisplayAdapter(getSupportFragmentManager(),pageClickListener);
        int prevSel = addressSelIndex;
        addressSelIndex = 0;
        for(int i=0; i < addresses.size(); i++){
            ConfirmOrderAddressFragment fragment = new ConfirmOrderAddressFragment();
            Bundle bundle = new Bundle();
            Address address = addresses.get(i);
            bundle.putSerializable(CheckoutConstants.KEY_ADDRESS, address);
            bundle.putInt(CheckoutConstants.KEY_ADDRESS_PG_NO, i);

            //finding the index of the last used address
            addressSelIndex = address.isCastOff()? i : addressSelIndex;

            fragment.setArguments(bundle);
            fragment.setAddress(address);
            adapter.addFragment(fragment);
        }

        // Binds the Adapter to the ViewPager
        binding.viewpager.setAdapter(adapter);

        addressSelIndex = prevSel==-1? addressSelIndex : prevSel;


        //Current highlighted page is selected
        binding.viewpager.addOnPageChangeListener(pageChangeListener);

        //Selecting last used address otherwise selects first one.
        binding.viewpager.setCurrentItem(addressSelIndex);
        pageChangeListener.onPageSelected(addressSelIndex);
    }

    View.OnClickListener pageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pos = Integer.valueOf(v.getTag().toString());
            pageChangeListener.onPageSelected(pos);
            binding.viewpager.setCurrentItem(pos);
        }
    };

    private void displayBillDetailsView(boolean isViewed) {

/*        if(true) {



            Animation animation = AnimationUtils.loadAnimation(this, isViewed? R.anim.slide_up:R.anim.slide_up);
            findViewById(R.id.bill_details_layout).startAnimation(animation);


            return;
        }*/

        if (isViewed) {
            //Display Bill Detailsview at bottom
            binding.billDetails.setOnInflateListener(new ViewStub.OnInflateListener() {
                @Override
                public void onInflate(ViewStub stub, View inflated) {
                    BillDetailsBinding binding = DataBindingUtil.bind(inflated);
                    binding.setConfirmOrderHandler(onClickBillDetails);

                    if(billDetailsAdapter==null){
                        billDetailsAdapter =
                                new BillDetailsAdapter(ConfirmOrderActivity.this,cart);
                    }
                    binding.billDetailsList.setAdapter(billDetailsAdapter);
                }
            });
            if (!this.binding.billDetails.isInflated()) {
                billDetailsViewStub.inflate();
            }else {
                billDetailsViewStub.setVisibility(View.VISIBLE);
            }

           /* Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up);
            viewById.startAnimation(animation);*/

            /*View viewById = findViewById(R.id.bill_details_layout);
            ObjectAnimator axisY = ObjectAnimator.ofFloat(viewById,"translationY",-viewById.getHeight(), viewById.getHeight());
            axisY.setDuration(500).start();*/

            binding.grandTotalLyt.setVisibility(View.GONE);
        } else {
            billDetailsViewStub.setVisibility(View.GONE);
            binding.grandTotalLyt.setVisibility(View.VISIBLE);
            /*View viewById = findViewById(R.id.bill_details_layout);
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_down);
            viewById.startAnimation(animation);*/
        }
    }
    public View.OnClickListener onClickBillDetails = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            displayBillDetailsView(false);
        }
    };

    public View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v.getId() == binding.backBtn.getId()){
                clearCoupon();
                finish();
            }else if(v.getId() == binding.grandTotalLyt.getId()){
                displayBillDetailsView(true);
            }else if(v.getId() == binding.confirmOrder.getId()){
                clickOnConfirmOrder();
            }else if(v.getId() == binding.couponCode.getId() ||
                    v.getId() == binding.couponCodeClearLyt.getId()||
                    v.getId() == binding.couponCodeTitle.getId() ||
                    v.getId() == binding.couponCodeText.getId()){
                editCoupon();
            }else if(v.getId() == binding.addNewAddress.getId()){
                addNewAddress();
            }else if(v.getId() == binding.clearCouponBtn.getId()){
                clearCoupon();
                updateBillingDetails();
            }

        }
    };

    private void updateBillingDetails() {
        if(billDetailsAdapter!=null){
            billDetailsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onBackPressed() {
        if(binding.grandTotalLyt.getVisibility() == View.GONE){
            displayBillDetailsView(false);
        }else {
            super.onBackPressed();
            clearCoupon();
        }
    }

    private void clearCoupon() {
        if(cart!= null){
            cart.clearCoupon();
        }
    }

    @Override
    protected void onDestroy() {
        binding.viewpager.setAdapter(null);
        super.onDestroy();
    }
}
