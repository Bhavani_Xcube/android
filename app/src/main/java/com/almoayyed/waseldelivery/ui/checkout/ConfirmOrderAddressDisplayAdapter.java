package com.almoayyed.waseldelivery.ui.checkout;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.ui.home.OutletDetails.SelectOutletItemsFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 28 Nov 2016 11:19 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Displays address in confirm order page
 */

public class ConfirmOrderAddressDisplayAdapter extends FragmentStatePagerAdapter {

    private final List<ConfirmOrderAddressFragment> mFragmentList = new ArrayList<>();
    private View.OnClickListener pageClickListener;
    private int selectAddressPos = -1; //Indicates the index of selected addres index


    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public ConfirmOrderAddressDisplayAdapter(FragmentManager manager, View.OnClickListener pageClickListener) {
        super(manager);
        this.pageClickListener = pageClickListener;
    }

    @Override
    public Fragment getItem(int position) {


        ConfirmOrderAddressFragment fragment = mFragmentList.get(position);
        fragment.setClickListener(position,pageClickListener);

        return fragment;
    }

    public void addFragment(ConfirmOrderAddressFragment fragment) {
        mFragmentList.add(fragment);
    }

    public void setPageClickListener(View.OnClickListener pageClickListener) {
        this.pageClickListener = pageClickListener;
    }

    //Updates the outer background color
    public void updateSelectedAddress(int position, boolean state) {

        mFragmentList.get(position).updateSelectionState(state);
    }

    public ConfirmOrderAddressFragment getChild(int position) {

       return mFragmentList.get(position);
    }

    public int getSelectAddressPos() {
        return selectAddressPos;
    }

    public void setSelectAddressPos(int selectAddressPos) {
        this.selectAddressPos = selectAddressPos;
    }


    //Returns the selected address
    public Address getSelectedAddress() {
        return mFragmentList.get(selectAddressPos).getAddress();
    }

    //Returns the set of user addresses
    public ArrayList<Address> getAddresses() {
        ArrayList<Address> addresses = new ArrayList<>();
        for (ConfirmOrderAddressFragment addressFragment : mFragmentList){
            addresses.add(addressFragment.getAddress());
        }
        return addresses;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}