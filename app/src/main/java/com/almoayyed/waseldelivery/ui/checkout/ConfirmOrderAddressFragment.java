package com.almoayyed.waseldelivery.ui.checkout;


import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.databinding.ConfirmOrderAddressLayoutBinding;
import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.ui.BaseFragment;

/**
 * Created on 28 Nov 2016 6:47 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class ConfirmOrderAddressFragment extends BaseFragment{

    private Address address;
    private int pgNo;
    private View.OnClickListener itemClickListener;
    private SelectionStatus selectionStatus = new SelectionStatus();
    ConfirmOrderAddressLayoutBinding binding;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(fragView == null){
            binding = DataBindingUtil.inflate(
                    inflater, R.layout.confirm_order_address_layout, container, false);
            fragView = binding.getRoot();
            //Fetching the amenity's(category) id, used to load outlets under this amenity from server.
            Bundle data = getArguments();
            if(data!=null){
                address = data.containsKey(CheckoutConstants.KEY_ADDRESS)?(Address)data.getSerializable(CheckoutConstants.KEY_ADDRESS):null;
                binding.setAddress(address);
                binding.setSelection(selectionStatus);
                binding.addressSelectBg.setTag(pgNo);
                binding.addressSelectBg.setOnClickListener(itemClickListener);
                /*updateSelectionState(selected);*/
            }
        }
        return fragView;
    }

    public void setClickListener(int pageNo, View.OnClickListener clickListener){
        pgNo = pageNo;
        itemClickListener = clickListener;
    }

   /* @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState!=null){
            address = (Address) savedInstanceState.getSerializable("address");
            pgNo = savedInstanceState.getInt("pgNo");
            selectionStatus.setState(savedInstanceState.getBoolean("selState"));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("address",address);
        outState.putInt("pgNo",pgNo);
        outState.putBoolean("selState",selectionStatus.isState());
    }*/

    public void updateSelectionState(final boolean selected){
        selectionStatus.setState(selected);

        /*binding.addressSelectImg.setVisibility(selected?View.VISIBLE:View.INVISIBLE);
        binding.addressSelectBg.setChecked(selected);*/
    }

    public Address getAddress(){
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public class SelectionStatus extends BaseObservable{
        boolean state;

        @Bindable
        public boolean isState() {
            return state;
        }

        public void setState(boolean state) {
            this.state = state;
            notifyPropertyChanged(BR.state);
        }
    }

}
