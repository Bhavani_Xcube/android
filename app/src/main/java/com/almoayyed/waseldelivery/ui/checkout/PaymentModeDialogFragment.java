package com.almoayyed.waseldelivery.ui.checkout;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.PaymentModeBinding;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.models.OutletItem;
import com.almoayyed.waseldelivery.models.OutletItemCustomization;
import com.almoayyed.waseldelivery.network_interface.NetworkStatusException;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.history.OrderDetailsActivity;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselProgressDialog;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created on 15 Nov 2016 12:50 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Display bottom sheet with slide animation
 */
public class PaymentModeDialogFragment extends BottomSheetDialogFragment{
    private static final long ORDER_PLACED_TIME_OUT = 1000; //Order placed successfully message is displayed for 1sec
    private static final String REMOVED_ITEMS = "removedItems";
    private final String PAYMENT_MODE_CARD = "CARD";
    private final String PAYMENT_MODE_CASH = "CASH";

    WaselProgressDialog progressDialog;
    private View contentView;
    private Observable<Order> placeOrderObservable;
    private Subscription placeOrderSubscribe;
    CompositeSubscription startStopCompositeSubscription = new CompositeSubscription();
    Cart cart;
    private int checkoutFlow;
    private PaymentModeBinding binding;
    private Resources resources;
    private List<OutletItem> removedItems = new ArrayList<>();

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.payment_mode, null, false);
        binding.setPaymentHandler(this);
        contentView = binding.getRoot();
        dialog.setContentView(contentView);
        initView();
    }

    private void initView() {
        progressDialog =  new WaselProgressDialog(getContext());

        resources = getResources();
        cart =(Cart) getArguments().getSerializable(CheckoutConstants.KEY_DATA);
        checkoutFlow = (int) getArguments().get(CheckoutConstants.KEY_CHECKOUT_FLOW);

        GradientDrawable gd = (GradientDrawable)binding.orderPlacedImg.getBackground();
        gd.setStroke(2, resources.getColor(R.color.textColor2));


        //Assigning data to payment modes
        binding.paymentCashType.paymentModeTypeLayout.setOnClickListener(onPaymentModeChangeListener);
        binding.paymentCardType.paymentModeTypeLayout.setOnClickListener(onPaymentModeChangeListener);
        binding.paymentCashType.paymentText.setText(getString(R.string.cash));
        binding.paymentCashType.paymentSubtext.setText(getString(R.string.cash_substring));
        binding.paymentCardType.paymentText.setText(getString(R.string.card));
        binding.paymentCardType.paymentSubtext.setText(getString(R.string.card_substring));
        binding.paymentCashType.paymentModeTypeLayout.performClick();

        showOrderPlacedLyt(false);
    }

    public View.OnClickListener onDoneBtnClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //TODO: Temp commented as outlet info is not completely shared by server(while repeating order)
            if(cart.getOrderType().equalsIgnoreCase(CheckoutConstants.CHECKOUT_NORMAL_ORDER)
                    && !cart.getOutlet().isOutletAvailable()){
                WaselToast.showToast(getString(R.string.order_placing_for_closed_outlet));
                return;
            }
            makePayment();
        }
    };

    private void makePayment() {
        progressDialog.show();

        setCancelable(false);
        Order order = cart.convertToOrder();
        placeOrderObservable = WaselDeliveryService.getService().confirmOrder(order);
        placeOrderSubscribe = placeOrderObservable.subscribe(placeOrderObserver);
        startStopCompositeSubscription.clear();
        startStopCompositeSubscription.add(placeOrderSubscribe);
    }

    View.OnClickListener onPaymentModeChangeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.payment_card_type:
                    cart.setPaymentType(PAYMENT_MODE_CARD);
                    updatePaymentModeUI(false);
                    break;
                default:
                    cart.setPaymentType(PAYMENT_MODE_CASH);
                    updatePaymentModeUI(true);
                    break;
            }
        }
    };

    private void updatePaymentModeUI(boolean isPaymentCashType) {
        if(isPaymentCashType){
            binding.paymentCashType.paymentModeImg.setChecked(true);
            binding.paymentCashType.paymentText.setTextColor(resources.getColor(R.color.bg));
            binding.paymentCashType.paymentSubtext.setAlpha(1);
            binding.paymentCardType.paymentModeImg.setChecked(false);
            binding.paymentCardType.paymentText.setTextColor(resources.getColor(R.color.textColor12));
            binding.paymentCardType.paymentSubtext.setAlpha((float)0.5);
        }else {
            binding.paymentCardType.paymentModeImg.setChecked(true);
            binding.paymentCardType.paymentText.setTextColor(resources.getColor(R.color.bg));
            binding.paymentCardType.paymentSubtext.setAlpha(1);
            binding.paymentCashType.paymentModeImg.setChecked(false);
            binding.paymentCashType.paymentText.setTextColor(resources.getColor(R.color.textColor12));
            binding.paymentCashType.paymentSubtext.setAlpha((float)0.5);
        }
    }

    /*
            * Observer for placing new order api call
            * */
    Observer<Order> placeOrderObserver = new Observer<Order>() {

        @Override
        public void onCompleted() {
            progressDialog.dismiss();
            setCancelable(true);
        }

        @Override
        public void onError(Throwable e) {
            progressDialog.dismiss();
            setCancelable(true);
            if(e instanceof NetworkStatusException){
                WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
            }else if(e instanceof HttpException && ((HttpException) e).code() == 303){
                try{
                    final WaselDialog dialog = new WaselDialog(getContext());
                    dialog.setTitle(resources.getString(R.string.app_name));

                    if (((HttpException) e).response() != null && ((HttpException) e).response().errorBody() != null) {
                        JSONObject jsonObject = new JSONObject(((HttpException) e).response().errorBody().string());
//                        JSONObject jsonObject = new JSONObject(String.valueOf(((HttpException) e).response().errorBody()));
                        final JSONArray jsonArray = jsonObject.getJSONArray(REMOVED_ITEMS);
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                OutletItem outletItem = new OutletItem();
                                JSONObject jObject = jsonArray.getJSONObject(i);
                                outletItem.setId(jObject.getString("id"));
                                outletItem.setName(jObject.getString("name"));
                                outletItem.setItemDescription(jObject.getString("description"));
                                outletItem.setPrice(Float.parseFloat(jObject.getString("price")));
                                outletItem.setActive(jObject.getBoolean("active"));
                                outletItem.setDelete(jObject.getBoolean("delete"));

                                removedItems.add(outletItem);
                            }
                        }
                        if (cart.getItems().size() == removedItems.size()) {
                            dialog.setMessage(resources.getString(R.string.all_items_not_available));
                            dialog.setNeutralButton(resources.getString(R.string.ok),
                                    new View.OnClickListener() {
                                        public void onClick(View view) {
                                            dialog.cancel();
                                            cart.clear();
                                            navigateToHome();
                                        }
                                    });
                            dialog.setNeutralBtnColor(android.R.color.white);
                            dialog.show();
                        } else {

                            dialog.setMessage(resources.getString(R.string.few_items_not_available));
                            dialog.setPositiveButton(resources.getString(R.string.yes),
                                    new View.OnClickListener() {
                                        public void onClick(View view) {
                                            dialog.cancel();

//                                            List<Integer> removedIds = new ArrayList<>();
                                            for (int j = 0; j < removedItems.size(); j++) {
                                                if (!removedItems.get(j).isActive() || removedItems.get(j).isDelete()) {
//                                                    removedIds.add(j);
                                                    cart.removeItem(removedItems.get(j));

                                                    /*//Remove item from cart
                                                    OutletItem outletItem = removedItems.get(j);
                                                    if(outletItem.getCartQuantity()>0){
                                                        cart.removeItem(j);
                                                        checkoutAdapter.notifyDataSetChanged();
                                                    }*/
                                                }
                                            }
                                            makePayment() ;
                                         /*   for (int k = 0; k < cart.getItems().size(); k++) {
                                                if(removedIds.size()>0) {
                                                    if (cart.getItems().contains(removedIds.get(k))) {
                                                        cart.removeItem(k);
                                                    }
                                                }
                                            }

                                            for (int l = 0; l < removedItems.size(); l++) {
                                                removedItems.remove(l);
                                            }*/

                                          /*  if (removedItems.size() != 0) {
                                                try {
                                                    JSONArray cusArr = jsonArray.getJSONObject(0).getJSONArray("customizedItems");
                                                    List<Integer> isInActiveIds = new ArrayList<>();
                                                    OutletItemCustomization couOutletItemCustomization = new OutletItemCustomization();

                                                    for (int m = 0; m < cusArr.length(); m++) {
                                                        if (!removedItems.get(m).isActive() || removedItems.get(m).isDelete()) {
                                                            JSONObject jbObject = cusArr.getJSONObject(m);
                                                            couOutletItemCustomization.setId(jbObject.getString("id"));
                                                            isInActiveIds.add(Integer.parseInt(couOutletItemCustomization.getId()));
                                                        }
                                                    }

                                                    couOutletItemCustomization.setQuantity(0);
                                                    for (int n = 0; n < cart.getItems().size(); n++) {
                                                        for (int o = 0; o < cart.getItems().get(n).getCustomizedItems().size(); o++) {
                                                            if (isInActiveIds.contains(Integer.parseInt(cart.getItems().get(n).getCustomizedItems().get(o).getId()))) {
                                                                cart.getItems().get(n).getCustomizedItems().get(o).setQuantity(0);
                                                            }
                                                        }
                                                    }

                                                } catch (Exception e) {

                                                }
                                            }*/
                                        }
                                    });
                            dialog.setNegativeButton(resources.getString(R.string.no),
                                    new View.OnClickListener() {
                                        public void onClick(View view) {
                                            dialog.cancel();
                                            cart.clear();
                                            navigateToHome();
                                        }
                                    });
                            dialog.setPositiveBtnColor(R.color.bg);
                            dialog.setNegetiveBtnColor(android.R.color.white);
                            dialog.show();

                        }
                    }
                }catch (Exception ex){
                    ParseServerErrorMessage.getServerErrorMessage(e);
                }

            }else{
                WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
            }

        }

        @Override
        public void onNext(final Order order) {
            //Clear cart
            WaselApplication.getCart().clear();
            //Order successfully placed
            //Navigate to order tacking screen with some time lapse
            progressDialog.hide();
            showOrderPlacedLyt(true);

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    navigateToOrderDetails(order);
                }
            };
            Handler handler = new Handler();
            handler.postDelayed(runnable, ORDER_PLACED_TIME_OUT);

        }
    };

    private void navigateToHome() {
        Intent i = new Intent(getActivity(), MainActivity.class);
        i.putExtra(CheckoutConstants.KEY_ITEMS_UPDATED_FROM_SERVER, true);
//        i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW, checkoutFlow);
        startActivity(i);
        getActivity().finish();
    }

    private void showOrderPlacedLyt(boolean isOrderPlaced) {
        binding.paymentModeLayout.setVisibility(isOrderPlaced?View.GONE: View.VISIBLE);

        GradientDrawable gd = (GradientDrawable)binding.orderPlacedImg.getBackground();
        gd.setColor(resources.getColor(android.R.color.white));
        gd.setStroke(1, resources.getColor(R.color.textColor7));
        binding.orderPlacedLayout.setVisibility(isOrderPlaced?View.VISIBLE: View.GONE);
    }

    private void navigateToOrderDetails(Order order) {
        Intent i = new Intent(getActivity(), OrderDetailsActivity.class);
        i.putExtra(CheckoutConstants.KEY_DATA,order);
        i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,checkoutFlow);
        startActivity(i);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        startStopCompositeSubscription.clear();
    }
}