package com.almoayyed.waseldelivery.ui.feedback;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioGroup;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.databinding.ActivityFeedbackBinding;
import com.almoayyed.waseldelivery.models.Feedback;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseActivity;
import com.almoayyed.waseldelivery.ui.home.Outlets.CategoryViewPagerAdapter;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 19 Dec 2016 02:19 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class FeedbackActivity extends BaseActivity{

    private FeedbackViewPagerAdapter adapter;
    ActivityFeedbackBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_feedback);
        if(getIntent() != null){
            List<Order> feedbackOrders;
            feedbackOrders = getIntent().hasExtra(CheckoutConstants.KEY_FEEDBACK_DATA)?
                    (ArrayList<Order>)getIntent().getSerializableExtra(CheckoutConstants.KEY_FEEDBACK_DATA)
                    :null;
            if(feedbackOrders == null || feedbackOrders.size()==0){
                finish();
            }else {
                adapter = new FeedbackViewPagerAdapter(getSupportFragmentManager());
                for(Order feedbackOrder : feedbackOrders){
                    FeedbackFragment feedbackFragment = new FeedbackFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(CheckoutConstants.KEY_FEEDBACK_DATA, feedbackOrder);
                    feedbackFragment.setArguments(bundle);
                    adapter.addFragment(feedbackFragment,feedbackOrder);
                }
                binding.feedbackPager.setAdapter(adapter);
                binding.feedbackIndicator.updateCount(adapter.getCount());
                binding.feedbackPager.setOnPageChangeListener(pageChangeListener);
                updateIndicatorVisibility();
            }
        }
    }

    public ProgressBarHandler getProgressHandler(){
        if(mProgressBarHandler == null){
            mProgressBarHandler = new ProgressBarHandler(this,binding.feedbackLayout);
        }
        return mProgressBarHandler;
    }

    private void updateIndicatorVisibility() {
        if(adapter.getCount()<=1){
            binding.feedbackIndicator.setVisibility(View.INVISIBLE);
        }else {
            binding.feedbackIndicator.setVisibility(View.VISIBLE);
        }
    }

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            binding.feedbackIndicator.setSelection(binding.feedbackPager.getCurrentItem());
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public void removeFeedbackFromUI(String orderId){
        if(adapter.getCount()==1){
            finish();
            return;
        }

        adapter.removeByOrderId(orderId);
        binding.feedbackPager.refreshDrawableState();

        //Set the feedback indicator count & its selection position
        binding.feedbackIndicator.removeAtPos(adapter.getCount());
        binding.feedbackIndicator.setSelection(binding.feedbackPager.getCurrentItem());

        updateIndicatorVisibility();
    }

    @Override
    public void onBackPressed() {
        //Move back to background
        Intent i = new Intent();
        i.setAction(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        this.startActivity(i);
    }

}
