package com.almoayyed.waseldelivery.ui.feedback;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.OrderConstants;
import com.almoayyed.waseldelivery.databinding.FeedbackLayoutBinding;
import com.almoayyed.waseldelivery.models.Feedback;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 02 Jan 2016 05:40 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class FeedbackFragment extends BaseFragment{

    private FeedbackLayoutBinding binding;
    private Order feedbackOrder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(fragView == null){
            binding = DataBindingUtil.inflate(inflater,R.layout.feedback_layout, container, false);
            fragView = binding.getRoot();
            binding.setFeedbackHandler(onSubmitClickListener);
            mProgressBarHandler = ((FeedbackActivity)getActivity()).getProgressHandler();
            if(getArguments() != null){
                feedbackOrder = getArguments().containsKey(CheckoutConstants.KEY_FEEDBACK_DATA)?
                        (Order)getArguments().getSerializable(CheckoutConstants.KEY_FEEDBACK_DATA)
                        :null;
            }
        }
       /* binding.feedbackComments.setMovementMethod(new ScrollingMovementMethod());
            //to stop parent linearlayout scrollview when touched on textview
        binding.feedbackComments.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });*/
        clearFeedbackUI();
        return fragView;
    }

    private void clearFeedbackUI() {
        binding.feedbackDeliveryRating.clearCheck();
        binding.feedbackProductRating.setRating(0);
        if(CheckoutConstants.CHECKOUT_SPECIAL_ORDER.equalsIgnoreCase(feedbackOrder.getOrderType())){
            if(feedbackOrder.getItems().size()>1){
                binding.feedbackProductTitle.setText(String.format(getString(R.string.sp_product_feedback_title), getString(R.string.product), feedbackOrder.getPickUpLocation()));
            }else {
                binding.feedbackProductTitle.setText(String.format(getString(R.string.sp_product_feedback_title), getString(R.string.products), feedbackOrder.getPickUpLocation()));
            }
        }else {
            if(feedbackOrder.getItems().size()>1){
                binding.feedbackProductTitle.setText(String.format(getString(R.string.product_feedback_title), getString(R.string.product), feedbackOrder.getOutlet().getName()));
            }else {
                binding.feedbackProductTitle.setText(String.format(getString(R.string.product_feedback_title), getString(R.string.products), feedbackOrder.getOutlet().getName()));
            }
        }
        binding.feedbackComments.setText("");
    }

    public View.OnClickListener onSubmitClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(binding.feedbackDeliveryRating.getCheckedRadioButtonId() == -1 && binding.feedbackProductRating.getRating() == 0){
                WaselToast.showToast(getString(R.string.rate_your_experience));
            }else if(binding.feedbackDeliveryRating.getCheckedRadioButtonId() == -1){
                WaselToast.showToast(getString(R.string.empty_delivery_feedback));
            }else if(binding.feedbackProductRating.getRating() == 0){
                WaselToast.showToast(getString(R.string.empty_product_feedback));
            }else {
                Feedback feedback = new Feedback();
                feedback.setOrderId(feedbackOrder.getId());
                int checkedRadioButtonId = binding.feedbackDeliveryRating.getCheckedRadioButtonId();
                feedback.setDeliveryRating(Float.parseFloat(binding.feedbackDeliveryRating.findViewById(checkedRadioButtonId).getTag().toString()));
                feedback.setRating(binding.feedbackProductRating.getRating());
                feedback.setComment(binding.feedbackComments.getText().toString());

                ArrayList<Feedback> reqFeedbackObj = new ArrayList<>();
                reqFeedbackObj.add(feedback);
                Observable<Response> feedbackObservable = WaselDeliveryService.getService().setOrderFeedback(reqFeedbackObj);
                addObserver(feedbackObservable, setOrderFeedbackObserver);
            }
        }
    };

    /*
    * Observer for setting feedback
    * */
    public Observer<Response> setOrderFeedbackObserver = new Observer<Response>() {

        @Override
        public void onCompleted() {
            clearSubscriptions();
        }

        @Override
        public void onError(Throwable e) {
            WaselToast.showToast(getServerErrorMessage(e));
            clearSubscriptions();
        }

        @Override
        public void onNext(Response response) {
            if(response!=null) {
                /*//Show the response status
                WaselToast.showToast(response.getMessage());*/

                clearFeedbackUI();

                //Feedback updated successfully. Clear this page
                ((FeedbackActivity)getActivity()).removeFeedbackFromUI(feedbackOrder.getId());

            }
        }
    };
}
