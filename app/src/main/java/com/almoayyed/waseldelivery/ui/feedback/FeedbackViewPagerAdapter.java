package com.almoayyed.waseldelivery.ui.feedback;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import com.almoayyed.waseldelivery.models.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 02 Jan 2017 7:30 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */


/** An adapter for creating categories list like food, electronics, jewellery etc
 **/
public class FeedbackViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<FeedbackFragment> mFragmentList = new ArrayList<>();
    private final List<Order> orders = new ArrayList<>();

    public FeedbackViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public int getItemPosition(Object object){
        return PagerAdapter.POSITION_NONE;
    }

    public void addFragment(FeedbackFragment fragment, Order order) {
        mFragmentList.add(fragment);
        orders.add(order);
    }

    public int removeByOrderId(String orderId){
        for(int index=0; index<orders.size(); index++){
            if (orders.get(index).getId().equalsIgnoreCase(orderId)){
                orders.remove(index);
                mFragmentList.remove(index);
                notifyDataSetChanged();
                return index;
            }
        }
        return -1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

}