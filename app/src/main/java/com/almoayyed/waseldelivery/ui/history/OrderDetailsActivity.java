package com.almoayyed.waseldelivery.ui.history;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.PushNotificationConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.OrderDetailsBinding;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseActivity;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.checkout.CheckoutActivity;
import com.almoayyed.waseldelivery.ui.checkout.ConfirmOrderActivity;
import com.almoayyed.waseldelivery.utils.Converters;
import com.almoayyed.waseldelivery.utils.HandleWebClientLinks;
import com.almoayyed.waseldelivery.utils.NetworkConnection;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.utils.ScreenUtils;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.views.WaselToast;

import rx.Observable;
import rx.Observer;

/**
 * Created on 28 Nov 2016 3:22 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OrderDetailsActivity extends BaseActivity {

    private Order order;
    private OrderDetailsAdapter orderDetailsAdapter;
    private LinearLayoutManager linearLayoutManager;
    private OrderDetailsBinding binding;
    private View rootView;
    private int checkoutFlow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.order_details);
        binding.setClickHandler(backPressListener);
        rootView = binding.getRoot();
        initViews(rootView);

        IntentFilter filter = new IntentFilter();
        filter.addAction(PushNotificationConstants.orderStatusFilter);
        registerReceiver(receivedFromPush, filter);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        updateOrderDetails();
    }

    private void initViews(View v) {
        mProgressBarHandler = new ProgressBarHandler(this, binding.detailsMainLyt);

        binding.backBtn.getDrawable().setAutoMirrored(true);
        //TODO: load driver tracing data
        //Show proress while loading link
        binding.orderTracking.setWebViewClient(new HelloWebViewClient());
        binding.orderTracking.getSettings().setJavaScriptEnabled(true);
        binding.orderTracking.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBarHandler.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBarHandler.hide();
            }
        });

        updateOrderDetails();
    }

    private void updateOrderDetails() {
        //load order details (raised when click on push notification)
        Intent intent = getIntent();
        checkoutFlow = intent.hasExtra(CheckoutConstants.KEY_CHECKOUT_FLOW) ?
                intent.getIntExtra(CheckoutConstants.KEY_CHECKOUT_FLOW, CheckoutConstants.KEY_CHECKOUT_CART_ITEMS) :
                CheckoutConstants.KEY_CHECKOUT_CART_ITEMS;
        String orderId;
        switch (checkoutFlow) {
            case CheckoutConstants.KEY_CHECKOUT_CLICK_PUSH:
                //Read order id & fetch order info
                orderId = intent.getIntExtra(CheckoutConstants.KEY_ORDER_ID, 0) + "";
                break;
            default:
                order = (Order) intent.getSerializableExtra(CheckoutConstants.KEY_DATA);
                orderId = order.getId();
                break;
        }
        binding.titleText.setText(String.format(getString(R.string.order_id_short_format), orderId));
        Observable<Order> orderDetailsObservable = WaselDeliveryService.getService().getOrderDetails(orderId);
        addObserver(orderDetailsObservable, orderDetailsObserver);
    }

    private void updateUI(Order order) {
        this.order = order;
        order.updateOrderItemDetails();
        binding.setOrder(order);

        linearLayoutManager = new LinearLayoutManager(this);
        binding.orderDetailsList.setLayoutManager(linearLayoutManager);

        orderDetailsAdapter = new OrderDetailsAdapter(getBaseContext(), order);
        binding.orderDetailsList.setAdapter(orderDetailsAdapter);
        orderDetailsAdapter.setRepeatOrderClickListener(repeatOrderClickListener);

        /*binding.orderTracking.setWebChromeClient(new WebChromeClient(){

            @Override
            public boolean onJsBeforeUnload(WebView view, String url, String message, JsResult result) {
                view.loadUrl("about:blank");
                return super.onJsBeforeUnload(view, url, message, result);
            }
        });*/
        updateTrackingDetails(order);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(receivedFromPush);
        } catch (IllegalArgumentException e) {
            if (e.getMessage().contains("Receiver not registered")) {
                Loggers.info("Tried to unregister the reciver when it's not registered");
            }
            else
            {
                throw e;
            }
        }
    }

    private void updateTrackingDetails(Order order) {
        binding.orderTracking.getSettings().setJavaScriptEnabled(true);
        binding.orderTracking.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                return HandleWebClientLinks.handleLink(OrderDetailsActivity.this, url)? true: super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    // Do something for lollipop and above versions
                    String url = request.getUrl().toString();
                    return HandleWebClientLinks.handleLink(OrderDetailsActivity.this, url);
                } else{
                    // do something for phones running an SDK before lollipop
                    return super.shouldOverrideUrlLoading(view, request);
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBarHandler.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBarHandler.hide();
            }
        });


        if(order.isOrderCompleted()){
            /*CoordinatorLayout coordinatorLayout = (CoordinatorLayout)binding.detailsCoordinateLyt;*/
            // The View with the BottomSheetBehavior
            View bottomSheet = binding.detailsBottomSheet;
            ScreenUtils screenUtils = new ScreenUtils(OrderDetailsActivity.this);
            int peekHt = screenUtils.getHeight() - binding.header.getHeight() - getStatusBarHeight();
            BottomSheetBehavior<View> behavior = BottomSheetBehavior.from(bottomSheet);

            behavior.setPeekHeight(peekHt); //behavior.getPeekHeight() + binding.orderTracking.getMeasuredHeight()
        }else {
            if (order.isOrderOnTheWay()) {
                binding.orderTracking.loadUrl(order.getDeliveryTracingLink());
            } else {
                binding.orderTracking.loadUrl(order.getPickupTrackingLink());
            }

            if(order.isDriverAccepted()){
                //update map
                binding.orderTracking.reload();
            }
        }
    }

    public View.OnClickListener backPressListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            closeDetailsPage();
        }
    };

    private void closeDetailsPage() {
        Intent i = new Intent(OrderDetailsActivity.this, MainActivity.class);
        i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW_DONE, true);
        i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW, checkoutFlow);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        switch (checkoutFlow) {
            case CheckoutConstants.KEY_CHECKOUT_CLICK_PUSH:
                finish();
                break;
            default:
                closeDetailsPage();
                break;
        }
    }

    /*
     * Observer for fetching Orders details
     * */
    Observer<Order> orderDetailsObserver = new Observer<Order>() {

        @Override
        public void onCompleted() {
            clearSubscriptions();
        }

        @Override
        public void onError(Throwable e) {
            clearSubscriptions();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
        }

        @Override
        public void onNext(Order order) {
            // load order data to cart
            updateUI(order);
            clearSubscriptions();
        }
    };

    View.OnClickListener repeatOrderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Check internet before proceeding to checkout an order
            if(!NetworkConnection.isNetworkAvailable()){
                WaselToast.showToast(getString(R.string.check_internet));
                return;
            }

            Intent i;
            if(LoginCredentialsPreference.getLoginPreference().isUserLoggedIn()){
                if(CheckoutConstants.CHECKOUT_NORMAL_ORDER.equalsIgnoreCase(order.getOrderType())){
                    i = new Intent(OrderDetailsActivity.this, CheckoutActivity.class);
                    i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,CheckoutConstants.KEY_CHECKOUT_REPEAT_NORMAL_ORDER);
                    i.putExtra(CheckoutConstants.KEY_DATA,order);
                    startActivity(i);
                }else {

                    i = new Intent(OrderDetailsActivity.this, ConfirmOrderActivity.class);
                    i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,CheckoutConstants.KEY_CHECKOUT_REPEAT_SPECIAL_ORDER);
                    Cart repeatSepicalOrderCart = WaselApplication.getCart();
                    order.convertToCart(repeatSepicalOrderCart);
                    i.putExtra(CheckoutConstants.KEY_DATA,repeatSepicalOrderCart);
                    startActivity(i);
                }
            }else {
                //TODO: go to login in flow
                WaselToast.showToast("Please login to checkout order");
            }
        }
    };

    private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    private BroadcastReceiver receivedFromPush = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent)
        {
            int orderId = intent.getIntExtra(CheckoutConstants.KEY_ORDER_ID, 0);
            if(order!=null && order.getId().equalsIgnoreCase(orderId +"")){
                order.setStatus(intent.getStringExtra(CheckoutConstants.KEY_ORDER_STATUS));

                if((order.isOrderConfirmed() || order.isDriverAccepted()) && TextUtils.isEmpty(order.getPickupTrackingLink())){
                    order.setPickupTrackingLink(intent.getStringExtra(CheckoutConstants.KEY_ORDER_TRACKING_LINK));
                }else if(TextUtils.isEmpty(order.getDeliveryTracingLink())
                        && order.isOrderOnTheWay()){
                    order.setDeliveryTracingLink(intent.getStringExtra(CheckoutConstants.KEY_ORDER_TRACKING_LINK));
                }

                orderDetailsAdapter.notifyDataSetChanged();
                updateTrackingDetails(order);
            }
        }
    };

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}


