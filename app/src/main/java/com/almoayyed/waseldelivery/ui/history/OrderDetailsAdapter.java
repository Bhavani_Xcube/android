package com.almoayyed.waseldelivery.ui.history;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.OrderConstants;
import com.almoayyed.waseldelivery.constants.OutletItemCustomFieldConstants;
import com.almoayyed.waseldelivery.databinding.OrderDetailsDeliveryAddressBinding;
import com.almoayyed.waseldelivery.databinding.OrderDetailsItemBinding;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.models.OrderItem;
import com.almoayyed.waseldelivery.ui.history.ViewHolders.OrderListViewHolder;
import com.almoayyed.waseldelivery.ui.history.ViewHolders.OrderStatusViewHolder;
import com.almoayyed.waseldelivery.views.CircleView;

/**
 * Created on 28 Nov 2016 4:07 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OrderDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int ORDER_TRACKING_MAP = 1;
    private static final int ORDER_STATUS_END = 2;
    private static final int ORDER_STATUS_INPROGRESS = 3;
    private static final int ORDER_ITEM = 4;
    private static final int ORDER_DELIVERY_CHARGE = 5;
    private static final int ORDER_DISCOUNT_AMT = 6;
    private static final int ORDER_TOTAL_AMT = 7;
    private static final int ORDER_DELIVERY_ADDRESS = 8;

    private Context context;
    private Order order;
    private View view;
    private View.OnClickListener orderClickListener;
    private View.OnClickListener repeatOrderClickListener;


    public OrderDetailsAdapter(Context context, Order order) {
        if(order == null){
            this.order = new Order();
        }else{
            this.order = order;
        }
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ORDER_STATUS_INPROGRESS){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_tracking_item, parent, false);
            return new OrderStatusViewHolder(view);
        }else if(viewType == ORDER_STATUS_END){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_item, parent, false);
            return new OrderListViewHolder(view);
        }else if(viewType == ORDER_ITEM){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_details_item, parent, false);
            return new OrderItemVH(view);
        }else if(viewType == ORDER_DELIVERY_CHARGE){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_details_delivery_charge, parent, false);
            return new OrderDeliveryChargeVH(view);
        }else if(viewType == ORDER_DISCOUNT_AMT){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_details_discount, parent, false);
            return new OrderTotalAmtVH(view);
        } else if(viewType == ORDER_TOTAL_AMT){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_details_total_amt, parent, false);
            return new OrderTotalAmtVH(view);
        }else if(viewType == ORDER_DELIVERY_ADDRESS){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_details_delivery_address, parent, false);
            return new OrderDeliveryAddressVH(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        holder.itemView.setTag(position);
        if(holder.getItemViewType() == ORDER_STATUS_INPROGRESS){
            //Displaying order progress status
            /*OrderStatusViewHolder orderStatusViewHolder = (OrderStatusViewHolder) holder;
            orderStatusViewHolder.totalCost.setTag(position);
            orderStatusViewHolder.getBinding().setVariable(BR.order,order);
            if(order.getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_ON_THE_WAY)){
                orderStatusViewHolder.getBinding().setVariable(BR.showStatus,3);
            }else if(order.getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_PREPARING)){
                orderStatusViewHolder.getBinding().setVariable(BR.showStatus,2);
            }else if(order.getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_CONFIRM)){
                orderStatusViewHolder.getBinding().setVariable(BR.showStatus,1);
            }else {
                orderStatusViewHolder.getBinding().setVariable(BR.showStatus,0);
            }*/

            OrderStatusViewHolder orderStatusViewHolder = (OrderStatusViewHolder) holder;
            orderStatusViewHolder.totalCost.setTag(position);
            updateOrderStatus(orderStatusViewHolder);
            Log.d("height ",""+holder.itemView.getLayoutParams().height);
        }else if(holder.getItemViewType() == ORDER_STATUS_END){
            //Displaying order completed status
            OrderListViewHolder orderListViewHolder = (OrderListViewHolder) holder;
            GradientDrawable gd = (GradientDrawable)orderListViewHolder.orderHistoryDeliveryStatus.getBackground();
            if(order.getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_FAILED)){
                orderListViewHolder.orderHistoryDeliveryStatus.setImageResource(R.drawable.close_white);
                gd.setColor(context.getResources().getColor(R.color.reddish_orange));
                gd.setStroke(0, context.getResources().getColor(R.color.textColor7));
            }else {
                orderListViewHolder.orderHistoryDeliveryStatus.setImageResource(R.drawable.black_tick);
                gd.setColor(context.getResources().getColor(android.R.color.white));
                gd.setStroke(2, context.getResources().getColor(R.color.textColor7));
            }
            orderListViewHolder.orderHistoryRepeat.setOnClickListener(repeatOrderClickListener);
            orderListViewHolder.orderHistoryId.setTag(position);
            orderListViewHolder.orderHistoryDeliveryStatus.setTag(position);
            orderListViewHolder.getBinding().setVariable(BR.order,order);
        }else if(holder.getItemViewType() == ORDER_ITEM){
            OrderItemVH orderItemVH = (OrderItemVH) holder;
            OrderItem orderItem = order.getItems().get(position-1);
            OrderDetailsItemBinding binding = (OrderDetailsItemBinding)orderItemVH.getBinding();
            binding.setVariable(BR.order,order);
            binding.setVariable(BR.item,orderItem);

            String description = orderItem.getDescription();
            if(description != null)
            {
                binding.itemDesc.setVisibility(View.VISIBLE);
                binding.itemDesc.setText(description);
            }
            else
            {
                binding.itemDesc.setVisibility(View.GONE);
            }

            //Updating custom fields info (veg, spicy etc info)
            if(orderItem.getOutletItemCustomFieldValue(OutletItemCustomFieldConstants.FIELD_SPICY)){
                binding.foodItemName.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.spicy,0);
            }else {
                binding.foodItemName.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
            }


            binding.foodItemVegType.setVisibility(View.VISIBLE);
            if(orderItem.isOutletItemCustomFieldExist(OutletItemCustomFieldConstants.FIELD_VEG)){
                ((CircleView)binding.foodItemVegType).setVisibility(View.VISIBLE);
                if(orderItem.getOutletItemCustomFieldValue(OutletItemCustomFieldConstants.FIELD_VEG)){
                    //Veg indication
                    ((CircleView)binding.foodItemVegType).setCircleColor(WaselApplication.getContext().getResources().getColor(R.color.bg));
                }else {
                    ((CircleView)binding.foodItemVegType).setCircleColor(WaselApplication.getContext().getResources().getColor(R.color.reddish_orange));
                }
            }else {
                ((CircleView)binding.foodItemVegType).setVisibility(View.GONE);
            }
        }else if(holder.getItemViewType() == ORDER_DELIVERY_CHARGE){
            OrderDeliveryChargeVH deliveryChargeVH = (OrderDeliveryChargeVH) holder;
            deliveryChargeVH.getBinding().setVariable(BR.order,order);
        }else if(holder.getItemViewType() == ORDER_DISCOUNT_AMT){
            OrderTotalAmtVH discountAmtVH = (OrderTotalAmtVH) holder;
            discountAmtVH.getBinding().setVariable(BR.order,order);
        }else if(holder.getItemViewType() == ORDER_TOTAL_AMT){
            OrderTotalAmtVH totalAmtVH = (OrderTotalAmtVH) holder;
            totalAmtVH.getBinding().setVariable(BR.order,order);
        }else if(holder.getItemViewType() == ORDER_DELIVERY_ADDRESS){
            OrderDeliveryAddressVH deliveryAddressVH = (OrderDeliveryAddressVH) holder;
            deliveryAddressVH.getBinding().setVariable(BR.order,order);
            if(order.getShippingAddress().getAddressType().equalsIgnoreCase(context.getString(R.string.home))){
                deliveryAddressVH.getBinding().btnHome.setCompoundDrawablesRelativeWithIntrinsicBounds(0,R.drawable.save_address_home_selector,0,0);
            }else if(order.getShippingAddress().getAddressType().equalsIgnoreCase(context.getString(R.string.office))){
                deliveryAddressVH.getBinding().btnHome.setCompoundDrawablesRelativeWithIntrinsicBounds(0,R.drawable.save_address_work_selector,0,0);
            }else {
                deliveryAddressVH.getBinding().btnHome.setCompoundDrawablesRelativeWithIntrinsicBounds(0,R.drawable.save_address_other_selector,0,0);
            }
        }
    }

    private void animateOnTheWayState(OrderStatusViewHolder orderStatusViewHolder, GradientDrawable gd2) {
        GradientDrawable animatedBg;
        orderStatusViewHolder.getBinding().image1.setImageResource(R.drawable.ic_done);
        orderStatusViewHolder.getBinding().image2.setImageResource(R.drawable.circle_without_stroke_background);
        animatedBg = (GradientDrawable) orderStatusViewHolder.getBinding().image2.getDrawable();
        animatedBg.setColor(context.getResources().getColor(android.R.color.white));
        gd2.setColor(context.getResources().getColor(R.color.bg));

        //Animate on-the-way status view
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.order_animation);
        orderStatusViewHolder.getBinding().circle2.startAnimation(myFadeInAnimation);
    }

    private void animateDeliveredState(OrderStatusViewHolder orderStatusViewHolder, GradientDrawable gd3) {
        GradientDrawable animatedBg;
        orderStatusViewHolder.getBinding().image1.setImageResource(R.drawable.ic_done);
        orderStatusViewHolder.getBinding().image2.setImageResource(R.drawable.ic_done);
        orderStatusViewHolder.getBinding().image3.setImageResource(R.drawable.circle_without_stroke_background);
        animatedBg = (GradientDrawable) orderStatusViewHolder.getBinding().image3.getDrawable();
        animatedBg.setColor(context.getResources().getColor(android.R.color.white));
        gd3.setColor(context.getResources().getColor(R.color.bg));

        //Animate on-the-way status view
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.order_animation);
        orderStatusViewHolder.getBinding().circle3.startAnimation(myFadeInAnimation);
    }

    @Override
    public int getItemCount() {
        //Apart from order items;
        // map, order info, delivery charge, total amount & delivery address should
        int count;
        if(order.getOrderType().equalsIgnoreCase(CheckoutConstants.CHECKOUT_NORMAL_ORDER)){
            count = order.getItems().size() + 4;
        }else {
            count = order.getItems().size() + 3;
        }

        count = order.getDiscountAmount()>0? count + 1 : count;
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        /*if(position==0){
            return ORDER_TRACKING_MAP;
        }else*/
        int shouldIncludeDiscount = order.getDiscountAmount()>0? 1 : 0;
        if(position==0){
            if(order.getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_FAILED) ||
                    order.getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_COMPLETED)){
                return ORDER_STATUS_END;
            }else {
                return ORDER_STATUS_INPROGRESS;
            }
        }else if(position >= 1 &&
                position <(order.getItems().size() + 1)){
            return ORDER_ITEM;
        }else if(position ==(order.getItems().size() + 1)){
            return ORDER_DELIVERY_CHARGE;
        }else {//if(position>=order.getItems().size() + 2 && position<=order.getItems().size() + 2 + shouldIncludeDiscount)
            if(shouldIncludeDiscount > 0 && position == order.getItems().size() + 2){
                return ORDER_DISCOUNT_AMT;
            }else if(position<=order.getItems().size() + 2 + shouldIncludeDiscount &&
                    order.getOrderType().equalsIgnoreCase(CheckoutConstants.CHECKOUT_NORMAL_ORDER)){
                return ORDER_TOTAL_AMT;
            }else{
                return ORDER_DELIVERY_ADDRESS;
            }
        }/*else if(position ==(order.getItems().size() + 2) && order.getOrderType().equalsIgnoreCase(CheckoutConstants.CHECKOUT_NORMAL_ORDER)){
            return ORDER_TOTAL_AMT;
        }else {
            return ORDER_DELIVERY_ADDRESS;
        }*/
    }

    private void updateOrderStatus(OrderStatusViewHolder orderStatusViewHolder) {
        orderStatusViewHolder.getBinding().setVariable(BR.order,order);

        GradientDrawable gd3 = (GradientDrawable)orderStatusViewHolder.getBinding().circle3.getBackground();
        gd3.setColor(context.getResources().getColor(R.color.textColor7));
        GradientDrawable gd2 = (GradientDrawable)orderStatusViewHolder.getBinding().circle2.getBackground();
        GradientDrawable gd1 = (GradientDrawable)orderStatusViewHolder.getBinding().circle1.getBackground();
        //Clear animation before setting  data
            /*orderStatusViewHolder.getBinding().circle1.clearAnimation();
            orderStatusViewHolder.getBinding().circle2.clearAnimation();*/

        GradientDrawable animatedBg;

        if(order.getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_ON_THE_WAY) ||
                order.getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_PREPARING)){
            //picked-up & on-the-way are consider as 'on-the-way' from now onwards
            orderStatusViewHolder.getBinding().setVariable(BR.showStatus,3);

            animateDeliveredState(orderStatusViewHolder, gd3);
        }/*else if(list.get(position).getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_PREPARING)){
                orderStatusViewHolder.getBinding().setVariable(BR.showStatus,2);
            }*/
        else if(order.getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_CONFIRM)||
                order.getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_DRIVER_ACCEPT)){
            orderStatusViewHolder.getBinding().setVariable(BR.showStatus,1);
            animateOnTheWayState(orderStatusViewHolder, gd2);
        }else {
            orderStatusViewHolder.getBinding().setVariable(BR.showStatus,0);
            orderStatusViewHolder.getBinding().image1.setImageResource(R.drawable.circle_without_stroke_background);
            animatedBg = (GradientDrawable) orderStatusViewHolder.getBinding().image1.getDrawable();
            animatedBg.setColor(context.getResources().getColor(android.R.color.white));
            gd1.setColor(context.getResources().getColor(R.color.bg));
            gd2.setColor(context.getResources().getColor(R.color.textColor7));

            //Animate confirm status view
            Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.order_animation);
            orderStatusViewHolder.getBinding().circle1.startAnimation(myFadeInAnimation);
        }
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        int pos = (int)holder.itemView.getTag();
        if(getItemViewType(pos) == ORDER_STATUS_INPROGRESS){
            updateOrderStatus((OrderStatusViewHolder)holder);
        }
    }

    public void setRepeatOrderClickListener(View.OnClickListener clickListener) {
        this.repeatOrderClickListener = clickListener;
    }

    /*----------------start: View holders for order details screen -----------*/
    public class OrderMapTrackingVH extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public OrderMapTrackingVH(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    public class OrderItemVH extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public OrderItemVH(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
        public ViewDataBinding getBinding() {
            return binding;
        }
    }
    public class OrderDeliveryChargeVH extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public OrderDeliveryChargeVH(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    public class OrderTotalAmtVH extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public OrderTotalAmtVH(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    public class OrderDeliveryAddressVH extends RecyclerView.ViewHolder {
        private final OrderDetailsDeliveryAddressBinding binding;

        public OrderDeliveryAddressVH(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }
        public OrderDetailsDeliveryAddressBinding getBinding() {
            return binding;
        }
    }
    /*----------------ends: View holders for order details screen -----------*/
}
