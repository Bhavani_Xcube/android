package com.almoayyed.waseldelivery.ui.history;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.PushNotificationConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.OrderDetailsBinding;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.checkout.CheckoutActivity;
import com.almoayyed.waseldelivery.ui.checkout.ConfirmOrderActivity;
import com.almoayyed.waseldelivery.utils.Converters;
import com.almoayyed.waseldelivery.utils.HandleWebClientLinks;
import com.almoayyed.waseldelivery.utils.NetworkConnection;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.utils.ScreenUtils;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.views.WaselToast;

/**
 * Created on 28 Nov 2016 3:22 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OrderDetailsFragment extends BaseFragment{

    private Order order;
    private OrderDetailsAdapter orderDetailsAdapter;
    private LinearLayoutManager linearLayoutManager;
    private OrderDetailsBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(fragView == null){
            binding = DataBindingUtil.inflate(
                    inflater, R.layout.order_details, container, false);
            fragView = binding.getRoot();
            initViews(fragView);
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(PushNotificationConstants.orderStatusFilter);
        getActivity().registerReceiver(receivedFromPush, filter);

        return fragView;
    }

    private void initViews(View v) {
        mProgressBarHandler = new ProgressBarHandler(getActivity(), binding.detailsMainLyt);
        order =(Order) getArguments().getSerializable(CheckoutConstants.KEY_DATA);
        order.updateOrderItemDetails();

        linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.orderDetailsList.setLayoutManager(linearLayoutManager);

        orderDetailsAdapter = new OrderDetailsAdapter(getContext(), order);
        binding.orderDetailsList.setAdapter(orderDetailsAdapter);
        orderDetailsAdapter.setRepeatOrderClickListener(repeatOrderClickListener);

        binding.setClickHandler(backPressListener);

        //TODO: load driver tracing data
        updateTrackingDetails();

        binding.setOrder(order);
        binding.setCheckoutConst(orderDetailsAdapter);
    }

    private void updateTrackingDetails() {
        binding.orderTracking.getSettings().setJavaScriptEnabled(true);
        binding.orderTracking.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                return HandleWebClientLinks.handleLink(getActivity(), url)? true: super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    // Do something for lollipop and above versions
                    String url = request.getUrl().toString();
                    return HandleWebClientLinks.handleLink(getActivity(), url);
                } else{
                    // do something for phones running an SDK before lollipop
                    return super.shouldOverrideUrlLoading(view, request);
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBarHandler.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBarHandler.hide();
            }
        });
        if(order.isOrderOnTheWay()){
            binding.orderTracking.loadUrl(order.getDeliveryTracingLink());
        }else {
            binding.orderTracking.loadUrl(order.getPickupTrackingLink());
        }

        if(order.isDriverAccepted()){
            //update map
            binding.orderTracking.reload();
        }

        if(order.isOrderCompleted()){
            /*CoordinatorLayout coordinatorLayout = (CoordinatorLayout)binding.detailsCoordinateLyt;*/
            // The View with the BottomSheetBehavior
            View bottomSheet = binding.detailsBottomSheet;
            ScreenUtils screenUtils = new ScreenUtils(getContext());
            BottomSheetBehavior<View> behavior = BottomSheetBehavior.from(bottomSheet);

            //Action bar height
            final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(
                    new int[] { android.R.attr.actionBarSize });
            int mActionBarSize = (int) styledAttributes.getDimension(0, 0);
            styledAttributes.recycle();

            int height = screenUtils.getHeight();
            int bottomBarHt = (int)Converters.pxToDp(getContext(),70);
            int peekHeight = height - (mActionBarSize + bottomBarHt);
            behavior.setPeekHeight(peekHeight);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            getActivity().unregisterReceiver(receivedFromPush);
        } catch (IllegalArgumentException e) {
            if (e.getMessage().contains("Receiver not registered")) {
                Loggers.info("Tried to unregister the reciver when it's not registered");
            }
            else
            {
                throw e;
            }
        }
    }

    public View.OnClickListener backPressListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((MainActivity)getActivity()).onBackPressed();
        }
    };

    View.OnClickListener repeatOrderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Check internet before proceeding to checkout an order
            if(!NetworkConnection.isNetworkAvailable()){
                WaselToast.showToast(getString(R.string.check_internet));
                return;
            }
            Intent i;
            if(LoginCredentialsPreference.getLoginPreference().isUserLoggedIn()){
                if(CheckoutConstants.CHECKOUT_NORMAL_ORDER.equalsIgnoreCase(order.getOrderType())){
                    i = new Intent(getActivity(), CheckoutActivity.class);
                    i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,CheckoutConstants.KEY_CHECKOUT_REPEAT_NORMAL_ORDER);
                    i.putExtra(CheckoutConstants.KEY_DATA,order);
                    startActivity(i);
                }else {

                    i = new Intent(getActivity(), ConfirmOrderActivity.class);
                    i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,CheckoutConstants.KEY_CHECKOUT_REPEAT_SPECIAL_ORDER);
                    Cart repeatSepicalOrderCart = WaselApplication.getCart();
                    order.convertToCart(repeatSepicalOrderCart);
                    i.putExtra(CheckoutConstants.KEY_DATA,repeatSepicalOrderCart);
                    startActivity(i);
                }
            }else {
                //TODO: go to login in flow
                WaselToast.showToast("Please login to checkout order");
            }
        }
    };

    private BroadcastReceiver receivedFromPush = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent)
        {
            int orderId = intent.getIntExtra(CheckoutConstants.KEY_ORDER_ID, 0);
            if(order!=null && order.getId().equalsIgnoreCase(orderId +"")){
                order.setStatus(intent.getStringExtra(CheckoutConstants.KEY_ORDER_STATUS));
                if(order.isOrderConfirmed() && TextUtils.isEmpty(order.getPickupTrackingLink())){
                    order.setPickupTrackingLink(intent.getStringExtra(CheckoutConstants.KEY_ORDER_TRACKING_LINK));
                }else if(TextUtils.isEmpty(order.getDeliveryTracingLink())){
                    order.setDeliveryTracingLink(intent.getStringExtra(CheckoutConstants.KEY_ORDER_TRACKING_LINK));
                }
                orderDetailsAdapter.notifyDataSetChanged();
                updateTrackingDetails();
            }
        }
    };
}
