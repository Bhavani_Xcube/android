package com.almoayyed.waseldelivery.ui.history;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.constants.OrderConstants;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.ui.LoadMoreViewHolder;
import com.almoayyed.waseldelivery.ui.history.ViewHolders.OrderListViewHolder;
import com.almoayyed.waseldelivery.ui.history.ViewHolders.OrderStatusViewHolder;
import com.almoayyed.waseldelivery.utils.Converters;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 28 Nov 2016 4:07 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OrderHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final int ORDER_STATUS_END = 1;
    private static final int ORDER_STATUS_INPROGRESS = 2;
    private static final int LOAD_MORE_TYPE = 3;
    private final Animation myFadeInAnimation;
    private Context context;
    private List<Order> list;
    private View view;
    private View.OnClickListener repeatOrderClickListener;
    private View.OnClickListener orderItemClickListener;

    //StartIndex used for pagination of Orders list loaded from server
    private int startIndex = 0;
    //maxResult specifies no.of Orders items load from server per api call
    final int limit = 10;
    private boolean loadMore;


    public OrderHistoryAdapter(Context context, List<Order> list) {
        if(list == null){
            this.list = new ArrayList<>();
        }else{
            this.list = list;
        }
        this.context = context;

        myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.order_animation);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == ORDER_STATUS_INPROGRESS){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_tracking_item, parent, false);
            return new OrderStatusViewHolder(view);
        }else if(viewType == ORDER_STATUS_END){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_item, parent, false);
            return new OrderListViewHolder(view);
        }else if(viewType == LOAD_MORE_TYPE){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_more_lyt, parent, false);
            return new LoadMoreViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getItemViewType() == ORDER_STATUS_INPROGRESS){

            OrderStatusViewHolder orderStatusViewHolder = (OrderStatusViewHolder) holder;
            orderStatusViewHolder.totalCost.setTag(position);
            updateOrderStatus(position, orderStatusViewHolder);

        }else if(holder.getItemViewType() == ORDER_STATUS_END){


            OrderListViewHolder orderListViewHolder = (OrderListViewHolder) holder;
            GradientDrawable gd = (GradientDrawable)orderListViewHolder.orderHistoryDeliveryStatus.getBackground();
            if(list.get(position).getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_FAILED)){
                orderListViewHolder.orderHistoryDeliveryStatus.setImageResource(R.drawable.close_white);
                gd.setColor(context.getResources().getColor(R.color.reddish_orange));
                gd.setStroke(0, context.getResources().getColor(R.color.textColor7));
            }else {
                orderListViewHolder.orderHistoryDeliveryStatus.setImageResource(R.drawable.black_tick);
                gd.setColor(context.getResources().getColor(android.R.color.white));
                gd.setStroke(2, context.getResources().getColor(R.color.textColor7));
            }
            //Repeat order click listener
            orderListViewHolder.orderHistoryRepeat.setTag(list.get(position));
            orderListViewHolder.orderHistoryRepeat.setOnClickListener(repeatOrderClickListener);


            orderListViewHolder.orderHistoryId.setTag(position);
            orderListViewHolder.orderHistoryDeliveryStatus.setTag(position);
            orderListViewHolder.getBinding().setVariable(BR.order,list.get(position));
        }
        //Click on item displays order details screen
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(orderItemClickListener);
    }

    private void updateOrderStatus(int position, OrderStatusViewHolder orderStatusViewHolder) {
        orderStatusViewHolder.getBinding().setVariable(BR.order,list.get(position));

        GradientDrawable gd3 = (GradientDrawable)orderStatusViewHolder.getBinding().circle3.getBackground();
        gd3.setColor(context.getResources().getColor(R.color.textColor7));
        GradientDrawable gd2 = (GradientDrawable)orderStatusViewHolder.getBinding().circle2.getBackground();
        GradientDrawable gd1 = (GradientDrawable)orderStatusViewHolder.getBinding().circle1.getBackground();
        //Clear animation before setting  data
        orderStatusViewHolder.getBinding().circle1.clearAnimation();
        orderStatusViewHolder.getBinding().circle2.clearAnimation();
        orderStatusViewHolder.getBinding().circle3.clearAnimation();
        GradientDrawable animatedBg;

        if(list.get(position).getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_ON_THE_WAY) ||
                list.get(position).getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_PREPARING)){
            //picked-up & on-the-way are consider as 'on-the-way' from now onwards
            orderStatusViewHolder.getBinding().setVariable(BR.showStatus,3);

            animateDeliveredState(orderStatusViewHolder, gd3);
        }/*else if(list.get(position).getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_PREPARING)){
            orderStatusViewHolder.getBinding().setVariable(BR.showStatus,2);
        }*/
        else if(list.get(position).getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_CONFIRM) ||
                list.get(position).getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_DRIVER_ACCEPT)){
            orderStatusViewHolder.getBinding().setVariable(BR.showStatus,1);
            animateOnTheWayState(orderStatusViewHolder, gd2);

        }else {
            orderStatusViewHolder.getBinding().setVariable(BR.showStatus,0);
            orderStatusViewHolder.getBinding().image1.setImageResource(R.drawable.circle_without_stroke_background);
            animatedBg = (GradientDrawable) orderStatusViewHolder.getBinding().image1.getDrawable();
            animatedBg.setColor(context.getResources().getColor(android.R.color.white));
            gd1.setColor(context.getResources().getColor(R.color.bg));
            gd2.setColor(context.getResources().getColor(R.color.textColor7));

            //Animate confirm status view

            orderStatusViewHolder.getBinding().circle1.startAnimation(myFadeInAnimation);
        }
    }

    private void animateDeliveredState(OrderStatusViewHolder orderStatusViewHolder, GradientDrawable gd3) {
        GradientDrawable animatedBg;
        orderStatusViewHolder.getBinding().image1.setImageResource(R.drawable.ic_done);
        orderStatusViewHolder.getBinding().image2.setImageResource(R.drawable.ic_done);
        orderStatusViewHolder.getBinding().image3.setImageResource(R.drawable.circle_without_stroke_background);
        animatedBg = (GradientDrawable) orderStatusViewHolder.getBinding().image3.getDrawable();
        animatedBg.setColor(context.getResources().getColor(android.R.color.white));
        gd3.setColor(context.getResources().getColor(R.color.bg));

        //Animate on-the-way status view
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.order_animation);
        orderStatusViewHolder.getBinding().circle3.startAnimation(myFadeInAnimation);
    }

    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        int pos = (int)holder.itemView.getTag();
        if(getItemViewType(pos) == ORDER_STATUS_INPROGRESS){
            updateOrderStatus((int)holder.itemView.getTag(), (OrderStatusViewHolder)holder);
        }
    }

    private void animateOnTheWayState(OrderStatusViewHolder orderStatusViewHolder, GradientDrawable gd2) {
        GradientDrawable animatedBg;
        orderStatusViewHolder.getBinding().image1.setImageResource(R.drawable.ic_done);
        orderStatusViewHolder.getBinding().image2.setImageResource(R.drawable.circle_without_stroke_background);
        animatedBg = (GradientDrawable) orderStatusViewHolder.getBinding().image2.getDrawable();
        animatedBg.setColor(context.getResources().getColor(android.R.color.white));
        gd2.setColor(context.getResources().getColor(R.color.bg));

        //Animate on-the-way status view
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.order_animation);
        orderStatusViewHolder.getBinding().circle2.startAnimation(myFadeInAnimation);
    }

    public Order getItemApPos(int orderPos) {
        if(list==null || orderPos>=list.size()){
            return null;
        }
        return list.get(orderPos);
    }

    public void setRepeatOrderClickListener(View.OnClickListener clickListener) {
        this.repeatOrderClickListener = clickListener;
    }

    public void setOrderItemClickListener(View.OnClickListener clickListener) {
        this.orderItemClickListener = clickListener;
    }

    public List<Order> getOrdersList(){
        return list;
    }

    @Override
    public int getItemCount() {
        return loadMore? list.size()+1 : list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(position>=list.size()){
            return LOAD_MORE_TYPE;
        }
        if(list.get(position).getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_FAILED) ||
                list.get(position).getStatus().equalsIgnoreCase(OrderConstants.ORDER_STATUS_COMPLETED)){
            return ORDER_STATUS_END;
        }else{
            return ORDER_STATUS_INPROGRESS;
        }
    }



    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getLimit() {
        return limit;
    }

    public void addOrdersList(List<Order> orders) {
        list.addAll(orders);
        startIndex  ++;
        notifyDataSetChanged();
    }

    public void incStartIndexByValue(int incrementValue) {
        this.startIndex += incrementValue;
    }

    public void clearData() {
        startIndex = 0;
        list.clear();
        loadMore = false;
        notifyDataSetChanged();
    }

    public void showLoadMoreStatus(){
        loadMore = true;
        notifyDataSetChanged();
    }

    public void hideLoadMoreStatus(){
        loadMore = false;
        notifyDataSetChanged();
    }
}
