package com.almoayyed.waseldelivery.ui.history;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.PushNotificationConstants;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.OrderHistoryBinding;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.checkout.CheckoutActivity;
import com.almoayyed.waseldelivery.ui.checkout.ConfirmOrderActivity;
import com.almoayyed.waseldelivery.ui.login.LoginActivity;
import com.almoayyed.waseldelivery.utils.NetworkConnection;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created on 28 Nov 2016 3:22 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OrderHistoryFragment extends BaseFragment{

    private OrderHistoryAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private Observable<List<Order>> ordersObserable = null;
    private Observable<Order> orderDetailsObserable;
    private Subscription subscribe;
    CompositeSubscription startStopCompositeSubscription = new CompositeSubscription();
    private boolean isDownloading = false; //Indicates the download has started
    private boolean hasMoreData = true; //Indicates more data to load from server
    private int visibleThreshold = 1;
    private LoginCredentialsPreference loginPreference;
    private OrderHistoryBinding binding;
    private boolean showBtmPrg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(fragView == null){
            binding = DataBindingUtil.inflate(inflater,R.layout.order_history, container, false);
            fragView = binding.getRoot();
            initViews(fragView);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(PushNotificationConstants.orderStatusFilter);
        getActivity().registerReceiver(receivedFromPush, filter);
        return fragView;
    }

    private void initViews(View v) {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.orderHistoryList.setLayoutManager(linearLayoutManager);

        loginPreference = LoginCredentialsPreference.getLoginPreference();
        mProgressBarHandler = new ProgressBarHandler(getContext(), binding.orderHistoryMainLyt);

        adapter =
                new OrderHistoryAdapter(getActivity(),null);
        adapter.setRepeatOrderClickListener(repeatOrderClickListener);
        adapter.setOrderItemClickListener(itemClickListener);
        binding.orderHistoryList.setAdapter(adapter);

        //handles pagination
        binding.orderHistoryList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == RecyclerView.SCROLL_STATE_IDLE){
                    int totalItemCount = linearLayoutManager.getItemCount();
                    int lastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                    // reached the threshold limit while scrolling. & do pagination
                    if (!isDownloading  && totalItemCount <= (lastVisibleItem + visibleThreshold)) { //&& hasMoreData
                        showBtmPrg = true;
                        loadOrderHistoryFromServer();
                    }
                }else if(newState == RecyclerView.SCROLL_STATE_DRAGGING){
                    adapter.showLoadMoreStatus();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });

        //Pull to refresh
        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshOrders();
            }
        });

        refreshOrders();
    }

    private void loadOrderHistoryFromServer() {
        isDownloading = true;
        ordersObserable = WaselDeliveryService.getService().getOrderHistory(loginPreference.getId(),adapter.getStartIndex(), adapter.getLimit());
        addObserver(ordersObserable,ordersObserver,!showBtmPrg);
    }

    /*private void showBottomProgress(boolean show){
        if(show){
            binding.progressLoader.setVisibility(View.VISIBLE);
            mProgressBarHandler.hide();
        }else {
            binding.progressLoader.setVisibility(View.GONE);
            mProgressBarHandler.show();
        }
    }

    private void hideBottomProgress() {
        showBtmPrg = false;
        binding.progressLoader.setVisibility(View.GONE);
    }*/

    View.OnClickListener repeatOrderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Check internet before proceeding to checkout an order
            if(!NetworkConnection.isNetworkAvailable()){
                WaselToast.showToast(getString(R.string.check_internet));
                return;
            }
            Order order = (Order)view.getTag();
            Intent i;
            if(loginPreference.isUserLoggedIn()){
                if(CheckoutConstants.CHECKOUT_NORMAL_ORDER.equalsIgnoreCase(order.getOrderType())){
                    i = new Intent(getActivity(), CheckoutActivity.class);
                    i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,CheckoutConstants.KEY_CHECKOUT_REPEAT_NORMAL_ORDER);
                    i.putExtra(CheckoutConstants.KEY_DATA,order);
                    startActivity(i);
                }else {
                    i = new Intent(getActivity(), ConfirmOrderActivity.class);
                    i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,CheckoutConstants.KEY_CHECKOUT_REPEAT_SPECIAL_ORDER);
                    Cart repeatSepicalOrderCart = WaselApplication.getCart();
                    order.convertToCart(repeatSepicalOrderCart);
                    i.putExtra(CheckoutConstants.KEY_DATA, repeatSepicalOrderCart);
                    startActivity(i);
                }
            }else {
                //TODO: go to login in flow
                //Only logged user can view history. So this case never triggers
                WaselToast.showToast("Please login to checkout order");

            }
        }
    };

    //On select item displays order details screen
    View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int orderPos= (Integer) v.getTag();
            Order order = adapter.getItemApPos(orderPos);
            if(adapter!=null && order!=null){
                orderDetailsObserable = WaselDeliveryService.getService().getOrderDetails(order.getId());
                addObserver(orderDetailsObserable, orderDetailsObserver);
            }
        }
    };

    //Include pull to refresh
    @Override
    public void onStart() {
        super.onStart();

        //loading order history from starting onwards
        //Reset download param
        refreshOrders();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            getActivity().unregisterReceiver(receivedFromPush);
        } catch (IllegalArgumentException e) {
            if (e.getMessage().contains("Receiver not registered")) {
                Loggers.info("Tried to unregister the reciver when it's not registered");
            }
            else
            {
                throw e;
            }
        }
    }

    private void refreshOrders() {
        if(ordersObserable==null){
            isDownloading = false;
            hasMoreData = true;
            adapter.clearData();
            showBtmPrg = false;
            if(loginPreference.isUserLoggedIn()){
                loadOrderHistoryFromServer();
            }else {
                showEmptyScreen(true);
            }
        }
    }

    /*
      * Observer for fetching Orders List
      * */
    Observer<List<Order>> ordersObserver = new Observer<List<Order>>() {

        @Override
        public void onCompleted() {
            ordersObserable = null;
        }

        @Override
        public void onError(Throwable e) {
            isDownloading = false;
            clearSubscriptions();
            adapter.hideLoadMoreStatus();
            binding.swipeRefreshLayout.setRefreshing(false);
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
            ordersObserable = null;
        }

        @Override
        public void onNext(List<Order> orders) {
            if (orders.size()>0){
                adapter.addOrdersList(orders);
            }

            //deciding no further data to download
            if(orders.size()<adapter.getLimit()){
                hasMoreData = false;
            }

            //Update empty screen when there is no history data
            showEmptyScreen(adapter.getItemCount()==0);

            isDownloading = false;
            clearSubscriptions();
            adapter.hideLoadMoreStatus();
            binding.swipeRefreshLayout.setRefreshing(false);
            ordersObserable = null;
        }
    };

    /*
     * Observer for fetching Orders List
     * */
    Observer<Order> orderDetailsObserver = new Observer<Order>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            clearSubscriptions();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
        }

        @Override
        public void onNext(Order order) {
            //Display details screen
           /* OrderDetailsFragment orderDetailsFragment = new OrderDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(CheckoutConstants.KEY_DATA,order);
            orderDetailsFragment.setArguments(bundle);
            ((MainActivity)getActivity()).addFragment(orderDetailsFragment);
            clearSubscriptions();*/

            Intent i = new Intent(getActivity(), OrderDetailsActivity.class);
            i.putExtra(CheckoutConstants.KEY_DATA,order);
            i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,CheckoutConstants.KEY_CHECKOUT_HISTORY_DETAILS);
            startActivity(i);
        }
    };

    private void showEmptyScreen(boolean show) {
        if(show){
            binding.emptyOrderHistoryLyt.getRoot().setVisibility(View.VISIBLE);
            binding.emptyOrderHistoryLyt.comingSoonImg.setImageResource(R.drawable.empty_order_history);
            binding.emptyOrderHistoryLyt.comingSoonText.setText(getResources().getString(R.string.order_history_empty_title));
            Resources resources = getResources();
            binding.emptyOrderHistoryLyt.comingSoonDesc.setText(loginPreference.isUserLoggedIn()?
                    resources.getString(R.string.order_history_empty_desc) :
                    resources.getString(R.string.order_history_empty_without_login_desc));
            binding.emptyOrderHistoryLyt.loginBtn.setVisibility(loginPreference.isUserLoggedIn()?View.GONE:View.VISIBLE);
            binding.emptyOrderHistoryLyt.loginBtn.setOnClickListener(loginBtnClickHandler);
            binding.orderHistoryList.setVisibility(View.GONE);
        }else{
            binding.orderHistoryList.setVisibility(View.VISIBLE);
            binding.emptyOrderHistoryLyt.getRoot().setVisibility(View.GONE);
        }
    }

    public View.OnClickListener loginBtnClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
            loginIntent.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM, RegistrationBundleKeyConstants.LOGIN_FROM_ORDER_HISTORY);
            startActivity(loginIntent);
        }
    };
    private BroadcastReceiver receivedFromPush = new BroadcastReceiver(){
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if(adapter == null){
                return;
            }
            List<Order> ordersList = adapter.getOrdersList();
            for (Order item : ordersList){
                if(item.getId().equalsIgnoreCase(intent.getIntExtra(CheckoutConstants.KEY_ORDER_ID,0)+"")){
                    item.setStatus(intent.getStringExtra(CheckoutConstants.KEY_ORDER_STATUS));
                    adapter.notifyDataSetChanged();
                    return;
                }
            }

        }
    };
}
