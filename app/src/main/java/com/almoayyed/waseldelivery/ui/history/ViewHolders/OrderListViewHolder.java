package com.almoayyed.waseldelivery.ui.history.ViewHolders;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;

/**
 * Created on 2 Dec 2016 8:16 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OrderListViewHolder extends RecyclerView.ViewHolder {
    private final ViewDataBinding binding;
    public TextView orderHistoryId;
    public TextView orderHistoryTime;
    public ImageView orderHistoryDeliveryStatus;
    public TextView orderHistoryRepeat;

    public OrderListViewHolder(View view) {
        super(view);
        orderHistoryId = (TextView) view.findViewById(R.id.order_history_id);
        orderHistoryTime = (TextView) view.findViewById(R.id.order_history_time);
        orderHistoryDeliveryStatus = (ImageView) view.findViewById(R.id.order_history_delivery_status);
        orderHistoryRepeat = (TextView) view.findViewById(R.id.order_history_repeat);
        binding = DataBindingUtil.bind(view);
    }

    public ViewDataBinding getBinding() {
        return binding;
    }
}