package com.almoayyed.waseldelivery.ui.history.ViewHolders;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.databinding.OrderHistoryItemBinding;
import com.almoayyed.waseldelivery.databinding.OrderHistoryTrackingItemBinding;

/**
 * Created on 2 Dec 2016 8:50 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public class OrderStatusViewHolder extends RecyclerView.ViewHolder {
    private final OrderHistoryTrackingItemBinding binding;
    public final TextView orderHistoryId;
    public final TextView orderHistoryTime;
    public TextView totalCost;

    public OrderStatusViewHolder(View view) {
        super(view);
        orderHistoryId = (TextView) view.findViewById(R.id.order_history_id);
        orderHistoryTime = (TextView) view.findViewById(R.id.order_history_time);
        totalCost = (TextView) view.findViewById(R.id.order_history_total_cost);
        binding = DataBindingUtil.bind(view);
    }
    public OrderHistoryTrackingItemBinding getBinding() {
        return binding;
    }
}
