package com.almoayyed.waseldelivery.ui.home.OutletDetails;

import java.io.Serializable;

import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

/**
 * Created on 18 Nov 2016 6:07 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public abstract class AbstractModelItem<VH extends FlexibleViewHolder>
            extends AbstractFlexibleItem<VH>
            implements Serializable {

        private static final long serialVersionUID = -6882745111884490060L;

        private String id;
        private String title;
        private String subtitle;
        private int amenityId;

        public AbstractModelItem(String id) {
            this.id = id;
        }

        @Override
        public boolean equals(Object inObject) {
            if (inObject instanceof AbstractModelItem) {
                AbstractModelItem inItem = (AbstractModelItem) inObject;
                return this.id.equals(inItem.id);
            }
            return false;
        }

        /**
         * Override this method too, when using functionalities like StableIds, Filter or CollapseAll.
         * FlexibleAdapter is making use of HashSet to improve performance, especially in big list.
         */
        @Override
        public int hashCode() {
            return id.hashCode();
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        @Override
        public String toString() {
            return "id=" + id +
                    ", title=" + title;
        }

    public int getAmenityId() {
        return amenityId;
    }

    public void setAmenityId(int amenityId) {
        this.amenityId = amenityId;
    }
}