package com.almoayyed.waseldelivery.ui.home.OutletDetails;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.databinding.CustomizeOutletItemScreenBinding;
import com.almoayyed.waseldelivery.models.CustomizationItem;
import com.almoayyed.waseldelivery.models.OutletItem;
import com.almoayyed.waseldelivery.models.OutletItemCustomization;
import com.almoayyed.waseldelivery.ui.MainActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 30 Nov 2016 12:05 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CustomizeOutletItemDialog extends DialogFragment {

    private View view;
    private OutletItem outletItem;
    private View.OnClickListener doneClickListener;
    private View.OnClickListener cancelClickListener;

    public CustomizeOutletItemDialog(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setStyle(STYLE_NO_TITLE,R.style.Theme_CustomDialog);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        CustomizeOutletItemScreenBinding binding = DataBindingUtil.inflate(inflater,R.layout.customize_outlet_item_screen,container,true);

        view = binding.getRoot();
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.customize_tabs);
        ViewPager pager = (ViewPager) view.findViewById(R.id.customize_viewpager);

        //Read bundle data
        outletItem = getArguments().containsKey(CheckoutConstants.KEY_CUSTOMIZATION_ITEM)?
                (OutletItem)getArguments().getSerializable(CheckoutConstants.KEY_CUSTOMIZATION_ITEM)
                : new OutletItem();
        binding.setOutletItem(outletItem);

        //Process the input data
        HashMap<String, ArrayList<OutletItemCustomization>> processedData = new HashMap<>();
        LinkedHashMap<String, ArrayList<OutletItemCustomization>> sortedData;
        ArrayList<OutletItemCustomization> customizedItems = outletItem.getCustomizedItems();
        for(OutletItemCustomization item : customizedItems){
            if(item.getType().equalsIgnoreCase(CheckoutConstants.CUSTOM_ITEM_ANY_ONE)){
                item.getCategory().setName("0"+ item.getCategory().getName());
            }
            String tabName = item.getCategory().getName();
            if(!processedData.containsKey(tabName)){
                processedData.put(tabName,new ArrayList<OutletItemCustomization>());
            }
            processedData.get(tabName).add(item);
        }

        sortedData = sortByComparator(processedData, true);

        //holding the previous data. Used to revert previous customization data if cancel is pressed
        outletItem.setCustomizedItemsCopy();

        MyFragmentAdapter adapter = new MyFragmentAdapter(getChildFragmentManager());
        //Adding data to adapter
        Set<String> categoryNames = sortedData.keySet();
        for (String categoryName : categoryNames) {
            CustomizeOutletItemFragment itemFragment = new CustomizeOutletItemFragment();
            Bundle bundle = new Bundle();
            ArrayList<OutletItemCustomization> items = sortedData.get(categoryName);
            bundle.putSerializable(CheckoutConstants.KEY_CUSTOMIZATION_ITEM, outletItem);
            bundle.putSerializable(CheckoutConstants.KEY_CUSTOMIZATION_DATA, items);

            //For single selection by default first item has to be selected
            if(!outletItem.isCustomized()
                    && items.get(0).getType().equalsIgnoreCase(CheckoutConstants.CUSTOM_ITEM_ANY_ONE)){
                items.get(0).setQuantity(1);
            }

            itemFragment.setArguments(bundle);
            if(items.get(0).getType().equalsIgnoreCase(CheckoutConstants.CUSTOM_ITEM_ANY_ONE)){
                categoryName = TextUtils.substring(categoryName,1,categoryName.length());
                for(OutletItemCustomization item : items){
                    item.getCategory().setName(categoryName);
                }
            }
            adapter.addFragment(categoryName, itemFragment);
        }
        tabLayout.setupWithViewPager(pager);
        pager.setAdapter(adapter);

        view.findViewById(R.id.customize_done).setTag(outletItem);
        view.findViewById(R.id.customize_done).setOnClickListener(doneClickListener);
        view.findViewById(R.id.customize_cancel).setTag(outletItem);
        view.findViewById(R.id.customize_cancel).setOnClickListener(cancelClickListener);
        return view;
    }

    public void setDoneClickListener(View.OnClickListener doneClickListener) {
        this.doneClickListener = doneClickListener;
    }

    public void setCancelClickListener(View.OnClickListener cancelClickListener) {
        this.cancelClickListener = cancelClickListener;
    }

    private static LinkedHashMap<String, ArrayList<OutletItemCustomization>> sortByComparator(Map<String, ArrayList<OutletItemCustomization>> unsortMap, final boolean order)
    {

        List<Map.Entry<String, ArrayList<OutletItemCustomization>>> list = new LinkedList<Map.Entry<String, ArrayList<OutletItemCustomization>>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, ArrayList<OutletItemCustomization>>>()
        {
            public int compare(Map.Entry<String, ArrayList<OutletItemCustomization>> o1,
                               Map.Entry<String, ArrayList<OutletItemCustomization>> o2)
            {
                if (order)
                {
                    return o1.getKey().compareTo(o2.getKey());
                }
                else
                {
                    return o2.getKey().compareTo(o1.getKey());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        LinkedHashMap<String, ArrayList<OutletItemCustomization>> sortedMap = new LinkedHashMap<String, ArrayList<OutletItemCustomization>>();
        for (Map.Entry<String, ArrayList<OutletItemCustomization>> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

    public class MyFragmentAdapter extends FragmentStatePagerAdapter
    {
        List<CustomizeOutletItemFragment> fragmentList = new ArrayList<>();
        ArrayList<String> titles = new ArrayList<>();

        public MyFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return titles.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }

        public void addFragment(String title, CustomizeOutletItemFragment fragment){
            titles.add(title);
            fragmentList.add(fragment);
        }
    }
}