package com.almoayyed.waseldelivery.ui.home.OutletDetails;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.models.OutletItem;
import com.almoayyed.waseldelivery.models.OutletItemCustomization;

import java.util.ArrayList;

/**
 * Created on 30 Nov 2016 12:05 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CustomizeOutletItemFragment extends Fragment {
    int id;
    public static final String ID_BUNDLE = "bundle";
    private RecyclerView customizeItemsListView;
    private ArrayList<OutletItemCustomization> customItems;
    private CustomizeOutletItemsAdapter itemsAdapter;
    private OutletItem outletItem;

    public CustomizeOutletItemFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.customize_outlet_items, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        customizeItemsListView = (RecyclerView) view.findViewById(R.id.customize_items);
        customizeItemsListView.setLayoutManager(linearLayoutManager);

        //Reading data
        if(getArguments()!=null){
            outletItem = getArguments().containsKey(CheckoutConstants.KEY_CUSTOMIZATION_ITEM)?
                    (OutletItem) getArguments().getSerializable(CheckoutConstants.KEY_CUSTOMIZATION_ITEM)
                    : new OutletItem();
            customItems = getArguments().containsKey(CheckoutConstants.KEY_CUSTOMIZATION_DATA)?
                    (ArrayList<OutletItemCustomization>) getArguments().getSerializable(CheckoutConstants.KEY_CUSTOMIZATION_DATA)
                    : new ArrayList<OutletItemCustomization>();
            itemsAdapter = new CustomizeOutletItemsAdapter(customItems);
            itemsAdapter.setClickListener(clickListener);
            customizeItemsListView.setAdapter(itemsAdapter);
        }
    }


    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.customize_item_add:
                    addCustomization(v);
                    break;
                case R.id.customize_item_remove:
                    removeCustomization(v);
                    break;
                case R.id.customize_item_select:
                    multiSelectCustomization(v);
                    break;
                case R.id.customize_item_single_select:
                    singleSelectCustomization(v);
                    break;
                case R.id.customize_lyt:
                default:
                    handleLayoutClick(v);
                    break;
            }

        }
    };

    //User taps on layout but not on checkbox
    private void handleLayoutClick(View v) {
        if(v.getTag() == null){
            return;
        }
        int pos = Integer.parseInt(v.getTag().toString());
        OutletItemCustomization item  = itemsAdapter.getItemAtPos(pos);
        if(CheckoutConstants.CUSTOM_ITEM_ANY_ONE.equalsIgnoreCase(item.getType())){
            itemsAdapter.setSingleSelectionIndex(pos);
        }else if(CheckoutConstants.CUSTOM_ITEM_MULTIPLE.equalsIgnoreCase(item.getType())){
            item.setQuantity(item.getQuantity()>0?0:1);
        }
        outletItem.notifyCustomPrice();
    }

    private void addCustomization(View v) {
        //inc the customization quantity
        int pos = Integer.parseInt(v.getTag().toString());
        itemsAdapter.incItemCountAtPos(pos);

        //TODO: update the total customization price
        outletItem.notifyCustomPrice();
    }

    private void removeCustomization(View v) {
        //dec the customization quantity
        int pos = Integer.parseInt(v.getTag().toString());
        itemsAdapter.decItemCountAtPos(pos);
        // dec the total customization price
        outletItem.notifyCustomPrice();
    }

    private void multiSelectCustomization(View v) {
        //set this customization element if it not set previously
        int pos = Integer.parseInt(v.getTag().toString());
        OutletItemCustomization item  = itemsAdapter.getItemAtPos(pos);
        item.setQuantity(item.getQuantity()>0?0:1);

        //set the total customization price
        outletItem.notifyCustomPrice();
    }

    private void singleSelectCustomization(View v) {
        //single selection implementation
        int pos = Integer.parseInt(v.getTag().toString());
        OutletItemCustomization item  = itemsAdapter.getItemAtPos(pos);
        itemsAdapter.setSingleSelectionIndex(pos);

        outletItem.notifyCustomPrice();
    }


}