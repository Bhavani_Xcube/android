package com.almoayyed.waseldelivery.ui.home.OutletDetails;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.databinding.CustomizeOutletItemElementBinding;
import com.almoayyed.waseldelivery.models.OutletItemCustomization;

import java.util.ArrayList;

/**
 * Created on 30 Nov 2016 12:05 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Map customization items to outlet sub item
 */

public class CustomizeOutletItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final ArrayList<OutletItemCustomization> itemCustomizations;
    private View.OnClickListener clickListener;
    private int singleSelIndex = 0; //Used to highlight one field

    public CustomizeOutletItemsAdapter(ArrayList<OutletItemCustomization> itemCustomizations){
        this.itemCustomizations = itemCustomizations;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customize_outlet_item_element, parent, false);
        return new CustomizeOutletItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        OutletItemCustomization  item = itemCustomizations.get(position);
        CustomizeOutletItemElementBinding binding = ((CustomizeOutletItemViewHolder) holder).getBinding();
        binding.setVariable(BR.custom_item,item);
        binding.customizeText.setCustomText(item.getName()+ (item.getPrice()!= 0d ? " (@ "+
                String.format(WaselApplication.getContext().getString(R.string.price_format),item.getPrice())+")" : ""));
        if(CheckoutConstants.CUSTOM_ITEM_QTY.equalsIgnoreCase(item.getType())){
            //Displays add & dec button
            binding.customizeItemAdd.setTag(position);
            binding.customizeItemAdd.setOnClickListener(clickListener);
            binding.customizeItemRemove.setTag(position);
            binding.customizeItemRemove.setOnClickListener(clickListener);
        }else if(CheckoutConstants.CUSTOM_ITEM_MULTIPLE.equalsIgnoreCase(item.getType())){
            binding.customizeLyt.setTag(position);
            binding.customizeLyt.setOnClickListener(clickListener);
            holder.itemView.setTag(position);
            holder.itemView.setOnClickListener(clickListener);
            binding.customizeText.setTag(position);
            binding.customizeText.setOnClickListener(clickListener);
            binding.customizeItemSelect.setTag(position);
            binding.customizeItemSelect.setOnClickListener(clickListener);
        }else if(CheckoutConstants.CUSTOM_ITEM_ANY_ONE.equalsIgnoreCase(item.getType())){
            binding.customizeLyt.setTag(position);
            binding.customizeLyt.setOnClickListener(clickListener);
            binding.customizeText.setTag(position);
            binding.customizeText.setOnClickListener(clickListener);
            binding.customizeItemSingleSelect.setTag(position);
            binding.customizeItemSingleSelect.setOnClickListener(clickListener);

            if(item.getQuantity()>0){
                singleSelIndex = position;
                binding.customizeItemSingleSelect.setChecked(true);
            }else {
                binding.customizeItemSingleSelect.setChecked(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return itemCustomizations.size();
    }

    public OutletItemCustomization getItemAtPos(int pos){
        return itemCustomizations.get(pos);
    }

    public void incItemCountAtPos(int pos){
        if(pos<0 || pos >= itemCustomizations.size()){
            return;
        }
        itemCustomizations.get(pos).incQuantity();
    }

    public void decItemCountAtPos(int pos){
        if(pos<0 || pos >= itemCustomizations.size()){
            return;
        }
        if(itemCustomizations.get(pos).getQuantity()>0){
            itemCustomizations.get(pos).decQuantity();
        }
    }

    public int setSingleSelectionIndex(int index){
        if(singleSelIndex == index){
            return singleSelIndex;
        }

        if(singleSelIndex != -1){
            itemCustomizations.get(singleSelIndex).setQuantity(0);
        }
        singleSelIndex = index;
        itemCustomizations.get(singleSelIndex).setQuantity(1);
        notifyDataSetChanged();
        return singleSelIndex;
    }

    public void setClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    private class CustomizeOutletItemViewHolder extends RecyclerView.ViewHolder {
        private final CustomizeOutletItemElementBinding binding;

        public CustomizeOutletItemViewHolder(View view) {
            super(view);
            binding = DataBindingUtil.bind(view);
        }

        public CustomizeOutletItemElementBinding getBinding() {
            return binding;
        }
    }
}
