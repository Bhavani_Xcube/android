package com.almoayyed.waseldelivery.ui.home.OutletDetails;

import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;

import java.util.ArrayList;
import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IExpandable;
import eu.davidea.flexibleadapter.items.IHeader;
import eu.davidea.viewholders.ExpandableViewHolder;

/**
 * Created on 10 Nov 2016 10:55 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class ExpandableHeaderItem
        extends AbstractModelItem<ExpandableHeaderItem.ExpandableHeaderViewHolder>
        implements IExpandable<ExpandableHeaderItem.ExpandableHeaderViewHolder, ExpandableSubItem>,
        IHeader<ExpandableHeaderItem.ExpandableHeaderViewHolder>{

    private static final long serialVersionUID = -1882711111814491060L;

    /* Flags for FlexibleAdapter */
    private boolean mExpanded = false;

    /* subItems list */
    private List<ExpandableSubItem> mSubItems;


    public ExpandableHeaderItem(String id) {
        super(id);
        setDraggable(true);
        //We start with header shown and expanded
        setHidden(false);
        setExpanded(true);
        //NOT selectable (otherwise ActionMode will be activated on long click)!
        setSelectable(false);
    }

    @Override
    public boolean isExpanded() {
        return mExpanded;
    }

    @Override
    public void setExpanded(boolean expanded) {
        mExpanded = expanded;
    }

    @Override
    public int getExpansionLevel() {
        return 0;
    }

    @Override
    public List<ExpandableSubItem> getSubItems() {
        return mSubItems;
    }

    public final boolean hasSubItems() {
        return mSubItems!= null && mSubItems.size() > 0;
    }

    public boolean removeSubItem(ExpandableSubItem item) {
        return item != null && mSubItems.remove(item);
    }

    public boolean removeSubItem(int position) {
        if (mSubItems != null && position >= 0 && position < mSubItems.size()) {
            mSubItems.remove(position);
            return true;
        }
        return false;
    }

    public void addSubItem(ExpandableSubItem subItem) {
        if (mSubItems == null)
            mSubItems = new ArrayList<>();
        mSubItems.add(subItem);
    }

    public void addSubItem(int position, ExpandableSubItem subItem) {
        if (mSubItems != null && position >= 0 && position < mSubItems.size()) {
            mSubItems.add(position, subItem);
        } else
            addSubItem(subItem);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.outlet_item_select_header;
    }

    @Override
    public ExpandableHeaderViewHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {
        return new ExpandableHeaderViewHolder(inflater.inflate(getLayoutRes(), parent, false), adapter);
    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, ExpandableHeaderViewHolder holder, int position, List payloads) {
        holder.mTitle.setText(getTitle());
        if(isExpanded()){
            holder.mHandleView.setImageResource(R.drawable.collaps_menu);
            holder.mTitle.setTextColor(ContextCompat.getColor(WaselApplication.getContext(), R.color.textColor9));
        }else {
            holder.mHandleView.setImageResource(R.drawable.expand_menu);
            holder.mTitle.setTextColor(ContextCompat.getColor(WaselApplication.getContext(), R.color.textColor7));
        }
    }

    /**
     * Provide a reference to the views for each data item.
     * Complex data labels may need more than one view per item, and
     * you provide access to all the views for a data item in a view holder.
     */
    class ExpandableHeaderViewHolder extends ExpandableViewHolder {

        public TextView mTitle;
        public ImageView mHandleView;

        public ExpandableHeaderViewHolder(View view, FlexibleAdapter adapter) {
            super(view, adapter, true);//True for sticky
            mTitle = (TextView) view.findViewById(R.id.food_item_header);
            this.mHandleView = (ImageView) view.findViewById(R.id.food_item_header_action);
            //TODO: need to recheck once
                /*if (adapter.isHandleDragEnabled()) {
                    this.mHandleView.setVisibility(View.VISIBLE);
                    setDragHandleView(mHandleView);
                } else {
                    this.mHandleView.setVisibility(View.GONE);
                }*/

                /*//Support for StaggeredGridLayoutManager
                if (itemView.getLayoutParams() instanceof StaggeredGridLayoutManager.LayoutParams) {
                    ((StaggeredGridLayoutManager.LayoutParams) itemView.getLayoutParams()).setFullSpan(true);
                }*/
        }

        @Override
        protected boolean isViewExpandableOnClick() {
            return true;//true by default
        }

        @Override
        protected void expandView(int position) {
            super.expandView(position);
            Log.d("list ","expand "+position);
            //Let's notify the item has been expanded
            if (mAdapter.isExpanded(position)) {
                Log.d("list ","expanded "+position);
//                mAdapter.notifyDataSetChanged();
                mAdapter.notifyItemChanged(position, true);
            }
        }

        @Override
        protected void collapseView(int position) {
            super.collapseView(position);
            Log.d("list ","collapse "+position);
            //Let's notify the item has been collapsed
            if (!mAdapter.isExpanded(position)) {
                Log.d("list ","collapsed "+position);
//                mAdapter.notifyDataSetChanged();
                mAdapter.notifyItemChanged(position, true);
            }
        }
    }



    @Override
    public String toString() {
        return "ExpandableHeaderItem[" + super.toString() + "//SubItems" + mSubItems + "]";
    }

}