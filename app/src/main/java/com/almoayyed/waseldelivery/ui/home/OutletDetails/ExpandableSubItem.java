package com.almoayyed.waseldelivery.ui.home.OutletDetails;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.OutletItemCustomFieldConstants;
import com.almoayyed.waseldelivery.databinding.OutletItemForSelectionBinding;
import com.almoayyed.waseldelivery.databinding_eventhandler.LoadImage;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.models.OutletItem;
import java.io.Serializable;
import java.util.List;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.IFilterable;
import eu.davidea.flexibleadapter.items.IHeader;
import eu.davidea.flexibleadapter.items.ISectionable;
import com.almoayyed.waseldelivery.BR;

import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletDetailsResponse;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletItemDetails;
import com.almoayyed.waseldelivery.ui.home.OutletDetails.ViewHolders.ExpandableItemViewHolder;
import com.almoayyed.waseldelivery.views.CircleView;

import static com.almoayyed.waseldelivery.constants.AmenityConstants.setAppropriateCatagoryImage;
import static com.almoayyed.waseldelivery.databinding_eventhandler.LoadImage.loadImage;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

class ExpandableSubItem extends AbstractModelItem<ExpandableItemViewHolder>
        implements Serializable, ISectionable<ExpandableItemViewHolder, IHeader>, IFilterable {

    private static final long serialVersionUID = 2519281529221244210L;
    private static final int FOOD_AMENITY_ID = 1;
    private final OutletItem outletItem;
    private final OutletItemDetails outletDetails = new OutletItemDetails();
    private View.OnClickListener addItemToCartListener;
    private View.OnClickListener removeItemFromCartListener;
    private View.OnClickListener showCustomizeDlgListener;

    /**
     * The header of this item
     */
    IHeader header;
    private boolean canAccessImage;

    public ExpandableSubItem(OutletItem foodItem) {
        super(String.valueOf(foodItem.getId()));
        setDraggable(true);
        this.outletItem = foodItem;
    }

    @Override
    public IHeader getHeader() {
        return header;
    }

    @Override
    public void setHeader(IHeader header) {
        this.header = header;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.outlet_item_for_selection;
    }

    @Override
    public ExpandableItemViewHolder createViewHolder(FlexibleAdapter adapter, LayoutInflater inflater, ViewGroup parent) {
        return new ExpandableItemViewHolder(inflater.inflate(getLayoutRes(), parent, false), adapter);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void bindViewHolder(FlexibleAdapter adapter, ExpandableItemViewHolder holder, int position, List payloads) {

        holder.getBinding().setVariable(BR.item, outletItem);
        holder.getBinding().executePendingBindings();

        //Setting click listener for adding & removing item from cart
        holder.addItemView.setTag(outletItem);
        holder.addItemView.setOnClickListener(addItemToCartListener);
        holder.removeItemView.setTag(outletItem);
        holder.removeItemView.setOnClickListener(removeItemFromCartListener);
        holder.getBinding().getRoot().findViewById(R.id.food_item_customize).setTag(outletItem);
        holder.getBinding().getRoot().findViewById(R.id.food_item_customize).setOnClickListener(showCustomizeDlgListener);

        if(outletItem.getOutletItemCustomFieldValue(OutletItemCustomFieldConstants.FIELD_SPICY)){
            holder.foodItemName.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.spicy,0);
        }else {
            holder.foodItemName.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        }


        holder.foodItemVegType.setVisibility(View.VISIBLE);
        if(outletItem.isOutletItemCustomFieldExist(OutletItemCustomFieldConstants.FIELD_VEG)){
            ((CircleView)holder.foodItemVegType).setVisibility(View.VISIBLE);
            if(outletItem.getOutletItemCustomFieldValue(OutletItemCustomFieldConstants.FIELD_VEG)){
                //Veg indication
                ((CircleView)holder.foodItemVegType).setCircleColor(WaselApplication.getContext().getResources().getColor(R.color.bg));
            }else {
                ((CircleView)holder.foodItemVegType).setCircleColor(WaselApplication.getContext().getResources().getColor(R.color.reddish_orange));
            }
        }else {
            ((CircleView)holder.foodItemVegType).setVisibility(View.GONE);
        }


        /*if(canAccessImage){
            holder.getBinding().getRoot().findViewById(R.id.food_item_image).setVisibility(View.VISIBLE);
        }else {
            holder.getBinding().getRoot().findViewById(R.id.food_item_image).setVisibility(View.GONE);
        }*/

        //hidiing image if the Category is disabled from server
        if(outletItem.isHideImage()){
            ((OutletItemForSelectionBinding)holder.getBinding()).foodItemImage.setVisibility(View.GONE);
        }else{
            ((OutletItemForSelectionBinding)holder.getBinding()).foodItemImage.setVisibility(View.VISIBLE);
        }


        /*
        //Image loaded using databinding concept
        if(TextUtils.isEmpty(outletItem.getImageUrl())){
            ((OutletItemForSelectionBinding)holder.getBinding()).foodItemImage.
                    setImageResource(setAppropriateCatagoryImage(getAmenityId()));
        }else{
            loadImage(((OutletItemForSelectionBinding)holder.getBinding()).foodItemImage, outletItem.getImageUrl());
        }*/

        //Enabling customization text
        if((outletItem.getCustomizedItems() != null && outletItem.getCustomizedItems().size()>0)){
            holder.getBinding().getRoot().findViewById(R.id.food_item_customize).setVisibility(View.VISIBLE);
        }else {
            holder.getBinding().getRoot().findViewById(R.id.food_item_customize).setVisibility(View.GONE);
        }
    }

    public void setAddItemToCartListener(View.OnClickListener addItemToCartListener) {
        this.addItemToCartListener = addItemToCartListener;
    }

    public void setRemoveItemFromCartListener(View.OnClickListener removeItemFromCartListener) {
        this.removeItemFromCartListener = removeItemFromCartListener;
    }

    public void setShowCustomizeDlgListener(View.OnClickListener showCustomizeDlgListener) {
        this.showCustomizeDlgListener = showCustomizeDlgListener;
    }

    @Override
    public boolean filter(String constraint) {
        return getTitle() != null && getTitle().toLowerCase().trim().contains(constraint);
    }


    @Override
    public String toString() {
        return "SubItem[" + super.toString() + "]";
    }

    public void setCanAccessImage(boolean canAccessImage) {
        this.canAccessImage = canAccessImage;
    }
}