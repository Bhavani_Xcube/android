package com.almoayyed.waseldelivery.ui.home.OutletDetails;

import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.support.design.widget.TabLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.databinding.OutletDetailsBinding;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.models.OutletItem;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletDetailsResponse;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletItemDetails;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.CartViewDisplayInterface;
import com.almoayyed.waseldelivery.ui.LoadFragmentActivity;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.CustomBudgetView;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * A simple {@link Fragment} subclass.
 */
public class OutletDetailsFragment extends BaseFragment{

    public static final String OUTLET_DETAIL = "Outlet_Details";
    private static final String FROM_SEARCH = "from_search";
    public static final String AMENITY_ID = "amenityId";
    private ViewPager outletDetailsViewPager;
    private TabLayout tabLayout;
    private ViewPagerAdapter outletDetailsPagerAdapter;
    private int amenityId;
    private ImageView header_bg;
    OutletDetailsBinding binding;
    private Observable<OutletDetailsResponse> outletMenuObservable;
    Outlet outlet;
    LinkedHashMap<String, LinkedHashMap<String,ArrayList<OutletItem>>> menu;
    private Resources resources;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(fragView==null){
            binding = DataBindingUtil.inflate(
                    inflater, R.layout.outlet_details, container, false);
            fragView = binding.getRoot();
            initViews(fragView,binding);
        }

        return fragView;
    }
    @Override
    public void onStart() {
        super.onStart();

        //if outlet items are not loaded as api req cancelled due to tab shifting etc
        if(outletDetailsPagerAdapter.getCount() == 0){
            //outlet details API call
            outletMenuObservable = WaselDeliveryService.getService().getOutletDetails(outlet.getId());
            addObserver(outletMenuObservable, outletMenuObserver);
        }

    }

    @Override
    public void onStop() {
        clearSubscriptions();
        super.onStop();
    }
    private void initViews(View view, OutletDetailsBinding binding) {
        resources = getResources();

        outletDetailsViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        header_bg = (ImageView) view.findViewById(R.id.header_bg);
        outletDetailsPagerAdapter = new ViewPagerAdapter(getFragmentManager());

        mProgressBarHandler = new ProgressBarHandler(getActivity(), binding.outletDetailsMainLyt);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(outletDetailsViewPager);

        ContextCompat.getDrawable(getActivity(),R.drawable.rectangle_background);

        //Setting data to layout through databinding
        Bundle data = getArguments();
        if(data!=null){
            outlet = (Outlet) data.getSerializable(OUTLET_DETAIL);
            binding.setOutlet(outlet);
            amenityId = data.getInt(AMENITY_ID);
            /* //Outlet budget is not displayed
            CustomBudgetView budgetView = (CustomBudgetView) view.findViewById(R.id.res_det_budget);
            String budget = outlet.getBudget();
            int budgetVal;
            if(budget.equalsIgnoreCase("low")){
                budgetVal = 1;
            }else if(budget.equalsIgnoreCase("medium")){
                budgetVal = 2;
            } else {
                budgetVal = 3;
            }
            budgetView.setBudgetValue(budgetVal);*/

            //Setting the click listener
            binding.setClickEventHandler(this);
            //Binding outlet instance to cart
            ((CartViewDisplayInterface)getActivity()).setOutlet(amenityId,outlet);

            //Clearing different outlet cart
            Outlet tmpOutlet = WaselApplication.getCart().getOutlet();
            if(!(tmpOutlet==null || this.outlet.getId().equalsIgnoreCase(tmpOutlet.getId()))){
                WaselApplication.getCart().clear();
            }

        }

    }

    /*@Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if(outletDetailsPagerAdapter!=null){
            outState.putStringArrayList("outletItems", outletDetailsPagerAdapter.get());
        }
    }

    @Override
    public void onActivityCreated(@NonNull Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null && outletDetailsPagerAdapter!=null){
            outletDetailsPagerAdapter.setTitles(savedInstanceState.getStringArrayList("amenities"));
        }
    }*/
    public View.OnClickListener eventClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.back_btn:
                    getActivity().onBackPressed();

                    break;
                case R.id.partner_title:
                    onClickPartnership();
                    break;
            }
        }
    };

    private void showCartClearDialog() {
        final WaselDialog dialog =  new WaselDialog(getContext());
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setMessage(getResources().getString(R.string.clear_cart_msg));
        dialog.setPositiveButton(getResources().getString(R.string.clear),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        /*((CartViewDisplayInterface)getActivity()).getCart().clear();
                        outletDetailsPagerAdapter.notifyDataSetChanged();*/
                        ((CartViewDisplayInterface)getActivity()).displayCartView(false);
                        clearData();
                        dialog.cancel();
                        getActivity().onBackPressed();
                    }
                });
        dialog.setNegativeButton(getResources().getString(R.string.cancel),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });
        dialog.setPositiveBtnColor(R.color.ok_btn);
        dialog.setNegetiveBtnColor(android.R.color.white);
        dialog.show();
    }

    //Handling action when device back is pressed
    @Override
    public boolean onPressDeviceBackButton() {
        super.onPressDeviceBackButton();
        if(((CartViewDisplayInterface)getActivity()).getCart().isEmpty()){
            ((LoadFragmentActivity)getActivity()).displayCartView(false);
            clearData();
            return true;
        }
        showCartClearDialog();
        return false;
    }

    private void onClickPartnership() {
        final WaselDialog dialog =  new WaselDialog(getContext());
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setMessage(String.format(getResources().getString(R.string.wasel_partner_dialog_text),outlet.getName()));
        dialog.setNeutralButton(getResources().getString(R.string.ok),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });
        dialog.setNeutralBtnColor(android.R.color.white);
        dialog.show();
    }

    /*View.OnClickListener searchButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SearchFragment searchFragment = new SearchFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(RestaurantDetailsFragment.OUTLET_DETAIL,outletItemSearchList);
            bundle.putString(FROM_SEARCH,"res_details_search");
            searchFragment.setArguments(bundle);
            ((MainActivity)getActivity()).addFragment(searchFragment);
        }
    };*/

    /*
   * Observer for outlet menu downloaded from server & add them to recycler views.
   * */
    public Observer<OutletDetailsResponse> outletMenuObserver = new Observer<OutletDetailsResponse>() {

        @Override
        public void onCompleted() {
            //clear progress bar
            clearSubscriptions();
        }

        @Override
        public void onError(Throwable e) {
//            Toast.makeText(getActivity(),e.getVersion(),Toast.LENGTH_SHORT).show();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
            //clear progress bar
            Log.d("details",ParseServerErrorMessage.getServerErrorMessage(e));
            clearSubscriptions();
        }

        @Override
        public void onNext(OutletDetailsResponse categories) {
            if(categories==null || categories.getOutletItems()==null || categories.getOutletItems().size()==0){
                showEmptyMenuItemLyt();
                return;
            }
            outlet.setAddress(categories.getAddress());
            outlet.setOutletItems(categories.getOutletItems());
            outlet.setAddress(categories.getAddress());

            //Update cart data on UI when activity is destroyed
            ArrayList<OutletItem> cartItems = new ArrayList<OutletItem>();
            cartItems.addAll( WaselApplication.getCart().getItems());
            WaselApplication.getCart().getItems().clear();

            //Parsing the outlet details data
            menu = new LinkedHashMap<>();
            String recommendedStr = WaselApplication.getContext().getResources().getString(R.string.recommended);
            menu.put(recommendedStr, new LinkedHashMap<String, ArrayList<OutletItem>>());
            menu.get(recommendedStr).put(recommendedStr, new ArrayList<OutletItem>());
            for(OutletItemDetails details : categories.getOutletItems()) {
                OutletItemDetails.Parent accordionData = details.getItemCategory();
                HashMap<String, ArrayList<OutletItem>> menuTab;

                if (accordionData.getParent() != null) {
                    OutletItem outletItem = details.getOutletItem();

                    //Adding item to recommended
                    if (details.isRecommend()) {
                        ArrayList<OutletItem> recommendedTab = menu.get(recommendedStr).get(recommendedStr);
                        recommendedTab.add(outletItem);
                    }

                    //Adding item to corresponding subtab
                    //Checking for tab whether it is previously added or not
                    if (!menu.containsKey(accordionData.getParent().getName())) {
                        menu.put(accordionData.getParent().getName(), new LinkedHashMap<String, ArrayList<OutletItem>>());
                    }
                    menuTab = menu.get(accordionData.getParent().getName());

                    //Checking for accordion whether it is previously added or not
                    ArrayList<OutletItem> accordionTab;
                    if (!menuTab.containsKey(accordionData.getName())) {
                        menuTab.put(accordionData.getName(), new ArrayList<OutletItem>());
                    }
                    accordionTab = menuTab.get(accordionData.getName());
                    accordionTab.add(outletItem);

                    //Update quantity when cart is not empty
                    if(cartItems.size()>0){
                        for(int index = 0; index <cartItems.size(); index++){
                            OutletItem cartItem = cartItems.get(index);
                            if(outletItem.getId().equalsIgnoreCase(cartItem.getId())){
                                outletItem.setCartQuantity(cartItem.getCartQuantity()-1);
                                outletItem.setCustomized(true);
                                WaselApplication.getCart().addItem(outletItem);
                                cartItems.remove(index);
                                break;
                            }
                        }
                    }
                }
            }

            //Clearing recommended tab if items are not present
            if(menu.get(recommendedStr).get(recommendedStr).size() == 0){
                menu.keySet().remove(recommendedStr);
            }

            //Creating fragment for each menu-tab
            if(menu!=null) {
                Set<String> categoryNames = menu.keySet();
                for (String categoryName : categoryNames) {
                    SelectOutletItemsFragment outletItemsFragment = new SelectOutletItemsFragment();
                    Bundle bundle = new Bundle();
                    HashMap<String, ArrayList<OutletItem>> items = menu.get(categoryName);
                    bundle.putInt(SelectOutletItemsFragment.AMENITY_ID,amenityId);
                    bundle.putBoolean(SelectOutletItemsFragment.CAN_ACCESS_IMG,outlet.isCanAcessImages());
                    bundle.putString(SelectOutletItemsFragment.CATEGORY, categoryName);
                    bundle.putSerializable(SelectOutletItemsFragment.KEY_OUTLET_ITEMS, items);
                    bundle.putSerializable(SelectOutletItemsFragment.KEY_CART, ((CartViewDisplayInterface)getActivity()).getCart());
                    //Pass 'cart' instance for adding & removing items to it.
                    outletItemsFragment.setArguments(bundle);
                    outletDetailsPagerAdapter.addFragment(outletItemsFragment, categoryName);

                }
                outletDetailsViewPager.setAdapter(outletDetailsPagerAdapter);
            }

            //Update bottom cart layout when activity relaunched after destroy with cart items
            if(WaselApplication.getCart().getItemCount()>0){
                ((CartViewDisplayInterface)getActivity()).displayCartView(true);
            }
        }
    };

    public void clearData() {
        WaselApplication.getCart().clear();
        if(menu!=null){
            menu.clear();
        }
        if(outletDetailsPagerAdapter!=null){
            outletDetailsPagerAdapter.clear();
            outletDetailsPagerAdapter.notifyDataSetChanged();
        }
    }

    private void showEmptyMenuItemLyt() {
        binding.emptySearchLyt.getRoot().setVisibility(View.VISIBLE);
        binding.emptySearchLyt.comingSoonImg.setImageResource(R.drawable.empty_search);
        binding.emptySearchLyt.comingSoonText.setText(resources.getString(R.string.oops));
        binding.emptySearchLyt.comingSoonDesc.setText(String.format(resources.getString(R.string.empty_menu_items)));
    }

    /*
         * An outletDetailsPagerAdapter for creating outlet menu page(a fragment) under view pager
         **/
    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<SelectOutletItemsFragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(SelectOutletItemsFragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void clear() {
            mFragmentTitleList.clear();
            for(SelectOutletItemsFragment frg: mFragmentList){
                frg.clearData();
            }
            mFragmentList.clear();
        }
    }
}
