package com.almoayyed.waseldelivery.ui.home.OutletDetails;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.databinding.OutletItemForSelectionBinding;
import com.almoayyed.waseldelivery.ui.home.OutletDetails.ViewHolders.SelectOutletItemViewHolder;
import com.almoayyed.waseldelivery.models.OutletItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created on 25 Nov 2016 8:29 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class SelectOutletItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    Context context;
    ArrayList<OutletItem> foodItems;
    HashMap<String,Map<String,List<OutletItem>>> foodItemSearchList;


    public SelectOutletItemsAdapter(Context context, ArrayList<OutletItem> items){
        if(items==null){
            foodItems = new ArrayList<>();
        }else {
            this.foodItems = items;
        }
        this.context = context;
    }

    public SelectOutletItemsAdapter(Context context, ArrayList<OutletItem> items, HashMap<String,Map<String,List<OutletItem>>> foodItemSearchList){
        if(items==null){
            foodItems = new ArrayList<>();
        }else {
            this.foodItems = items;
        }
        this.context = context;
        this.foodItemSearchList = foodItemSearchList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        OutletItemForSelectionBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.outlet_item_for_selection, parent, false);
//        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.outlet_item_for_selection, parent, false);
        view = binding.getRoot();
        return new SelectOutletItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof SelectOutletItemViewHolder){
            SelectOutletItemViewHolder selectFoodItemViewHolder = (SelectOutletItemViewHolder) holder;
            OutletItem foodItem = foodItems.get(position);
            if(foodItem.getImageUrl().isEmpty() ){
                selectFoodItemViewHolder.mFoodItemImageView.setVisibility(View.GONE);
            }else{
                selectFoodItemViewHolder.mFoodItemImageView.setVisibility(View.VISIBLE);
            }
            selectFoodItemViewHolder.mFoodItemNameView.setText(foodItem.getName());
            selectFoodItemViewHolder.mFoodItemDescView.setText(foodItem.getItemDescription());
            selectFoodItemViewHolder.mFoodItemPriceView.setText(String.valueOf(foodItem.getTotalPrice()));

        }
    }

    @Override
    public int getItemCount() {
        if(foodItems != null){
            return foodItems.size();
        }
        return 0;
    }

    public void addItems(List<OutletItem> items){
        this.foodItems.clear();
        this.foodItems.addAll(items);
        notifyDataSetChanged();
    }


    /*
       * Check for changes at input text for search of restaurants
       * */
    public TextWatcher inputTextWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start,
                                      int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start,
                                  int before, int count) {
            //TODO: call restaurants search API.
            Log.e("TAG","foodItemSearchList : "+ foodItemSearchList);
            if(foodItemSearchList!=null) {
                List<OutletItem> searchItems = new ArrayList<>();
                Set<String> categoryNames = foodItemSearchList.keySet();

                for (String categoryName : categoryNames) {
                    HashMap<String, List<OutletItem>> searchedFoodItems = (HashMap<String, List<OutletItem>>) foodItemSearchList.get(categoryName);
                    Log.e("TAG","searchedFoodItems : "+ searchedFoodItems.get(categoryName));

                    Set<String> subCategories =searchedFoodItems.keySet();
                    Log.e("TAG","items : "+ subCategories);

                    for (String subCategoryName : subCategories) {
                        List<OutletItem> foodItems = (List<OutletItem>)searchedFoodItems.get(subCategoryName);
                        for(OutletItem foodItem : foodItems){
                            if (foodItem.getName().toLowerCase().contains(s.toString().toLowerCase())) {
                                searchItems.add(foodItem);
                            }
                        }
                    }
                }
                addItems(searchItems);
            }
        }
    };

}

