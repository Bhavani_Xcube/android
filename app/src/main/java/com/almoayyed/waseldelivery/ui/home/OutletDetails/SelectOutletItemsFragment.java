package com.almoayyed.waseldelivery.ui.home.OutletDetails;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.AppLimitations;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.OutletItem;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.CartViewDisplayInterface;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.utils.DividerItemDecoration;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.common.SmoothScrollLinearLayoutManager;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectOutletItemsFragment extends BaseFragment {
    public static final String KEY_OUTLET_ITEMS = "outletItems";
    public static final String KEY_CART = "cart";
    public static final String CATEGORY = "categoryName";
    public static final String AMENITY_ID = "amenityId";
    public static final String CAN_ACCESS_IMG = "canAccessImage";

    private RecyclerView outletItemRecyclerView;
    private HashMap<String, List<OutletItem>> subCategory;
    private List<AbstractFlexibleItem> mItems = new ArrayList<AbstractFlexibleItem>();

    //Holds the current fragment top-level views
    private View fragView;
    private String categoryName;
    private Cart cart;
    private int amenityId;
    private boolean canAccessImage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(fragView ==null){
            fragView = inflater.inflate(R.layout.outlet_item_selection_list,null,false);
            initViews(fragView);
        }


        return fragView;
    }

    private void initViews(View view) {
        outletItemRecyclerView = (RecyclerView) view.findViewById(R.id.food_item_select_list_view);
        outletItemRecyclerView.setLayoutManager(new SmoothScrollLinearLayoutManager(getActivity()));
        /*RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
        outletItemRecyclerView.addItemDecoration(itemDecoration);*/

        Bundle data = getArguments();
        if(data!=null){
            categoryName = data.getString(CATEGORY);
            amenityId = data.getInt(AMENITY_ID);
            canAccessImage = data.getBoolean(CAN_ACCESS_IMG);
            subCategory = (HashMap<String, List<OutletItem>>) data.getSerializable(KEY_OUTLET_ITEMS);
            cart = (Cart) data.getSerializable(KEY_CART);
        }
        setData(); //adding static datasetDisplayHeadersAtStartUp to array list

        /*SelectFoodItemsAdapterTmp foodItemListAdapter = new SelectFoodItemsAdapterTmp(getActivity(),foodItems);
        outletItemRecyclerView.setAdapter(foodItemListAdapter);*/

    }

    @NonNull
    private void setData() {

        Set<String> subCategoryTitles = subCategory.keySet();
        for(String subCategoryStr : subCategoryTitles){
            ExpandableHeaderItem expandableItem = new ExpandableHeaderItem(categoryName+"_"+subCategoryStr);
            expandableItem.setTitle(subCategoryStr);
            List<OutletItem> foodItems = subCategory.get(subCategoryStr);
            int subItems = foodItems.size();
            for (int i=0;i<subItems;i++){
                ExpandableSubItem subItem = new ExpandableSubItem(foodItems.get(i));
                subItem.setId(categoryName+"_"+foodItems.get(i).getId());
                subItem.setTitle(foodItems.get(i).getName());
                subItem.setHeader(expandableItem);
                subItem.setAddItemToCartListener(addItemToCart);
                subItem.setRemoveItemFromCartListener(removeItemFromCart);
                subItem.setShowCustomizeDlgListener(showCustomDlgListener);
                subItem.setAmenityId(amenityId);
                subItem.setCanAccessImage(canAccessImage);
                expandableItem.addSubItem(subItem);
            }
            mItems.add(expandableItem);
        }

        RestaurantMenuAdapter flexibleAdapter = new RestaurantMenuAdapter(mItems);
        //Hiding header when there is only one accordion & expand the lists
        if(subCategoryTitles.size()<=1){
            flexibleAdapter.hideAllHeaders();
            flexibleAdapter.expandAll();
        }

        outletItemRecyclerView.setAdapter(flexibleAdapter);
        /*flexibleAdapter.collapseAll();
        flexibleAdapter.expand(mItems.get(0));*/
    }

    View.OnClickListener addItemToCart = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //add item to cart & inc its quantity
            OutletItem outletItem = (OutletItem) v.getTag();
            if(cart.getItemCount() >= AppLimitations.MAX_CART_ITEMS){
                WaselToast.showToast(getString(R.string.max_item_limit));
                return;
            }

            if(!outletItem.isCustomized()
                    && (outletItem.getCustomizedItems() != null && outletItem.getCustomizedItems().size()>0)){
                showCustomizationDialog(outletItem);
            }else {
                cart.addItem(outletItem);
                if(cart.getItemCount()==1){
                    ((CartViewDisplayInterface)getActivity()).displayCartView(true);
                }
            }
        }
    };

    View.OnClickListener removeItemFromCart = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //remove item from cart  & dec its quantity
            OutletItem outletItem = (OutletItem) v.getTag();
            if(outletItem.getCartQuantity()>0){
                cart.removeItem(outletItem);

                //Clear bottom cart layout when no item is present in cart
                if(cart.isEmpty()){
                    ((CartViewDisplayInterface)getActivity()).displayCartView(false);
                }
            }
        }
    };

    View.OnClickListener showCustomDlgListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //add item to cart & inc its quantity
            OutletItem outletItem = (OutletItem) v.getTag();
            showCustomizationDialog(outletItem);

        }
    };

    private void showCustomizationDialog(OutletItem outletItem) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CheckoutConstants.KEY_CUSTOMIZATION_ITEM,outletItem);

        //Creating customization dialog
        final CustomizeOutletItemDialog dlg = new CustomizeOutletItemDialog();
        dlg.setArguments(bundle);

        View.OnClickListener doneClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Updating customization is updated
                OutletItem outletItem = (OutletItem) v.getTag();
                outletItem.setCustomized(true);

                if(cart.getItemCount() >= AppLimitations.MAX_CART_ITEMS){
                    WaselToast.showToast(getString(R.string.max_item_limit));
                    dlg.dismiss();
                    return;
                }

                //Add item to cart if it not added previously
                if(outletItem.getCartQuantity() == 0){
                    ((CartViewDisplayInterface)getActivity()).displayCartView(true);
                    cart.addItem(outletItem);
                }

                //Update cart values on UI
                cart.notifyTotalPriceChanged();
                dlg.dismiss();
            }
        };

        View.OnClickListener cancelClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: display prompt for clear the content
                OutletItem outletItem = (OutletItem) v.getTag();
                //Set the previous data
                outletItem.revertCustomization();
                dlg.dismiss();
            }
        };

        dlg.setDoneClickListener(doneClickListener);
        dlg.setCancelClickListener(cancelClickListener);

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.add(dlg,"fragment_dialog");
        ft.commit();
    }

    public void clearData() {
        if(subCategory!=null){
            subCategory.clear();
        }
        if(mItems!=null){
            mItems.clear();
        }
    }

    //Solves sticky header in the expandable-collapsable list
    class RestaurantMenuAdapter extends FlexibleAdapter{

        public RestaurantMenuAdapter(@Nullable List items) {
            super(items);
            setDisplayHeadersAtStartUp(true);
            /*enableStickyHeaders();*/
            setStickyHeaders(true, (ViewGroup) fragView.findViewById(R.id.sticky_header_container));
//            hideAllHeaders();
            collapseAll();
        }

        /*@Override
        public ViewGroup getStickySectionHeadersHolder() {
            return (ViewGroup) fragView.findViewById(R.id.sticky_header_contai ner);
        }*/
    }
}
