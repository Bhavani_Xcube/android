package com.almoayyed.waseldelivery.ui.home.OutletDetails.ViewHolders;

import android.animation.Animator;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.views.CircleView;

import java.util.List;

import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.helpers.AnimatorHelper;
import eu.davidea.viewholders.FlexibleViewHolder;

/**
 * Created on 18 Nov 2016 6:13 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Provide a reference to the views for each data item.
 * Complex data labels may need more than one view per item, and
 * you provide access to all the views for a data item in a view holder.
 */
public class ExpandableItemViewHolder extends FlexibleViewHolder {

    public final View mView;
    public final View addItemView;
    public final View removeItemView;
    private ViewDataBinding binding;
    public CircleView foodItemVegType;
    public TextView foodItemName;
//    public ImageView foodItemSpicyType;

    public ExpandableItemViewHolder(View view, FlexibleAdapter adapter) {
        super(view,adapter);
        mView = view;
        foodItemVegType = (CircleView)view.findViewById(R.id.food_item_veg_type);
        foodItemName = (TextView)view.findViewById(R.id.food_item_name);
//        foodItemSpicyType = (ImageView)view.findViewById(R.id.food_item_spicy_type);

        addItemView = view.findViewById(R.id.food_item_add);
        removeItemView = view.findViewById(R.id.food_item_remove);
        binding = DataBindingUtil.bind(view);
    }

    public ViewDataBinding getBinding() {
        return binding;
    }

    @Override
    public void scrollAnimators(@NonNull List<Animator> animators, int position, boolean isForward) {
        AnimatorHelper.scaleAnimator(animators, itemView, 0f);
    }
}