package com.almoayyed.waseldelivery.ui.home.OutletDetails.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.views.CircleView;
import com.almoayyed.waseldelivery.views.CustomBudgetView;

/**
 * Created on 24 Nov 2016 3:06 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

//Holds food list item views
public class SelectOutletItemViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final ImageView mFoodItemImageView;
    public final CircleView mFoodItemVegTypeView;
    public final TextView mFoodItemNameView;
    public final TextView mFoodItemDescView;
    public final TextView mFoodItemPriceView;
    public final ImageView mFoodItemAddView;
    public final ImageView mFoodItemRemoveView;
    public final TextView mFoodItemCartQuantity;

    public SelectOutletItemViewHolder(View view) {
        super(view);
        mView = view;
        mFoodItemImageView = (ImageView)view.findViewById(R.id.food_item_image);
        mFoodItemVegTypeView = (CircleView) view.findViewById(R.id.food_item_veg_type);
        mFoodItemNameView = (TextView) view.findViewById(R.id.food_item_name);
        mFoodItemDescView = (TextView)view.findViewById(R.id.food_item_desc);
        mFoodItemPriceView = (TextView)view.findViewById(R.id.food_item_price);
        mFoodItemAddView = (ImageView)view.findViewById(R.id.food_item_add);
        mFoodItemRemoveView = (ImageView)view.findViewById(R.id.food_item_remove);
        mFoodItemCartQuantity = (TextView)view.findViewById(R.id.food_item_cart_quantity);
    }
}