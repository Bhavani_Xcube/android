package com.almoayyed.waseldelivery.ui.home.Outlets;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.almoayyed.waseldelivery.models.Amenity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 14 Dec 2016 6:49 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */


/** An adapter for creating categories list like food, electronics, jewellery etc
     **/
public class CategoryViewPagerAdapter extends FragmentStatePagerAdapter {
    private final List<OutletsFragment> mFragmentList = new ArrayList<>();
    private final List<Amenity> mFragmentTitleList = new ArrayList<>();

    public CategoryViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentTitleList.size();
    }

    public void addFragment(OutletsFragment fragment, Amenity amenity) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(amenity);
    }

    /*Clear the existing outlets information & load new */
    public void clearData() {
        for (OutletsFragment fragView : mFragmentList) {
            fragView.refreshOutlets();
        }
    }

    public int getAmenityIndex(int amenityId){
        for (int index = 0; index <mFragmentTitleList.size() ; index++){
            if(mFragmentTitleList.get(index).getId() == amenityId){
                return index;
            }
        }
        return -1;
    }
    @Override
    public CharSequence getPageTitle(int position) {

          /*  Drawable image = ContextCompat.getDrawable(getActivity(), R.drawable.category_food_selector);
            image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
            SpannableString sb = new SpannableString(mFragmentTitleList.get(position));
            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
            sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);*/
        return null;
    }


    public int getAmenitiesList() {
        for (OutletsFragment fragment : mFragmentList) {
            return fragment.getAmenityId();
        }
        return 0;
    }
}