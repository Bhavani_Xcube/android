package com.almoayyed.waseldelivery.ui.home.Outlets;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.data_saving.AmenityPreference;
import com.almoayyed.waseldelivery.databinding.HomePageLayoutBinding;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.models.Amenity;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.databinding_eventhandler.ClickEventHandler;
import com.almoayyed.waseldelivery.ui.location.LocationActivity;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.utils.NetworkConnection;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.utils.ScreenUtils;
import com.almoayyed.waseldelivery.views.TintedImageView;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 17 Nov 2016 6:04 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class HomeFragment extends BaseFragment {

    public static final int MANUAL_LOC_REQUEST_CODE = 200;

    private CategoryViewPagerAdapter adapter;
    private UserLocation userLocation;
    private HomePageLayoutBinding binding;
    private Observable<List<Amenity>> amenitiesObservable;
    private Subscription amenitySubscribe;
    public static final String AMENITY_ID = "amenityId";
    private ClickEventHandler eventHandler;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onStart() {
        super.onStart();

        if(isFragmentVisible())
        {
            refreshData();
        }

        //Load amenities if they are not loaded earlier
        //When user shift to another tab before loading amenities.
        if(adapter.getCount()==0){
            getAmenities();
        }
    }
    //Update page data while internet is altered
    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            ((OutletsFragment)adapter.getItem(position)).updateData();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
    private void refreshData() {
        if (LocationPreference.getCountryName().equalsIgnoreCase("Bahrain")) {
            binding.outOfBahrainView.getRoot().setVisibility(View.GONE);
            binding.categoryTabs.setVisibility(View.VISIBLE);
            binding.categoryViewpager.setVisibility(View.VISIBLE);

            Bundle data = getArguments();
            if(data!=null && data.containsKey(AMENITY_ID)){
                int amenityId = data.getInt(AMENITY_ID);
                data.remove(AMENITY_ID);
                binding.categoryViewpager.setCurrentItem(adapter.getAmenityIndex(amenityId), true);

            }

        } else {
            binding.outOfBahrainView.getRoot().setVisibility(View.VISIBLE);
            binding.outOfBahrainView.comingSoonImg.setImageResource(R.drawable.empty_food);
            binding.outOfBahrainView.comingSoonText.setText(getResources().getString(R.string.coming_soon_txt));
            binding.outOfBahrainView.comingSoonDesc.setText(getResources().getString(R.string.coming_soon_desc));
            binding.categoryViewpager.setVisibility(View.INVISIBLE);
            binding.categoryTabs.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (fragView == null) {
            binding = DataBindingUtil.inflate(
                    inflater, R.layout.home_page_layout, container, false);
            fragView = binding.getRoot();

            //binding user location text click listener
            eventHandler = new ClickEventHandler();
            binding.setEventHandler(eventHandler);
            //binding user location details
            userLocation = new UserLocation();
            binding.setUserLoc(userLocation);

            initViews();
            getAmenities();
        }


        return fragView;
    }

    private void getAmenities()
    {
        if(NetworkConnection.isNetworkAvailable()) {
            amenitiesObservable = WaselDeliveryService.getService().getAmenities();
            if(amenitySubscribe !=null){
                removeFromSubscription(amenitySubscribe);
            }
            amenitySubscribe = addObserver(amenitiesObservable, amenitiesObserver);

        }else{
            WaselToast.showToast(getContext().getResources().getString(R.string.check_internet));
        }
    }

    private void initViews() {

        userLocation.setFormattedAddress(LocationPreference.getCurrentLocName());
        userLocation.setLatitude(LocationPreference.getCurrentLat());
        userLocation.setLongitude(LocationPreference.getCurrentLng());

        eventHandler.onClickListener = locationChangeListener();


        //TODO: need to apply databinding (uses BindingAdapter concept)
        adapter = new CategoryViewPagerAdapter(getChildFragmentManager());
        binding.categoryViewpager.addOnPageChangeListener(pageChangeListener);
        binding.categoryTabs.setupWithViewPager(binding.categoryViewpager);

        /*Settings.System.putInt(getActivity().getContentResolver(),
                Settings.System.SOUND_EFFECTS_ENABLED, 1);*/

        KeyboardHandler.setKeyboardHandler(binding.getRoot());

        mProgressBarHandler = new ProgressBarHandler(getContext(), (RelativeLayout) binding.getRoot().findViewById(R.id.list_frame));
        //Loading categories static data
        //loadAmenities(amenityId);
    }

    private TabLayout.Tab setTabIcon(TabLayout.Tab tab, int resId) {
        View view = LayoutInflater.from(binding.categoryTabs.getContext()).inflate(R.layout.category_tab_icon_lay, null);

        ImageView mImageView = (ImageView) view.findViewById(R.id.custom_tab_icon);
        mImageView.setImageResource(resId);
        return tab.setCustomView(mImageView);
    }

    /*
       * Click listener to change the location
       * */
    @NonNull
    private View.OnClickListener locationChangeListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Opens location manual entry screen
                Intent i = new Intent(getActivity(), LocationActivity.class);
                getActivity().startActivityForResult(i, MANUAL_LOC_REQUEST_CODE);
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case (MANUAL_LOC_REQUEST_CODE): {
                if (resultCode == Activity.RESULT_OK) {
                    userLocation.setFormattedAddress(data.getStringExtra("location"));
                    userLocation.setLatitude(data.getDoubleExtra("lat", userLocation.getLatitude()));
                    userLocation.setLongitude(data.getDoubleExtra("lng", userLocation.getLongitude()));
                    //TODO: Update the corresponding category list

                    if (adapter != null) {
                        adapter.clearData();
                    }

                    refreshData();
                    break;
                }
            }
        }
    }

    /*
   * Observer for restaurants list downloaded from server & add them to recycler view.
   * */
    Observer<List<Amenity>> amenitiesObserver = new Observer<List<Amenity>>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            WaselToast.showToast(getServerErrorMessage(e));
            mProgressBarHandler.hide();
        }

        @Override
        public void onNext(List<Amenity> amenities) {
            Set<String> ids = new HashSet<>();
            if (amenities != null && amenities.size() > 0) {
                //Adding amenities title
                for (Amenity amenity : amenities) {
                    OutletsFragment outletsFragment = new OutletsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(OutletsFragment.AMENITY_ID, amenity.getId());
                    outletsFragment.setArguments(bundle);
                    adapter.addFragment(outletsFragment, amenity);
                    ids.add(amenity.getId().toString()+ "-"+amenity.getImageUrl().toString()+"-"+amenity.getName());
                }
                AmenityPreference amenityPreference = AmenityPreference.getAmenityPreference();
                amenityPreference.saveAmenityIds(ids);

                //Adding adapter to view pager
                String[] array = WaselApplication.getContext().getResources().getStringArray(R.array.categories_array);
                binding.categoryViewpager.setAdapter(adapter);


                //Loading category tab icons from drawables
                for (int subIndex = 0; subIndex < amenities.size(); subIndex++) {
                    Amenity amenity  = amenities.get(subIndex);
                    LayoutInflater inflater = (LayoutInflater) WaselApplication.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View tabView = inflater.inflate(R.layout.tab_layout, null, false);
                    ((TextView) tabView.findViewById(R.id.tab_text)).setText(amenity.getName());
                    boolean imageFound = false;
                    for (int index = 0; index < array.length; index++) {
                        String[] splittedItem = array[index].split("-");
                        if (amenity.getImageUrl() == Integer.parseInt(splittedItem[0])) {
                            int resID = WaselApplication.getContext().getResources().getIdentifier(splittedItem[2], "drawable", WaselApplication.getContext().getPackageName());
                            /*ImageView categoryImageView = (ImageView) tabView.findViewById(R.id.tab_icon);
                            categoryImageView.setImageResource(resID);*/
                           /* ImageButton categoryImageView = (ImageButton) tabView.findViewById(R.id.tab_icon);
                            categoryImageView.setImageResource(resID);*/
                            TintedImageView categoryImageView = (TintedImageView) tabView.findViewById(R.id.tab_icon);
                            categoryImageView.setImageResource(resID);
                            imageFound = true;
                            break;
                        }
                    }
                    if(!imageFound){
                        TintedImageView categoryImageView = (TintedImageView) tabView.findViewById(R.id.tab_icon);
                        categoryImageView.setImageResource(R.drawable.category_miscellaneous_selector);
                    }

                    binding.categoryTabs.getTabAt(subIndex).setCustomView(tabView);
                }

                adjustTabLayoutBounds(binding.categoryTabs,getDisplayMetrics());

               /*  float myTabLayoutSize = 360;
                ScreenUtils screenUtils = new ScreenUtils(getContext());
                if (screenUtils.getWidth() > binding.categoryTabs.getWidth() ){
                    tabLayout.setTabMode(TabLayout.MODE_FIXED);
                } else {
                    tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                }
                ViewGroup.LayoutParams params = binding.categoryTabs.getLayoutParams();
                params.width = screenUtils.getWidth();*/
                clearSubscriptions();

                refreshData();
            }
        }
    };

    private void adjustTabLayoutBounds(final TabLayout tabLayout, final DisplayMetrics displayMetrics) {

        final ViewTreeObserver vto = tabLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                tabLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                int totalTabPaddingPlusWidth = 0;
                int maxTabWidth=0;
                for(int i=0; i < tabLayout.getTabCount(); i++){

                    final View tabView = ((LinearLayout)tabLayout.getChildAt(0)).getChildAt(i);
                    totalTabPaddingPlusWidth += (tabView.getMeasuredWidth()/* + tabView.getPaddingLeft() + tabView.getPaddingRight()*/);

                    if(maxTabWidth<tabView.getMeasuredWidth()){
                        maxTabWidth = tabView.getMeasuredWidth();
                    }
                    Log.e("tabLayout","tabLayout="+tabLayout.getTabAt(i).getCustomView().getMeasuredWidth() + "="+tabView.getMeasuredWidth());
                }

                ScreenUtils screenUtils = new ScreenUtils(getContext());
                if (totalTabPaddingPlusWidth <= screenUtils.getWidth()){

                    int diff = screenUtils.getWidth() - totalTabPaddingPlusWidth;
                    int avg = diff/tabLayout.getTabCount();
                    if(maxTabWidth<(screenUtils.getWidth()/tabLayout.getTabCount())){
                        tabLayout.setTabMode(TabLayout.MODE_FIXED);
                        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                    }else{
                        for(int i=0; i < tabLayout.getTabCount(); i++){
                            final View tabView = ((LinearLayout)tabLayout.getChildAt(0)).getChildAt(i);
                            tabView.setMinimumWidth(tabView.getMeasuredWidth()+avg);
                            Log.e("tabLayout","tabLayout="+tabLayout.getTabAt(i).getCustomView().getMeasuredWidth() + "="+tabView.getMeasuredWidth() + "="+tabView.getWidth()+avg);
                        }

                    }


                }else{
                    tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                }

//                tabLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
            }
        });
    }

    public DisplayMetrics getDisplayMetrics() {

        final WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        final Display display = wm.getDefaultDisplay();
        final DisplayMetrics displayMetrics = new DisplayMetrics();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            display.getMetrics(displayMetrics);

        }else{
            display.getRealMetrics(displayMetrics);
        }

        return displayMetrics;
    }
}
