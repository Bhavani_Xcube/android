package com.almoayyed.waseldelivery.ui.home.Outlets;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.models.Cuisine;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.home.Outlets.ViewHolders.OutletFilterViewHolder;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.CustomBudgetView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created on 25 Nov 2016 8:29 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OutletFilterActivity extends AppCompatActivity implements View.OnClickListener, OutletFilterViewHolder.ItemClickListener {

    public static String FILTER_MAP = "filter_map";
    public static String RATING_PRIORITY = "rating";
    public static String BUDGET = "budgets";
    public static String CUISINES_LIST = "cuisines";

    private static final int LOW_BUDGET = 0;
    private static final int MEDIUM_BUDGET = 1;
    private static final int HIGH_BUDGET = 2;

    private RecyclerView restaurantFilterListView;
    private OutletFilterListAdapter restaurantFilterRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;
    private boolean loading = false;
    private boolean loadMoreData = true;
    private RelativeLayout rating_priority;
    private LinearLayout low_budget,medium_budget,high_budget;
    private TextView done, clear;

    private HashMap<String, Object> map = new HashMap<String , Object>();
    private ArrayList<Integer> budgetList = null;
    private ArrayList<Integer> cuisineList = null;

    private ProgressBarHandler mProgressBarHandler;
    private Observable<List<Cuisine>> cuisineObservable;
    private Subscription subscribe;
    private List<Integer> cuisineIdsList;
    private CustomBudgetView low_budget_icon;
    private CustomBudgetView medium_budget_icon;
    private CustomBudgetView high_budget_icon;
    private RelativeLayout outletFilterMainLyt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet_filter);

        initViews();

    }

    private void initViews(){
        linearLayoutManager = new LinearLayoutManager(this);
        restaurantFilterListView = (RecyclerView) findViewById(R.id.restaurant_filter_list);
        restaurantFilterListView.setLayoutManager(linearLayoutManager);

        outletFilterMainLyt = (RelativeLayout) findViewById(R.id.outlet_filter_main_lyt);
        rating_priority = (RelativeLayout) findViewById(R.id.rating_priority);
        low_budget = (LinearLayout) findViewById(R.id.low_budget);
        medium_budget = (LinearLayout) findViewById(R.id.medium_budget);
        high_budget = (LinearLayout) findViewById(R.id.high_budget);
        low_budget_icon = (CustomBudgetView)findViewById(R.id.low_budget_icon);
        medium_budget_icon = (CustomBudgetView)findViewById(R.id.medium_budget_icon);
        high_budget_icon = (CustomBudgetView)findViewById(R.id.high_budget_icon);
        done = (TextView) findViewById(R.id.filter_done);
        clear = (TextView) findViewById(R.id.filter_clear);

        rating_priority.setOnClickListener(this);
        low_budget.setOnClickListener(this);
        medium_budget.setOnClickListener(this);
        high_budget.setOnClickListener(this);
        done.setOnClickListener(this);
        clear.setOnClickListener(this);

        restaurantFilterListView.addOnScrollListener(onScrollListener);

        if(restaurantFilterRecyclerViewAdapter==null){
            restaurantFilterRecyclerViewAdapter =
                    new OutletFilterListAdapter(this, null,null);
        }
        restaurantFilterListView.setAdapter(restaurantFilterRecyclerViewAdapter);

        mProgressBarHandler = new ProgressBarHandler(this, outletFilterMainLyt);
        mProgressBarHandler.show();

        //Reading previous filter data
        HashMap<String, Object>  filterData = (HashMap<String, Object> ) getIntent().getSerializableExtra(FILTER_MAP);
        if(filterData!=null){
            // HashMap<String, Object>  filterData = (HashMap<String, Object> )bundle.getSerializable(FILTER_MAP);
            rating_priority.setSelected(filterData.containsKey(RATING_PRIORITY)?(boolean)filterData.get(RATING_PRIORITY):false);
            budgetList = filterData.containsKey(BUDGET)?(ArrayList<Integer>) filterData.get(BUDGET):new ArrayList<Integer>();
            cuisineList = new ArrayList<Integer>();
            cuisineIdsList = filterData.containsKey(OutletFilterActivity.CUISINES_LIST)?(List<Integer>) filterData.get(CUISINES_LIST):new ArrayList<Integer>();

            low_budget.setSelected(budgetList.contains(LOW_BUDGET));
            medium_budget.setSelected(budgetList.contains(MEDIUM_BUDGET));
            high_budget.setSelected(budgetList.contains(HIGH_BUDGET));
            int selColor = ContextCompat.getColor(this, R.color.colorPrimary);
            int unSelColor = ContextCompat.getColor(this, R.color.textColor7);
            if(budgetList.contains(LOW_BUDGET)){
                low_budget_icon.setPrimaryColors(selColor,Color.TRANSPARENT,selColor);
            }else {
                low_budget_icon.setPrimaryColors(unSelColor,Color.TRANSPARENT,unSelColor);
            }
            if(budgetList.contains(MEDIUM_BUDGET)){
                medium_budget_icon.setPrimaryColors(selColor,Color.TRANSPARENT,selColor);
            }else {
                medium_budget_icon.setPrimaryColors(unSelColor,Color.TRANSPARENT,unSelColor);
            }
            if(budgetList.contains(HIGH_BUDGET)){
                high_budget_icon.setPrimaryColors(selColor,Color.TRANSPARENT,selColor);
            }else {
                high_budget_icon.setPrimaryColors(unSelColor,Color.TRANSPARENT,unSelColor);
            }
        }else{
            rating_priority.setSelected(false);
            budgetList = new ArrayList<Integer>();
            cuisineList = new ArrayList<Integer>();
        }

        map.put(RATING_PRIORITY,rating_priority.isSelected());
        map.put(BUDGET,budgetList);
        map.put(CUISINES_LIST,cuisineIdsList);

        cuisineObservable = WaselDeliveryService.getService().getCuisines();
        subscribe = cuisineObservable.subscribe(cuisineObserver);

        if(!rating_priority.isSelected() && budgetList.size() == 0 && cuisineList.size() == 0 ){
            done.setText(getResources().getString(R.string.done));
        }else{
            done.setText(getResources().getString(R.string.apply));
        }
    }
    /*
   * Scrolling listener of recycler listview.
   * Handles pagination
   * */
    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int totalItemCount = linearLayoutManager.getItemCount();
            int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
            if (!loading && loadMoreData ) {
                // End has been reached
                // Do something
                loading = true;
            }
        }
    };

    @Override
    public void onClick(View view, int position) {
        //  Toast.makeText(this,"Selected pos : "+ position,Toast.LENGTH_LONG).show();
        List<Cuisine> cuisines_List = restaurantFilterRecyclerViewAdapter.changeImage(position);
        if(cuisines_List.get(position).isCheckBoxEnabled) {
            cuisineList.add(position);
            map.put(CUISINES_LIST, (Object)cuisineList);
        }else{
            cuisineList.removeAll(Arrays.asList(position));
        }
        if(!rating_priority.isSelected() && budgetList.size() == 0 && cuisineList.size() == 0 ){
            done.setText(getResources().getString(R.string.done));
        }else{
            done.setText(getResources().getString(R.string.apply));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rating_priority:
                rating_priority.setSelected(!rating_priority.isSelected());
                map.put(RATING_PRIORITY, rating_priority.isSelected());

                break;

            case R.id.low_budget:
                low_budget.setSelected(!low_budget.isSelected());

                if(low_budget.isSelected()){
                    budgetList.add(LOW_BUDGET);
                    map.put(BUDGET, (Object)budgetList);
                    int color = ContextCompat.getColor(this, R.color.colorPrimary);
                    low_budget_icon.setPrimaryColors(color, Color.TRANSPARENT,color);
                }else{
                    budgetList.removeAll(Arrays.asList(LOW_BUDGET));
                    int color = ContextCompat.getColor(this, R.color.textColor7);
                    low_budget_icon.setPrimaryColors(color, Color.TRANSPARENT,color);
                }
                break;
            case R.id.medium_budget:
                medium_budget.setSelected(!medium_budget.isSelected());
                if(medium_budget.isSelected()){
                    budgetList.add(MEDIUM_BUDGET);
                    map.put(BUDGET, (Object)budgetList);
                    int color = ContextCompat.getColor(this, R.color.colorPrimary);
                    medium_budget_icon.setPrimaryColors(color, Color.TRANSPARENT,color);
                }else{
                    budgetList.removeAll(Arrays.asList(MEDIUM_BUDGET));
                    int color = ContextCompat.getColor(this, R.color.textColor7);
                    medium_budget_icon.setPrimaryColors(color, Color.TRANSPARENT,color);
                }
                break;
            case R.id.high_budget:
                high_budget.setSelected(!high_budget.isSelected());
                if(high_budget.isSelected()){
                    budgetList.add(HIGH_BUDGET);
                    map.put(BUDGET, (Object)budgetList);
                    int color = ContextCompat.getColor(this, R.color.colorPrimary);
                    high_budget_icon.setPrimaryColors(color, Color.TRANSPARENT,color);
                }else{
                    budgetList.removeAll(Arrays.asList(HIGH_BUDGET));
                    int color = ContextCompat.getColor(this, R.color.textColor7);
                    high_budget_icon.setPrimaryColors(color, Color.TRANSPARENT,color);
                }
                break;
            case  R.id.filter_clear:
                clearAllFields();
                break;
            case R.id.filter_done:
                Intent i = new Intent();
                //TODO: get cuisines ids from cuisine list(which holds only positions)
                List<Integer> cuisines = restaurantFilterRecyclerViewAdapter.getSelectedCuisineIds();
                map.put(CUISINES_LIST,cuisines);
                if(!rating_priority.isSelected() && budgetList.size() == 0 && cuisineList.size() == 0){
                    map.clear();
                }
                i.putExtra("filter_map", map);
                setResult(RESULT_OK, i);
                finish();
                break;
            default:

                break;
        }
        if(!rating_priority.isSelected() && budgetList.size() == 0 && cuisineList.size() == 0 ){
            done.setText(getResources().getString(R.string.done));
        }else{
            done.setText(getResources().getString(R.string.apply));
        }
    }

    private void clearAllFields(){
        rating_priority.setSelected(false);
        low_budget.setSelected(false);
        medium_budget.setSelected(false);
        high_budget.setSelected(false);
        int unSelColor = ContextCompat.getColor(this, R.color.textColor7);

        low_budget_icon.setPrimaryColors(unSelColor,Color.TRANSPARENT,unSelColor);
        medium_budget_icon.setPrimaryColors(unSelColor,Color.TRANSPARENT,unSelColor);
        high_budget_icon.setPrimaryColors(unSelColor,Color.TRANSPARENT,unSelColor);
        restaurantFilterRecyclerViewAdapter.clearCheckedList();
        budgetList.clear();
        cuisineList.clear();
        map.clear();
    }

    @Override
    public void onResume() {
        super.onResume();

        //Listens for the server response
        if(cuisineObservable!=null && subscribe!=null && subscribe.isUnsubscribed()){
            subscribe = cuisineObservable.subscribe(cuisineObserver);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        //Stop Listens for the server response
        if(cuisineObservable!=null && subscribe!=null && !subscribe.isUnsubscribed()){
            subscribe.unsubscribe();
        }
    }

    /*
    * Observer for cuisine list downloaded from server & add them to recycler view.
    * */
    Observer<List<Cuisine>> cuisineObserver = new Observer<List<Cuisine>>() {

        @Override
        public void onCompleted() {
            //TODO: dismiss progress dialog
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            //TODO: dismiss progress dialog
            mProgressBarHandler.hide();
        }

        @Override
        public void onNext(List<Cuisine> cuisines) {

            List<Cuisine> tmpCuisineList = new ArrayList<>();

            loadMoreData = false;

            if(cuisines != null && cuisines.size() >0){
                tmpCuisineList.addAll(cuisines);
                loadMoreData = true;
            }
            Log.d("loading","cuisineList:"+tmpCuisineList.get(0) + "  "+tmpCuisineList.size());
            if(restaurantFilterRecyclerViewAdapter == null){
                restaurantFilterRecyclerViewAdapter =
                        new OutletFilterListAdapter(OutletFilterActivity.this, tmpCuisineList,cuisineIdsList);
                restaurantFilterListView.setAdapter(restaurantFilterRecyclerViewAdapter);
            }else {
                int refreshPoint = restaurantFilterRecyclerViewAdapter.getItemCount();
                restaurantFilterRecyclerViewAdapter.setCuisineIds(cuisineIdsList);
                restaurantFilterRecyclerViewAdapter.addCuisinesToList(tmpCuisineList);
                restaurantFilterRecyclerViewAdapter.notifyItemRangeInserted(refreshPoint,tmpCuisineList.size());
            }

            loading = false;

        }
    };


}
