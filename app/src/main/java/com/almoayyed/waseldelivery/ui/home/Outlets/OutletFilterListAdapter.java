package com.almoayyed.waseldelivery.ui.home.Outlets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.models.Cuisine;
import com.almoayyed.waseldelivery.ui.home.Outlets.ViewHolders.OutletFilterViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 28 Sep 2016 11:23 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OutletFilterListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Integer> cuisineIds;
    Context context;
    private final List<Cuisine> cuisineFilterList;
    public OutletFilterViewHolder.ItemClickListener clickListener;

    public OutletFilterListAdapter(Context context, List<Cuisine> cuisineFilterList, List<Integer> cuisineIds) {

        this.clickListener = (OutletFilterViewHolder.ItemClickListener) context;
        if (cuisineFilterList == null) {
            this.cuisineFilterList = new ArrayList<>();
        } else {
            this.cuisineFilterList = cuisineFilterList;
        }
        this.cuisineIds = cuisineIds;
        this.context = context;
        updateUI();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.outlet_filter_items, parent, false);
        return new OutletFilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof OutletFilterViewHolder) {
            Cuisine tmpFilterCuisine = cuisineFilterList.get(position);
            OutletFilterViewHolder restaurantFilterViewHolder = (OutletFilterViewHolder) holder;
            restaurantFilterViewHolder.mCuisineNameView.setText(tmpFilterCuisine.getName());
            if(tmpFilterCuisine.isCheckBoxEnabled()){
                restaurantFilterViewHolder.mCuisineNameView.setSelected(true);
                restaurantFilterViewHolder.mCheckbox.setButtonDrawable(R.drawable.tick_filter);
            }else{
                restaurantFilterViewHolder.mCuisineNameView.setSelected(false);
                restaurantFilterViewHolder.mCheckbox.setButtonDrawable(context.getResources().getDrawable(android.R.color.transparent));
            }
                restaurantFilterViewHolder.setClickListener(clickListener);
        }
    }

    @Override
    public int getItemCount() {
        if (cuisineFilterList != null) {
            return cuisineFilterList.size();
        }
        return cuisineFilterList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public List<Cuisine> changeImage(int index) {
        Log.e("TAG","cuisineFilterList.get(index).isCheckBoxEnabled : "+ cuisineFilterList.get(index).isCheckBoxEnabled);
        if(!cuisineFilterList.get(index).isCheckBoxEnabled) {
            cuisineFilterList.get(index).setCheckBoxEnabled(true);
            notifyItemChanged(index);
        }else{
            cuisineFilterList.get(index).setCheckBoxEnabled(false);
            notifyItemChanged(index);
        }
        return cuisineFilterList;
    }

    public void clearCheckedList(){
        for(int i=0;i<cuisineFilterList.size();i++){
            cuisineFilterList.get(i).setCheckBoxEnabled(false);
            notifyItemChanged(i);
        }
    }


   /* public void setClickListener(RestaurantFilterViewHolder.ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }*/


    public List<Integer> getSelectedCuisineIds(){
        List<Integer> cuisinesIds = new ArrayList<>();
        for(Cuisine tmpCuisine : cuisineFilterList){
            if(tmpCuisine.isCheckBoxEnabled){
                cuisinesIds.add(Integer.parseInt(tmpCuisine.getId()));
            }
        }
        return cuisinesIds;
    }

    public void updateUI(){
        if(cuisineFilterList!=null && cuisineFilterList.size()>0 && cuisineIds!=null && cuisineIds.size()>0){
            for(int j=0;j<cuisineFilterList.size();j++){
                if(cuisineIds.contains(Integer.parseInt(cuisineFilterList.get(j).getId()))){
                    cuisineFilterList.get(j).setCheckBoxEnabled(true);
                }
            }
        }
    }
   /* CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
           // compoundButton.setButtonDrawable(isChecked ? compoundButton.setButtonDrawable
            // (context.getResources().getDrawable(android.R.color.transparent))  : R.drawable.check );
            if (isChecked){
                Toast.makeText(context, "isChecked : "+isChecked , Toast.LENGTH_SHORT).show();
                compoundButton.setButtonDrawable(R.drawable.tick_filter);

            }else{
                Toast.makeText(context, "isChecked else: "+isChecked , Toast.LENGTH_SHORT).show();
                compoundButton.setButtonDrawable(context.getResources().getDrawable(android.R.color.transparent));
            }
        }
    };*/

    public void addCuisinesToList(List<Cuisine> cuisineList) {
        cuisineFilterList.addAll(cuisineList);
        updateUI();
        notifyDataSetChanged();
    }

    public void setCuisineIds(List<Integer> cuisineIds){
        this.cuisineIds = cuisineIds;
    }
}
