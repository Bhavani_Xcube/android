package com.almoayyed.waseldelivery.ui.home.Outlets;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.databinding_eventhandler.LoadImage;
import com.almoayyed.waseldelivery.models.Amenity;
import com.almoayyed.waseldelivery.ui.LoadMoreViewHolder;
import com.almoayyed.waseldelivery.ui.home.Outlets.ViewHolders.OutletViewHolder;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.utils.ScreenUtils;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.almoayyed.waseldelivery.constants.AmenityConstants.setAppropriateCatagoryImage;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OutletsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Serializable{
    private final int screenWidth;
    Context context;
    private final int OUTLET_ITEM_TYPE = 1;
    private final int LOAD_MORE_TYPE = 2;

    //holds the restaurant list data
    private final ArrayList<Outlet> outletList;
    //StartIndex used for pagination of outlet list loaded from server
    private int startIndex = 0;
    //maxResult specifies no.of outlet items load from server per api call
    final int maxResult = 10;
    //Holds current amenity or category id
    private int amenityId;
    //Holds current loading more outlet status
    private boolean loadMore = false;

    View.OnClickListener outletClickListener;
    public OutletsAdapter(Context context, int amenityId, ArrayList<Outlet> outlets){
        this.amenityId = amenityId;

        if(outlets == null){
            this.outletList = new ArrayList<>();
        }else {
            this.outletList = outlets;

        }

        this.context = context;

        ScreenUtils screenUtils = new ScreenUtils(context);
        screenWidth = screenUtils.getWidth();
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(viewType == OUTLET_ITEM_TYPE){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.outlet_item, parent, false);
            return new OutletViewHolder(view);
        }else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_more_lyt, parent, false);
            return new LoadMoreViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder.getItemViewType() ==OUTLET_ITEM_TYPE){
            OutletViewHolder restaurantViewHolder = (OutletViewHolder) holder;
            Outlet tmpOutlet = outletList.get(position);

            //Setting binding variables
            restaurantViewHolder.getBinding().setVariable(BR.outlet,tmpOutlet);

            restaurantViewHolder.itemView.setOnClickListener(outletClickListener);
            restaurantViewHolder.itemView.setTag(position);

            //image set using databinding concept

                LoadImage.loadImage(restaurantViewHolder.mRestaurantIconView, tmpOutlet.getImageUrl(), context.getResources().getDrawable(R.drawable.default_outlet), screenWidth, restaurantViewHolder.mRestaurantIconView.getHeight());

        }
    }

    @Override
    public int getItemCount() {
        if(outletList != null){
            return loadMore? outletList.size() + 1 : outletList.size();
        }
        return 0;
    }

    public int getOutletsCount() {
        if(outletList != null){
            return outletList.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if(position<outletList.size()){
            return OUTLET_ITEM_TYPE;
        }else {
            return LOAD_MORE_TYPE;
        }
    }

    public Outlet getOutletAtPos(int position){
        if(position<0 || position > outletList.size()-1){
            return null;
        }
        return outletList.get(position);
    }

    public void clearRestaurantsList(){
        startIndex = 0;
        outletList.clear();
        loadMore = false;
        notifyDataSetChanged();
    }

    public void addRestaurantsToList(List<Outlet> resList) {
        int refreshPoint  = getItemCount();
        outletList.addAll(resList);
        notifyItemRangeInserted(refreshPoint, outletList.size());
    }

    public int getAmenityId() {
        return amenityId;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getMaxResult() {
        return maxResult;
    }

    public void incStartIndexByValue(int incrementValue) {
        this.startIndex += incrementValue;
    }

    public void setOutletClickListener(View.OnClickListener outletClickListener) {
        this.outletClickListener = outletClickListener;
    }

    public ArrayList<Outlet> getOutletList(){
        return outletList;
    }

    public void showLoadMoreStatus(){
        loadMore = true;
        notifyDataSetChanged();
    }

    public void hideLoadMoreStatus(){
        loadMore = false;
        notifyDataSetChanged();
    }

    public void setAmenityId(int amenityId) {
        this.amenityId = amenityId;
    }
}
