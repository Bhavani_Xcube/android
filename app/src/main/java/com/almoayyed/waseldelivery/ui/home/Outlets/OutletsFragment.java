package com.almoayyed.waseldelivery.ui.home.Outlets;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.FragmentsIndex;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.databinding.OutletItemsBinding;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.network_interface.NetworkStatusException;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletsReqBody;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.LoadFragmentActivity;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.home.OutletDetails.OutletDetailsFragment;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.NetworkConnection;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OutletsFragment extends BaseFragment {
    public static final String AMENITY_ID = "amenityId";
    public static final String POSITION = "pos";
    OutletsAdapter outletListRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    private boolean outletsLoading = false;
    private int visibleThreshold = 2;
    private boolean hasMoreOutlets = true;

    private Observable outletsObservable = null;
    private Subscription subscribe = null;

    private boolean isSearchView = false;
    private boolean searchRestaurantsloading = false;
    private boolean hasMoreSearchRestaurants = true;
    private int amenityId;
    private LinearLayout comingSoonLayout;
    private ImageView comingSoonImg;
    private TextView comingSoonText, comingSoonDesc;
    private RelativeLayout outletItemsMainLyt;
    private OutletItemsBinding binding;
    private boolean showBtmPrg = false;

    /*@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        //Fetching the amenity's(category) id, used to load outlets under this amenity from server.
        Bundle data = getArguments();
        if (data != null) {
            amenityId = data.getInt(AMENITY_ID);
        }

        restoreData(savedInstanceState);
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (fragView == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.outlet_items, container, false);
            fragView = binding.getRoot();

            //Fetching the amenity's(category) id, used to load outlets under this amenity from server.
            Bundle data = getArguments();
            if (data != null) {
                amenityId = data.getInt(AMENITY_ID);
            }

            initViews(fragView);
        }

        return fragView;
    }

    private void initViews(View view) {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.restaurantList.setLayoutManager(linearLayoutManager);
        outletListRecyclerViewAdapter = new OutletsAdapter(getActivity(), amenityId, null);
        outletListRecyclerViewAdapter.setOutletClickListener(outletSelectListener);
        binding.restaurantList.setAdapter(outletListRecyclerViewAdapter);
        outletItemsMainLyt = (RelativeLayout) view.findViewById(R.id.outlet_items_main_lyt);
        comingSoonLayout = (LinearLayout) view.findViewById(R.id.coming_soon_lyt);
        comingSoonImg = (ImageView) view.findViewById(R.id.coming_soon_img);
        comingSoonText = (TextView) view.findViewById(R.id.coming_soon_text);
        comingSoonText.setVisibility(View.GONE);
        comingSoonDesc = (TextView) view.findViewById(R.id.coming_soon_desc);

        //handles pagination
        binding.restaurantList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public int lastVisibleItem;
            public int totalItemCount;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                    if (isSearchView) {//Indicates search restaurants
                        // reached the threshold limit while scrolling. & do pagination
                        if (!searchRestaurantsloading && hasMoreSearchRestaurants && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            //TODO: load search restaurants from server
                        }
                    } else {
                        //loads restaurants list
                        if (!outletsLoading && outletListRecyclerViewAdapter.getOutletsCount() <= (lastVisibleItem + visibleThreshold)) {
                            //&& hasMoreOutlets (removed as to check continuously for out outlet when user scroll down)
                            showBtmPrg = true;
                            loadRestaurantsFromServer();
                        }
                    }
                } else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    outletListRecyclerViewAdapter.showLoadMoreStatus();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });

        //Pull to refresh
        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshOutlets();
            }
        });
        mProgressBarHandler = new ProgressBarHandler(getContext(), outletItemsMainLyt);
        showBtmPrg = false;
        loadRestaurantsFromServer();

        KeyboardHandler.setKeyboardHandler(view);
    }

    //Display 'check internet' toast when there is no internet
    //Otherwise download when there is no data
    public void updateData() {
        if (!isFragmentVisible()) {
            return;
        }

        if (NetworkConnection.isNetworkAvailable()) {
            //load if they are loaded previously
            if (outletListRecyclerViewAdapter != null && outletListRecyclerViewAdapter.getItemCount() == 0) {
                refreshOutlets();
            } else {
                if (binding != null) {
                    binding.restaurantList.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if (comingSoonLayout != null) {
                comingSoonLayout.setVisibility(View.GONE);
            }
            if (binding != null) {
                binding.restaurantList.setVisibility(View.GONE);
            }
            WaselToast.showToast(getString(R.string.check_internet));
        }
    }

    /*
        * Removes current outlet items &
        * load new outlet items from server based on current lat/lng
        * */
    public void refreshOutlets() {
        if (outletListRecyclerViewAdapter != null) {
            outletListRecyclerViewAdapter.clearRestaurantsList();
            showBtmPrg = false;
            loadRestaurantsFromServer();
        }
    }

    public int getAmenityId() {
        return amenityId;
    }

    private void loadRestaurantsFromServer() {
        outletsLoading = true;
        OutletsReqBody postBody = new OutletsReqBody();
        postBody.setLatitude(LocationPreference.getCurrentLat());
        postBody.setLongitude(LocationPreference.getCurrentLng());
        postBody.setAmenityId(outletListRecyclerViewAdapter.getAmenityId());
        postBody.setStart(outletListRecyclerViewAdapter.getStartIndex());
        postBody.setMaxResult(outletListRecyclerViewAdapter.getMaxResult());
        /* Removing filter data
        if(filterData!=null){
            OutletsReqBody.Filter filter = postBody.getFilter();
            filter.setRating(filterData.containsKey(OutletFilterActivity.RATING_PRIORITY)?(boolean)filterData.get(OutletFilterActivity.RATING_PRIORITY):false);
            filter.setBudgets(filterData.containsKey(OutletFilterActivity.BUDGET)?(List<Integer>) filterData.get(OutletFilterActivity.BUDGET):null);
            filter.setCuisines(filterData.containsKey(OutletFilterActivity.CUISINES_LIST)?(List<Integer>) filterData.get(OutletFilterActivity.CUISINES_LIST):null);
        }*/
        outletsObservable = WaselDeliveryService.getService().getOutlets(postBody);
        if (subscribe != null) {
            removeFromSubscription(subscribe);
        }
        subscribe = addObserver(outletsObservable, outletsObserver, !showBtmPrg);
    }

    //On selecting particular outlet, shows its inner page (menu list)
    View.OnClickListener outletSelectListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (NetworkConnection.isNetworkAvailable()) {
                int resIndex = Integer.parseInt(v.getTag().toString());
                Outlet tmpRes = outletListRecyclerViewAdapter.getOutletAtPos(resIndex);
                if (tmpRes.isOutletAvailable()) {
                    Intent loadFragmentIntent = new Intent(getActivity(), LoadFragmentActivity.class);
//                    OutletDetailsFragment restaurantDetailsFragment = new OutletDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AMENITY_ID, amenityId);
                    bundle.putSerializable(OutletDetailsFragment.OUTLET_DETAIL, tmpRes);
                    bundle.putInt(FragmentsIndex.FRAGMENT_TYPE, FragmentsIndex.FRAGMENT_OUTLET_DETAILS);
                    loadFragmentIntent.putExtra(FragmentsIndex.FRAGMENT_DATA, bundle);
                    startActivity(loadFragmentIntent);
//                    ((MainActivity)getActivity()).addFragment(restaurantDetailsFragment);
                }
            } else {
                WaselToast.showToast(getString(R.string.check_internet));
            }

        }
    };

    /*
    * Observer for restaurants list downloaded from server & add them to recycler view.
    * */
    Observer<List<Outlet>> outletsObserver = new Observer<List<Outlet>>() {

        @Override
        public void onCompleted() {
            clearDownloadingProgres();
        }

        @Override
        public void onError(Throwable e) {
            clearDownloadingProgres();
            if (e instanceof NetworkStatusException) {
                WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
            }
        }

        @Override
        public void onNext(List<Outlet> outlets) {
            if (outlets != null && outlets.size() > 0) {
                comingSoonLayout.setVisibility(View.GONE);
                binding.restaurantList.setVisibility(View.VISIBLE);
                outletListRecyclerViewAdapter.addRestaurantsToList(outlets);
                outletListRecyclerViewAdapter.incStartIndexByValue(outlets.size());
                hasMoreOutlets = true;
            } else {
                hasMoreOutlets = false;
            }

            //Checking for empty screen condition
            if (outletListRecyclerViewAdapter.getItemCount() == 0) {
                comingSoonLayout.setVisibility(View.VISIBLE);
                comingSoonImg.setImageResource(R.drawable.coming_soon);
                comingSoonDesc.setText(getResources().getString(R.string.outlet_coming_soon_desc));
                binding.restaurantList.setVisibility(View.GONE);
            }
            clearDownloadingProgres();
        }
    };

    private void clearDownloadingProgres() {
        outletsLoading = false;
        clearSubscriptions();
        //Stop refresh animation
        outletListRecyclerViewAdapter.hideLoadMoreStatus();
        binding.swipeRefreshLayout.setRefreshing(false);
    }

}
