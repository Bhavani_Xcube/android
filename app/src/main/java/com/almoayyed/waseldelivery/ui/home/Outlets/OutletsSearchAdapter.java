package com.almoayyed.waseldelivery.ui.home.Outlets;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.databinding.OutletItemBinding;
import com.almoayyed.waseldelivery.databinding.OutletItemForSelectionBinding;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.home.OutletDetails.OutletDetailsFragment;
import com.almoayyed.waseldelivery.ui.home.OutletDetails.ViewHolders.SelectOutletItemViewHolder;
import com.almoayyed.waseldelivery.ui.home.Outlets.ViewHolders.OutletViewHolder;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OutletsSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    Context context;
    private final List<Outlet> restaurantList;
    private int pageNo = 1;
    public Observable<List<Outlet>> searchObservable = null;
    public Subscription searchSubscribe = null;
    private ProgressBarHandler mProgressBarHandler;

    public OutletsSearchAdapter(Context context, List<Outlet> restaurantList){
        if(restaurantList == null){
            this.restaurantList = new ArrayList<>();
        }else {
            this.restaurantList = restaurantList;

        }

        this.context = context;
    }

   /* public RestaurantsSearchAdapter(Context context, List<Restaurant> restaurantList){
        mProgressBarHandler = new ProgressBarHandler(context);
        if(restaurantList == null){
            this.restaurantList = new ArrayList<>();
        }else {
            this.restaurantList = restaurantList;

        }

        this.context = context;
    }*/
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        OutletItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.outlet_item, parent, false);
        view = binding.getRoot();
        mProgressBarHandler = new ProgressBarHandler(context, binding.outletItemMainLyt);
        return new OutletViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof OutletViewHolder){
            Outlet tmpRestaurant = restaurantList.get(position);
            OutletViewHolder restaurantViewHolder = (OutletViewHolder) holder;
            if(WaselDeliveryService.getServiceType() == WaselDeliveryService.SERVICE_OFFLINE){
                int resID = WaselApplication.getContext().getResources().getIdentifier(tmpRestaurant.getImageUrl(), "drawable", WaselApplication.getContext().getPackageName());
                restaurantViewHolder.mRestaurantIconView.setImageResource(resID);
            }else {
                //TODO: load image into view. (when actual data is loading from server)
                /*LoadImage.loadImage(tmpRestaurant.getImageUrl(),restaurantViewHolder.mRestaurantIconView);*/
            }
            restaurantViewHolder.mRestaurantNameView.setText(tmpRestaurant.getName());
            restaurantViewHolder.mRestaurantCuisineView.setText(tmpRestaurant.getDescription());
            /*restaurantViewHolder.mRestaurantRatingView.setText(String.valueOf(tmpRestaurant.getRating()));
            restaurantViewHolder.mRatingBar.setRating(tmpRestaurant.getRating());
            //setting budget value
            restaurantViewHolder.mRestaurantBudgetView.setBudgetValue(tmpRestaurant.getBudget());*/

            //TODO: Opens restaurant details page on selecting restaurant_item
            restaurantViewHolder.itemView.setOnClickListener(restaurantSelectListener);
            restaurantViewHolder.itemView.setTag(position);
        }
    }

    @Override
    public int getItemCount() {
        if(restaurantList != null){
            return restaurantList.size();
        }
        return 0;
    }
    public void clearRestaurantsList(){
        pageNo = 1;
        restaurantList.clear();
        notifyDataSetChanged();
    }
    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public void incPageNo() {
        this.pageNo ++;
    }

    public void addRestaurantsToList(List<Outlet> resList) {
        restaurantList.addAll(resList);
        notifyDataSetChanged();
    }

    public void setRestaurantsToList(List<Outlet> resList) {
        restaurantList.clear();
        addRestaurantsToList(resList);
    }

    //Restaurant item click event is handled here
    View.OnClickListener restaurantSelectListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            /*int resIndex = Integer.parseInt(v.getTag().toString());
            Outlet tmpRes = restaurantList.get(resIndex);
            OutletDetailsFragment restaurantDetailsFragment = new OutletDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(OutletDetailsFragment.OUTLET_DETAIL,tmpRes);
            restaurantDetailsFragment.setArguments(bundle);
            ((MainActivity)context).addFragment(restaurantDetailsFragment);*/
        }
    };

    /*
  * Listener for text change in search edittext view
  * */
    public Action1 searchTextChangeListener = new Action1<CharSequence>() {
        @Override
        public void call(CharSequence value) {

            searchObservable = WaselDeliveryService.getService().searchOutlets(String.valueOf(value));
            searchSubscribe = searchObservable.subscribe(searchObserver);
        }
    };

    /*
   * Observer for restaurants search list downloaded from server & add them to recycler view.
   * */
    public Observer<List<Outlet>> searchObserver = new Observer<List<Outlet>>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            mProgressBarHandler.hide();
        }

        @Override
        public void onNext(List<Outlet> restaurants) {
            if (restaurants != null && restaurants.size() > 0) {
                setRestaurantsToList(restaurants);
                incPageNo();
                // hasMoreRestaurants = true;
            } else {
                clearRestaurantsList();
                // hasMoreRestaurants = false;
            }
            //  restaurantsloading = false;

        }
    };
}
