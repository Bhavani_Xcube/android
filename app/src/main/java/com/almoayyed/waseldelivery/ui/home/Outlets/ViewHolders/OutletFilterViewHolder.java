package com.almoayyed.waseldelivery.ui.home.Outlets.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.almoayyed.waseldelivery.R;

/**
 * Created on 9 Nov 2016 11:23 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Holds Cuisines List items
 */
public class OutletFilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public final TextView mCuisineNameView;
    public final CheckBox mCheckbox;
    public final RelativeLayout filter_list;
    private ItemClickListener clickListener;

    public OutletFilterViewHolder(View view) {
        super(view);
        mCuisineNameView = (TextView) view.findViewById(R.id.cuisine_names);
        mCheckbox = (CheckBox) view.findViewById(R.id.filter_check);
        filter_list = (RelativeLayout) view.findViewById(R.id.filter_list);
        itemView.setOnClickListener(this);
    }

    public ItemClickListener getClickListener() {
        return clickListener;
    }

    public void setClickListener(ItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @Override
    public void onClick(View view) {
        if (clickListener != null) clickListener.onClick(view, getAdapterPosition());
    }


    public interface ItemClickListener {
        void onClick(View view, int position);
    }
}
