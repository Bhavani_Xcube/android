package com.almoayyed.waseldelivery.ui.home.Outlets.ViewHolders;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;

/**
 * Created on 9 Nov 2016 11:23 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

//Holds restaurants list items views
public class OutletViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final ImageView mRestaurantIconView;
    public final TextView mRestaurantNameView;
    public final TextView mRestaurantCuisineView;
    /*public final CustomBudgetView mRestaurantBudgetView;
    public final RatingBar mRatingBar;
    public final TextView mRestaurantRatingView;*/
    private ViewDataBinding binding;

    public OutletViewHolder(View view) {
        super(view);
        mView = view;
        mRestaurantIconView = (ImageView)view.findViewById(R.id.outlet_image);
        mRestaurantNameView = (TextView) view.findViewById(R.id.outlet_title);
        mRestaurantCuisineView = (TextView) view.findViewById(R.id.outlet_sub_title);
            /*mRestaurantRatingView = (TextView)view.findViewById(R.id.restaurant_rating_val);
            mRatingBar = (RatingBar) view.findViewById(R.id.restaurant_rating_indicator);
            mRestaurantBudgetView = (CustomBudgetView)view.findViewById(R.id.restaurant_budget);*/
        binding = DataBindingUtil.bind(view);
    }
    public ViewDataBinding getBinding() {
        return binding;
    }
}