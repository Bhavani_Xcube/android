package com.almoayyed.waseldelivery.ui.location;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.network_interface.location.ReverseGeoCode;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * GPS location functionality
 */

/**
 * Created on 10 Nov 2016 4:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class GPSLocationFragment extends BaseFragment implements View.OnClickListener {
    private ReverseGeoCode reverseGeoCodeThread = null;
    private TextView findGpsLocButton2;
    private TextView findManualLocButton2;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(fragView==null){
            fragView = inflater.inflate(R.layout.gps_location_layout,container,false);
             initViews(fragView);
        }
        return fragView;
    }

    private void initViews(View v) {

        //---------GPS location  layout views starts------
        findGpsLocButton2 = (TextView) v.findViewById(R.id.find_gps_loc_button2);
        findGpsLocButton2.setOnClickListener(this);

        findManualLocButton2 = (TextView) v.findViewById(R.id.find_manual_loc_button2);
        findManualLocButton2.setOnClickListener(this);
        //---------GPS location  layout views ends------

        KeyboardHandler.setKeyboardHandler(v);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.find_gps_loc_button2:
                //Find current user loc by GPS

                ((LocationActivity)getActivity()).findGpsLocation();
                break;
            case R.id.find_manual_loc_button2:
                getActivity().getIntent().putExtra(LocationActivity.DISPLAY_LYT,LocationActivity.DISPLAY_MANUAL_LYT);
                ((LocationActivity)getActivity()).setDisplayLayout(LocationActivity.DISPLAY_MANUAL_LYT);
                break;
        }
    }
}