package com.almoayyed.waseldelivery.ui.location;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.PermissionConstants;
import com.almoayyed.waseldelivery.data_saving.WaselDeliveryDB;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.network_interface.location.ReverseGeoCode;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.utils.GpsLocation.GPSTracker;
import com.almoayyed.waseldelivery.utils.GpsLocation.GpsLocationListener;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselToast;

import com.google.android.gms.common.ConnectionResult;

import rx.Subscription;
import rx.functions.Action1;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

import static com.almoayyed.waseldelivery.views.WaselToast.showToast;

/**
 * Created on 11 Nov 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class LocationActivity extends AppCompatActivity implements GpsLocationListener {

    public static final String DISPLAY_LYT = "DisplayLayout";
    public static final int DISPLAY_MANUAL_LYT = 1;
    public static final int DISPLAY_GPS_LYT = 2;
    public static final int GPS_ENABLE_CODE = 201;
    private int currentDispalyLyt;
    private ReverseGeoCode reverseGeoCodeThread = null;
    private GPSTracker gpsTracker;
    public ProgressBarHandler mProgressBarHandler;
    private RelativeLayout mainLayout;

    //revGeoCodeSubject: Creates an "Observable" to send reverse geo code data
    PublishSubject<Object> revGeoCodeSubject = PublishSubject.create();
    //startStopCompositeSubscription:
    CompositeSubscription startStopCompositeSubscription = new CompositeSubscription();
    Subscription subscription;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if(intent!=null){
            currentDispalyLyt = intent.getIntExtra(DISPLAY_LYT, DISPLAY_MANUAL_LYT);
        }
        setContentView(R.layout.location_lyt);

        initViews();
    }

    private void initViews() {
        mainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        mProgressBarHandler = new ProgressBarHandler(this, mainLayout);
        setDisplayLayout(currentDispalyLyt);

        subscription = revGeoCodeSubject
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object data) {
                        if(data instanceof UserLocation){
                            onReceiveLocationDetails((UserLocation)data);
                        }else if(data instanceof String){
                            onReceiveError((String)data);
                        }
                    }
                });

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    public void replaceDisplayLayout(int dispalyLyt) {
        if(dispalyLyt == DISPLAY_MANUAL_LYT){
            replaceFragment(new ManualLocationFragment());
        }else {
            replaceFragment(new GPSLocationFragment());
        }
        currentDispalyLyt = dispalyLyt;
    }

    public void setDisplayLayout(int dispalyLyt) {
        if(dispalyLyt == DISPLAY_MANUAL_LYT){
            addFragment(new ManualLocationFragment());
        }else {
            addFragment(new GPSLocationFragment());
        }
        currentDispalyLyt = dispalyLyt;
    }
    /*
      * Adds the fragment in the current tab
      * */
    public void addFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.location_frame,fragment);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragmentTransaction.commit();
    }



    /*
   * Replaces the fragment in the current tab
   * */
    public void replaceFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        // Replace whatever is in the manual_base_layout view with this fragment,
        fragmentTransaction.replace(R.id.location_frame, fragment);
        //fragmentTransaction.addToBackStack(null);
        // Commit the transaction
        fragmentTransaction.commit();
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onLocationChanged(Location location) {
        getLocDetailsByLatLng(location);
        gpsTracker.disconnect();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Location location = gpsTracker.getLastKnownLocation();
        if(location != null){
            gpsTracker.disconnect();
            getLocDetailsByLatLng(location);
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mProgressBarHandler.hide();
//        Toast.makeText(this, "Fail to get location", Toast.LENGTH_SHORT).show();
        WaselToast.showToast(this.getResources().getString(R.string.loc_fetch_failed));
    }

    @Override
    public void onConnectionSuspended(int i) {
        mProgressBarHandler.hide();
//        Toast.makeText(this, "Connection suspended", Toast.LENGTH_SHORT).show();
        WaselToast.showToast(this.getResources().getString(R.string.suspended_conn));
    }
    /*
    * Use GpsTracker to find current GPS location if GPS is enabled
    * */
    public void getCurrentGpsLoc(boolean enableGPS) {

        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // check if enabled and if not send user to the GSP settings
        // Better solution would be to display a dialog and suggesting to
        // go to the settings

        if (enabled) {
            startStopCompositeSubscription.clear();
            startStopCompositeSubscription.add(subscription);
            mProgressBarHandler.show();
            gpsTracker = new GPSTracker(this,this);
            gpsTracker.connect();
        }else {
            if(enableGPS){
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent,GPS_ENABLE_CODE);
            }else {
                mProgressBarHandler.hide();
            }
        }
    }


    /*
    * Trigger to find current GPS location by checkinfindGpsLocationg the permissions
    * */
    public void findGpsLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

            }

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PermissionConstants.REQUEST_LOCATION_PERMISSION);
        }
        else {
            getCurrentGpsLoc(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode)
        {
            case PermissionConstants.REQUEST_LOCATION_PERMISSION:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getCurrentGpsLoc(true);

                } else {

//                    Toast.makeText(this, "Allow Location permission", Toast.LENGTH_SHORT).show();
                    WaselToast.showToast(this.getResources().getString(R.string.allow_loc));
                }
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case LocationActivity.GPS_ENABLE_CODE:
                getCurrentGpsLoc(false);
                break;
        }
    }

    private void getLocDetailsByLatLng(Location location) {
        //Creates thread for reverse geo-code location
        if(reverseGeoCodeThread==null){
            reverseGeoCodeThread = new ReverseGeoCode(revGeoCodeSubject);
            reverseGeoCodeThread.start();
        }
        UserLocation userLocation = new UserLocation();
        userLocation.setLatitude(location.getLatitude());
        userLocation.setLongitude(location.getLongitude());
        reverseGeoCodeThread.searchLocationByLatLng(userLocation);
    }

    public void getLocDetailsByPlaceId(String placeId) {
        //Creates thread for reverse geo-code location
        if(reverseGeoCodeThread==null){
            reverseGeoCodeThread = new ReverseGeoCode(revGeoCodeSubject);
            reverseGeoCodeThread.start();
        }
        reverseGeoCodeThread.searchLocationByPlaceId(placeId);
        mProgressBarHandler.show();
    }

    /*
    * Input: UserLocation details
    * Desc: Persists the location details & navigates to MainActivity
    * */
    public void onReceiveLocationDetails(final UserLocation revGeoCode){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(revGeoCode!=null && revGeoCode.getFormattedAddress() !=  null){
                    //reverse geo code location details found successfully
                    //Update the location preference data

                    //Saving location search in DB.
                    WaselDeliveryDB deliveryDB = new WaselDeliveryDB(WaselApplication.getContext());
                    deliveryDB.checkAndAddLocation(revGeoCode);

                    //Check initial location is setted or not.
                    // (happens when app is opened for first time or app-data is cleared)
                    if(LocationPreference.hasLocation()){
                        gotoLandingPage(revGeoCode);
                    }else {
                        Intent resultIntent = new Intent(LocationActivity.this, MainActivity.class);
                        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        LocationPreference.saveCurrentLocation(revGeoCode);
                        startActivity(resultIntent);
                        finish();
                    }
                }else{
                    //When unable to reverse geo code location details
                    setResult(Activity.RESULT_CANCELED, null);
                }
                mProgressBarHandler.hide();
            }
        });
    }

    public void gotoLandingPage(UserLocation revGeoCode) {
        LocationPreference.saveCurrentLocation(revGeoCode);
        Intent resultIntent = new Intent(LocationActivity.this, MainActivity.class);
        resultIntent.putExtra("location",revGeoCode.getFormattedAddress());
        resultIntent.putExtra("lat",revGeoCode.getLatitude());
        resultIntent.putExtra("lng",revGeoCode.getLongitude());
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    /*
    * Input: Error message string
    * Desc: Display the received message.
    * */
    public void onReceiveError(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Toast.makeText(LocationActivity.this,message,Toast.LENGTH_SHORT).show();
                WaselToast.showToast(message);
                mProgressBarHandler.hide();
            }
        });

    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()<=1){
            finish();
            return;
        }
        super.onBackPressed();
    }

    /*
    * Closes the search-places thread.
    * */
    @Override
    public void onDestroy() {
        super.onDestroy();
        startStopCompositeSubscription.clear();
        if(reverseGeoCodeThread!=null){
            reverseGeoCodeThread.clear();
        }
    }
}
