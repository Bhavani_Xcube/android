package com.almoayyed.waseldelivery.ui.location;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.ui.home.Outlets.ViewHolders.OutletViewHolder;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 11 Nov 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class LocationHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    Context context;

    //holds the restaurant list data
    private final List<UserLocation> locations;
    //PageNo used for pagination of restaurants list loaded from server
    View.OnClickListener locationClickListener;
    public LocationHistoryAdapter(Context context, List<UserLocation> userLocations){

        if(userLocations == null){
            this.locations = new ArrayList<>();
        }else {
            this.locations = userLocations;

        }

        this.context = context;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_history_item, parent, false);
        return new OutletViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof OutletViewHolder){
            OutletViewHolder viewHolder = (OutletViewHolder) holder;
            UserLocation tmpLoc = locations.get(position);

            //Setting binding variables
            viewHolder.getBinding().setVariable(BR.location,tmpLoc);

            viewHolder.itemView.setOnClickListener(locationClickListener);
            viewHolder.itemView.setTag(position);
        }
        KeyboardHandler.setKeyboardHandler(holder.itemView);
    }

    @Override
    public int getItemCount() {
        if(locations != null){
            return locations.size();
        }
        return 0;
    }

    public UserLocation getLocationAtPos(int position){
        if(position<0 || position > locations.size()-1){
            return null;
        }
        return locations.get(position);
    }

    public void clearRestaurantsList(){
        locations.clear();
        notifyDataSetChanged();
    }


    public void setLocationClickListener(View.OnClickListener outletClickListener) {
        this.locationClickListener = outletClickListener;
    }

    public class LocHistoryViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private ViewDataBinding binding;

        public LocHistoryViewHolder(View view) {
            super(view);
            mView = view;
            binding = DataBindingUtil.bind(view);
        }
        public ViewDataBinding getBinding() {
            return binding;
        }
    }
}
