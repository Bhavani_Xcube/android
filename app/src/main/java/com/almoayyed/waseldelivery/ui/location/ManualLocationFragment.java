package com.almoayyed.waseldelivery.ui.location;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.data_saving.WaselDeliveryDB;
import com.almoayyed.waseldelivery.databinding.FindLocationBinding;
import com.almoayyed.waseldelivery.models.LocationSearchResult;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.network_interface.location.SearchPlaceViaGms;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.utils.DividerItemDecoration;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

/**
 * Created on 11 Nov 2016 7:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class ManualLocationFragment extends BaseFragment implements View.OnClickListener{

    private PlacesListAdapter placesListAdapter;
    private final int inputThresholdLmt = 1;
    private SearchPlaceViaGms searchPlaceThread = null;
    private SearchPlaceViaGms.SearchPlaceInput searchPlaceInput;
    private String selectedPlace = "";

    PublishSubject<Object> revGeoCodeSubject = PublishSubject.create();
    Subscription subscription;

    //History
    private List<UserLocation> history;
    private LocationHistoryAdapter locHistoryAdapter;
    private RecyclerView locHistoryRecyclerView;
    private FindLocationBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(fragView==null){
            binding = DataBindingUtil.inflate(inflater,R.layout.find_location,container,false);
            fragView = binding.getRoot();
            initViews(fragView);
        }
        return fragView;
    }

    private void initViews(View v) {

        binding.closeBtn.setOnClickListener(this);
        binding.closeBtn.getDrawable().setAutoMirrored(true);

        //search location recycler view & adapter
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        placesListAdapter = new PlacesListAdapter(getContext());
        binding.placesListview.setLayoutManager(layoutManager);
        binding.placesListview.setAdapter(placesListAdapter);

        //GPS button view
        binding.findGpsLocButton1.setOnClickListener(this);

        //Observer that listens the search places list.
        subscription = revGeoCodeSubject
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object data) {
                        if(data instanceof SearchPlaceViaGms.SearchPlaceOutput){
                            onReceiveSearchPlaces((SearchPlaceViaGms.SearchPlaceOutput)data);
                        }else if(data instanceof String){
                            ((LocationActivity)getActivity()).onReceiveError((String)data);
                        }
                    }
                });

        //Thread that returns the places of search text
        searchPlaceThread = new SearchPlaceViaGms(revGeoCodeSubject);
        searchPlaceThread.start();
        searchPlaceInput = new SearchPlaceViaGms.SearchPlaceInput();

        //show the history if & only if loc-history records exist
        WaselDeliveryDB deliveryDB = new WaselDeliveryDB(WaselApplication.getContext());
        history = deliveryDB.getLocations(-1);
        if(history.size()>0){
            binding.locationHistoryFrame.getViewStub().inflate();
            locHistoryRecyclerView = (RecyclerView)fragView.findViewById(R.id.loc_history_listview);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            locHistoryAdapter = new LocationHistoryAdapter(getActivity(),history);
            locHistoryAdapter.setLocationClickListener(selectHistoryItem);
            locHistoryRecyclerView.setLayoutManager(linearLayoutManager);
            RecyclerView.ItemDecoration itemDecoration = new
                    DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
            locHistoryRecyclerView.addItemDecoration(itemDecoration);
            locHistoryRecyclerView.setAdapter(locHistoryAdapter);
        }

        KeyboardHandler.setKeyboardHandler(v);

        //Search from keyboard
        binding.manualLocEntry.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch(binding.manualLocEntry.getText());
                    return true;
                }
                return false;
            }
        });
    }

    /*
   * Check for changes at input text
   * Pass the changed text to search-places thread
   * */
    private void registerLocSearchTextWatcher() {

        Subscriber<CharSequence> editTextSubscription = new Subscriber<CharSequence>() {
            @Override
            public void onCompleted() {
                Loggers.temp("TextViewAfterTextChangeEvent = onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Loggers.temp("TextViewAfterTextChangeEvent = " + e.getMessage());
            }


            public void onNext(CharSequence textViewAfterTextChangeEvent) {

                performSearch(textViewAfterTextChangeEvent);
            }
        };

        Subscription searchSubscribe = RxTextView.textChanges(binding.manualLocEntry)
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(editTextSubscription);
        addToSubscription(searchSubscribe);
    }

    private void performSearch(CharSequence textViewAfterTextChangeEvent) {
        if (textViewAfterTextChangeEvent.length() >= inputThresholdLmt) {
            if(selectedPlace.equalsIgnoreCase(textViewAfterTextChangeEvent.toString())){
                return;
            }
            if(placesListAdapter.getItemCount()==0){
                showList(false);
            }

            //Start searching for places
            searchPlaceInput.searchString = textViewAfterTextChangeEvent.toString();
            ((LocationActivity)getActivity()).mProgressBarHandler.show();
            searchPlaceThread.onSearchTextChange(textViewAfterTextChangeEvent.toString());
        }else {
            showList(false);
        }
    }

    /*
    * Method: onReceiveSearchPlaces
    * Desc: Receives the auto search places result & display them.
    * */
    public void onReceiveSearchPlaces(SearchPlaceViaGms.SearchPlaceOutput output){
        Log.i("received","places received");
        final ArrayList<LocationSearchResult> results = output.results;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((LocationActivity)getActivity()).mProgressBarHandler.hide();
                if(results.size()>0){
                    showList(true);
                    placesListAdapter.setData(results);
                }else {
                    showList(false);
                    placesListAdapter.clearData();
                }
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.close_btn:
                if (LocationPreference.hasLocation()) {
                    getActivity().finish();
                } else {
                    getActivity().getIntent().putExtra(LocationActivity.DISPLAY_LYT, LocationActivity.DISPLAY_GPS_LYT);
                    ((LocationActivity)getActivity()).replaceDisplayLayout(LocationActivity.DISPLAY_GPS_LYT);
                }
                break;
            case R.id.find_gps_loc_button1:
                ((LocationActivity)getActivity()).findGpsLocation();
                break;
        }
    }

    /*
   * Set list view visible when search data exist and vice-versa
   * */
    private void showList(boolean show) {
        if(show){
            binding.placesListview.setVisibility(View.VISIBLE);
            binding.findGpsLocButton1.setVisibility(View.INVISIBLE);
            if(history.size()>0){
                binding.locationHistoryFrame.getRoot().setVisibility(View.INVISIBLE);
            }
        }else {
            binding.placesListview.setVisibility(View.INVISIBLE);
            binding.findGpsLocButton1.setVisibility(View.VISIBLE);
            if(history.size()>0){
                binding.locationHistoryFrame.getRoot().setVisibility( (history.size()>0) ? View.VISIBLE : View.INVISIBLE);
            }
        }
    }

    View.OnClickListener selectHistoryItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int)v.getTag();
            UserLocation location = locHistoryAdapter.getLocationAtPos(position);

            //Update the used time of this current location
            WaselDeliveryDB deliveryDB = new WaselDeliveryDB(WaselApplication.getContext());
            deliveryDB.updateLocation(location);

            ((LocationActivity)getActivity()).gotoLandingPage(location);
        }
    };

    /*
   * Updates the search places list on to the recycler view
   * */
    public class PlacesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

        private final LayoutInflater mInflater;
        private ArrayList<LocationSearchResult> resultList;

        Context mContext;
        public PlacesListAdapter(Context context) {
            this.mContext = context;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            resultList = new ArrayList<>();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.manual_place_item, parent, false);
            return new PlaceViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            PlaceViewHolder viewHolder = (PlaceViewHolder) holder;
            viewHolder.itemView.setOnClickListener(getLocationDetailsByPlaceId());
            viewHolder.itemView.setTag(position);
            viewHolder.autoText.setText(resultList.get(position).getDescription());
        }

        @NonNull
        private View.OnClickListener getLocationDetailsByPlaceId() {
            return new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Reverse geo code the selected place(based on place_id)
                    if(v.getTag() != null){

                        int pos = Integer.parseInt(v.getTag().toString());
                        LocationSearchResult userLocation = resultList.get(pos);
                        ((LocationActivity)getActivity()).getLocDetailsByPlaceId(userLocation.getPlace_id());
                        selectedPlace = userLocation.getDescription();
                        binding.manualLocEntry.setText(selectedPlace);
                        showList(false);

                        binding.manualLocEntry.clearFocus();
                    }
                }
            };
        }

        @Override
        public int getItemCount() {
            return resultList.size();
        }

        public void clearData(){
            resultList.clear();
            notifyDataSetChanged();
        }

        public void setData(ArrayList searchData) {
            resultList.clear();
            resultList.addAll(searchData);
            notifyDataSetChanged();
        }

        class PlaceViewHolder extends RecyclerView.ViewHolder {
            TextView autoText;
            public PlaceViewHolder(View view){
                super(view);
                autoText = (TextView) view.findViewById(R.id.search_loc_text);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        registerLocSearchTextWatcher();

    }

    @Override
    public void onStop() {
        super.onStop();
        KeyboardHandler.hideKeyboardIfAvailable(fragView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(searchPlaceThread!=null){
            searchPlaceThread.clear();
        }
    }
}
