package com.almoayyed.waseldelivery.ui.login;

import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;

import com.almoayyed.waseldelivery.BuildConfig;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.AppLimitations;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.databinding.ActivityAddPhoneNumberBinding;
import com.almoayyed.waseldelivery.models.Registration;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.network_interface.wasel_api.GenerateOtpReqBody;
import com.almoayyed.waseldelivery.utils.Facebook.Facebook;
import com.almoayyed.waseldelivery.utils.Facebook.LogoutFromFacebookListener;
import com.almoayyed.waseldelivery.utils.GooglePlus.Google;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Phone number input screen while in registrationData process
 */
public class AddPhoneNumberActivity extends AppCompatActivity{
    Registration registrationData;
    private ProgressBarHandler mProgressBarHandler = null;
    private Subscription subscribe = null;
    CompositeSubscription startStopCompositeSubscription = new CompositeSubscription();
    private ActivityAddPhoneNumberBinding binding;
    private Resources resources;
    private int loginFrom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_phone_number);
        binding.setAddPhoneNumHandler(this);
        mProgressBarHandler = new ProgressBarHandler(this, binding.addPhNumMainLyt);
        resources = this.getResources();
        //Binding registrationData instance to the layout
        registrationData = (Registration)getIntent().getExtras().getSerializable(RegistrationBundleKeyConstants.KEY_REGISTRATION_DATA);
        binding.setRegistration(registrationData);
        binding.backBtn.getDrawable().setAutoMirrored(true);
        //Identifying from where login had triggered
        //Used for navigation
        if(getIntent().getExtras() != null){
            loginFrom = getIntent().hasExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM)?
                    getIntent().getExtras().getInt(RegistrationBundleKeyConstants.KEY_LOGIN_FROM):
                    RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }else {
            loginFrom = RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }
        KeyboardHandler.setKeyboardHandler(binding.getRoot());
        checkForRTL();
    }

    private void checkForRTL() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            boolean isRightToLeft = getResources().getBoolean(R.bool.is_right_to_left);
            if(isRightToLeft){
                binding.phNumLyt.setGravity(Gravity.END);
                binding.emailInput.setGravity(Gravity.END);
            }else{
                binding.phNumLyt.setGravity(Gravity.START);
                binding.emailInput.setGravity(Gravity.START);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if(subscribe != null) {
            startStopCompositeSubscription.add(subscribe);
        }

    }

    @Override
    public void onStop() {
        startStopCompositeSubscription.clear();

        super.onStop();
    }

    public View.OnClickListener onBackBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(registrationData.getAccountType()==RegistrationBundleKeyConstants.ACC_TYPE_FACEBOOK){
                //Clear current session. if user navigate to back to this screen while registering.
                Facebook.signOut(AddPhoneNumberActivity.this, new LogoutFromFacebookListener(){
                    @Override
                    public void onLoggedOutFromFacebook() {
                        finish();
                    }
                });
            }if(registrationData.getAccountType()==RegistrationBundleKeyConstants.ACC_TYPE_GOOGLE){
                //Clear current session. if user navigate to back to this screen while registering.
                Google.signOut(new ResultCallback() {
                    @Override
                    public void onResult(@NonNull Result result) {
                        finish();
                    }
                });
            }
            else {
                finish();
            }
        }
    };

    public View.OnClickListener onProceedBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(registrationData.isShouldReadEmailId()){
            //Verifying email id when use logged from facebook with mobile number
                if (TextUtils.isEmpty(registrationData.getEmailId()) || TextUtils.isEmpty(registrationData.getEmailId().trim())) {
                    WaselToast.showToast(resources.getString(R.string.empty_mail_id));
                    return;
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(registrationData.getEmailId()).matches()) {
                    WaselToast.showToast(resources.getString(R.string.invalid_email));
                    return;
                }
            }

            //Verify phone no before proceeding to OTP verification
            if(TextUtils.isEmpty(registrationData.getMobile())){
                WaselToast.showToast(resources.getString(R.string.empty_phone_no));
                return;
            }else if(registrationData.getMobile().length()< BuildConfig.MOBILE_MAX_DIGITS ||!android.util.Patterns.PHONE.matcher(registrationData.getMobile()).matches()){
                WaselToast.showToast(resources.getString(R.string.invalid_phno));
                return;
            }

            mProgressBarHandler.disableBottomViews();
            mProgressBarHandler.show();


            // Generate OTP for registered mobile no
            GenerateOtpReqBody generateOtpReqBody = new GenerateOtpReqBody();
            generateOtpReqBody.setMobile(AppLimitations.MOBILE_NO_PREFIX + registrationData.getMobile());
            if(registrationData.getAccountType() == RegistrationBundleKeyConstants.ACC_TYPE_FORGOT_PWD){
                generateOtpReqBody.setNewUser(false);
            }else{
                generateOtpReqBody.setNewUser(true);
            }
            Observable<Response> generateOtpObservable = WaselDeliveryService.getService().generateOTP(generateOtpReqBody);
            subscribe = generateOtpObservable.subscribe(generateOtpObserver);
            startStopCompositeSubscription.clear();
            startStopCompositeSubscription.add(subscribe);
        }
    };

    /*
    * * Observer for listening to generateOTP response
    * */
    Observer<Response> generateOtpObserver = new Observer<Response>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            WaselToast.showToast(getServerErrorMessage(e));
            mProgressBarHandler.hide();
        }

        @Override
        public void onNext(Response s) {
            /*WaselToast.showToast(s.getMessage());*/
            mProgressBarHandler.hide();

            //Navigate to OTP verification screen.
            Intent i = new Intent(AddPhoneNumberActivity.this,OTPVerificationActivity.class);
            i.putExtra(RegistrationBundleKeyConstants.KEY_REGISTRATION_DATA, registrationData);
            i.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM,loginFrom);
            startActivity(i);
        }
    };

}
