package com.almoayyed.waseldelivery.ui.login;

import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.AppLimitations;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.ActivityLoginBinding;
import com.almoayyed.waseldelivery.models.Registration;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.network_interface.wasel_api.UserReqBody;
import com.almoayyed.waseldelivery.social_login_listeners.SocialLoginListenerhandler;
import com.almoayyed.waseldelivery.utils.Facebook.Facebook;
import com.almoayyed.waseldelivery.utils.GooglePlus.Google;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.views.WaselToast;

import rx.Observable;

/**
 * Created on 23 Nov 2016 1:13 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class LoginActivity extends AppCompatActivity{

    //HashKey: FjgBX0DvUE0yjT9H5M7mhTCd0B8=
    private UserReqBody userReqBody;
    private SocialLoginListenerhandler socialLoginListenerhandler;
    private ActivityLoginBinding binding;
    private Observable<UserLoginDetails> loginObservable;
    private Resources resources;
    private int loginFrom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setLoginHandler(this);

        initViews();
        //login_btn_layout.startAnimation(slide_up);
        /*expand(login_btn_layout);
        login_btn_layout.animate().translationY(300);*/

        // To generate KeyHash for FB


    }

    private void initViews() {
        resources = this.getResources();
        binding.backBtn.getDrawable().setAutoMirrored(true);
        //Identifying from where login had triggered
        //Used for navigation
        if(getIntent().getExtras() != null){
            loginFrom = getIntent().hasExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM)?
                    getIntent().getExtras().getInt(RegistrationBundleKeyConstants.KEY_LOGIN_FROM):
                    RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }else {
            loginFrom = RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }

        socialLoginListenerhandler = new SocialLoginListenerhandler(this,binding.loginMainLyt,loginFrom);

        String regId = LoginCredentialsPreference.getLoginPreference().getRegId();
        userReqBody = new UserReqBody();
        userReqBody.setDeviceToken(regId);
        binding.setUserInputsHandler(userReqBody);
        KeyboardHandler.setKeyboardHandler(binding.getRoot());
        checkForRTL();
    }

    private void checkForRTL() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            boolean isRightToLeft = resources.getBoolean(R.bool.is_right_to_left);
            if(isRightToLeft){
                binding.mobileNumInput.setGravity(Gravity.END);
                binding.pwdInput.setGravity(Gravity.END);
            }else{
                binding.mobileNumInput.setGravity(Gravity.START);
                binding.pwdInput.setGravity(Gravity.START);
            }
        }
    }

    public View.OnClickListener onBackBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    public View.OnClickListener onForgetPwdClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(LoginActivity.this, AddPhoneNumberActivity.class);
            Registration registrationData = new Registration();
            registrationData.setAccountType(RegistrationBundleKeyConstants.ACC_TYPE_FORGOT_PWD);
            i.putExtra(RegistrationBundleKeyConstants.KEY_REGISTRATION_DATA, registrationData);
            i.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM,loginFrom);
            startActivity(i);
        }
    };

    public View.OnClickListener onSignInClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if((TextUtils.isEmpty(userReqBody.getMobile())) || TextUtils.isEmpty(userReqBody.getPassword())){
                WaselToast.showToast(resources.getString(R.string.emptyLoginDetails));
            }else{
                //Login details API call
                socialLoginListenerhandler.mProgressBarHandler.show();
                UserReqBody tmpUserReqBody = new UserReqBody(userReqBody);
                tmpUserReqBody.setMobile(AppLimitations.MOBILE_NO_PREFIX + userReqBody.getMobile());
                loginObservable = WaselDeliveryService.getService().getLoginDetails(tmpUserReqBody);
                socialLoginListenerhandler.subscribe = loginObservable.subscribe(socialLoginListenerhandler.loginDetailsObserver);
                socialLoginListenerhandler.startStopCompositeSubscription.add(socialLoginListenerhandler.subscribe);
            }
        }
    };

    public View.OnClickListener onFBLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            socialLoginListenerhandler.mProgressBarHandler.show();
            Facebook.signUp(LoginActivity.this, socialLoginListenerhandler.fbSigninCallback);
        }
    };

    public View.OnClickListener onGoogleLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            socialLoginListenerhandler.mProgressBarHandler.show();
            Google.signUp(LoginActivity.this, socialLoginListenerhandler.googleSigninListener);
        }
    };

    public View.OnClickListener onRegisterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
            i.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM,loginFrom);
            startActivity(i);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_OK)
        {
            socialLoginListenerhandler.mProgressBarHandler.hide();
            return;
        }
        switch (requestCode) {
            case Facebook.RC_FACEBOOK:
                Facebook.onFacebookActivityResult(requestCode, resultCode, data);
                //TODO: log current user information & further navigations
                /*socialLoginListenerhandler.mProgressBarHandler.hide();*/
                break;
            case Google.RC_SIGN_IN:
                socialLoginListenerhandler.onGoogleLoginSuccess(data);
                break;
            default:
                socialLoginListenerhandler.mProgressBarHandler.hide();
                break;
        }

    }

    @Override
    protected void onDestroy() {
        socialLoginListenerhandler.startStopCompositeSubscription.clear();
        super.onDestroy();
    }
}
