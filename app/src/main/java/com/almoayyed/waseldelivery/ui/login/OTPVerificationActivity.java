package com.almoayyed.waseldelivery.ui.login;

import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.AppLimitations;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.ActivityOtpVerificationBinding;
import com.almoayyed.waseldelivery.models.DeviceIds;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.wasel_api.GenerateOtpReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.VerifyOtpReqBody;
import com.almoayyed.waseldelivery.network_interface.wasel_api.ApiResponse;
import com.almoayyed.waseldelivery.ui.BaseActivity;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.checkout.CheckoutActivity;
import com.almoayyed.waseldelivery.ui.location.LocationActivity;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.ui.ViewRenderObserver;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.jakewharton.rxbinding.widget.RxTextView;

import rx.Observable;
import rx.Subscriber;
import com.almoayyed.waseldelivery.models.Registration;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;

import java.util.ArrayList;

import rx.Observer;
import rx.Subscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Verifies OTP
 */
public class OTPVerificationActivity extends BaseActivity{

    private EditText dummyEt;
    private Registration registrationData;
    private ProgressBarHandler mProgressBarHandler = null;
    private LoginCredentialsPreference loginPreference;
    private Resources resources;
    private int loginFrom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityOtpVerificationBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_otp_verification);
        binding.setOtpHandler(this);
        binding.backBtn.getDrawable().setAutoMirrored(true);

        mProgressBarHandler = new ProgressBarHandler(this, binding.otpVerficationMainLyt);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        dummyEt = (EditText) findViewById(R.id.dummy_et);
        loginPreference = LoginCredentialsPreference.getLoginPreference();
        resources = this.getResources();

        OTPTextWatcher(dummyEt);
        registrationData = (Registration)getIntent().getExtras().getSerializable(RegistrationBundleKeyConstants.KEY_REGISTRATION_DATA);

        if(getIntent().getExtras() != null){
            loginFrom = getIntent().hasExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM)?
                    getIntent().getExtras().getInt(RegistrationBundleKeyConstants.KEY_LOGIN_FROM):
                    RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }else {
            loginFrom = RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }

        KeyboardHandler.setKeyboardHandler(binding.getRoot());

    }

    private void OTPTextWatcher(final EditText dummyEt) {

        ViewRenderObserver viewRenderObserver = new ViewRenderObserver();
        viewRenderObserver.setOnObserverCallback(dummyEt, new ViewRenderObserver.ViewRenderObserverCallback() {
            @Override
            public void onPreDraw() {
                KeyboardHandler.showSoftKeyboard(dummyEt);
            }
        });

        final TextView[] textViews  = new TextView[]{
                (TextView) findViewById(R.id.otp_et1),
                (TextView) findViewById(R.id.otp_et2),
                (TextView) findViewById(R.id.otp_et3),
                (TextView) findViewById(R.id.otp_et4)};


        Observable<CharSequence> observable = RxTextView.textChanges(dummyEt);

        observable.subscribe(new Subscriber<CharSequence>() {
            @Override
            public void onCompleted() {
                Log.e("Subscriber","Subscriber=onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.e("Subscriber","Subscriber=onError"+e.getMessage());
            }

            @Override
            public void onNext(CharSequence charSequence) {
                String[] split = charSequence.toString().trim().split("");
                Log.e("Subscriber","Subscriber="+charSequence);
                for (int i=0 ; i< textViews.length; i++) {

                    textViews[i].setText(split.length> i+1 ? split[i+1] : "-");

                }
            }
        });
    }
    public void onShowKeyboardClick(View view)
    {
        KeyboardHandler.showSoftKeyboard(dummyEt);
    }

    public View.OnClickListener onbackBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    public View.OnClickListener onVerifyMeBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Validating input otp
            if(TextUtils.isEmpty(dummyEt.getText()) || dummyEt.getText().length()<4){
//            Toast.makeText(this,getString(R.string.empty_otp),Toast.LENGTH_SHORT).show();
                WaselToast.showToast(resources.getString(R.string.empty_otp));
                return;
            }
            //Verifies the otp with given mobile no

            verifyOTP();
        }
    };

    public View.OnClickListener onResendOtpClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mProgressBarHandler.show();

            // Generates new OTP for registered mobile no
            GenerateOtpReqBody generateOtpReqBody = new GenerateOtpReqBody();
            generateOtpReqBody.setMobile(AppLimitations.MOBILE_NO_PREFIX+registrationData.getMobile());
            if(registrationData.getAccountType() == RegistrationBundleKeyConstants.ACC_TYPE_FORGOT_PWD){
                generateOtpReqBody.setNewUser(false);
            }else{
                generateOtpReqBody.setNewUser(true);
            }
            Observable<Response> generateOtpObservable = WaselDeliveryService.getService().generateOTP(generateOtpReqBody);
            addObserver(generateOtpObservable, generateOtpObserver);
        }
    };

    /*
    * Verifies OTP by passing mobile no & OTP to server
    * */
    private void verifyOTP() {
        mProgressBarHandler.show();

        VerifyOtpReqBody verifyOtpReqBody = new VerifyOtpReqBody();
        verifyOtpReqBody.setMobile(AppLimitations.MOBILE_NO_PREFIX+registrationData.getMobile());
        verifyOtpReqBody.setOtp(dummyEt.getText().toString());
        Observable<ApiResponse> verifyOtpResponseObservable = WaselDeliveryService.getService().verifyOTP(verifyOtpReqBody);
        addObserver(verifyOtpResponseObservable, verifyOtpResponseObserver);
    }

    /*
    * make server call to send the registration data
    * */
    private void registerUser() {
        mProgressBarHandler.show();
        ArrayList<DeviceIds> regIds = new ArrayList<>();
        String regId = loginPreference.getRegId();
        DeviceIds ids = new DeviceIds();
        ids.setDeviceToken(regId);
        regIds.add(ids);
        Registration tmpRegistrationData = new Registration(registrationData);
        tmpRegistrationData.setDevice(regIds);
        tmpRegistrationData.setMobile(AppLimitations.MOBILE_NO_PREFIX+registrationData.getMobile());
        Observable registrationObservable = WaselDeliveryService.getService().registerUser(tmpRegistrationData);
        addObserver(registrationObservable, registerObserver);
    }

    /*
    * Observer for registering user
    * */
    Observer<UserLoginDetails> registerObserver = new Observer<UserLoginDetails>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            mProgressBarHandler.hide();
            WaselToast.showToast(getServerErrorMessage(e));
        }

        @Override
        public void onNext(UserLoginDetails loginDetails) {
            //Persisting user information
            loginPreference.saveUserCredentials(loginDetails);

            //Registration data successfully send to user.
            //OTP will be send to phone via server.
            mProgressBarHandler.hide();

            //Navigate to corresponding screen(location, landing or login page)
            if(LocationPreference.hasLocation()){
                Intent i ;
                switch (loginFrom){
                    case RegistrationBundleKeyConstants.LOGIN_FROM_CHECKOUT_NORMAL:
                        i = new Intent(OTPVerificationActivity.this, CheckoutActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        break;
                    case RegistrationBundleKeyConstants.LOGIN_FROM_CHECKOUT_SPECIAL:
                    case RegistrationBundleKeyConstants.LOGIN_FROM_ORDER_HISTORY:
                    case RegistrationBundleKeyConstants.LOGIN_FROM_PROFILE_TAB:
                    case RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH:
                    default:
                        i = new Intent(OTPVerificationActivity.this, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        break;
                }
                i.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM,loginFrom);
                startActivity(i);
            }else {
                Intent i = new Intent(OTPVerificationActivity.this,LocationActivity.class);
                i.putExtra(LocationActivity.DISPLAY_LYT, LocationActivity.DISPLAY_GPS_LYT);
                i.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM,loginFrom);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        }
    };

    /*
    * * Observer for listening to generateOTP response
    * */
    Observer<ApiResponse> verifyOtpResponseObserver = new Observer<ApiResponse>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
//            Toast.makeText(OTPVerificationActivity.this,getServerErrorMessage(e),Toast.LENGTH_SHORT).show();
            WaselToast.showToast(getServerErrorMessage(e));
            mProgressBarHandler.hide();
        }

        @Override
        public void onNext(ApiResponse verifyOtpResponse) {
//            Toast.makeText(OTPVerificationActivity.this,verifyOtpResponse.getVersion(),Toast.LENGTH_SHORT).show();
            /*WaselToast.showToast(verifyOtpResponse.getMessage());*/
            mProgressBarHandler.hide();

            if(registrationData.getAccountType() == RegistrationBundleKeyConstants.ACC_TYPE_FORGOT_PWD){
                //Displays change pwd screen for forgot password
                Intent i = new Intent(OTPVerificationActivity.this,ResetPasswordActivity.class);
                i.putExtra(RegistrationBundleKeyConstants.KEY_MOBILE_NO, registrationData.getMobile());
                i.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM,loginFrom);
                startActivity(i);
            }else {
                //Register user once OTP is verified for WASEL, FACEBOOK or GOOGLE registering user
                registerUser();
            }
        }
    };

    /*
    * * Observer for listening to generateOTP response
    * */
    Observer<Response> generateOtpObserver = new Observer<Response>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            WaselToast.showToast(e.getMessage());
            mProgressBarHandler.hide();
        }

        @Override
        public void onNext(Response s) {
            WaselToast.showToast(s.getMessage());
            mProgressBarHandler.hide();
        }
    };
}
