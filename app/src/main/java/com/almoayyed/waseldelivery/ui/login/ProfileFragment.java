package com.almoayyed.waseldelivery.ui.login;


import android.app.NotificationManager;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.FragmentsIndex;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.FragmentProfileBinding;
import com.almoayyed.waseldelivery.databinding_eventhandler.LoadImage;
import com.almoayyed.waseldelivery.models.LogoutReq;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.social_login_listeners.SocialLoginListenerhandler;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.LoadFragmentActivity;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.profile.AboutWaselFragment;
import com.almoayyed.waseldelivery.ui.profile.CouponsListFragment;
import com.almoayyed.waseldelivery.ui.profile.EditProfileFragment;
import com.almoayyed.waseldelivery.ui.profile.HelpAndSupportFragment;
import com.almoayyed.waseldelivery.ui.profile.LegalPolicyFragment;
import com.almoayyed.waseldelivery.ui.profile.ManageAddressFragment;
import com.almoayyed.waseldelivery.utils.Facebook.Facebook;
import com.almoayyed.waseldelivery.utils.Facebook.LogoutFromFacebookListener;
import com.almoayyed.waseldelivery.utils.GooglePlus.Google;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;

import rx.Observable;
import rx.Observer;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/** Created on 10 Dec 2016 6:47 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.ViewTextWatcherListener
 */

public class ProfileFragment extends BaseFragment {


    private FragmentProfileBinding binding;
    private LoginCredentialsPreference loginPreference;
    private SocialLoginListenerhandler socialLoginListenerhandler;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(fragView == null) {
            // Inflate the layout for this fragment
            binding = (FragmentProfileBinding) DataBindingUtil.inflate(
                    inflater, R.layout.fragment_profile, container, false);
            binding.setClickHandler(clickHandler);
            socialLoginListenerhandler = new SocialLoginListenerhandler(getContext(),binding.profileMainLyt,
                    RegistrationBundleKeyConstants.LOGIN_FROM_PROFILE_TAB);

            fragView = binding.getRoot();
            binding.emailId.setSelected(true);
        }

        return fragView;
    }


    //Include pull to refresh
    @Override
    public void onStart() {
        super.onStart();

        //load profile
        loadProfileInfo();
    }

    private void loadProfileInfo() {
        loginPreference = LoginCredentialsPreference.getLoginPreference();
        if(loginPreference.isUserLoggedIn()){
            binding.setProfile(loginPreference.getUserCredentials());
            binding.profileLyt.setVisibility(View.VISIBLE);
            binding.loginBtn.setText(getResources().getString(R.string.sign_out));
            if(!(loginPreference.getImgUrl().contains("null")
                    || TextUtils.isEmpty(loginPreference.getImgUrl()))){
                String imgPath = loginPreference.getImgUrl();
                LoadImage.loadImage(binding.profilePic, imgPath);
            }
        }else {
            binding.profileLyt.setVisibility(View.GONE);
            binding.loginBtn.setText(getResources().getString(R.string.sign_in));
        }
    }


    private void performSigninAction() {
        if(loginPreference.isUserLoggedIn()){
            socialLoginListenerhandler.mProgressBarHandler.show();
            //Logout API call
            LogoutReq reqData = new LogoutReq();
            reqData.setId(loginPreference.getId());
            reqData.setDevice(socialLoginListenerhandler.getRegsId());
            Observable<Response> loginObservable = WaselDeliveryService.getService().logOut(reqData);
            socialLoginListenerhandler.subscribe = loginObservable.subscribe(logoutObserver);
            socialLoginListenerhandler.startStopCompositeSubscription.add(socialLoginListenerhandler.subscribe);

        }else {
            //Navigate to splash temporarily until login module completed
            Intent i = new Intent(getActivity(), LoginActivity.class);
            startActivity(i);
        }
    }


    public View.OnClickListener clickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent loadFragmentIntent = new Intent(getActivity(), LoadFragmentActivity.class);
            Bundle bundle = new Bundle();
            if(view.getId() == binding.manageAddr.getId()){
                /*ManageAddressFragment manageAddressFragment = new ManageAddressFragment();
                ((MainActivity)getActivity()).addFragment(manageAddressFragment);*/
                bundle.putInt(FragmentsIndex.FRAGMENT_TYPE, FragmentsIndex.FRAGMENT_MANAGE_ADDRESS);
            }else if(view.getId() == binding.offers.getId()){
                /*CouponsListFragment couponsListFragment = new CouponsListFragment();
                ((MainActivity)getActivity()).addFragment(couponsListFragment);*/
                bundle.putInt(FragmentsIndex.FRAGMENT_TYPE, FragmentsIndex.FRAGMENT_COUPONS);
            }else if(view.getId() == binding.editProfile.getId()){
                /*EditProfileFragment editProfileFragment = new EditProfileFragment();
                Bundle bundle = new Bundle();*/
                bundle.putString(ProfileConstants.NAME,loginPreference.getName());
                bundle.putString(ProfileConstants.EMAIL,loginPreference.getEmail());
                bundle.putString(ProfileConstants.PHONE_NO,loginPreference.getPhoneNo());
                bundle.putInt(FragmentsIndex.FRAGMENT_TYPE, FragmentsIndex.FRAGMENT_EDIT_PROFILE);
                /*editProfileFragment.setArguments(bundle);
                ((MainActivity)getActivity()).addFragment(editProfileFragment);*/
            }else if(view.getId() == binding.helpSupport.getId()){
                /*HelpAndSupportFragment helpAndSupportFragment = new HelpAndSupportFragment();
                ((MainActivity)getActivity()).addFragment(helpAndSupportFragment);*/
                bundle.putInt(FragmentsIndex.FRAGMENT_TYPE, FragmentsIndex.FRAGMENT_HELP_SUPPORT);
            }else if(view.getId() == binding.abtWasel.getId()){
                /*AboutWaselFragment aboutWaselFragment = new AboutWaselFragment();
                ((MainActivity)getActivity()).addFragment(aboutWaselFragment);*/
                bundle.putInt(FragmentsIndex.FRAGMENT_TYPE, FragmentsIndex.FRAGMENT_ABOUT_WASEL);
            }else if(view.getId() == binding.legal.getId()){
                /*LegalPolicyFragment legalPolicyFragment = new LegalPolicyFragment();
                ((MainActivity)getActivity()).addFragment(legalPolicyFragment);*/
                bundle.putInt(FragmentsIndex.FRAGMENT_TYPE, FragmentsIndex.FRAGMENT_LEGAL);
            }else if(view.getId() == binding.loginBtn.getId()){
                performSigninAction();
            }

            if(bundle.containsKey(FragmentsIndex.FRAGMENT_TYPE)){
                loadFragmentIntent.putExtra(FragmentsIndex.FRAGMENT_DATA, bundle);
                startActivity(loadFragmentIntent);
            }
        }
    };
    /*
  * Observer for Logout Api.
  * */
    Observer<Response> logoutObserver = new Observer<Response>() {

        @Override
        public void onCompleted() {
            socialLoginListenerhandler.mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
//            Toast.makeText(SignInActivity.this,getServerErrorMessage(e),Toast.LENGTH_SHORT).show();
            WaselToast.showToast(getServerErrorMessage(e));
            socialLoginListenerhandler.mProgressBarHandler.hide();
        }

        @Override
        public void onNext(Response response) {

            Facebook.signOut(getActivity(),new LogoutFromFacebookListener(){
                @Override
                public void onLoggedOutFromFacebook() {
                }
            });
            Google.signOut(new ResultCallback() {
                @Override
                public void onResult(@NonNull Result result) {
                }
            });

            //Clear login details
            loginPreference.clearLoginDetails();

            //Clear push notifications
            NotificationManager nm = (NotificationManager) getActivity().getSystemService(getActivity().NOTIFICATION_SERVICE);
            nm.cancelAll();

            binding.profileLyt.setVisibility(View.GONE);
            binding.loginBtn.setText(getResources().getString(R.string.sign_in));
            socialLoginListenerhandler.mProgressBarHandler.hide();
        }
    };
}
