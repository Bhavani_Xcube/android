package com.almoayyed.waseldelivery.ui.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.databinding.ActivityRegisterBinding;
import com.almoayyed.waseldelivery.models.Registration;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Registration screen
 */
public class RegisterActivity extends AppCompatActivity {

    Registration registrationData = new Registration();
    private int loginFrom;
    private ActivityRegisterBinding binding;
    private static final String SAVED_NAME = "name";
    private static final String SAVED_PWD = "password";
    private static final String SAVED_CONFIRM_PWD = "confirm_password";
    private static final String SAVED_EMAIL = "email";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        binding.setRegisterHandler(this);
        binding.setRegistration(registrationData);
        binding.backBtn.getDrawable().setAutoMirrored(true);
        registrationData.setAccountType(RegistrationBundleKeyConstants.ACC_TYPE_WASEL);

        if (getIntent().getExtras() != null) {
            loginFrom = getIntent().hasExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM) ?
                    getIntent().getExtras().getInt(RegistrationBundleKeyConstants.KEY_LOGIN_FROM) :
                    RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        } else {
            loginFrom = RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }
        checkForRTL();
    }

    private void checkForRTL() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            boolean isRightToLeft = getResources().getBoolean(R.bool.is_right_to_left);
            if(isRightToLeft){
                binding.nameInput.setGravity(Gravity.END);
                binding.pwdInput.setGravity(Gravity.END);
                binding.confirmPwdInput.setGravity(Gravity.END);
                binding.emailInput.setGravity(Gravity.END);
            }else{
                binding.nameInput.setGravity(Gravity.START);
                binding.pwdInput.setGravity(Gravity.START);
                binding.confirmPwdInput.setGravity(Gravity.START);
                binding.emailInput.setGravity(Gravity.START);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SAVED_NAME,registrationData.getName());
        outState.putString(SAVED_PWD,registrationData.getPassword());
        outState.putString(SAVED_CONFIRM_PWD,registrationData.getConfirmPassword());
        outState.putString(SAVED_EMAIL,registrationData.getEmailId());
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String name = savedInstanceState.getString(SAVED_NAME);
        registrationData.setName(name);
        String pwd = savedInstanceState.getString(SAVED_PWD);
        registrationData.setPassword(pwd);
        String confirm_pwd = savedInstanceState.getString(SAVED_CONFIRM_PWD);
        registrationData.setConfirmPassword(confirm_pwd);
        String email = savedInstanceState.getString(SAVED_EMAIL);
        registrationData.setEmailId(email);
    }

    public View.OnClickListener onBackBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    public View.OnClickListener onRegisterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            RegisterUser();
        }
    };

    private void RegisterUser() {
        //Validating input fields
        //TODO: should place focus on respective edittext on showing error message.
        ArrayList<String> validationStr = new ArrayList<>();
        if (TextUtils.isEmpty((registrationData.getName() != null) ? registrationData.getName().trim() : null)) {
            validationStr.add(this.getResources().getString(R.string.empty_name));
        }else if(!registrationData.getName().matches(getString(R.string.name_pattern))){
            validationStr.add(getString(R.string.invalid_name));
        }

        if (TextUtils.isEmpty(registrationData.getPassword()) || TextUtils.isEmpty(registrationData.getPassword().trim())) {
            validationStr.add(this.getResources().getString(R.string.empty_pwd));
        }

        if (TextUtils.isEmpty(registrationData.getConfirmPassword()) || TextUtils.isEmpty(registrationData.getConfirmPassword().trim())) {
            validationStr.add(this.getResources().getString(R.string.empty_cnfrm_pwd));
        }

        if (TextUtils.isEmpty(registrationData.getPassword()) || TextUtils.isEmpty(registrationData.getPassword().trim())) {
            validationStr.add(this.getResources().getString(R.string.empty_pwd));
        } else if (registrationData.getPassword().length() < 6) {
            validationStr.add(this.getResources().getString(R.string.no_min_pwd_char));
        } else if (!registrationData.getPassword().matches(getString(R.string.password_pattern))) {
            validationStr.add(this.getResources().getString(R.string.spl_char_pwd));
        } else if (!TextUtils.isEmpty(registrationData.getConfirmPassword()) && !registrationData.getPassword().equals(registrationData.getConfirmPassword())) {
            validationStr.add(this.getResources().getString(R.string.pwd_mismatch));
        }


        if (TextUtils.isEmpty(registrationData.getEmailId()) || TextUtils.isEmpty(registrationData.getEmailId().trim())) {
            validationStr.add(this.getResources().getString(R.string.empty_mail_id));
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(registrationData.getEmailId()).matches()) {
            validationStr.add(this.getResources().getString(R.string.invalid_email));
        }


        /*if (validationStr.size() >= 2) {
            WaselToast.showToast(this.getResources().getString(R.string.ManditoryAllFields));
            return;
        } else if (validationStr.size() == 1) {
            WaselToast.showToast(validationStr.get(0));
            return;
        }*/

        if (validationStr.size() > 0) {
            WaselToast.showToast(validationStr.get(0));
            return;
        }

        Intent i = new Intent(this, AddPhoneNumberActivity.class);
        i.putExtra(RegistrationBundleKeyConstants.KEY_REGISTRATION_DATA, registrationData);
        i.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM, loginFrom);
        startActivity(i);
    }

}
