package com.almoayyed.waseldelivery.ui.login;

import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.AppLimitations;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.databinding.ActivityResetPwdBinding;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.network_interface.wasel_api.ApiResponse;
import com.almoayyed.waseldelivery.network_interface.wasel_api.UserReqBody;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselToast;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 8 Nov 2016 6:47 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class ResetPasswordActivity extends AppCompatActivity{

    CompositeSubscription startStopCompositeSubscription = new CompositeSubscription();
    private String mobileNo;
    private ProgressBarHandler mProgressBarHandler = null;
    private Resources resources;
    private ActivityResetPwdBinding binding;
    private int loginFrom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reset_pwd);
        binding.setResetPwdHandler(this);
        binding.backBtn.getDrawable().setAutoMirrored(true);
        mProgressBarHandler = new ProgressBarHandler(this, binding.resetPwdMainLyt);

        mobileNo = getIntent().getExtras().getString(RegistrationBundleKeyConstants.KEY_MOBILE_NO);
        resources = this.getResources();

        if(getIntent().getExtras() != null){
            loginFrom = getIntent().hasExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM)?
                    getIntent().getExtras().getInt(RegistrationBundleKeyConstants.KEY_LOGIN_FROM):
                    RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }else {
            loginFrom = RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }
        KeyboardHandler.setKeyboardHandler(binding.getRoot());
    }

    @Override
    protected void onDestroy() {
        startStopCompositeSubscription.clear();
        super.onDestroy();
    }

    public  View.OnClickListener onBackBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    public  View.OnClickListener onSaveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(TextUtils.isEmpty(binding.newPwd.getText())){
//            Toast.makeText(this,getString(R.string.empty_pwd),Toast.LENGTH_SHORT).show();
                WaselToast.showToast(resources.getString(R.string.empty_pwd));
                return;
            }else if(TextUtils.isEmpty(binding.confirmPwd.getText())){
//            Toast.makeText(this,getString(R.string.empty_cnfrm_pwd),Toast.LENGTH_SHORT).show();
                WaselToast.showToast(resources.getString(R.string.empty_cnfrm_pwd));
                return;
            } else if(!binding.newPwd.getText().toString().equals(binding.confirmPwd.getText().toString())){
//            Toast.makeText(this,getString(R.string.pwd_mismatch),Toast.LENGTH_SHORT).show();
                WaselToast.showToast(resources.getString(R.string.pwd_mismatch));
                return;
            } else if (!binding.newPwd.getText().toString().matches(getString(R.string.password_pattern))) {
                WaselToast.showToast(resources.getString(R.string.spl_char_pwd));
                return;
            }

            mProgressBarHandler.show();

            //call reset password api
            UserReqBody changePwdData = new UserReqBody();
            changePwdData.setMobile(AppLimitations.MOBILE_NO_PREFIX + mobileNo);
            changePwdData.setPassword(binding.newPwd.getText().toString());
            Observable<ApiResponse> changePwdObservable = WaselDeliveryService.getService().changePassword(changePwdData);
            Subscription subscribe = changePwdObservable.subscribe(changePwdObserver);
            startStopCompositeSubscription.clear();
            startStopCompositeSubscription.add(subscribe);
        }
    };

    /*
    * Observer for change password api call
    * */
    Observer<ApiResponse> changePwdObserver = new Observer<ApiResponse>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
//            Toast.makeText(ResetPasswordActivity.this,getServerErrorMessage(e),Toast.LENGTH_SHORT).show();
            WaselToast.showToast(getServerErrorMessage(e));
            mProgressBarHandler.hide();
        }

        @Override
        public void onNext(ApiResponse response) {
            //TODO: check whether password is successfully changed or not

            //Registration data successfully send to user.
            //OTP will be send to phone via server.
            mProgressBarHandler.hide();

            Intent i = new Intent(ResetPasswordActivity.this,SignInActivity.class);
            i.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM,loginFrom);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
        }
    };
}
