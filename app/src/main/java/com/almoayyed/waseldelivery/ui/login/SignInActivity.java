package com.almoayyed.waseldelivery.ui.login;

import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.AppLimitations;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.ActivitySigninBinding;
import com.almoayyed.waseldelivery.models.Registration;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.network_interface.wasel_api.UserReqBody;
import com.almoayyed.waseldelivery.social_login_listeners.SocialLoginListenerhandler;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.views.WaselToast;

import rx.Observable;

/**
 * Created on 7 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class SignInActivity extends AppCompatActivity{

    private UserReqBody userReqBody;
    private SocialLoginListenerhandler socialLoginListenerhandler;
    private Resources resources;
    ActivitySigninBinding binding;
    private static final String SAVED_MOBILE = "mobile_num";
    private static final String SAVED_PWD = "pwd";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signin);
        binding.setSigninHandler(this);

        int loginFrom;
        if(getIntent().getExtras() != null){
            loginFrom = getIntent().hasExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM)?
                    getIntent().getExtras().getInt(RegistrationBundleKeyConstants.KEY_LOGIN_FROM):
                    RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }else {
            loginFrom = RegistrationBundleKeyConstants.LOGIN_FROM_SPLASH;
        }

        socialLoginListenerhandler = new SocialLoginListenerhandler(this,binding.signInMainLyt,loginFrom);
        String regId = LoginCredentialsPreference.getLoginPreference().getRegId();
        userReqBody = new UserReqBody();
        userReqBody.setDeviceToken(regId);
        binding.setLoginHandler(userReqBody);
        resources = this.getResources();
        binding.backBtn.getDrawable().setAutoMirrored(true);
        KeyboardHandler.setKeyboardHandler(binding.getRoot());
        checkForRTL();
    }

    private void checkForRTL() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            boolean isRightToLeft = resources.getBoolean(R.bool.is_right_to_left);
            if(isRightToLeft){
                binding.mobileNumInput.setGravity(Gravity.END);
                binding.pwdInput.setGravity(Gravity.END);
            }else{
                binding.mobileNumInput.setGravity(Gravity.START);
                binding.pwdInput.setGravity(Gravity.START);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SAVED_MOBILE,userReqBody.getMobile());
        outState.putString(SAVED_PWD,userReqBody.getPassword());
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String mobile_num = savedInstanceState.getString(SAVED_MOBILE);
        userReqBody.setMobile(mobile_num);
        String pwd = savedInstanceState.getString(SAVED_PWD);
        userReqBody.setPassword(pwd);
    }

    public View.OnClickListener onBackBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    public View.OnClickListener onForgetPwdClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
//TODO: OTP screen redirection
            Intent i = new Intent(SignInActivity.this, AddPhoneNumberActivity.class);
            Registration registrationData = new Registration();
            registrationData.setAccountType(RegistrationBundleKeyConstants.ACC_TYPE_FORGOT_PWD);
            i.putExtra(RegistrationBundleKeyConstants.KEY_REGISTRATION_DATA, registrationData);
            startActivity(i);
        }
    };

    public View.OnClickListener onSignInClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if((TextUtils.isEmpty(userReqBody.getMobile())) || TextUtils.isEmpty(userReqBody.getPassword())){
//            Toast.makeText(this,getResources().getString(R.string.emptyLoginDetails),Toast.LENGTH_LONG).show();
                WaselToast.showToast(resources.getString(R.string.emptyLoginDetails));
            }else{
                socialLoginListenerhandler.mProgressBarHandler.show();
                //Login details API call
                UserReqBody tmpUserReqBody = new UserReqBody(userReqBody);
                tmpUserReqBody.setMobile(AppLimitations.MOBILE_NO_PREFIX + userReqBody.getMobile());
                Observable<UserLoginDetails> loginObservable = WaselDeliveryService.getService().getLoginDetails(tmpUserReqBody);
                socialLoginListenerhandler.subscribe = loginObservable.subscribe(socialLoginListenerhandler.loginDetailsObserver);
                socialLoginListenerhandler.startStopCompositeSubscription.add(socialLoginListenerhandler.subscribe);
            }
        }
    };

    @Override
    protected void onDestroy() {
        socialLoginListenerhandler.startStopCompositeSubscription.clear();
        super.onDestroy();
    }
}
