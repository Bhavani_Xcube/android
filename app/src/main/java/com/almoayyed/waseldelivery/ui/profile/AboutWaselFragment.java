package com.almoayyed.waseldelivery.ui.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.BuildConfig;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.databinding.AboutWaselFragmentBinding;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.utils.NetworkConnection;
import com.almoayyed.waseldelivery.views.WaselToast;

/** Created on 19 Dec 2016 2:56 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public class AboutWaselFragment extends BaseFragment{
    private AboutWaselFragmentBinding binding;
    private String FB_URL = "https://www.facebook.com/people/Wasel-Delivery/100014860331175";
    private String TWITTER_URL = "https://twitter.com/wasel_delivery";
    private String INSTAGRAM_URL = "https://www.instagram.com/waseldelivery/?hl=en";
    private String ABOUT_URL = BuildConfig.BASE_URL + "public/about";
    private Resources resources;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(fragView==null){
            binding = DataBindingUtil.inflate(
                    inflater, R.layout.about_wasel_fragment, container, false);
            binding.setAbtWaselBtnsclickHandler(this);
            binding.backBtn.getDrawable().setAutoMirrored(true);
            resources = getResources();
            fragView = binding.getRoot();
            if(NetworkConnection.isNetworkAvailable()){
                binding.abtUsWebview.loadUrl(ABOUT_URL);
            }else{
                WaselToast.showToast(getString(R.string.check_internet));
            }
        }

        return fragView;
    }

    public View.OnClickListener backButtonClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().onBackPressed();
        }
    };

    public View.OnClickListener facebookClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(getActivity(),WebViewActivity.class);
            i.putExtra("name",resources.getString(R.string.facebook));
            i.putExtra("social_url",FB_URL);
            startActivity(i);
        }
    };

    public View.OnClickListener twitterClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(getActivity(),WebViewActivity.class);
            i.putExtra("name",resources.getString(R.string.twitter));
            i.putExtra("social_url",TWITTER_URL);
            startActivity(i);
        }
    };

    public View.OnClickListener instagramClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(getActivity(),WebViewActivity.class);
            i.putExtra("name",resources.getString(R.string.instagram));
            i.putExtra("social_url",INSTAGRAM_URL);
            startActivity(i);
        }
    };
}
