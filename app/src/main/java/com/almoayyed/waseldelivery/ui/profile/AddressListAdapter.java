package com.almoayyed.waseldelivery.ui.profile;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.models.UserLoginDetails;

/** Created on 20 Dec 2016 12:34 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.ViewTextWatcherListener
 */


public class AddressListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private UserLoginDetails userLoginDetails;
    private View view;
    private View.OnClickListener clickListener;
    private boolean isEditable;
    private final int ADDRESS_LIST = 1;
    private Resources resource;

    public AddressListAdapter(Context context, UserLoginDetails list) {
        if(list == null){
            this.userLoginDetails = new UserLoginDetails();
        }else{
            this.userLoginDetails = list;
        }

        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.manage_addr_list_items, parent, false);
        resource =context.getResources();
        return new ManageAddressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == ADDRESS_LIST) {
            ManageAddressViewHolder manageAddressViewHolder = (ManageAddressViewHolder) holder;
            String address = getManageAddressString(position);

            manageAddressViewHolder.address.setText(address);
            manageAddressViewHolder.address.setTag(position);
            manageAddressViewHolder.addressType.setText(userLoginDetails.getAddress().get(position).getAddressType());
            manageAddressViewHolder.addressType.setTag(position);
            if(isEditable){
                manageAddressViewHolder.addressTypeImg.setVisibility(View.GONE);
                manageAddressViewHolder.addressType.setVisibility(View.GONE);
                manageAddressViewHolder.deleteImg.setVisibility(View.VISIBLE);
                manageAddressViewHolder.deleteImg.setOnClickListener(clickListener);
                manageAddressViewHolder.deleteImg.setTag(position);
            }else{
                manageAddressViewHolder.addressTypeImg.setVisibility(View.VISIBLE);
                manageAddressViewHolder.addressType.setVisibility(View.VISIBLE);
                manageAddressViewHolder.deleteImg.setVisibility(View.GONE);
            }
            if(userLoginDetails.getAddress().get(position).getAddressType().equalsIgnoreCase("Home")){
                manageAddressViewHolder.addressTypeImg.setImageDrawable(resource.getDrawable(R.drawable.ic_home_off));
            }else if(userLoginDetails.getAddress().get(position).getAddressType().equalsIgnoreCase("Office")){
                manageAddressViewHolder.addressTypeImg.setImageDrawable(resource.getDrawable(R.drawable.ic_work_off));
            }else{
                manageAddressViewHolder.addressTypeImg.setImageDrawable(resource.getDrawable(R.drawable.ic_others_off));
            }
        }
    }

    @NonNull
    private String getManageAddressString(int position) {
        StringBuffer str = new StringBuffer();
        com.almoayyed.waseldelivery.models.Address address = userLoginDetails.getAddress().get(position);
        if(address.getDoorNumber()!= null && !TextUtils.isEmpty(address.getDoorNumber().trim())) {
            str.append(address.getDoorNumber().trim()+"\n");
        }
        if(address.getLandmark()!= null && !TextUtils.isEmpty(address.getLandmark().trim())) {
            str.append(address.getLandmark().trim()+"\n");
        }
        if(address.getLocation()!= null && !TextUtils.isEmpty(address.getLocation().trim())) {
            str.append(address.getLocation().trim());
        }
        return str.toString();
    }

    public void setItemClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public UserLoginDetails getAddresses(){
        return userLoginDetails;
    }

    @Override
    public int getItemCount() {
        return userLoginDetails.getAddress().size();
    }

    @Override
    public int getItemViewType(int position) {
        return ADDRESS_LIST;
    }

    public void addAddressToList(UserLoginDetails address) {
        userLoginDetails.setAddress(address.getAddress());
        notifyDataSetChanged();
    }

    public void setEditableMode(boolean isInEditMode) {
        isEditable = isInEditMode;
        notifyDataSetChanged();
    }

    public void deleteSelectedAddress(int pos) {

    }

    public void clearData() {
        userLoginDetails.getAddress().clear();
    }

    private class ManageAddressViewHolder extends RecyclerView.ViewHolder {
        private TextView address;
        private TextView addressType;
        private ImageView addressTypeImg;
        private ImageView deleteImg;

        public ManageAddressViewHolder(View view) {
            super(view);
            address = (TextView) view.findViewById(R.id.address);
            addressType = (TextView) view.findViewById(R.id.address_type);
            addressTypeImg = (ImageView) view.findViewById(R.id.address_img);
            deleteImg = (ImageView) view.findViewById(R.id.del_icon);
        }

    }

}
