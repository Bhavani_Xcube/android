package com.almoayyed.waseldelivery.ui.profile;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.databinding.CouponsListFragmentBinding;
import com.almoayyed.waseldelivery.models.Coupons;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.special_order.CouponsListAdapter;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/** Created on 19 Dec 2016 7:42 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.ViewTextWatcherListener
 */

public class CouponsListFragment extends BaseFragment{

    private CouponsListFragmentBinding binding;
    private CouponsListAdapter adapter;
    //startStopCompositeSubscription:
    private Observable<List<Coupons>> couponsObserable;
    private Subscription subscribe;
    CompositeSubscription startStopCompositeSubscription = new CompositeSubscription();
    private ProgressBarHandler mProgressBarHandler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(fragView==null){
            binding = DataBindingUtil.inflate(
                    inflater, R.layout.coupons_list_fragment, container, false);
            binding.setCouponsHandler(this);
            fragView = binding.getRoot();
            initViews();
        }

        return fragView;
    }

    private void initViews() {
        if(adapter==null){
            adapter =
                    new CouponsListAdapter(getActivity(), ProfileConstants.FROM_PROFILE,null);
        }
        adapter.setCouponSelectClickListener(useCodeClickListener);
//        adapter.setItemClickListener(couponsListClickListener);
        binding.profileCouponsList.setAdapter(adapter);
        binding.backBtn.getDrawable().setAutoMirrored(true);

        //placing order
        if(mProgressBarHandler == null){
            mProgressBarHandler = new ProgressBarHandler(getActivity(), binding.couponsMainLyt);
        }
        mProgressBarHandler.show();
        couponsObserable = WaselDeliveryService.getService().getCoupons();
        subscribe = couponsObserable.subscribe(couponsObserver);
        startStopCompositeSubscription.clear();
        startStopCompositeSubscription.add(subscribe);

        KeyboardHandler.setKeyboardHandler(binding.getRoot());
    }

    public View.OnClickListener backBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().onBackPressed();
        }
    };

    @Override
    public void onStart() {
        super.onStart();

        if(subscribe != null) {
            startStopCompositeSubscription.add(subscribe);
        }

    }

    @Override
    public void onStop() {
        startStopCompositeSubscription.clear();

        super.onStop();
    }

    View.OnClickListener useCodeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int pos = Integer.parseInt(view.getTag().toString());
            adapter.setSelectedCoupon(pos);
            setClipboard(adapter.getSelectedCoupon().getCode());
        }
    };

    // copy text to clipboard
    private void setClipboard(String text) {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
        }
    }

    /*
   * Observer for fetching coupons List
   * */
    Observer<List<Coupons>> couponsObserver = new Observer<List<Coupons>>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            mProgressBarHandler.hide();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
        }

        @Override
        public void onNext(List<Coupons> coupons) {
            //TODO: show Coupons List
            adapter.addCouponsList(coupons);
        }
    };
}
