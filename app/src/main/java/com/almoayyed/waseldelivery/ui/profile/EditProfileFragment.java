package com.almoayyed.waseldelivery.ui.profile;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.EditProfileFragmentBinding;
import com.almoayyed.waseldelivery.databinding_eventhandler.LoadImage;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.utils.Converters;
import com.almoayyed.waseldelivery.utils.FileUtils;
import com.almoayyed.waseldelivery.utils.ImageUtils;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Observer;

/** Created on 19 Dec 2016 3:40 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class EditProfileFragment extends BaseFragment {

    private EditProfileFragmentBinding binding;
    //    private Bundle data;
    private String mImgPath;
    private LoginCredentialsPreference preference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(fragView==null){
            binding = DataBindingUtil.inflate(
                    inflater, R.layout.edit_profile_fragment, container, false);
            binding.setEditProfileHandler(this);
            fragView = binding.getRoot();
            initViews();

        }
        //setupCameraIntentHelper();
        return fragView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null){
            mImgPath = savedInstanceState.getString("filePath",mImgPath);
            binding.nameEt.setText(savedInstanceState.getString("name",binding.nameEt.getText().toString()));
        }
        // mCameraIntentHelper.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //mCameraIntentHelper.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("filePath",mImgPath);
        savedInstanceState.putString("name",binding.nameEt.getText().toString());
    }

    private void initViews() {
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        binding.backBtn.getDrawable().setAutoMirrored(true);
        preference = LoginCredentialsPreference.getLoginPreference();
        binding.nameEt.setText(preference.getName());
        binding.nameEt.setSelection(binding.nameEt.getText().length());
        binding.phNum.setText(preference.getPhoneNo());
        binding.emailId.setText(preference.getEmail());
        if(!(preference.getImgUrl().contains("null")
                || TextUtils.isEmpty(preference.getImgUrl()))){
            String imgPath = preference.getImgUrl();
            LoadImage.loadImage(binding.profilePicture1, imgPath,getResources().getDrawable(R.drawable.default_profile));
        }

        KeyboardHandler.setKeyboardHandler(binding.getRoot());
        checkForRTL();
    }

    private void checkForRTL() {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            boolean isRightToLeft = getResources().getBoolean(R.bool.is_right_to_left);
            if(isRightToLeft){
                binding.nameEt.setGravity(Gravity.END);
                binding.phNum.setGravity(Gravity.END);
                binding.emailId.setGravity(Gravity.END);
            }else{
                binding.nameEt.setGravity(Gravity.START);
                binding.phNum.setGravity(Gravity.START);
                binding.emailId.setGravity(Gravity.START);
            }
        }
    }

    public View.OnClickListener onBackBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().onBackPressed();
        }
    };

    private static class EditProfileView extends View
    {

        public EditProfileView(Context context) {
            super(context);
        }

        protected void onDraw(Canvas canvas)
        {

        }
    }
    //Handling action when device back is pressed
    @Override
    public boolean onPressDeviceBackButton() {
        super.onPressDeviceBackButton();
        File file = new File(TextUtils.isEmpty(mImgPath)?" " :mImgPath);
        if(!preference.getName().equalsIgnoreCase(binding.nameEt.getText().toString()) ||
                file.exists()) {
            final WaselDialog dialog = new WaselDialog(getContext());
            dialog.setTitle(getResources().getString(R.string.app_name));
            dialog.setMessage(getResources().getString(R.string.clear_profile_edit_msg));
            dialog.setPositiveButton(getResources().getString(R.string.save),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            saveUpdatedDetails();
                            dialog.cancel();
                        }
                    });
            dialog.setNegativeButton(getResources().getString(R.string.discard),
                    new View.OnClickListener() {
                        public void onClick(View view) {
                            getActivity().finish();
                            dialog.cancel();
                        }
                    });
            dialog.setPositiveBtnColor(R.color.ok_btn);
            dialog.setNegetiveBtnColor(android.R.color.white);
            dialog.show();
            return false;
        }
        return true;
    }

    private void showCancelAlert() {
        final WaselDialog dialog = new WaselDialog(getContext());
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setMessage(getResources().getString(R.string.clear_profile_edit_msg));
        dialog.setPositiveButton(getResources().getString(R.string.save),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        saveUpdatedDetails();
                        dialog.cancel();
                    }
                });
        dialog.setNegativeButton(getResources().getString(R.string.cancel),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        getActivity().onBackPressed();
                        dialog.cancel();
                    }
                });
        dialog.setPositiveBtnColor(R.color.ok_btn);
        dialog.setNegetiveBtnColor(android.R.color.white);
        dialog.show();
    }

    public View.OnClickListener saveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            saveUpdatedDetails();
        }
    };

    private void saveUpdatedDetails() {

        if(mProgressBarHandler == null){
            mProgressBarHandler = new ProgressBarHandler(getContext(), binding.profileMainLyt);
        }
        mProgressBarHandler.show();

        File file = new File(TextUtils.isEmpty(mImgPath)?" " :mImgPath);
        if(file.exists()){
            MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("media/*"), file));
            String userId = preference.getId();

            Observable<UserLoginDetails> saveProfileObserable = WaselDeliveryService.getService().saveProfilePic(RequestBody.create(
                    MediaType.parse("text/plain"), userId), body );
            addObserver(saveProfileObserable, saveProfileObserver);
        }else{
            if (TextUtils.isEmpty((binding.nameEt.getText().toString() != null) ? binding.nameEt.getText().toString().trim() : null)) {
                mProgressBarHandler.hide();
                WaselToast.showToast(getContext().getResources().getString(R.string.empty_name));
            }else if(!binding.nameEt.getText().toString().matches(getString(R.string.name_pattern))){
                mProgressBarHandler.hide();
                WaselToast.showToast(getContext().getResources().getString(R.string.invalid_name));
            }else if(!binding.nameEt.getText().toString().equalsIgnoreCase(preference.getName())) {
                UserLoginDetails details = new UserLoginDetails();
                details.setName(binding.nameEt.getText().toString());
                details.setId(preference.getId());

                Observable<UserLoginDetails> saveProfileDetailsObservable = WaselDeliveryService.getService().updateProfile(details);
                addObserver(saveProfileDetailsObservable, saveProfileDetailsObserver);
            }else{
                getActivity().onBackPressed();
            }
        }
    }

    /*
 * Observer for fetching User details
 * */
    Observer<UserLoginDetails> saveProfileObserver = new Observer<UserLoginDetails>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            mProgressBarHandler.hide();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
        }

        @Override
        public void onNext(UserLoginDetails details) {
            //TODO: show UserDetails
            Log.e("TAG","Details : "+ details);
            details.setId(preference.getId());
            details.setImageUrl(details.getImageUrl());
            if (TextUtils.isEmpty((binding.nameEt.getText().toString() != null) ? binding.nameEt.getText().toString().trim() : null)) {
                WaselToast.showToast(getContext().getResources().getString(R.string.empty_name));
            }else if(!binding.nameEt.getText().toString().matches(getString(R.string.name_pattern))){
                WaselToast.showToast(getContext().getResources().getString(R.string.invalid_name));
            }else{
                if(!binding.nameEt.getText().toString().equalsIgnoreCase(preference.getName())) {
                    details.setName(binding.nameEt.getText().toString());
                }
            /*List<Address> delAddr = new ArrayList<Address>();
            delAddr.add(adapter.getAddresses().getAddress().get(pos));
            deleteSelectedAddr.setAddress(delAddr);*/

                Observable<UserLoginDetails> saveProfileDetailsObservable = WaselDeliveryService.getService().updateProfile(details);
                addObserver(saveProfileDetailsObservable, saveProfileDetailsObserver);
            }
        }
    };

    /*
* Observer for updating profile
* */
    Observer<UserLoginDetails> saveProfileDetailsObserver = new Observer<UserLoginDetails>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            mProgressBarHandler.hide();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
        }

        @Override
        public void onNext(UserLoginDetails details) {
            //TODO: show UserDetails
            Log.e("TAG","Details : "+ details);
            preference.saveUserCredentials(details);
            getActivity().finish();
        }
    };


    public View.OnClickListener changePicClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            selectPicture();
        }
    };

    private void selectPicture() {

        BottomSheetDialogFragment bottomSheetDialogFragment = new ProfilePicSelectionDialogFragment();
        bottomSheetDialogFragment.setTargetFragment(this, ProfileConstants.EDIT_PROFILE_FRAGMENT);
        bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());

        /*final CharSequence[] items = { "Take Photo", "Choose from Gallery",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("      Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    checkForCameraPermissions();
                } else if (items[item].equals("Choose from Gallery")) {
                    checkForGalleryPermissions();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();*/
    }

    private void checkForGalleryPermissions() {
        String[] galleryPicPermissions = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
        };
        requestPermissions(galleryPicPermissions,
                ProfileConstants.GALLERY_RESULT);
    }

    private void checkForCameraPermissions() {
        String[] cameraPicPermissions = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        };
        requestPermissions(cameraPicPermissions,
                ProfileConstants.CAMERA_RESULT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ProfileConstants.CAMERA_RESULT: {
                // If request is cancelled, the result arrays are empty.
                if (isAllPermissionsGranted(permissions, grantResults)) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    cameraIntent();
                } else {
                    WaselToast.showToast("No camera permissions");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
            }
            case ProfileConstants.GALLERY_RESULT:
                if (isAllPermissionsGranted(permissions, grantResults)) {
                    galleryIntent();
                } else {
                    WaselToast.showToast("No storage permissions");
                }
                break;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private boolean isAllPermissionsGranted(String permissions[], int[] grantResults) {
        int perGrantedCnt = 0;
        for(int i=0; i<permissions.length; i++){
            if(grantResults[i]==0){
                perGrantedCnt ++;
            }
        }
        return perGrantedCnt==permissions.length? true:false;
    }

    private void galleryIntent() {
        mImgPath = "";
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType(ProfileConstants.IMAGE_STR);
        getActivity().startActivityForResult(galleryIntent, ProfileConstants.GALLERY_RESULT);
    }

    private void cameraIntent() {
        Uri mPhotoUri = getContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new ContentValues());

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        File dir = FileUtils.getTempImageFolder();
        File directoryDcim = new File(dir.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        mImgPath = directoryDcim.getAbsolutePath();
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(directoryDcim));
        getActivity().startActivityForResult(cameraIntent, ProfileConstants.CAMERA_RESULT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                default:
                    compressAndLoadImage();

                    break;
                //comes under this case if user selects the photo from gallery
                case ProfileConstants.GALLERY_RESULT:
                    handleGalleryImageData(data);
                    break;
                case ProfileConstants.EDIT_PROFILE_FRAGMENT:
                        String uploadType = data.getExtras().getString(ProfileConstants.PROFILE_PIC_UPLOAD_TYPE, "");

                        if (uploadType.equalsIgnoreCase(ProfileConstants.CAMERA)) {
                            checkForCameraPermissions();
                        } else if (uploadType.equalsIgnoreCase(ProfileConstants.GALLERY)) {
                            checkForGalleryPermissions();
                        }
                    break;
            }
        }
    }

    private void compressAndLoadImage() {
        if(!TextUtils.isEmpty(mImgPath)){
            mImgPath = ImageUtils.compressImage(mImgPath, 480f, 480f);

            Bitmap scaledBitmap = ImageUtils.createResizedBitmap(mImgPath, binding.profilePicture1.getWidth());
            binding.profilePicture1.setImageBitmap(scaledBitmap);
        }
    }

    /**
     *This method is called when user selects image from gallery.
     *It handles the Uri object that we get and converts to bitmap.
     *
     * @param data(intent)
     *
     */
    private void handleGalleryImageData(Intent data) {
        Uri selectedImageUri = data.getData();
        try {
            if (selectedImageUri != null) {
                String scheme = selectedImageUri.getScheme();
                if ("file".equals(scheme)) { // process as a uri that points to a file
                    mImgPath = selectedImageUri.getPath();
                }
                else if ("content".equals(scheme)) { // process as a uri that points to a content item
                    mImgPath = ImageUtils.getRealPathFromURI(data.getData());
                }
                mImgPath = ImageUtils.compressImage(mImgPath, 480f, 480f);

                int dimen = Converters.pxToDp(115);
                Bitmap scaledBitmap = ImageUtils.createResizedBitmap(mImgPath, dimen);
                binding.profilePicture1.setImageBitmap(scaledBitmap);

            }
        } catch (Exception e) {
            Log.e("TAG", Log.getStackTraceString(e));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

//        FileUtils.deleteTempImageFolder();
    }
}