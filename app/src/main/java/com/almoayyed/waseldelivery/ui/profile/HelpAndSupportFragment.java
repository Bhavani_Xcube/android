package com.almoayyed.waseldelivery.ui.profile;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.databinding.HelpAndSupportFragmentBinding;
import com.almoayyed.waseldelivery.models.HelpAndSupport;
import com.almoayyed.waseldelivery.models.Order;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.utils.HandleWebClientLinks;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselToast;

import rx.Observable;
import rx.Observer;

/** Created on 19 Dec 2016 5:56 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.ViewTextWatcherListener
 */


public class HelpAndSupportFragment extends BaseFragment{
    private HelpAndSupportFragmentBinding binding;
    private HandleWebClientLinks handleWebClientLinks;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(fragView==null){
            binding = DataBindingUtil.inflate(
                    inflater, R.layout.help_and_support_fragment, container, false);
            binding.setClickHandler(clickHandler);
            fragView = binding.getRoot();
            binding.backBtn.getDrawable().setAutoMirrored(true);

            mProgressBarHandler = new ProgressBarHandler(getActivity(), binding.profileMainLyt);

            Observable<HelpAndSupport> helpAndSupportObservable = WaselDeliveryService.getService().getHelpAndSupportDetails();
            addObserver(helpAndSupportObservable, helpAndSupportDetailsObserver);

            handleWebClientLinks = new HandleWebClientLinks();
        }

        return fragView;
    }

    public View.OnClickListener clickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getId() == binding.backBtn.getId()){
                getActivity().onBackPressed();
            }else if(view.getId() == binding.supportContactNumber.getId()){
                handleWebClientLinks.handleLink(getContext(),"tel:"+binding.supportContactNumber.getText().toString());
            }else if(view.getId() == binding.supportEmail.getId()) {
                handleWebClientLinks.handleLink(getContext(),"mailto:"+binding.supportEmail.getText().toString());
            }

        }
    };

    Observer<HelpAndSupport> helpAndSupportDetailsObserver = new Observer<HelpAndSupport>() {

        @Override
        public void onCompleted() {
            clearSubscriptions();
        }

        @Override
        public void onError(Throwable e) {
            clearSubscriptions();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
        }

        @Override
        public void onNext(HelpAndSupport order) {
            //Update help & support information
            binding.setHelpSupportInfo(order);
            clearSubscriptions();
        }
    };
}
