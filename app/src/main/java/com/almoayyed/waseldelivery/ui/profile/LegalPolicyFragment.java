package com.almoayyed.waseldelivery.ui.profile;

import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.BuildConfig;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.databinding.LegalPolicyFragmentBinding;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.views.WaselToast;

/** Created on 20 Jan 2017 12:00 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public class LegalPolicyFragment extends BaseFragment{
    private LegalPolicyFragmentBinding binding;
    private String TermsOfService_URL = BuildConfig.BASE_URL + "public/terms";
    private String LegalPolicy_URL = BuildConfig.BASE_URL + "public/privacy";
    private Resources resources;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(fragView==null){
            binding = DataBindingUtil.inflate(
                    inflater, R.layout.legal_policy_fragment, container, false);
            fragView = binding.getRoot();
            resources = getResources();
            binding.setClickHandler(clickHandler);
            binding.backBtn.getDrawable().setAutoMirrored(true);
        }
        return fragView;
    }

    public View.OnClickListener clickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getId() == binding.termsOfService.getId()){
                loadUrl(resources.getString(R.string.terms_of_service),TermsOfService_URL);
            }else if(view.getId() == binding.policy.getId()){
                loadUrl(resources.getString(R.string.privacy_policy),LegalPolicy_URL);
            }else if(view.getId() == binding.backBtn.getId()){
                getActivity().onBackPressed();
            }
        }
    };

    private void loadUrl(String name, String url) {
        Intent i = new Intent(getActivity(),WebViewActivity.class);
        i.putExtra("name",name);
        i.putExtra("social_url", url);
        startActivity(i);
    }

}
