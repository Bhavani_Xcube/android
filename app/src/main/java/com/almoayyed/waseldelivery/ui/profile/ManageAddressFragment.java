package com.almoayyed.waseldelivery.ui.profile;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.LocalPermissionConstants;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.ManageAddressFragmentBinding;
import com.almoayyed.waseldelivery.models.Address;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.models.UserLoginDetails;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.CartViewDisplayInterface;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.address.AddAddressActivity;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/** Created on 20 Dec 2016 12:16 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.ViewTextWatcherListener
 */

public class ManageAddressFragment extends BaseFragment {

    public static final String LOGIN_DETAILS = "login_details";
    private ManageAddressFragmentBinding binding;
    private Observable<UserLoginDetails> getAddressObservable;
    private Subscription subscribe;
    CompositeSubscription startStopCompositeSubscription = new CompositeSubscription();
    private AddressListAdapter adapter;
    private boolean isInEditMode = false;
    private LoginCredentialsPreference loginPreference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(fragView==null){
            binding = DataBindingUtil.inflate(
                    inflater, R.layout.manage_address_fragment, container, false);
            binding.setManageAddrHandler(this);
            fragView = binding.getRoot();
            initViews();
        }

        return fragView;
    }

    private void initViews() {

        adapter = new AddressListAdapter(getActivity(),null);

        /*if(details != null && details.getAddress().size() != 0){
            adapter.addAddressToList(details);
        }*/
        binding.backBtn.getDrawable().setAutoMirrored(true);
        binding.manageAddrList.setAdapter(adapter);
        adapter.setItemClickListener(deleteBtnClickListener);

        //placing order
        if(mProgressBarHandler == null){
            mProgressBarHandler = new ProgressBarHandler(getActivity(), binding.manageAddrMainLyt);
        }
        mProgressBarHandler.show();
        loginPreference = LoginCredentialsPreference.getLoginPreference();
        //Loading addresses information from server
        fetchAddressFromServer();

        //Pull to refresh
        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshAddressDetails();
            }
        });

    }

    private void refreshAddressDetails() {
        adapter.clearData();
        fetchAddressFromServer();
    }

    @Override
    public void onStart() {
        super.onStart();

        //User successfully login while pressing checkout button
        Bundle data = getArguments();
        if (data != null && data.containsKey(CheckoutConstants.KEY_ADDRESS)) {
            binding.noAddrTxt.setVisibility(View.GONE);
            binding.edit.setVisibility(View.VISIBLE);
            isInEditMode = true;
            updateFragmentToEditMode();
            adapter.addAddressToList((UserLoginDetails) data.getSerializable(CheckoutConstants.KEY_ADDRESS));
            data.clear();
        }

    }

    View.OnClickListener deleteBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int pos = Integer.parseInt(view.getTag().toString());
            showDeleteAlert(pos);
        }
    };

    private void showDeleteAlert(final int pos) {
        final WaselDialog dialog =  new WaselDialog(getContext());
        dialog.setTitle(getResources().getString(R.string.are_you_sure));
        dialog.setMessage(getResources().getString(R.string.del_addr));
        dialog.setPositiveButton(getResources().getString(R.string.yes),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        UserLoginDetails deleteSelectedAddr = new UserLoginDetails();
                        deleteSelectedAddr.setId(loginPreference.getId());
                        List<Address> delAddr = new ArrayList<Address>();
                        delAddr.add(adapter.getAddresses().getAddress().get(pos));
                        deleteSelectedAddr.setAddress(delAddr);

                        getAddressObservable = WaselDeliveryService.getService().updateProfile(deleteSelectedAddr);
                        subscribe = getAddressObservable.subscribe(deleteSelectedAddrObserver);
                        startStopCompositeSubscription.add(subscribe);
                        dialog.cancel();
                    }
                });
        dialog.setNegativeButton(getResources().getString(R.string.cancel),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });
        dialog.setPositiveBtnColor(R.color.ok_btn);
        dialog.setNegetiveBtnColor(android.R.color.white);
        dialog.show();
    }

    /*
* Observer for deleting selected address
* */
    public Observer<UserLoginDetails> deleteSelectedAddrObserver = new Observer<UserLoginDetails>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            WaselToast.showToast(getServerErrorMessage(e));
            mProgressBarHandler.hide();
        }

        @Override
        public void onNext(UserLoginDetails userLoginDetails) {
            Log.e("TAG","userDetails :"+userLoginDetails);
            if(userLoginDetails!=null) {
                if(userLoginDetails.getAddress().size()==0){
                    updateFragmentToEditMode();
                    binding.noAddrTxt.setVisibility(View.VISIBLE);
                    binding.edit.setVisibility(View.GONE);
                }else{
                    binding.noAddrTxt.setVisibility(View.GONE);
                    binding.edit.setVisibility(View.VISIBLE);
                }
                adapter.addAddressToList(userLoginDetails);
            }
        }
    };


    public View.OnClickListener backBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(isInEditMode){
                updateFragmentToEditMode();
            }else{
                getActivity().onBackPressed();
            }
        }
    };

    public View.OnClickListener editClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            updateFragmentToEditMode();
        }
    };

    public void updateFragmentToEditMode() {
        if(!isInEditMode){
            isInEditMode = true;
            binding.edit.setText(getResources().getString(R.string.done));
            binding.addNewAddress.setVisibility(View.GONE);
        }else{
            isInEditMode = false;
            binding.edit.setText(getResources().getString(R.string.edit));
            binding.addNewAddress.setVisibility(View.VISIBLE);
        }
        adapter.setEditableMode(isInEditMode);
    }


    public View.OnClickListener addNewAddressClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(getActivity(), AddAddressActivity.class);
            UserLocation mLocation = new UserLocation();
            mLocation.setLatitude(LocationPreference.getCurrentLat());
            mLocation.setLongitude(LocationPreference.getCurrentLng());
            StringBuffer currentLocName = new StringBuffer(LocationPreference.getCurrentLocName());
            currentLocName.replace(currentLocName.length()-2,currentLocName.length(),"");
            mLocation.setSubLocality3(currentLocName.toString());
            i.putExtra(CheckoutConstants.KEY_ADDRESS,mLocation);
            i.putExtra(ProfileConstants.FROM,ProfileConstants.FROM_PROFILE);
            startActivity(i);
        }
    };

    //Makes api to fetch address info from server
    private void fetchAddressFromServer(){
        mProgressBarHandler.show();
        getAddressObservable = WaselDeliveryService.getService().getProfile(loginPreference.getId());
        addObserver(getAddressObservable, getAddressesObserver);
    }

    /*
       * Observer for listening address list
       * */
    Observer<UserLoginDetails> getAddressesObserver = new Observer<UserLoginDetails>() {
        @Override
        public void onCompleted() {
            clearSubscriptions();
            subscribe = null;
        }

        @Override
        public void onError(Throwable e) {
            subscribe = null;
            clearSubscriptions();
            binding.swipeRefreshLayout.setRefreshing(false);
            WaselToast.showToast(getServerErrorMessage(e));
        }

        @Override
        public void onNext(UserLoginDetails profileDetails) {
            //TODO: show order tacking
            Log.e("TAG","userDetails :"+profileDetails);
            if(profileDetails!=null) {
                if(profileDetails.getAddress().size()==0){
                    isInEditMode = true;
                    updateFragmentToEditMode();
                    binding.noAddrTxt.setVisibility(View.VISIBLE);
                    binding.edit.setVisibility(View.GONE);
                }else{
                    binding.noAddrTxt.setVisibility(View.GONE);
                    binding.edit.setVisibility(View.VISIBLE);
                }
                adapter.addAddressToList(profileDetails);
            }
            binding.swipeRefreshLayout.setRefreshing(false);
            clearSubscriptions();
        }
    };
}
