package com.almoayyed.waseldelivery.ui.profile;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.databinding.ProfilePicSelectionLytBinding;

/**
 * Created on 16 Feb 2017 02:27 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public class ProfilePicSelectionDialogFragment extends BottomSheetDialogFragment {

    private ProfilePicSelectionLytBinding binding;
    private View contentView;

    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.profile_pic_selection_lyt, null, false);
        binding.setClickHandler(clickHandler);
        contentView = binding.getRoot();
        dialog.setContentView(contentView);
    }

    public View.OnClickListener clickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getId() == binding.takePic.getId()){
                Intent i = new Intent()
                        .putExtra(ProfileConstants.PROFILE_PIC_UPLOAD_TYPE, ProfileConstants.CAMERA);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
            }else if(view.getId() == binding.chooseFromGallery.getId()){
                Intent i = new Intent()
                        .putExtra(ProfileConstants.PROFILE_PIC_UPLOAD_TYPE, ProfileConstants.GALLERY);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
            }
            dismiss();
        }
    };
}
