package com.almoayyed.waseldelivery.ui.profile;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.almoayyed.waseldelivery.BuildConfig;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.databinding.ActivityWebviewBinding;
import com.almoayyed.waseldelivery.ui.BaseActivity;
import com.almoayyed.waseldelivery.utils.HandleWebClientLinks;
import com.almoayyed.waseldelivery.utils.NetworkConnection;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.views.WaselToast;

/** Created on 9 Jan 2017 10:44 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class WebViewActivity extends BaseActivity{

    private ActivityWebviewBinding binding;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_webview);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_webview);
        String url = getIntent().getExtras().getString("social_url");

        mProgressBarHandler = new ProgressBarHandler(this, binding.webViewLyt);
        binding.backBtn.getDrawable().setAutoMirrored(true);

        binding.webView.getSettings().setJavaScriptEnabled(true); // enable javascript

       /* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
        {
            // only for Kitkat and newer versions
            WebView.setWebContentsDebuggingEnabled(true);
        }*/

        binding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                return HandleWebClientLinks.handleLink(WebViewActivity.this, url)? true: super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
                    // Do something for lollipop and above versions
                    String url = request.getUrl().toString();
                    return HandleWebClientLinks.handleLink(WebViewActivity.this, url);
                } else{
                    // do something for phones running an SDK before lollipop
                    return super.shouldOverrideUrlLoading(view, request);
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                WaselToast.showToast(description);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressBarHandler.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBarHandler.hide();
            }
        });

        if(NetworkConnection.isNetworkAvailable()){
            binding.webView.loadUrl(url);
        }else{
            WaselToast.showToast(getString(R.string.check_internet));
        }
        binding.titleText.setText(getIntent().getExtras().getString("name"));
        binding.setClickHandler(clickListener);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
