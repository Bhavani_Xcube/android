package com.almoayyed.waseldelivery.ui.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.models.Amenity;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 30 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class AmenitiesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    //holds Amenities list data
    private final List<Amenity> amenitiesList;
    private String[] array;
    private View.OnClickListener amenitiesClickListener;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.amenities_list_layout, parent, false);
        return new AmenitiesListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof AmenitiesListViewHolder){
            AmenitiesListViewHolder viewHolder = (AmenitiesListViewHolder) holder;
            Amenity amenity = amenitiesList.get(position);
            if(amenity.getImageUrl() <= array.length){
                String[] splittedItem = array[amenity.getImageUrl()-1].split("-");

            /*if (amenitiesList.get(position).toString().equalsIgnoreCase(splittedItem[0])) {*/
                int resID = WaselApplication.getContext().getResources().getIdentifier(splittedItem[2], "drawable", WaselApplication.getContext().getPackageName());
                viewHolder.amenitiesListImg.setImageResource(resID);

            /*}*/
            }else {
                viewHolder.amenitiesListImg.setImageResource(R.drawable.miscellaneous);
            }
            viewHolder.amenitiesListImg.setSelected(true);
            viewHolder.amenitiesListImg.setTag(position);
            viewHolder.amenitiesListTxt.setText(amenity.getName());
            viewHolder.amenitiesListTxt.setTag(position);
            viewHolder.amenitiesLyt.setOnClickListener(amenitiesClickListener);
            viewHolder.amenitiesLyt.setTag(position);
        }
    }

    public AmenitiesListAdapter(Context context, List<Amenity> amenitiesList){

        if(amenitiesList == null){
            this.amenitiesList = new ArrayList<>();
        }else {
            this.amenitiesList = amenitiesList;

        }
        array = WaselApplication.getContext().getResources().getStringArray(R.array.categories_array);
        this.context = context;
    }

    @Override
    public int getItemCount() {
        if(amenitiesList != null){
            return amenitiesList.size();
        }
        return 0;
    }

    public void setAmenitiesClickListener(View.OnClickListener amenitiesClickListener) {
        this.amenitiesClickListener = amenitiesClickListener;
    }

    public Amenity getItem(int pos) {
        return amenitiesList.get(pos);
    }

    public class AmenitiesListViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private ImageView amenitiesListImg;
        private TextView amenitiesListTxt;
        private ImageView img;
        private LinearLayout amenitiesLyt;

        public AmenitiesListViewHolder(View view) {
            super(view);
            mView = view;
            img = new ImageView(context);
            amenitiesListImg = (ImageView) mView.findViewById(R.id.amenity_icon);
            amenitiesListTxt = (TextView) mView.findViewById(R.id.amenity_text);
            amenitiesLyt = (LinearLayout) mView.findViewById(R.id.amenities_lyt);
        }
    }


}
