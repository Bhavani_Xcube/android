package com.almoayyed.waseldelivery.ui.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;

import java.util.ArrayList;
import java.util.List;

import com.almoayyed.waseldelivery.models.Search;

/**
 * Created on 29 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class RecentSearchHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //holds the Searched list data
    private List<Search> searchedTextList;
    private View.OnClickListener deleteClickListener;
    private View.OnClickListener recentHistoryItemClickListener;
    private int maxCount;

    public RecentSearchHistoryAdapter(Context context, List<Search> searchedTextList, int maxCount){

        if(searchedTextList == null){
            this.searchedTextList = new ArrayList<>();
        }else {
            this.searchedTextList = searchedTextList;

        }
        this.maxCount = maxCount;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_history_item, parent, false);
        return new SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof SearchViewHolder){
            SearchViewHolder viewHolder = (SearchViewHolder) holder;
            viewHolder.searchedItemName.setText(searchedTextList.get(position).getSearchedName());
            viewHolder.searchHistoryImg.setImageResource(R.drawable.close);
            viewHolder.searchHistoryImg.setOnClickListener(deleteClickListener);
            viewHolder.searchHistoryImg.setTag(searchedTextList.get(position));
            viewHolder.recentHistoryListLyt.setOnClickListener(recentHistoryItemClickListener);
            viewHolder.recentHistoryListLyt.setTag(searchedTextList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if(searchedTextList != null){
            if(maxCount == 0){
                return searchedTextList.size();
            }else{
                return Math.min(maxCount,searchedTextList.size());
            }

        }
        return 0;
    }

    public void clearSearchedList(){
        searchedTextList.clear();
        notifyDataSetChanged();
    }


    public void setDeleteClickListener(View.OnClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }

    public void setRecentHistoryItemClickListener(View.OnClickListener recentHistoryItemClickListener) {
        this.recentHistoryItemClickListener = recentHistoryItemClickListener;
    }

    public void setSearchListAdapterData(List<Search> list){
        searchedTextList.clear();
        searchedTextList.addAll(list);
        notifyDataSetChanged();
    }

    public void updateSearchedList(Search searchedItem) {
        int searchTextExistingIndex = getSearchTextExistingIndex(searchedItem);
        if(searchTextExistingIndex != -1){
            searchedTextList.remove(searchTextExistingIndex);
        }
        searchedTextList.add(0,searchedItem);
        notifyDataSetChanged();

    }

    private int getSearchTextExistingIndex(Search searchedItem) {
        for (int index = 0; index<searchedTextList.size();index++) {
            if (searchedTextList.get(index).getSearchedName().equalsIgnoreCase(searchedItem.getSearchedName())) {
                return index;
            }
        }
        return -1;
    }

    public void deleteSelectedItem(String searchId) {
        for (int i=0; i<=searchedTextList.size();i++){
            if(searchedTextList.get(i).getSearchedId().equalsIgnoreCase(searchId)){
                searchedTextList.remove(i);
                notifyDataSetChanged();
                return;
            }
        }
    }

    public List<Search> getSearchedList() {
        return searchedTextList;
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {
        private ImageView searchHistoryImg;
        private TextView searchedItemName;
        private RelativeLayout recentHistoryListLyt;

        public SearchViewHolder(View view) {
            super(view);
            searchHistoryImg = (ImageView) view.findViewById(R.id.search_history_img);
            searchedItemName = (TextView) view.findViewById(R.id.searched_item_name);
            recentHistoryListLyt = (RelativeLayout) view.findViewById(R.id.recent_history_list_lyt);
        }
    }
}
