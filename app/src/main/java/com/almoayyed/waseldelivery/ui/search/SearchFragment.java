package com.almoayyed.waseldelivery.ui.search;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.FragmentsIndex;
import com.almoayyed.waseldelivery.constants.LocalPermissionConstants;
import com.almoayyed.waseldelivery.data_saving.AmenityPreference;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.data_saving.WaselDeliveryDB;
import com.almoayyed.waseldelivery.databinding.SearchLayoutBinding;
import com.almoayyed.waseldelivery.models.Amenity;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.models.Search;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.network_interface.wasel_api.OutletsReqBody;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.LoadFragmentActivity;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.home.OutletDetails.OutletDetailsFragment;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created on 14 Oct 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class SearchFragment extends BaseFragment implements View.OnClickListener{
    public static final String AMENITY_ID = "amenityId";
    private RecentSearchHistoryAdapter recentSearchHistoryAdapter;
    private SearchedOutletsAdapter searchedOutletsAdapter;
    private WaselDeliveryDB deliveryDB;
    private final int inputThresholdLmt = 1;

    private Subscription searchSubscriber = null;
    private ProgressBarHandler mProgressBarHandler;
    private boolean hasMoreOutlets = true;
    private boolean outletsLoading = false;
    private SearchLayoutBinding binding;
    private AmenitiesListAdapter amenitiesListAdapter;
    private Resources resources;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(fragView == null){
            binding = DataBindingUtil.inflate(inflater,R.layout.search_layout, container, false);
            fragView = binding.getRoot();
            binding.setBtnHandler(this);
            initViews(fragView);
        }

        return fragView;
    }


    private void initViews(View view) {
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity());
        binding.searchedRestaurantList.setLayoutManager(linearLayoutManager1);

        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getActivity());
        binding.restaurantList.setLayoutManager(linearLayoutManager2);

        mProgressBarHandler = new ProgressBarHandler(getContext(), binding.searchMainLyt);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        binding.amenitiesList.setLayoutManager(gridLayoutManager);

        binding.clearHistory.setOnClickListener(this);
        binding.recentHistoryList.setOnClickListener(this);

        deliveryDB = new WaselDeliveryDB(WaselApplication.getContext());

        KeyboardHandler.setKeyboardHandler(view);

        recentSearchHistoryAdapter = new RecentSearchHistoryAdapter(getContext(),null, 3);
        binding.searchedRestaurantList.setAdapter(recentSearchHistoryAdapter);
        getSearchedList();

        searchedOutletsAdapter = new SearchedOutletsAdapter(getContext(),null);
        searchedOutletsAdapter.setSearchedOutletClickListener(searchedItemClick);
        binding.restaurantList.setAdapter(searchedOutletsAdapter);


        if(recentSearchHistoryAdapter != null){
            recentSearchHistoryAdapter.setDeleteClickListener(deleteSelectedItem);
            recentSearchHistoryAdapter.setRecentHistoryItemClickListener(recentHistoryItemClickListener);
        }

        searchedOutletsAdapter.clearData();
        binding.searchInput.setText("");
        resources = getResources();

        //Search from keyboard
        binding.searchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch(binding.searchInput.getText().toString());
                    return true;
                }
                return false;
            }
        });
    }


    private void loadAmenitiesInfo() {

        List<Amenity> amenities = getAmenities();
        Collections.sort(amenities);

        amenitiesListAdapter = new AmenitiesListAdapter(getContext(), amenities);
        amenitiesListAdapter.setAmenitiesClickListener(selectedAmenityClick);
        binding.amenitiesList.setAdapter(amenitiesListAdapter);
    }

    List<Amenity> getAmenities() {
        try {
            ArrayList<Amenity> amenities = new ArrayList<>();
            AmenityPreference amenityPreference = AmenityPreference.getAmenityPreference();
            Set<String> data = amenityPreference.getAmenityIds();
            for (String amenityData : data){
                String[] amenityItem = amenityData.split("-");
                Amenity amenity = new Amenity();
                amenity.setId(Integer.parseInt(amenityItem[0]));
                amenity.setImageUrl(Integer.parseInt(amenityItem[1]));
                amenity.setName(amenityItem[2]);
                amenities.add(amenity);
            }

            return amenities;
        } catch(Exception ex) {
            Loggers.error(Log.getStackTraceString(ex));
        }
        return new ArrayList<>();
    }


    private void getSearchedList() {
        List<Search> searchedList = deliveryDB.getSearchedText();
        Log.e("TAg","searchedList : "+ searchedList.toString());

        updateHistoryView(searchedList.size() != 0);

        if(searchedList.size() >= 3){
            binding.recentHistoryList.setVisibility(View.VISIBLE);
            binding.moreImg.setVisibility(View.VISIBLE);
            binding.moreImgLine.setVisibility(View.VISIBLE);
        }else{
            binding.recentHistoryList.setVisibility(View.GONE);
            binding.moreImg.setVisibility(View.GONE);
            binding.moreImgLine.setVisibility(View.GONE);
        }
        recentSearchHistoryAdapter.setSearchListAdapterData(searchedList);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.clear_history:
                clearAllSearchedHistory();
                break;
            case R.id.recent_history_list:
                Intent i = new Intent(getContext(),SearchHistoryActivity.class);
                getActivity().startActivityForResult(i, LocalPermissionConstants.REQUEST_SEARCH_CLEAR);
                break;
        }
    }


    private void loadRestaurantsFromServer(){
        outletsLoading = true;
        mProgressBarHandler.show();
        OutletsReqBody postBody = new OutletsReqBody();
        postBody.setLatitude(LocationPreference.getCurrentLat());
        postBody.setLongitude(LocationPreference.getCurrentLng());
        postBody.setSearch(binding.searchInput.getText().toString());
        postBody.setStart(searchedOutletsAdapter.getStartIndex());
        postBody.setMaxResult(searchedOutletsAdapter.getMaxResult());

        Observable outletsObservable = WaselDeliveryService.getService().getSearchedOutlets(postBody);
        removeFromSubscription(searchSubscriber);
        searchSubscriber = addObserver(outletsObservable, searchObserver);
    }

    Observer<List<Outlet>> searchObserver = new Observer<List<Outlet>>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            mProgressBarHandler.hide();
            WaselToast.showToast(ParseServerErrorMessage.getServerErrorMessage(e));
        }

        @Override
        public void onNext(final List<Outlet> outlets) {

            if (outlets != null && outlets.size() > 0) {
                binding.emptySearchLyt.getRoot().setVisibility(View.GONE);
                searchedOutletsAdapter.addOutletsToList(outlets);
                hasMoreOutlets = true;
            } else {
                binding.emptySearchLyt.getRoot().setVisibility(View.VISIBLE);
                binding.emptySearchLyt.comingSoonImg.setImageResource(R.drawable.empty_search);
                binding.emptySearchLyt.comingSoonText.setText(resources.getString(R.string.oops));
                binding.emptySearchLyt.comingSoonDesc.setText(String.format(resources.getString(R.string.search_not_found),binding.searchInput.getText().toString()));
                binding.mainView.setVisibility(View.GONE);
                /*Outlet outlet = new Outlet();
                outlet.setName(binding.searchInput.getText().toString());
                outlets.add(outlet);
                searchedOutletsAdapter.addEmptyOutletsList(outlets, binding.searchInput.getText().toString());*/
                hasMoreOutlets = false;
                /*KeyboardHandler.hideKeyboardIfAvailable(fragView);*/
            }

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == LocalPermissionConstants.REQUEST_SEARCH_CLEAR){
                getSearchedList();
            }
        }
    }
    //Clear the location text field
    public View.OnClickListener clearSearchTextListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            clearSearchData();
        }
    };

    private void clearSearchData() {
        binding.searchInput.setText("");
        searchedOutletsAdapter.clearData();
        binding.mainView.setVisibility(View.VISIBLE);
        binding.emptySearchLyt.getRoot().setVisibility(View.GONE);
        binding.restaurantList.setVisibility(View.GONE);
        mProgressBarHandler.hide();
    }

    public void clearAllSearchedHistory(){
        if(recentSearchHistoryAdapter.getItemCount() != 0){
            final WaselDialog dialog =  new WaselDialog(getContext());
            dialog.setTitle(getResources().getString(R.string.app_name));
            dialog.setMessage(getResources().getString(R.string.clear_history_msg));
            dialog.setPositiveButton(getResources().getString(R.string.clear),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            deliveryDB.clearSearchHistory();
                            recentSearchHistoryAdapter.clearSearchedList();
                            updateHistoryView(false);
                            dialog.cancel();
                        }
                    });
            dialog.setNegativeButton(getResources().getString(R.string.cancel),
                    new View.OnClickListener() {
                        public void onClick(View view) {
                            dialog.cancel();
                        }
                    });
            dialog.setPositiveBtnColor(R.color.ok_btn);
            dialog.setNegetiveBtnColor(android.R.color.white);
            dialog.show();
        }
    }

    private void updateHistoryView(Boolean show) {
        binding.recentSearchLyt.setVisibility(show?View.VISIBLE:View.GONE);
        if(recentSearchHistoryAdapter.getItemCount() == 3){
            binding.recentHistoryList.setVisibility(View.VISIBLE);
            binding.moreImg.setVisibility(View.VISIBLE);
            binding.moreImgLine.setVisibility(View.VISIBLE);
        }else{
            binding.recentHistoryList.setVisibility(View.GONE);
            binding.moreImg.setVisibility(View.GONE);
            binding.moreImgLine.setVisibility(View.GONE);
        }
    }

    View.OnClickListener searchedItemClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if(!TextUtils.isEmpty(binding.searchInput.getText().toString())){
                Search search = new Search();
                long id = deliveryDB.checkAndAddSearchedText(binding.searchInput.getText().toString());
                search.setSearchedId(String.valueOf(id));
                search.setSearchedName(binding.searchInput.getText().toString());
                recentSearchHistoryAdapter.updateSearchedList(search);
                updateHistoryView(true);
                binding.searchInput.setText("");
                if (!recentSearchHistoryAdapter.getSearchedList().contains("We couldn't find anything for")) {
                    Outlet outlet = (Outlet) view.getTag();
                    Intent loadFragmentIntent = new Intent(getActivity(), LoadFragmentActivity.class);
//                    OutletDetailsFragment restaurantDetailsFragment = new OutletDetailsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(AMENITY_ID, Integer.parseInt(outlet.getId()));
                    bundle.putSerializable(OutletDetailsFragment.OUTLET_DETAIL, outlet);
                    bundle.putInt(FragmentsIndex.FRAGMENT_TYPE, FragmentsIndex.FRAGMENT_OUTLET_DETAILS);
                    loadFragmentIntent.putExtra(FragmentsIndex.FRAGMENT_DATA, bundle);
                    startActivity(loadFragmentIntent);
//                    ((MainActivity)getActivity()).addFragment(restaurantDetailsFragment);

                    //Clears the search data
                    clearSearchData();

                }

            }
        }
    };

    private void addToHistoryList(Search search) {
        long id = deliveryDB.saveSearchedText(binding.searchInput.getText().toString());
        search.setSearchedId(String.valueOf(id));
        search.setSearchedName(binding.searchInput.getText().toString());
        recentSearchHistoryAdapter.updateSearchedList(search);
        updateHistoryView(true);
        binding.searchInput.setText("");
    }

    View.OnClickListener selectedAmenityClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            /*int pos = Integer.parseInt(view.getTag().toString());
            ((MainActivity)getActivity()).navigateToHomeFragment(pos, (amenitiesListAdapter.amenitiesList.get(pos)));*/
            int pos = Integer.parseInt(view.getTag().toString());
            ((MainActivity)getActivity()).navigateToHomeFragment(amenitiesListAdapter.getItem(pos).getId());
        }
    };

    View.OnClickListener recentHistoryItemClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            Search search = (Search) view.getTag();
            binding.searchInput.setText(search.getSearchedName());
        }
    };

    View.OnClickListener deleteSelectedItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Search search = (Search) v.getTag();
            deliveryDB.deleteSearchedText(search.getSearchedId());
            recentSearchHistoryAdapter.deleteSelectedItem(search.getSearchedId());
            updateHistoryView(recentSearchHistoryAdapter.getItemCount() != 0);
        }
    };

    @Override
    public void onStart() {
        super.onStart();

        if(isFragmentVisible())
        {
            registerSearchTextWatcher();
        }

        if(amenitiesListAdapter==null || amenitiesListAdapter.getItemCount() == 0){
            loadAmenitiesInfo();
        }

    }



    private void registerSearchTextWatcher() {

        Subscriber<CharSequence> editTextSubscription = new Subscriber<CharSequence>() {
            @Override
            public void onCompleted() {
                Loggers.temp("TextViewAfterTextChangeEvent = onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Loggers.temp("TextViewAfterTextChangeEvent = " + e.getMessage());
            }


            public void onNext(CharSequence textViewAfterTextChangeEvent) {
                //Enabling close img
                binding.searchInput.setCompoundDrawablesWithIntrinsicBounds(0,0,
                        textViewAfterTextChangeEvent.length()>0?R.drawable.close:0,0);
                String searchText = textViewAfterTextChangeEvent.toString();

                Loggers.temp("TextViewAfterTextChangeEvent = " + searchText);

                performSearch(searchText);
            }
        };

        Subscription searchSubscribe = RxTextView.textChanges(binding.searchInput)
                .skip(1)
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(editTextSubscription);
        addToSubscription(searchSubscribe);
    }

    private void performSearch(String searchText) {
        searchedOutletsAdapter.clearData();
        if (searchText.length() >= inputThresholdLmt) {
            binding.mainView.setVisibility(View.GONE);
            binding.restaurantList.setVisibility(View.VISIBLE);
            outletsLoading = false;
            loadRestaurantsFromServer();
        } else {
            binding.mainView.setVisibility(View.VISIBLE);
            binding.emptySearchLyt.getRoot().setVisibility(View.GONE);
            binding.restaurantList.setVisibility(View.GONE);

        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        KeyboardHandler.hideKeyboardIfAvailable(fragView);
    }

    /*
   * unsubscribing objects subscribed in onCreateView
   * */
    @Override
    public void onDestroy() {
        super.onDestroy();

    }

}
