package com.almoayyed.waseldelivery.ui.search;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.data_saving.WaselDeliveryDB;
import com.almoayyed.waseldelivery.databinding.ActivitySearchHistoryBinding;
import com.almoayyed.waseldelivery.models.Search;
import com.almoayyed.waseldelivery.views.WaselDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 29 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class SearchHistoryActivity extends AppCompatActivity{

    private List<Search> searchedList = new ArrayList<>();
    private RecentSearchHistoryAdapter recentSearchHistoryAdapter;
    private WaselDeliveryDB deliveryDB;
    boolean isListModified = false;
    private ActivitySearchHistoryBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_search_history);
        binding.setSearchHistoryHandler(this);
        initViews();
    }

    private void initViews() {
        binding.backBtn.getDrawable().setAutoMirrored(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        binding.searchedList.setLayoutManager(linearLayoutManager);

        deliveryDB = new WaselDeliveryDB(WaselApplication.getContext());
        searchedList = deliveryDB.getSearchedText();
        Log.e("TAg","searchedList : "+ searchedList.toString());

        recentSearchHistoryAdapter = new RecentSearchHistoryAdapter(this,searchedList, 0);
        binding.searchedList.setAdapter(recentSearchHistoryAdapter);

        if(recentSearchHistoryAdapter != null){
            recentSearchHistoryAdapter.setDeleteClickListener(deleteSelectedItem);
        }
    }

    View.OnClickListener deleteSelectedItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Search search = (Search) v.getTag();
            deliveryDB.deleteSearchedText(search.getSearchedId());
            recentSearchHistoryAdapter.deleteSelectedItem(search.getSearchedId());
            isListModified = true;
        }
    };

    public View.OnClickListener onBackBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(isListModified){
                setResult(Activity.RESULT_OK);
            }
            finish();
        }
    };

    public View.OnClickListener onClearBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            clearAllSearchedHistory();
        }
    };

    public void clearAllSearchedHistory(){
        if(searchedList.size() != 0){
            final WaselDialog dialog =  new WaselDialog(this);
            dialog.setTitle(getResources().getString(R.string.app_name));
            dialog.setMessage(getResources().getString(R.string.clear_history_msg));
            dialog.setPositiveButton(getResources().getString(R.string.clear),
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            deliveryDB.clearSearchHistory();
                            recentSearchHistoryAdapter.clearSearchedList();
                            setResult(Activity.RESULT_OK);
                            dialog.cancel();
                        }
                    });
            dialog.setNegativeButton(getResources().getString(R.string.cancel),
                    new View.OnClickListener() {
                        public void onClick(View view) {
                            dialog.cancel();
                        }
                    });
            dialog.setPositiveBtnColor(R.color.ok_btn);
            dialog.setNegetiveBtnColor(android.R.color.white);
            dialog.show();
        }
    }
}
