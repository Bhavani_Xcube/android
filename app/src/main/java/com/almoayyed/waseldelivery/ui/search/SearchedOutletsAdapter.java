package com.almoayyed.waseldelivery.ui.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 05 Dec 2016 7:54 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class SearchedOutletsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Outlet> searchedOutletlist;
    private View.OnClickListener clickListener;
    //StartIndex used for pagination of outlet list loaded from server
    private int startIndex = 0;
    //maxResult specifies no.of outlet items load from server per api call
    private final int maxResult = 10;
    private String searchedText = "aa";

    public SearchedOutletsAdapter(Context context, List<Outlet> searchedOutletlist){

        if(searchedOutletlist == null){
            this.searchedOutletlist = new ArrayList<>();
        }else {
            this.searchedOutletlist = searchedOutletlist;

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.searched_outlets, null, false);
        return new SearchOutletsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof SearchOutletsViewHolder){
            SearchOutletsViewHolder viewHolder = (SearchOutletsViewHolder) holder;
          /*  if(!TextUtils.isEmpty(searchedText) &&
                    searchedText.equalsIgnoreCase(searchedOutletlist.get(position).getName())){
                viewHolder.searchedOutletName.setText(((SearchOutletsViewHolder) holder).mView.getContext().getResources().getString(R.string.unable_to_find_outlets)+" \""+searchedOutletlist.get(position).getName()+"\" ");
                viewHolder.searchItemLyt.setClickable(false);
            }else{*/
                viewHolder.searchedOutletName.setText(searchedOutletlist.get(position).getName());
//                viewHolder.searchItemLyt.setClickable(true);
                viewHolder.searchItemLyt.setOnClickListener(clickListener);
                viewHolder.searchItemLyt.setTag(searchedOutletlist.get(position));
//            }
            KeyboardHandler.setKeyboardHandler(viewHolder.itemView);
        }
    }

    @Override
    public int getItemCount() {
        if(searchedOutletlist !=null){
            return searchedOutletlist.size();
        }
        return 0;
    }


    public void setSearchedOutletClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }



    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getMaxResult() {
        return maxResult;
    }

    public void addOutletsToList(List<Outlet> resList) {
        /*int refreshPoint  = getItemCount();
        searchedOutletlist.addAll(resList);
        notifyItemRangeInserted(refreshPoint, searchedOutletlist.size());*/
        searchedOutletlist.clear();
        searchedOutletlist.addAll(resList);
        notifyDataSetChanged();
    }

    public void incStartIndexByValue(int incrementValue) {
        this.startIndex += incrementValue;
    }

    public void clearData() {
        searchedOutletlist.clear();
        notifyDataSetChanged();
    }

    /*public void addEmptyOutletsList(List<Outlet> searchedText, String searchText) {
        searchedOutletlist.clear();
        searchedOutletlist.addAll(searchedText);
        this.searchedText = searchText;
        notifyDataSetChanged();
    }*/

    public Outlet getOutletAtPos(int position){
        if(position<0 || position > searchedOutletlist.size()-1){
            return null;
        }
        return searchedOutletlist.get(position);
    }

    public class SearchOutletsViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        private TextView searchedOutletName;
        private LinearLayout searchItemLyt;

        public SearchOutletsViewHolder(View view) {
            super(view);
            mView = view;
            searchedOutletName = (TextView) mView.findViewById(R.id.searched_outlet_name);
            searchItemLyt = (LinearLayout) mView.findViewById(R.id.search_outlets_lyt);
        }
    }
}
