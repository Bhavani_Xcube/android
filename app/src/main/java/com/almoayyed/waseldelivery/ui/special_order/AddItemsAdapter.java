package com.almoayyed.waseldelivery.ui.special_order;

import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.models.AddItem;
import com.almoayyed.waseldelivery.models.Outlet;
import com.almoayyed.waseldelivery.ui.home.Outlets.ViewHolders.OutletViewHolder;
import com.almoayyed.waseldelivery.ui.special_order.ViewHolders.AddItemsViewHolder;
import com.almoayyed.waseldelivery.utils.ScreenUtils;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created on 11 Nov 2016 11:57 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class AddItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Fragment context;
    private View.OnClickListener clickListener;
    View view;
    //holds the items list data
    private final ArrayList<AddItem> itemList;
    private TextWatcher watcher;
    private boolean enableCloseIcon = false;
    private int maxCount = 50;

    public AddItemsAdapter(Fragment context, ArrayList<AddItem> itemList, TextWatcher watcher){
        if(itemList == null){
            this.itemList = new ArrayList<>();
        }else{
            this.itemList = itemList;
        }
        this.context = context;
        this.watcher = watcher;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.editview_list, parent, false);
        return new AddItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if(holder instanceof AddItemsViewHolder){
            AddItemsViewHolder addItemsViewHolder = (AddItemsViewHolder) holder;
            AddItem tmpItem = itemList.get(position);

            //Setting binding variables
            addItemsViewHolder.getBinding().setVariable(BR.addItem, tmpItem);
            addItemsViewHolder.mItemsList.setTag(position);
            addItemsViewHolder.mItemsList.addTextChangedListener(watcher);
            addItemsViewHolder.mItemsList.setSelection(addItemsViewHolder.mItemsList.getText().length());
            if(itemList.size()>1){
                addItemsViewHolder.closeIcon.setVisibility(View.VISIBLE);
                addItemsViewHolder.closeIcon.setTag(position);
            /*addItemsViewHolder.mItemsList.setOnClickListener(clickListener);*/
                addItemsViewHolder.closeIcon.setOnClickListener(clickListener);
            }else {
                addItemsViewHolder.closeIcon.setVisibility(View.GONE);
            }

            addItemsViewHolder.mItemsList.getText().toString().trim();

            checkForRTL(addItemsViewHolder.mItemsList);
        }
    }

    private void checkForRTL(EditText listItem) {
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            boolean isRightToLeft = context.getResources().getBoolean(R.bool.is_right_to_left);
            if(isRightToLeft){
                listItem.setGravity(Gravity.END);
            }else{
                listItem.setGravity(Gravity.START);
            }
        }
    }

    public void setItemClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public ArrayList<AddItem> getItemsData(){
        ArrayList<AddItem> filterItems = new ArrayList<>();
        for(int i=0;i<itemList.size();i++) {
            if (!TextUtils.isEmpty(itemList.get(i).getItemName())
                    && (!TextUtils.isEmpty(itemList.get(i).getItemName().trim()))) {
                filterItems.add(itemList.get(i));
            }
        }

        return filterItems;
    }

    public ArrayList<AddItem> getAbsoluteItemsData(){
        return itemList;
    }


    public int incCount(){
        AddItem addItem = new AddItem();
        itemList.add(addItem);
        ((SpecialOrderClearEnableInteface) context).enableClearText();
        return itemList.size();
    }

    public boolean removeItemAtPos(int pos){

        if(itemList.size() == 1){
            itemList.get(pos).setItemName("");
        }else{
            itemList.remove(pos);
        }
        ((SpecialOrderClearEnableInteface) context).enableClearText();
        notifyDataSetChanged();


        return true;
    }
    @Override
    public int getItemCount() {
        if(itemList.size() <= maxCount) {
            return itemList.size();
        }else{
            return Math.min(maxCount,itemList.size());
        }
    }

    public void clearAllItems() {
        itemList.clear();
        incCount();
        notifyDataSetChanged();
    }

    public boolean isListEmpty(){
        ArrayList<String> list = new ArrayList<>();
        for(int i=0; i<itemList.size();i++){
            if(!TextUtils.isEmpty(itemList.get(i).getItemName())) {
                list.add(itemList.get(i).getItemName().trim());
            }
        }
        Log.e("TAG","list: "+list);
        if(list.size()>0){
            return false;
        }else{
            return true;
        }
    }
    /*Checking the availability of atleast one item in the list*/
    public boolean isCleared() {
        if (itemList.size()==1 && TextUtils.isEmpty(itemList.get(0).getItemName()) && isListEmpty()) {
            return true;
        } else{
            return false;
        }
    }

    public void setItemList(ArrayList<AddItem> items){
        itemList.clear();
        itemList.addAll(items);
    }
}
