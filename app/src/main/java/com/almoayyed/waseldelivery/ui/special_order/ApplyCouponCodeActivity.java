package com.almoayyed.waseldelivery.ui.special_order;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.ActivityApplyCouponCodeBinding;
import com.almoayyed.waseldelivery.models.Coupons;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.ui.BaseActivity;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.ParseServerErrorMessage;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 25 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class ApplyCouponCodeActivity extends BaseActivity {

    private CouponsListAdapter adapter;
    private ActivityApplyCouponCodeBinding binding;
    //startStopCompositeSubscription:
    private Observable<List<Coupons>> couponsObserable;
    private Subscription subscribe;
    CompositeSubscription startStopCompositeSubscription = new CompositeSubscription();
    private ProgressBarHandler mProgressBarHandler;
    private Coupons prevSelCoupon;
    private Coupons selectedCoupon;
    /*private Coupons prevSelCoupon;*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_apply_coupon_code);
        binding.setVariable(BR.couponHandler,this);
        binding.backBtn.getDrawable().setAutoMirrored(true);

        if(getIntent().hasExtra(CheckoutConstants.KEY_COUPON_DATA)){
            prevSelCoupon =(Coupons) getIntent().getSerializableExtra(CheckoutConstants.KEY_COUPON_DATA);
            if(prevSelCoupon!=null){
                binding.couponCodeEt.setText(prevSelCoupon.getCode());
                binding.couponCodeEt.setSelection(binding.couponCodeEt.getText().length());
            }
        }
        initViews();
    }

    private void initViews() {
        if(adapter==null){
            adapter =
                    new CouponsListAdapter(ApplyCouponCodeActivity.this, ProfileConstants.FROM_COUPON,null);
        }

        //placing order
        if(mProgressBarHandler == null){
            mProgressBarHandler = new ProgressBarHandler(ApplyCouponCodeActivity.this,binding.couponsLyt);
        }
        mProgressBarHandler.show();
        couponsObserable = WaselDeliveryService.getService().getCoupons();
        addObserver(couponsObserable, couponsObserver);

        KeyboardHandler.setKeyboardHandler(binding.getRoot());


        searchForInputCoupon();
    }

    private void updateCouponsOnUI() {
        adapter.setCouponSelectClickListener(couponsListClickListener);
        binding.couponsList.setAdapter(adapter);
    }

    //Search the input text in coupons list & select it
    private void searchForInputCoupon() {
        Subscriber<CharSequence> editTextSubscription = new Subscriber<CharSequence>() {
            @Override
            public void onCompleted() {
                Loggers.temp("TextViewAfterTextChangeEvent = onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Loggers.temp("TextViewAfterTextChangeEvent = " + e.getMessage());
            }


            public void onNext(CharSequence inputText) {
                if(TextUtils.isEmpty(inputText)){
                    //Clear previously selected coupon
                    adapter.clearSelectedCoupon();
                    return;
                }

                //If coupon is selected & same text is placed
                if(adapter.getSelectedCoupon()!=null && adapter.getSelectedCoupon().getCode().equalsIgnoreCase(inputText.toString())){
                    return;
                }

                //Clear previously selected coupon
                adapter.clearSelectedCoupon();

                //Check for coupon & highlight it
                adapter.getSelectedCouponByCode(inputText.toString());
            }
        };
        Subscription searchSubscribe = RxTextView.textChanges(binding.couponCodeEt)
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(editTextSubscription);
        addToSubscription(searchSubscribe);
    }

    View.OnClickListener couponsListClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int pos = Integer.parseInt(view.getTag().toString());
            binding.couponCodeEt.setText(adapter.setSelectedCoupon(pos));
            binding.couponCodeEt.setSelection(binding.couponCodeEt.getText().length());
        }
    };

    public View.OnClickListener onApplyClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(TextUtils.isEmpty(binding.couponCodeEt.getText().toString())){
                String msg = getString(R.string.empty_coupon);
                if(TextUtils.isEmpty(msg)){
                    return;
                }
                showDialog(msg, false);
                return;
            }

            selectedCoupon = adapter.getSelectedCouponByCode(binding.couponCodeEt.getText().toString());
            if(selectedCoupon !=null){
                mProgressBarHandler.show();
                Observable<Response> applyCouponObservable = WaselDeliveryService.getService()
                        .verifyCoupon(LoginCredentialsPreference.getLoginPreference().getId(),selectedCoupon.getId());
                subscribe = applyCouponObservable.subscribe(applyCouponObserver);
                startStopCompositeSubscription.add(subscribe);
            }else {
                String msg = getString(R.string.select_coupon);
                if(TextUtils.isEmpty(msg)){
                    return;
                }
                showDialog(msg, false);
            }
        }
    };

    /*
  * Observer for verfiying the selected Coupon.
  * */
    Observer<Response> applyCouponObserver = new Observer<Response>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
            clearSubscriptions();
        }

        @Override
        public void onError(Throwable e) {
            /*WaselToast.showToast(getServerErrorMessage(e));*/
            mProgressBarHandler.hide();
            clearSubscriptions();
            String msg = getServerErrorMessage(e);
            if(TextUtils.isEmpty(msg)){
                return;
            }
            showDialog(msg, false);
        }

        @Override
        public void onNext(Response response) {
            Intent data = new Intent();
            data.putExtra(CheckoutConstants.KEY_COUPON_DATA,selectedCoupon);
            setResult(Activity.RESULT_OK, data);
            clearSubscriptions();
            finish();
        }
    };

    public View.OnClickListener onBackBtnClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    @Override
    public void onStart() {
        super.onStart();

        if(subscribe != null) {
            startStopCompositeSubscription.add(subscribe);
        }

    }

    @Override
    public void onStop() {
        startStopCompositeSubscription.clear();

        super.onStop();
    }

    /*
   * Observer for fetching coupons List
   * */
    Observer<List<Coupons>> couponsObserver = new Observer<List<Coupons>>() {

        @Override
        public void onCompleted() {
            mProgressBarHandler.hide();
        }

        @Override
        public void onError(Throwable e) {
            mProgressBarHandler.hide();
//            WaselToast.showToast(getServerErrorMessage(e));
            String msg = getServerErrorMessage(e);
            clearSubscriptions();
            if(TextUtils.isEmpty(msg)){
                return;
            }
            binding.couponsList.setVisibility(View.GONE);
            /*showDialog(msg,true);*/
        }

        @Override
        public void onNext(List<Coupons> coupons) {
            Log.e("TAG","Coupons list : "+ coupons);
            if(coupons==null || coupons.size()==0){
                showDialog(getResources().getString(R.string.coupons_not_available),true);
            }
            updateCouponsOnUI();
            adapter.addCouponsList(coupons);

            /*//Set previously selected coupon
            if(adapter.getSelectedCoupon()==null && prevSelCoupon !=null){
                adapter.setSelectedCouponById(prevSelCoupon.getId());
            }*/
        }
    };

    private void showDialog(String msg, final boolean shouldCloseScreen) {
        final WaselDialog dialog =  new WaselDialog(ApplyCouponCodeActivity.this);
        dialog.setTitle(getResources().getString(R.string.app_name));
        dialog.setMessage(msg);
        dialog.setNeutralButton(getResources().getString(R.string.ok),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        dialog.cancel();
                        if(shouldCloseScreen){
                            finish();
                        }
                    }
                });
        dialog.setNeutralBtnColor(android.R.color.white);
        dialog.show();
    }
}
