package com.almoayyed.waseldelivery.ui.special_order;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.databinding.CouponCodesListBinding;
import com.almoayyed.waseldelivery.models.Coupons;
import com.android.databinding.library.baseAdapters.BR;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 25 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CouponsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int RUNNING_OFFERS = 1;
    private static final int COUPONS_LIST = 2;
    private Context context;
    private List<Coupons> list;
    private View view;
    private int selectedCouponPos = -1;
    private View.OnClickListener clickListener;
    private String from;

    public CouponsListAdapter(Context context, String from, List<Coupons> list) {
        if(list == null){
            this.list = new ArrayList<>();
        }else{
            this.list = list;
        }
        this.context = context;
        this.from = from;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == RUNNING_OFFERS){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.running_offer_lyt, parent, false);
            return new RunningOffersViewHolder(view);
        }else if(viewType == COUPONS_LIST){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.coupon_codes_list, parent, false);
            return new CouponsListViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getItemViewType() == RUNNING_OFFERS){
            RunningOffersViewHolder runningOffersViewHolder = (RunningOffersViewHolder) holder;
            if(ProfileConstants.FROM_COUPON.equalsIgnoreCase(from)){
                runningOffersViewHolder.couponsAdapterLyt.setVisibility(View.VISIBLE);
                runningOffersViewHolder.runningOffersText.setTag(position);
            }else{
                runningOffersViewHolder.couponsAdapterLyt.setVisibility(View.GONE);
            }


        }else if(holder.getItemViewType() == COUPONS_LIST){
            CouponsListViewHolder couponsListViewHolder = (CouponsListViewHolder) holder;
            if(ProfileConstants.FROM_COUPON.equalsIgnoreCase(from)){
                position = position-1;
            }
            couponsListViewHolder.binding.couponCodeDesc.setTag(position);
            couponsListViewHolder.binding.couponCodeDesc.setText(list.get(position).getName());
            couponsListViewHolder.binding.couponCodeTxt.setTag(position);
            couponsListViewHolder.binding.couponCodeTxt.setText(list.get(position).getCode());
            couponsListViewHolder.binding.useCode.setTag(position);
            couponsListViewHolder.binding.useCode.setOnClickListener(clickListener);
            couponsListViewHolder.binding.couponSelectBg.setTag(position);
            couponsListViewHolder.binding.couponSelectBg.setOnClickListener(clickListener);

            couponsListViewHolder.getBinding().setVariable(BR.couponsList,list.get(position));
            couponsListViewHolder.getBinding().setVariable(BR.couponsAdapter,this);

            //Update the selected coupon background
            if(ProfileConstants.FROM_COUPON.equalsIgnoreCase(from)){
                if (selectedCouponPos == position){
                    couponsListViewHolder.binding.couponSelectBg.setChecked(true);
//                    couponsListViewHolder.binding.useCode.setText(R.string.code_applied);
                }else {
                    couponsListViewHolder.binding.couponSelectBg.setChecked(false);
//                    couponsListViewHolder.binding.useCode.setText(R.string.use_code);
                }
            }else {
                couponsListViewHolder.binding.couponSelectBg.setChecked(false);
                if (selectedCouponPos == position){
                    couponsListViewHolder.binding.useCode.setText(R.string.code_copied);
                }else {
                    couponsListViewHolder.binding.useCode.setText(R.string.use_code);
                }
            }
        }
    }

    public void setCouponSelectClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }
    public void setUseCodeClickListener(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public List<Coupons> getCouponsData(){
        return list;
    }

    @Override
    public int getItemCount() {
        if(ProfileConstants.FROM_COUPON.equalsIgnoreCase(from)) {
            return list.size() + 1;
        }else{
            return list.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(ProfileConstants.FROM_COUPON.equalsIgnoreCase(from)) {
            if (position == 0) {
                return RUNNING_OFFERS;
            } else {
                return COUPONS_LIST;
            }
        }else{
            return COUPONS_LIST;
        }
    }

    public void addCouponsList(List<Coupons> coupons) {
        list.addAll(coupons);
        notifyDataSetChanged();
    }

    private class RunningOffersViewHolder extends RecyclerView.ViewHolder {
        private TextView runningOffersText;
        private RelativeLayout couponsAdapterLyt;

        public RunningOffersViewHolder(View view) {
            super(view);
            runningOffersText = (TextView) view.findViewById(R.id.running_offers);
            couponsAdapterLyt = (RelativeLayout) view.findViewById(R.id.coupons_adapter_lyt);
        }
    }

    private class CouponsListViewHolder extends RecyclerView.ViewHolder {
        private RadioButton couponSelectBg;
        private TextView couponCodeDesc;
        private TextView useCodeTxt;
        private TextView couponCodeTxt;

        private final CouponCodesListBinding binding;

        public CouponsListViewHolder(View view) {
            super(view);
            couponSelectBg = (RadioButton) view.findViewById(R.id.coupon_select_bg);
            couponCodeDesc = (TextView) view.findViewById(R.id.coupon_code_desc);
            useCodeTxt = (TextView) view.findViewById(R.id.coupon_code_desc);
            couponCodeTxt = (TextView) view.findViewById(R.id.coupon_code_txt);

            binding = DataBindingUtil.bind(view);
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    public Coupons getSelectedCoupon() {
        if(selectedCouponPos == -1){
            return null;
        }
        return list.get(selectedCouponPos);
    }

    public String setSelectedCoupon(int selectedCouponPos) {
        this.selectedCouponPos = selectedCouponPos;
        notifyDataSetChanged();
        return list.get(selectedCouponPos).getCode();
    }

    public String setSelectedCouponById(long couponId) {
        for(int index=0; index<list.size(); index ++ ){
            if(list.get(index).getId() == couponId){
                this.selectedCouponPos = index;
                return list.get(selectedCouponPos).getCode();
            }
        }
        return "";
    }

    public void clearSelectedCoupon(){
        selectedCouponPos = -1;
        notifyDataSetChanged();
    }

    public  Coupons getSelectedCouponByCode(String couponCode) {

        for(int index=0; index<list.size(); index ++ ){
            if(list.get(index).getCode().equalsIgnoreCase(couponCode)){
                this.selectedCouponPos = index;
                notifyDataSetChanged();
                return list.get(selectedCouponPos);
            }
        }
        return null;
    }
}
