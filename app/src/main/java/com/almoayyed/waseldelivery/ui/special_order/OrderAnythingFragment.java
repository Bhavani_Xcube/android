package com.almoayyed.waseldelivery.ui.special_order;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.almoayyed.waseldelivery.BR;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.AppLimitations;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.LocalPermissionConstants;
import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.constants.RegistrationBundleKeyConstants;
import com.almoayyed.waseldelivery.data_saving.LocationPreference;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.databinding.OrderAnythingBinding;
import com.almoayyed.waseldelivery.models.AddItem;
import com.almoayyed.waseldelivery.models.Cart;
import com.almoayyed.waseldelivery.models.OutletItem;
import com.almoayyed.waseldelivery.models.UserLocation;
import com.almoayyed.waseldelivery.ui.BaseFragment;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.address.AddAddressActivity;
import com.almoayyed.waseldelivery.ui.checkout.ConfirmOrderActivity;
import com.almoayyed.waseldelivery.ui.login.LoginActivity;
import com.almoayyed.waseldelivery.utils.KeyboardHandler;
import com.almoayyed.waseldelivery.utils.NetworkConnection;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.views.WaselDialog;
import com.almoayyed.waseldelivery.views.WaselToast;

import java.util.ArrayList;


/**
 * Created on 2 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class OrderAnythingFragment extends BaseFragment implements SpecialOrderClearEnableInteface{
    private OrderAnythingBinding binding;
    private LinearLayoutManager linearLayoutManager;
    private AddItemsAdapter addItemsRecyclerViewAdapter;
    private int maxCount = 50;
    private Resources resources;
    LoginCredentialsPreference loginPreference;
    UserLocation userLocation = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(fragView==null) {
            // Inflate the layout for this fragment
            fragView = inflater.inflate(R.layout.order_anything, container, false);
            binding = DataBindingUtil.bind(fragView);
            binding.setVariable(BR.orderAnythingHandler,this);
            binding.setClickListener(onClickListener);
            initViews();

        }
        return fragView;
    }

    private void initViews() {

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        enablingClearText(false);
        /*binding.pickupLocEt.addTextChangedListener(pickupLocwatcher);*/
        linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.itemsList.setLayoutManager(linearLayoutManager);
        binding.itemsList.setNestedScrollingEnabled(false);

        resources = this.getResources();

        addItemsRecyclerViewAdapter =
                new AddItemsAdapter(this, null, watcher);
        addItemsRecyclerViewAdapter.setItemClickListener(closeIconClickListener);
        binding.itemsList.setAdapter(addItemsRecyclerViewAdapter);
        addItemsRecyclerViewAdapter.incCount();
        loginPreference = LoginCredentialsPreference.getLoginPreference();

       /* pickUpLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setEnabled(true);
                view.requestFocus();
            }
        });

        pickUpLoc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(!b)
                {
                    view.setEnabled(false);
                }
            }
        });*/
        KeyboardHandler.setKeyboardHandler(fragView);
    }

    private void enablingClearText(boolean b) {
        binding.clearText.setEnabled(b);
        binding.clearText.setTextColor(getResources().getColor(b?R.color.textColor13:R.color.textColor7));
    }

    public View.OnClickListener addItemsClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            addItemsRecyclerViewAdapter.incCount();
            addItemsRecyclerViewAdapter.notifyDataSetChanged();
            if(addItemsRecyclerViewAdapter.getItemCount() >= maxCount){
                WaselToast.showToast(getResources().getString(R.string.max_limit_reached));
            }
        }
    };

    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getId() == binding.pickupLocEt.getId()){
                //TODO: Update address
                updatePickupAddress();
            }else if(view.getId() == binding.checkoutBtn.getId()){
                checkoutOrder();
            }else if(view.getId() == binding.clearText.getId()){
                clearDetails();
            }
        }
    };

    private void updatePickupAddress() {
        Intent i = new Intent(getActivity(), AddAddressActivity.class);
        UserLocation mLocation = new UserLocation();
        if(userLocation == null){
            mLocation.setLatitude(LocationPreference.getCurrentLat());
            mLocation.setLongitude(LocationPreference.getCurrentLng());
            StringBuffer currentLocName = new StringBuffer(LocationPreference.getCurrentLocName());
            currentLocName.replace(currentLocName.length()-2,currentLocName.length(),"");
            mLocation.setSubLocality3(currentLocName.toString());
        }else {
            mLocation = userLocation;
        }
        i.putExtra(CheckoutConstants.KEY_ADDRESS,mLocation);
        i.putExtra(ProfileConstants.FROM,ProfileConstants.FROM_SP_ORDER);
        getActivity().startActivityForResult(i, LocalPermissionConstants.REQUEST_SPECIAL_ORDER_ADDRESS);
    }

    private void checkoutOrder() {
        /*String result = binding.pickupLocEt.getText().toString().replaceAll(" ", "");
        if (!binding.pickupLocEt.getText().toString().equals(result)) {
            binding.pickupLocEt.setText(result);
            *//*binding.pickupLocEt.setSelection(result.length());*//*
            // alert the user
        }*/
        if (binding.pickupLocEt.getText().length() == 0 ||
                userLocation==null) {
            //show prompt to enter location
            WaselToast.showToast(resources.getString(R.string.enter_loc));
        } else if (binding.pickupLocEt.getText().length() < 3) {
            WaselToast.showToast(resources.getString(R.string.enter_loc_min_length));
        } else if (addItemsRecyclerViewAdapter.getItemsData().size() == 0) {
            //show prompt to enter items
            WaselToast.showToast(resources.getString(R.string.enter_items));
        } else {
            //Check internet before proceeding to checkout an order
            if(!NetworkConnection.isNetworkAvailable()){
                WaselToast.showToast(getString(R.string.check_internet));
                return;
            }
            if (loginPreference.isUserLoggedIn()) {
                navigateToConfirmOrderScreen();
            } else {
                //go to login in flow
                Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
                loginIntent.putExtra(RegistrationBundleKeyConstants.KEY_LOGIN_FROM, RegistrationBundleKeyConstants.LOGIN_FROM_CHECKOUT_SPECIAL);
                startActivity(loginIntent);
            }
        }
    }


    /*public View.OnClickListener clearTextClickHandler = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            clearDetails();
        }
    };
*/

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState!=null){
            addItemsRecyclerViewAdapter.setItemList((ArrayList<AddItem>) savedInstanceState.getSerializable("items"));
            if(userLocation == null){
                userLocation = (UserLocation) savedInstanceState.getSerializable("pickUpLocation");
                binding.pickupLocEt.setText(userLocation==null? "" : userLocation.getLocDescription());
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("items",addItemsRecyclerViewAdapter.getAbsoluteItemsData());
        outState.putSerializable("pickUpLocation",userLocation);
    }

    private void clearDetails() {
        final WaselDialog dialog = new WaselDialog(getContext());
        dialog.setTitle(getContext().getResources().getString(R.string.app_name));
        dialog.setMessage(getResources().getString(R.string.clear_items_msg));
        dialog.setPositiveButton(getResources().getString(R.string.clear),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        clearSpecialOrderCartItems();
                        dialog.cancel();
                    }
                });
        dialog.setNegativeButton(getResources().getString(R.string.cancel),
                new View.OnClickListener() {
                    public void onClick(View view) {
                        dialog.cancel();
                    }
                });
        dialog.setPositiveBtnColor(R.color.ok_btn);
        dialog.setNegetiveBtnColor(android.R.color.white);
        dialog.show();
    }

    private void navigateToConfirmOrderScreen() {

        Intent i = new Intent(getActivity(), ConfirmOrderActivity.class);
        Cart cart = updateSpecialOrderDetailsToCart();
        i.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW, CheckoutConstants.KEY_CHECKOUT_ORDER_ANY_THING);
        i.putExtra(CheckoutConstants.KEY_DATA, cart);
        startActivity(i);
    }

    @NonNull
    public Cart updateSpecialOrderDetailsToCart() {
        Cart cart = WaselApplication.getCart();
        cart.clear();
        cart.setOrderType(CheckoutConstants.CHECKOUT_SPECIAL_ORDER);
        cart.setDeliveryCharge(AppLimitations.SPECIAL_ORDER_DELIVERY_CHARGE); //TODO: Temporarily fixed by including static delivery charge value as 2
        cart.setPickUpLocation(binding.pickupLocEt.getText().toString().trim());
        cart.setLatitude(userLocation==null? 0: userLocation.getLatitude());
        cart.setLongitude(userLocation==null? 0: userLocation.getLongitude());

        ArrayList<OutletItem> outletItems = new ArrayList<>();
        ArrayList<AddItem> itemsData = addItemsRecyclerViewAdapter.getItemsData();
        for (int index = 0; index < itemsData.size(); index++) {
            OutletItem outletItem = new OutletItem();
            outletItem.setName(itemsData.get(index).getItemName().trim());
            outletItem.setCartQuantity(1);
            outletItems.add(outletItem);
        }
        cart.addItem(outletItems);
        return cart;
    }

    public void clearSpecialOrderCartItems() {
        if (addItemsRecyclerViewAdapter != null) {
            addItemsRecyclerViewAdapter.clearAllItems();
        }
        binding.pickupLocEt.setText("");
        userLocation = null;
        enablingClearText(false);
    }

    /*Testing purpose*/
    View.OnClickListener closeIconClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            int pos = Integer.parseInt(view.getTag().toString());
            addItemsRecyclerViewAdapter.removeItemAtPos(pos);
            //mRecyclerView.getLayoutManager().findViewByPosition(pos - 1).findViewById(R.id.items_et).requestFocus();
//            List<AddItem> str = addItemsRecyclerViewAdapter.getItemsData();

            /*for(int i =0 ; i< str.size(); i++){
                Log.e(" closeIconClickListener","view + position : "+ view +" : "+pos + " : "+str.get(i).getItemName());
            }*/

        }
    };

    @Override
    public void onStart() {
        super.onStart();

        //User successfully login while pressing checkout button
        Bundle data = getArguments();
        if (data != null && data.containsKey(RegistrationBundleKeyConstants.KEY_LOGIN_FROM)) {
            if (loginPreference.isUserLoggedIn()) {
                navigateToConfirmOrderScreen();
            }
            data.remove(RegistrationBundleKeyConstants.KEY_LOGIN_FROM);
        }

    }

   /* public void registerPickUpLocationWatcher() {

       Subscriber<CharSequence> editTextSubscription = new Subscriber<CharSequence>() {
            @Override
            public void onCompleted() {
                Loggers.temp("TextViewAfterTextChangeEvent = onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Loggers.temp("TextViewAfterTextChangeEvent = " + e.getVersion());
            }


            public void onNext(CharSequence textViewAfterTextChangeEvent) {

             if(binding.pickupLocEt.getText().length() >0 || !addItemsRecyclerViewAdapter.isListEmpty()){
                 enablingClearText(true);
             }else{
                 enablingClearText(false);
             }
            }
        };

        Subscription locSubscribe = RxTextView.textChanges(binding.pickupLocEt)
                .skip(1)
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(editTextSubscription);
        addToSubscription(locSubscribe);
    }
*/
    @Override
    public void onStop() {
        super.onStop();
        KeyboardHandler.hideKeyboardIfAvailable(fragView);
    }

    TextWatcher pickupLocwatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            if(binding.pickupLocEt.getText().length() >0 || !addItemsRecyclerViewAdapter.isListEmpty()){
                enablingClearText(true);
            }else{
                enablingClearText(false);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            if(charSequence.length() > 0 || binding.pickupLocEt.getText().length() >0 || !addItemsRecyclerViewAdapter.isListEmpty()){
                enablingClearText(true);
            }else{
                enablingClearText(false);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

    @Override
    public void enableClearText() {
        if(binding.pickupLocEt.getText().length() >0 || !addItemsRecyclerViewAdapter.isListEmpty()){
            enablingClearText(true);
        }else{
            enablingClearText(false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == LocalPermissionConstants.REQUEST_SPECIAL_ORDER_ADDRESS){
                //Update pickup location on UI
                userLocation = data.hasExtra(CheckoutConstants.KEY_ADDRESS)?
                        (UserLocation) data.getSerializableExtra(CheckoutConstants.KEY_ADDRESS) : new UserLocation();
                binding.pickupLocEt.setText(userLocation.getLocDescription());
                enableClearText();
            }
        }
    }
}
