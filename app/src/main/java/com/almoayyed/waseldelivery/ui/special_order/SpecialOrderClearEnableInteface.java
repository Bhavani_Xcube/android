package com.almoayyed.waseldelivery.ui.special_order;

/**
 * Created on 22 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public interface SpecialOrderClearEnableInteface {

    public void enableClearText();
}
