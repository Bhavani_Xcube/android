package com.almoayyed.waseldelivery.ui.special_order.ViewHolders;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.ui.special_order.OrderAnythingFragment;

/**
 * Created on 11 Nov 2016 1:03 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class AddItemsViewHolder extends RecyclerView.ViewHolder{
    public final EditText mItemsList;
    public final ImageView closeIcon;
    private ViewDataBinding binding;

    public AddItemsViewHolder(View view) {
        super(view);
        mItemsList = (EditText) view.findViewById(R.id.items_et);
        closeIcon = (ImageView) view.findViewById(R.id.close_img);
        binding = DataBindingUtil.bind(view);
    }

    public ViewDataBinding getBinding(){
        return binding;
    }

}