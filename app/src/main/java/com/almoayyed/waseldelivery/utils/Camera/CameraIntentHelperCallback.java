package com.almoayyed.waseldelivery.utils.Camera;

import android.net.Uri;

import java.util.Date;

/**
 * Created on 21 Dec 2016 03:15 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public interface CameraIntentHelperCallback {
    void onPhotoUriFound(Date dateCameraIntentStarted, Uri photoUri, int rotateXDegrees);
    void deletePhotoWithUri(Uri photoUri);
    void onSdCardNotMounted();
    void onCanceled();
    void onCouldNotTakePhoto();
    void onPhotoUriNotFound();
    void logException(Exception e);
}
