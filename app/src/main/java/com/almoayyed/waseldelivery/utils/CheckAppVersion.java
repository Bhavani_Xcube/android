package com.almoayyed.waseldelivery.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created on 02 Jan 2017 6:16 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CheckAppVersion {

    public static String getAppVersion(Context context) {

        String versionName = "v2.1";
        final PackageManager packageManager = context.getPackageManager();
        if (packageManager != null) {
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
                versionName = packageInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                versionName = "v2.1";
            }
        }
        return versionName;
    }
}
