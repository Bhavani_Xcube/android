package com.almoayyed.waseldelivery.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;

import com.almoayyed.waseldelivery.WaselApplication;

/**
 * Created on 15 Nov 2016 11:06 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class Converters {
    public static DisplayMetrics getDisplayMetrics(Context context) {
        return context.getResources().getDisplayMetrics();
    }
    public static float dpToPx(Context context, float dp) {
        return Math.round(dp / getDisplayMetrics(context).density);
    }

    /*
   * Converts dp value to sp
   * */
    public static float pixelsToSp( float px) {
        float scaledDensity = WaselApplication.getContext().getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }

    /*
 * Converts px value to dp
 * */
    public static int pxToDp(Context context, float px) {
        return Math.round(px * getDisplayMetrics(context).density);
    }

    public static int pxToDp(float px) {
        return pxToDp(WaselApplication.getContext(), px);
    }

}
