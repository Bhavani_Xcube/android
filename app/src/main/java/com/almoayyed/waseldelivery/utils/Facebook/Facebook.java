package com.almoayyed.waseldelivery.utils.Facebook;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.List;
import android.service.textservice.SpellCheckerService.Session;

/**
 * Created on 17 Oct 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/*
* Implements Facebook signup and signout functionality*/

public class Facebook {

    private static final List<String> READ_PERMISSIONS = Arrays.asList("public_profile", "email");
    private static AccessToken accessToken;
    private static FacebookLoginCallback facebookCallback;
    protected static CallbackManager callbackManager;
    private ShareDialog shareDialog;
    public static final int RC_FACEBOOK = 64206;
    static final int RC_FACEBOOK_ERROR = 64207;

    public static void signUp(Activity context, FacebookLoginCallback fbCallback){
        //if (!FacebookSdk.isInitialized()) {
        FacebookSdk.sdkInitialize(context);
        //}
        if (getAccessToken() != null && Profile.getCurrentProfile() != null) {
            try{
                JSONObject jObj = new JSONObject();
                jObj.put("id",Profile.getCurrentProfile().getId());
                jObj.put("name",Profile.getCurrentProfile().getName());
                jObj.put("first_name",Profile.getCurrentProfile().getFirstName());
                fbCallback.onSuccess(jObj);
            }catch (Exception e){
                fbCallback.onFailed("Unable to load data");
            }
        }else {
            loginCheck(fbCallback, context);
        }
    }

    /*public  static  void signOut(Activity activity,final LogoutFromFacebookListener listener){
        LoginManager.getInstance().logOut();
        listener.onLoggedOutFromFacebook();
    }*/

    public static void signOut(Activity context,final LogoutFromFacebookListener listener) {

        if (!FacebookSdk.isInitialized()) {
            return;
        }
        if (AccessToken.getCurrentAccessToken() == null) {
            // already logged out
            listener.onLoggedOutFromFacebook();
            return;
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {

            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();
                listener.onLoggedOutFromFacebook();
            }
        }).executeAsync();
    }


    public static void loginCheck(final FacebookLoginCallback mFacebookLoginCallback, Activity context) {

        facebookCallback = mFacebookLoginCallback;

        //createHashKey(activity);
        //  if (!FacebookSdk.isInitialized()) {
        // FacebookSdk.sdkInitialize(context);
        //}
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(context, READ_PERMISSIONS);
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken();
                getUserDetails();
            }

            @Override
            public void onCancel() {
                facebookCallback.onFailed("Clicked on Cancel button");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("Facebook Error", "facebook error : " + error.toString());
            }
        });

    }

    public static void onFacebookActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);

            if (requestCode == RC_FACEBOOK_ERROR) {
                if (facebookCallback != null) {
                    facebookCallback.onFailed("You have cancelled login for Facebook.");
                }
            }
        }
    }

    private static void getUserDetails() {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                Log.i("Facebook", "response = " + response.toString());

                String name = object.optString("name");
                String id = object.optString("id");
                String imagePath = "https://graph.facebook.com/" + id + "/picture?type=large&width=100&height=100";
                Log.i("Facebook", "name = " + name + " id = " + id + " url =" + imagePath);

                if (facebookCallback != null) {
                    facebookCallback.onSuccess(object);
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields","id,first_name,last_name,name,email,gender,birthday,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /*public void shareData(final String title,final String description,final String videoUrl){
        shareDialog = new ShareDialog(context);

        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.i("test", "outside result.getPostId() ::::::::::: " + result);
            }

            @Override
            public void onCancel() {
                Log.i("test", "onCancel ::::::::::: ");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("test", "onError ::::::::::: " + error);
            }
        });

        accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken == null){
            loginCheck(new FacebookLoginCallback() {
                @Override
                public void onSuccess(JSONObject fbObject) {
                    showFbShareDialog(title,description,videoUrl);
                }

                @Override
                public void onFailed(String reason) {
                }
            });
        }else{
            showFbShareDialog(title,description,videoUrl);
        }

    }*/

    public static AccessToken getAccessToken(){
        return AccessToken.getCurrentAccessToken();
    }

    public void showFbShareDialog(final String title,final String description,final String videoUrl){
        if(ShareDialog.canShow(ShareLinkContent.class)){
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(title)
                    .setContentDescription(description)
                    .setContentUrl(Uri.parse(videoUrl))
                    .build();

            shareDialog.show(linkContent);
        }
    }

}