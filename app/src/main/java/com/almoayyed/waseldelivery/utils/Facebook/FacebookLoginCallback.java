package com.almoayyed.waseldelivery.utils.Facebook;

import org.json.JSONObject;

/**
 * Created on 18 Oct 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public interface FacebookLoginCallback {
    void onSuccess(JSONObject fbObject);
    void onFailed(String reason);
}
