package com.almoayyed.waseldelivery.utils.Facebook;

/**
 * Created on 25 Oct 2016 12:47 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public interface LogoutFromFacebookListener {

        void onLoggedOutFromFacebook();
    }