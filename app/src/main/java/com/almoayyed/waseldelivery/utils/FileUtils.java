package com.almoayyed.waseldelivery.utils;

import android.os.Environment;

import com.almoayyed.waseldelivery.constants.ProfileConstants;
import com.almoayyed.waseldelivery.utils.log.Loggers;

import java.io.File;

/**
 * Created on 27 Dec 2016 7:31 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public class FileUtils {

    public static File getTempImageFolder()
    {
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +"/"+ ProfileConstants.IMAGE_DIRECTORY_NAME);
        if(!dir.exists())
        {
            dir.mkdir();
        }

        return dir;
    }

    public static void deleteTempImageFolder()
    {
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +"/"+ ProfileConstants.IMAGE_DIRECTORY_NAME);
        deleteFolder(dir.getAbsolutePath());
    }

    public static void deleteFolder(String path)
    {

        File dir = new File(path);
        String[] list = dir.list();
        if(list.length != 0) {
            for (String file : list) {
                new File(dir.getAbsoluteFile() + "/" + file).delete();
            }
        }

        Loggers.temp("getTempImageFolder=="+dir.exists());
    }
}
