package com.almoayyed.waseldelivery.utils.GooglePlus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

/**
 * Created on 24 Nov 2016 7:52 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Google signup functionalities are handled here
 */
public class Google {

    public static final int RC_SIGN_IN = 7;
    private static GoogleApiClient mGoogleApiClient;

    // Configure sign-in to request the user's ID, email address, and basic
    // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
    private static GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build();

    /* Method: signUp
    *  Input:
      * activity - activity must extend FragmentActivity or AppCompatActivity (a subclass of FragmentActivity), both of which are part of the Android Support Library
      * connectionListener - handles call backs of sign-in process
    * Output: void
    * Description: Performs google sign-in operation by displaying existing google account.
    * */
    public static void signUp(AppCompatActivity activity, GoogleConnectionListener connectionListener) {
        if (mGoogleApiClient == null) {
            // Build a GoogleApiClient with access to the Google Sign-In API and the
            // options specified by gso.
            mGoogleApiClient = new GoogleApiClient.Builder(activity)
                    .enableAutoManage(activity, connectionListener)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }

        //Creates google sign-up intent & calls 'onActivityResult' method once sign-up operation completed.
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        activity.startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    /*
    * Method: signOut
    * Input:
      *  resultCallback: Call back listener for signout process
    * Output: none
    * Description: Performs google sign-out process of already sign-in user.
    * */
    public static void signOut(@NonNull ResultCallback resultCallback) {
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            resultCallback.onResult(new Result() {
                @Override
                public Status getStatus() {
                    return new Status(Status.CONTENTS_FILE_DESCRIPTOR);
                }
            });
            return;
        }
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(resultCallback);
    }
}
