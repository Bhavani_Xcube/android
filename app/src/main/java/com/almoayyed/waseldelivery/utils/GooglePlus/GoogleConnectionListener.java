package com.almoayyed.waseldelivery.utils.GooglePlus;

import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created on 24 Oct 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public interface GoogleConnectionListener extends GoogleApiClient.OnConnectionFailedListener {
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) ;
}
