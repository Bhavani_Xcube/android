package com.almoayyed.waseldelivery.utils.GpsLocation;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;

/**
 * Created on 25 Oct 2016 5:49 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/*
* GpsLocationListener interface defines the call back methods of finding current location
* */
public interface GpsLocationListener {

    void onLocationChanged(Location location);
    void onConnected(@Nullable Bundle bundle);
    void onConnectionFailed(@NonNull ConnectionResult connectionResult);
    void onConnectionSuspended(int i);

}
