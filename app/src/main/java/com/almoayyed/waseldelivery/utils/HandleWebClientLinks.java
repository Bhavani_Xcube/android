package com.almoayyed.waseldelivery.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created on 24 Jan 2016 02:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class HandleWebClientLinks {
    public static boolean handleLink(Context context, String url) {
        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(url).matches()){
            if (url.startsWith("tel:")) {
                context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(url)));
                return true;
            } else if (url.startsWith("sms:") || url.startsWith("smsto:") || url.startsWith("mms:") || url.startsWith("mmsto:")) {
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                return true;
            } else if (url.startsWith("mailto:")) {
                url = url.replaceFirst("mailto:", "");
                url = url.trim();
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("plain/text").putExtra(Intent.EXTRA_EMAIL,
                        new String[] { url });
                context.startActivity(i);

                return true;
            }
        }
        return false;
    }
}
