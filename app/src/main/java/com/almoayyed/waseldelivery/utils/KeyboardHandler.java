package com.almoayyed.waseldelivery.utils;

import android.app.Activity;
import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.almoayyed.waseldelivery.WaselApplication;

/**
 * Created on 10 Nov 2016 4:26 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Class to handle keyboard events throughout the application
 */
public class KeyboardHandler {
    public static void setKeyboardHandler(final View view){
        InputMethodManager inputMethodManager =
                (InputMethodManager) WaselApplication.getContext().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboardIfAvailable(view);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setKeyboardHandler(innerView);
            }
        }
    }

    public static void hideKeyboardIfAvailable(View view){
        InputMethodManager inputMethodManager =
                (InputMethodManager) WaselApplication.getContext().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        if(inputMethodManager!=null){
            inputMethodManager.hideSoftInputFromWindow(
                    view.getWindowToken(), 0);
            view.clearFocus();
        }
    }

    public static void showSoftKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) WaselApplication.getContext().getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(view, 0);
        }
    }

    public static void onKeyboardNextClicked(EditText view){
        view.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event){
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Some logic here.

                    return true; // Focus will do whatever you put in the logic.
                }
                return false;  // Focus will change according to the actionId
            }
        });
    }

}
