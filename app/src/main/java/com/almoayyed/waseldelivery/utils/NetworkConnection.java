package com.almoayyed.waseldelivery.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.almoayyed.waseldelivery.WaselApplication;

import rx.Observable;

/**
 * Created on 22 Nov 2016 7:19 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * NetworkConnection class gives the status of network(internet) status
 */
public class NetworkConnection {
    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) WaselApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
