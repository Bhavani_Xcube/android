package com.almoayyed.waseldelivery.utils;

import android.widget.Toast;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.network_interface.NetworkStatusException;
import com.almoayyed.waseldelivery.ui.login.SignInActivity;

import org.json.JSONObject;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Parse the error message from server
 */

public class ParseServerErrorMessage {
    public static String getServerErrorMessage(Throwable e){
        try{
            if(!NetworkConnection.isNetworkAvailable()){
                return WaselApplication.getContext().getString(R.string.check_internet);
            }

            if(e instanceof NetworkStatusException){
                return ((NetworkStatusException)e).getMessage();
            }else if(e instanceof HttpException){
                    String serverMessage = ((HttpException) e).response().errorBody().string();
                    JSONObject jsonObject = new JSONObject(serverMessage);
                    return jsonObject.getString("message");
            }else if(e instanceof SocketTimeoutException ||
                    e instanceof ConnectException){
                return WaselApplication.getContext().getString(R.string.unable_to_reach_server);
            }else {
                return "";
            }
        }catch (Exception ex){

        }
        return "";
    }
}
