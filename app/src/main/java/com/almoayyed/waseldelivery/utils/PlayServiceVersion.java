package com.almoayyed.waseldelivery.utils;

import android.app.Activity;
import android.content.Context;

import com.almoayyed.waseldelivery.constants.PermissionConstants;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/** Created on 20 Jan 2017 12:00 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class PlayServiceVersion {
    public static boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode, PermissionConstants.PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Loggers.info("This device is not supported.");
            }
            return false;
        }
        return true;
    }
}
