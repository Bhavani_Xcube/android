package com.almoayyed.waseldelivery.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.utils.ui.ViewRenderObserver;

import pl.droidsonroids.gif.GifImageView;

/**
 * Created on 05 Oct 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class ProgressBarHandler  {

    private final RelativeLayout relativeLayout;
    private final ViewRenderObserver viewRenderObserver;
    private GifImageView gifImageView;

    public ProgressBarHandler(Context context, RelativeLayout v){

        relativeLayout = v;
        gifImageView = new GifImageView(context);
        gifImageView.setImageResource(R.drawable.progress_animation);
        //gifImageView.setBackgroundColor(gifImageView.getContext().getResources().getColor(R.color.colorNavigationBar));
        gifImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        gifImageView.setOnClickListener(null);
        viewRenderObserver = new ViewRenderObserver();
    }

    public void show() {
        hide();
        relativeLayout.addView(gifImageView);
        viewRenderObserver.setOnObserverCallback(relativeLayout, new ViewRenderObserver.ViewRenderObserverCallback() {
            @Override
            public void onPreDraw() {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, relativeLayout.getHeight());
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                gifImageView.setLayoutParams(layoutParams);
            }
        });
    }

    public void hide() {
        relativeLayout.removeView(gifImageView);
        gifImageView.clearAnimation();
    }

    public void disableBottomViews() {
        gifImageView.bringToFront();
    }
}
