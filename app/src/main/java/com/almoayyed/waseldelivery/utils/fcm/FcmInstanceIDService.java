package com.almoayyed.waseldelivery.utils.fcm;

import android.util.Log;

import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.models.Notifications;
import com.almoayyed.waseldelivery.models.Response;
import com.almoayyed.waseldelivery.network_interface.WaselDeliveryService;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.views.WaselToast;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.almoayyed.waseldelivery.utils.ParseServerErrorMessage.getServerErrorMessage;

/**
 * Created on 07 Dec 2016 3:17 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class FcmInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    public Subscription subscribe;
    public CompositeSubscription startStopCompositeSubscription = new CompositeSubscription();

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //Displaying token on logcat
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    public void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        Log.e(TAG, "token: " + token);
        LoginCredentialsPreference.getLoginPreference().saveRegId(token);
        if(LoginCredentialsPreference.getLoginPreference().isUserLoggedIn()){
            triggerNotificationApi(token);
        }
    }

    private void triggerNotificationApi(String token) {
        //Triggering Notification Api to send DeviceToken to Server
        Notifications notifications = new Notifications();
        notifications.setAndroid(true);
        notifications.setDeviceToken(token);
        notifications.setUserId(LoginCredentialsPreference.getLoginPreference().getId());

        Observable<Response> notificationObservable = WaselDeliveryService.getService().getNotification(notifications);
        subscribe = notificationObservable.subscribe(notificationObserver);
        startStopCompositeSubscription.add(subscribe);
    }

    /*
* Observer for Notification check api.
* */
    Observer<Response> notificationObserver = new Observer<Response>() {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            WaselToast.showToast(getServerErrorMessage(e));
        }

        @Override
        public void onNext(Response response) {
            Loggers.error("response :"+response);
//            String serverVersion = response.getVersion().replaceAll("_",".").toString();

        }
    };
}