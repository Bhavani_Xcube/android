package com.almoayyed.waseldelivery.utils.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.constants.CheckoutConstants;
import com.almoayyed.waseldelivery.constants.LocalPermissionConstants;
import com.almoayyed.waseldelivery.constants.PushNotificationConstants;
import com.almoayyed.waseldelivery.data_saving.LoginCredentialsPreference;
import com.almoayyed.waseldelivery.ui.MainActivity;
import com.almoayyed.waseldelivery.ui.history.OrderDetailsActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;

import static com.almoayyed.waseldelivery.constants.PushNotificationConstants.ORDER_STATUS_COMPLETE;
import static com.almoayyed.waseldelivery.constants.PushNotificationConstants.ORDER_STATUS_CONFIRM;
import static com.almoayyed.waseldelivery.constants.PushNotificationConstants.ORDER_STATUS_ON_THE_WAY;
import static com.almoayyed.waseldelivery.constants.PushNotificationConstants.ORDER_STATUS_PENDING;
import static com.almoayyed.waseldelivery.constants.PushNotificationConstants.ORDER_STATUS_PREPARING;

/**
 * Created on 07 Dec 2016 3:19 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class FcmService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getData().get("message"));

        //Calling method to generate notification
        sendNotification(remoteMessage.getData());
    }

    private void sendNotification(Map<String, String> pushData) {


        if(pushData != null) {
            String body = pushData.containsKey("body") ? pushData.get("body") : null;
            Log.e(TAG, "sendNotification: body = "+body );
            try{
                if(body != null){
                    JSONObject json = new JSONObject(body);
                    String title = json.has("title") ? json.getString("title") : null;
                    int id = json.has("id") ? json.getInt("id") : 0;
                    StringBuffer notificationID = new StringBuffer();
                    notificationID.append(id);
                    int orderId = json.has("orderId") ? json.getInt("orderId") : 0;
                    String userId = json.has("userId") ? json.getString("userId") : "";
                    String message = pushData.get("message");
                    Log.e(TAG, "sendNotification: title = "+title );
                    Log.e(TAG, "sendNotification: message = "+message );

                    LoginCredentialsPreference loginPreference = LoginCredentialsPreference.getLoginPreference();
                    if(loginPreference.isUserLoggedIn() && loginPreference.getId().equalsIgnoreCase(userId)){
                        Intent pushIntent;
                        switch (id){
                            case ORDER_STATUS_PENDING:
                            case ORDER_STATUS_CONFIRM:
                            case ORDER_STATUS_PREPARING:
                            case ORDER_STATUS_ON_THE_WAY:
                            case ORDER_STATUS_COMPLETE:

                                // Data you need to pass to activity
                                Intent orderStatusIntent = new Intent(PushNotificationConstants.orderStatusFilter);
                                String orderStatus = json.has("status") ? json.getString("status") : "";
                                String link = json.has("link") ? json.getString("link") : "";
                                orderStatusIntent.putExtra(CheckoutConstants.KEY_ORDER_ID, orderId);
                                orderStatusIntent.putExtra(CheckoutConstants.KEY_ORDER_STATUS, orderStatus);
                                orderStatusIntent.putExtra(CheckoutConstants.KEY_ORDER_TRACKING_LINK, link);
                                WaselApplication.getContext().sendBroadcast(orderStatusIntent);

                                if(WaselApplication.getContext().isAppInBackground()){
                                    pushIntent= new Intent(this, OrderDetailsActivity.class);
                                    pushIntent.putExtra(CheckoutConstants.KEY_CHECKOUT_FLOW,CheckoutConstants.KEY_CHECKOUT_CLICK_PUSH);
                                    pushIntent.putExtra(CheckoutConstants.KEY_ORDER_ID,orderId);
                                }else { //App in foreground no order related push is shown
                                    return;
                                }
                                notificationID.append(orderId);
                                break;
                            default:
                                pushIntent= new Intent(this, MainActivity.class);
                                break;
                        }
                        pushIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack
                        stackBuilder.addParentStack(MainActivity.class);
// Adds the Intent to the top of the stack
                        stackBuilder.addNextIntent(pushIntent);
// Gets a PendingIntent containing the entire back stack
                        PendingIntent resultPendingIntent =
                                stackBuilder.getPendingIntent(Integer.parseInt(notificationID.toString()), PendingIntent.FLAG_UPDATE_CURRENT);


                        /*PendingIntent pendingIntent = PendingIntent.getActivity(this,
                                Integer.parseInt(notificationID.toString()), pushIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT); //LocalPermissionConstants.REQUEST_PUSH_NOTIFICATION*/

                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.push_icon)
                                .setColor(getResources().getColor(R.color.bg))
                                .setContentTitle(title)
                                .setContentText(message)
                                .setAutoCancel(true)
                                .setContentIntent(resultPendingIntent);

                        NotificationManager notificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                        notificationManager.notify(Integer.parseInt(notificationID.toString()), notificationBuilder.build());
                    }

                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }


}