package com.almoayyed.waseldelivery.utils.log;

import android.util.Log;

import com.almoayyed.waseldelivery.BuildConfig;


public class Loggers {

    private static String TAG = BuildConfig.APP_NAME;

    private static boolean ENABLE_LOGGERS = BuildConfig.ENABLE_LOGS;
    private static boolean ENABLE_VERBOSE = false;
    private static boolean ENABLE_DEBUG = false;
    private static boolean ENABLE_INFO = false;
    private static boolean ENABLE_ERRORS = true;
    private static boolean ENABLE_TEMP = BuildConfig.TEMP_LOGS;

    public static void verbose(String msg)
    {
        if(ENABLE_LOGGERS && ENABLE_VERBOSE) {
            Log.v(TAG, msg);
        }
    }

    public static void debug(String msg)
    {
        if(ENABLE_LOGGERS && ENABLE_DEBUG) {
            Log.d(TAG, msg);
        }
    }

    public static void info(String msg)
    {
        if(ENABLE_LOGGERS && ENABLE_INFO) {
            Log.i(TAG, msg);
        }
    }

    public static void error(String msg)
    {
        if(ENABLE_LOGGERS && ENABLE_ERRORS) {
            Log.e(TAG, msg);
        }
    }

    public static void temp(String msg)
    {
        if(ENABLE_LOGGERS && ENABLE_TEMP) {
            Log.e(TAG, msg);
        }
    }
}
