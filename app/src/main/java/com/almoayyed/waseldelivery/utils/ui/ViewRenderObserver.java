package com.almoayyed.waseldelivery.utils.ui;

import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Created on 10 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class ViewRenderObserver {

    private ViewRenderObserverCallback viewRenderObserverCallback;

    public void setOnObserverCallback(final View view, ViewRenderObserverCallback  callback)
    {
        viewRenderObserverCallback = callback;
        final ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {

                if(viewTreeObserver.isAlive())
                {
                    viewTreeObserver.removeOnPreDrawListener(this);
                }
                else
                {
                    ViewTreeObserver viewTreeObserver1 = view.getViewTreeObserver();
                    viewTreeObserver1.removeOnPreDrawListener(this);
                }

                viewRenderObserverCallback.onPreDraw();

                return false;
            }
        });
    }

    public interface ViewRenderObserverCallback
    {
        void onPreDraw();
    }


}



