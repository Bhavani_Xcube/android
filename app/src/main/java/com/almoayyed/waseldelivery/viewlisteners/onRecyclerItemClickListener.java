package com.almoayyed.waseldelivery.viewlisteners;

import android.view.View;

/**
 * Created on 15 Dec 2016 6:19 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public interface onRecyclerItemClickListener {

    void onItemClick(View v, int pos);


}
