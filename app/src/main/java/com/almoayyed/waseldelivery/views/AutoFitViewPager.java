package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.almoayyed.waseldelivery.utils.Converters;

/**
 * Created on 25 Jan 2017 1:00 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class AutoFitViewPager extends ViewPager {
    int height = 0;
    private Context context;
    public AutoFitViewPager(Context context) {
        super(context);
        this.context = context;
    }

    public AutoFitViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        for(int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);

            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            int h = child.getMeasuredHeight();
            if(h > height) height = h + (int) Converters.pxToDp(context,60);
        }

        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

}
