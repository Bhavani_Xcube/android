package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.almoayyed.waseldelivery.R;

/**
 * Created on 19 Sep 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CircleViewPageIndicator extends LinearLayout {
    private int count;
    private Context context;

    public CircleViewPageIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context,attrs);
    }

    public CircleViewPageIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        this.context = context;
        //setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CircleViewPageIndicator, 0, 0);

        try {
            //No. of indications to show
            count = attributes.getInteger(R.styleable.CircleViewPageIndicator_indicator_count,1);

            drawComponents();
        }finally {
            attributes.recycle();
        }
    }

    /*@Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        *//*removeAllViews();*//*
        *//*drawComponents();*//*
    }*/

    public void updateCount(int count){
        removeAllViews();
        this.count = count;
        drawComponents();
        invalidate();
        requestLayout();
    }

    private void drawComponents() {
        for(int i = 0; i <count;i++){
            View view = new View(context);
            setCircularBackground(view,i==0?true:false);
            view.setPadding(1,1,1,1);
            LayoutParams llp = new LayoutParams(getResources()
                    .getDimensionPixelSize(R.dimen.banner_indicator_size),getResources().getDimensionPixelSize(R.dimen.banner_indicator_size));
            llp.setMargins(3,3,3,3);
            view.setLayoutParams(llp);
            addView(view);
        }
    }

    public void setSelection(int index){
        int count = getChildCount();
        View v = null;
        for(int i=0; i<count; i++) {
            v = getChildAt(i);
            setCircularBackground(v,i==index?true:false);
        }
    }

    public void removeAtPos(int pos){
        removeViewAt(pos);
        count--;
        invalidate();
        requestLayout();
    }

    private void setCircularBackground(View view, boolean isSelected){
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.OVAL);
        if(isSelected) {
            shape.setColor(Color.WHITE);
        }
        else{
            shape.setColor(Color.TRANSPARENT);
        }
        shape.setStroke(1, Color.WHITE);
        view.setBackground(shape);
    }
}
