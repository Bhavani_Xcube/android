package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created on 05 Oct 2016 12:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CircularTextView extends CustomFontTextView {
    int strokeColor;
    int backgroundColor;
    int textcolor;

    public CircularTextView(Context context) {
        super(context);
    }

    public CircularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircularTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setCircularBackground(){
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.OVAL);
        shape.setColor(backgroundColor);
        shape.setStroke(1, strokeColor);
        setTextColor(textcolor);
        this.setBackground(shape);
        setMaxLines(1);
        /*setSizeToFit(true);*/
    }
}
