package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.ReplacementSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

/**
 * Created on 24 Nov 2016 2:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CurrencyHighlighterTextView extends TextView {


    public CurrencyHighlighterTextView(Context context) {
        super(context);
        init(context);
    }

    public CurrencyHighlighterTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context)
    {



        //setText(wordToSpan);
    }

    public void setCustomText(String text) {
        setText(text);

        int i = text.indexOf("BD");
        if(i == -1)
        {
            return;
        }

        int startIndex = i;
        int endIndex = startIndex + 2;

        Spannable wordToSpan = new SpannableString(getText().toString());
        wordToSpan.setSpan(new ForegroundColorSpan(Color.WHITE), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        RoundedBackgroundSpan what = new RoundedBackgroundSpan(getCurrentTextColor(), Color.WHITE);
        wordToSpan.setSpan(what, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        setPadding(0,0,0,0);

        setText(wordToSpan);
    }

    public class RoundedBackgroundSpan extends ReplacementSpan
    {

        private final int padding = 2;
        private int backgroundColor;
        private int textColor;


        public RoundedBackgroundSpan(int backgroundColor, int textColor) {
            super();
            this.backgroundColor = backgroundColor;
            this.textColor = textColor;
        }

        @Override
        public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
            return (int) (paint.measureText(text.subSequence(start, end).toString()));
        }

        @Override
        public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint)
        {
            Log.e("RoundedBackgroundSpan",getHeight()+"=="+start+"=="+x+"=="+y+"=="+top+"=="+bottom);
            Rect bounds = new Rect();
            paint.getTextBounds(text.toString(), start, end, bounds);
            paint.measureText(text.subSequence(start, end).toString());
            paint.setAntiAlias(true);
            paint.setColor(backgroundColor);
            float cx = x + Math.abs( bounds.right - bounds.left)/2; //x + padding;
            float cy = y - bounds.height() / 2f;; //y + Math.abs(bounds.top + bounds.bottom)/2;
            canvas.drawCircle(cx, cy, getTextSize()/2f, paint);
            paint.setColor(textColor);
            paint.setTextSize(getTextSize()/2f);

            canvas.drawText("BD", 0, 2, cx - (bounds.width()/4f), cy + (bounds.height()/4f), paint);
        }
    }


}
