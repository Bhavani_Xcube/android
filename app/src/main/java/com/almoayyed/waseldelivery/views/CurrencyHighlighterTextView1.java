package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.ImageSpan;
import android.text.style.MetricAffectingSpan;
import android.util.AttributeSet;

import com.almoayyed.waseldelivery.R;

/**
 * Created on 24 Nov 2016 2:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CurrencyHighlighterTextView1 extends CustomFontTextView {


    public CurrencyHighlighterTextView1(Context context) {
        super(context);
    }

    public CurrencyHighlighterTextView1(Context context, AttributeSet attrs) {
        super(context, attrs);
    }



    public void setCustomText(String text)
    {

        if(!text.contains("@"))
        {
            setText(text);
            return;
        }

        int startSpan = text.indexOf("@");
        Drawable res = getResources().getDrawable(R.drawable.b_d_active_1);
        res.setBounds(0, 0, res.getIntrinsicWidth(), res.getIntrinsicHeight());
        ImageSpan span = new ImageSpan(res, ImageSpan.ALIGN_BASELINE);

        SpannableString ss = new SpannableString(text);
        ss.setSpan(new SuperscriptSpanAdjuster(0.3), 0, text.indexOf("(")+1, 0);
        ss.setSpan(new SuperscriptSpanAdjuster(0.3), text.indexOf("@")+1, text.length(), 0);
        ss.setSpan(span, startSpan, startSpan+1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        setTextAlignment(TEXT_ALIGNMENT_CENTER);
        setText(ss);

    }

    public class SuperscriptSpanAdjuster extends MetricAffectingSpan {
        double ratio = 0.5;

        public SuperscriptSpanAdjuster(double ratio) {
            this.ratio = ratio;
        }

        @Override
        public void updateDrawState(TextPaint paint) {
            paint.baselineShift += (int) (paint.ascent() * ratio);
        }

        @Override
        public void updateMeasureState(TextPaint paint) {
            paint.baselineShift += (int) (paint.ascent() * ratio);
        }
    }


}
