package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.utils.Converters;

/**
 * Created on 16 Sep 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CustomAppBarLayout extends AppBarLayout implements AppBarLayout.OnOffsetChangedListener {
    private View headerLay;
    private View headerSubLay;
    private View headerTransitionBG;
    private TextView headerTitle;
    private ImageView backBtn;
    final float[] from = new float[3];
    final float[] to =   new float[3];
    final float[] to_black =   new float[3];
    private float[] hsv, hsv_black;
    private VectorDrawable vectorDrawable;
    private VectorDrawableCompat vectorDrawableCompat;
    int startColor;
    int endColor;
    int endBlackColor;
    private CollapsingToolbarLayout.LayoutParams titleLayoutParams;
    private LinearLayout.LayoutParams headerSubLayoutParams;
    private ImageView searchBtn;
    private VectorDrawableCompat searchVectorDrawableCompat;
    private VectorDrawable searchVectorDrawable;
    private float headerTitleSize;
    private TextView shiftTimings;
    private boolean isTitleFilled;


    public CustomAppBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        //int[] toolBarAttrs = {R.style.ToolBarAttrs};
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ToolBarAttrs, 0, 0);

        startColor = ta.getColor(R.styleable.ToolBarAttrs_startColor, Color.WHITE);
        endColor = ta.getColor(R.styleable.ToolBarAttrs_endColor, ContextCompat.getColor(context, R.color.colorPrimary));
        endBlackColor = ta.getColor(R.styleable.ToolBarAttrs_endColor, Color.BLACK);

        ta.recycle();

        init();
    }

    int titleWidth;
    public void init()
    {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        hsv  = new float[3];
        hsv_black = new float[3];
        Color.colorToHSV(startColor, from);   // from white
        Color.colorToHSV(endColor, to);
        Color.colorToHSV(endBlackColor,to_black);


        final ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {

                if(viewTreeObserver.isAlive())
                {
                    viewTreeObserver.removeOnPreDrawListener(this);
                }
                else
                {
                    ViewTreeObserver viewTreeObserver1 = getViewTreeObserver();
                    viewTreeObserver1.removeOnPreDrawListener(this);
                }

                headerLay = findViewById(R.id.header_layout);
                headerSubLay = findViewById(R.id.header_sub_layout);
                headerTransitionBG = findViewById(R.id.header_transition_bg);
                headerTitle = (TextView)findViewById(R.id.header_title);
                backBtn = (ImageView) findViewById(R.id.back_btn);
                searchBtn = (ImageView) findViewById(R.id.search_btn);
                searchBtn = (ImageView) findViewById(R.id.search_btn);
                shiftTimings = (TextView)findViewById(R.id.shift_timing);

                titleLayoutParams = (CollapsingToolbarLayout.LayoutParams)headerLay.getLayoutParams();
                headerSubLayoutParams = (LinearLayout.LayoutParams)headerSubLay.getLayoutParams();

                titleWidth = headerTitle.getWidth();
                int totalTitleWidth = titleWidth + (titleLayoutParams.leftMargin) + (backBtn.getWidth());

                if(totalTitleWidth >= getWidth())
                {
                    isTitleFilled = true;
                }

                addOnOffsetChangedListener(CustomAppBarLayout.this);

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    vectorDrawable = (VectorDrawable) backBtn.getDrawable();
                    searchVectorDrawable = (VectorDrawable) searchBtn.getDrawable();
                }
                else
                {
                    vectorDrawableCompat = (VectorDrawableCompat) backBtn.getDrawable();
                    searchVectorDrawableCompat = (VectorDrawableCompat) searchBtn.getDrawable();
                }

                headerTitleSize = Converters.pixelsToSp(headerTitle.getTextSize());


                return false;
            }
        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

        int maxScroll = getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;



        headerSubLay.setAlpha(1 - percentage);
        shiftTimings.setAlpha(1 - percentage);

        headerTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,headerTitleSize - (2 * percentage));

        //Icons colors (white to orange)
        hsv[0] = from[0] + (to[0] - from[0])* percentage;
        hsv[1] = from[1] + (to[1] - from[1])* percentage;
        hsv[2] = from[2] + (to[2] - from[2])* percentage;


        //color converts from white to black
        hsv_black[0] = from[0] + (to_black[0] - from[0])* percentage;
        hsv_black[1] = from[1] + (to_black[1] - from[1])* percentage;
        hsv_black[2] = from[2] + (to_black[2] - from[2])* percentage;


        headerTitle.setTextColor(Color.HSVToColor(hsv_black));

        int margin = titleLayoutParams.leftMargin;
        if(isTitleFilled)
        {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) headerTitle.getLayoutParams();
            int adjustWidth = (int) ((backBtn.getWidth() + titleLayoutParams.leftMargin) * percentage);
            layoutParams.width = titleWidth - adjustWidth;
            margin = -backBtn.getWidth();
        }

        int marginX = (getWidth() / 2) - (titleWidth /2) - margin;

            headerLay.setTranslationX(marginX * percentage);
        int marginY = headerSubLay.getHeight() + headerSubLayoutParams.bottomMargin +headerSubLayoutParams.topMargin;
        headerLay.setTranslationY(marginY * percentage);

        headerTransitionBG.setAlpha(percentage);


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            vectorDrawable.setColorFilter(Color.HSVToColor(hsv_black),PorterDuff.Mode.SRC_ATOP);
            searchVectorDrawable.setColorFilter(Color.HSVToColor(hsv),PorterDuff.Mode.SRC_ATOP);
        }
        else
        {
            vectorDrawableCompat.setColorFilter(Color.HSVToColor(hsv_black),PorterDuff.Mode.SRC_ATOP);
            searchVectorDrawableCompat.setColorFilter(Color.HSVToColor(hsv),PorterDuff.Mode.SRC_ATOP);
        }


    }
}
