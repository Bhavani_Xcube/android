package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;

import com.almoayyed.waseldelivery.R;

/**
 * Created on 09 Nov 2016 11:23 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CustomBudgetView extends LinearLayout {

    private int budgetVal;
    private int secondaryStroke;
    private int primaryStroke;
    private int count;
    private Context context;
    private int primaryTextColor;
    private int secondaryTextColor;
    private int primaryBgColor;
    private int secondaryBgColor;

    public CustomBudgetView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context,attrs);
    }

    public CustomBudgetView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }

    public void init(Context context, AttributeSet attrs){
        this.context = context;
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL|Gravity.RIGHT);

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.CustomBudgetView, 0, 0);

        try {
            //No. of budget items to display
            count = attributes.getInteger(R.styleable.CustomBudgetView_count,1);

            //No. of highlighted items in the budget list
            budgetVal = (budgetVal > 0) ? budgetVal : attributes.getInteger(R.styleable.CustomBudgetView_budget_value,0);
            primaryStroke = attributes.getColor(R.styleable.CustomBudgetView_primary_stroke,
                    ContextCompat.getColor(context, R.color.colorPrimary));
            secondaryStroke = attributes.getColor(R.styleable.CustomBudgetView_secondary_stroke,
                    ContextCompat.getColor(context, R.color.textColor7));
            primaryTextColor = attributes.getColor(R.styleable.CustomBudgetView_primary_text,
                    ContextCompat.getColor(context, R.color.colorPrimary));
            secondaryTextColor = attributes.getColor(R.styleable.CustomBudgetView_secondary_text,
                    ContextCompat.getColor(context, R.color.textColor7));
            primaryBgColor = attributes.getColor(R.styleable.CustomBudgetView_primary_bg,
                    Color.TRANSPARENT);
            secondaryBgColor = attributes.getColor(R.styleable.CustomBudgetView_secondary_bg,
                    Color.TRANSPARENT);


            drawBudgetComponent();

        } finally {
            attributes.recycle();
        }

    }
    public int getDipToPixel(float dip){
        Resources r = getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, r.getDisplayMetrics());
    }

    /*
    * Updates the highlighted budget value
    * */
    public void setBudgetValue(int budgetVal){
        this.budgetVal = budgetVal;
        invalidate();
        requestLayout();
    }

    /*
   * Updates the highlighted icon background color
   * */
    public void setPrimaryColors(int textColor,int bgColor,int strokeColor){
        this.primaryTextColor = textColor;
        this.primaryStroke = strokeColor;
        this.primaryBgColor = bgColor;
        invalidate();
        requestLayout();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
//        removeAllViews();
        updateComponent();
    }

    private void updateComponent() {
        for(int i = 1; i <= count;i++){
            CircularTextView textView = (CircularTextView)getChildAt(i-1);
            updateColors(i, textView);
        }
    }

    private void drawBudgetComponent() {
        for(int i = 1; i <= count;i++){
            //Use text view to display the budget icon
            CircularTextView textView = new CircularTextView(context);
            textView.setText("BD");
            Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat-SemiBold.otf" );
            textView.setTypeface(myTypeface);

            updateColors(i, textView);
            textView.setPadding(3,3,3,3);
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            llp.setMargins(2,2,2,2);
            textView.setWidth(getDipToPixel(16));
            textView.setHeight(getDipToPixel(16));
            textView.setGravity(Gravity.CENTER);
            textView.setLayoutParams(llp);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,7);

            addView(textView);

        }
    }

    private void updateColors(int i, CircularTextView textView) {
        if(i <= budgetVal){
            textView.strokeColor = primaryStroke;
            textView.backgroundColor = primaryBgColor;
            textView.textcolor = primaryTextColor;
        }else{
            textView.strokeColor = secondaryStroke;
            textView.backgroundColor = secondaryBgColor;
            textView.textcolor = secondaryTextColor;
        }
        textView.setCircularBackground();
    }
}
