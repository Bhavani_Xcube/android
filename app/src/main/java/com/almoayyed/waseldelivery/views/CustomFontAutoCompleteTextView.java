package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import com.almoayyed.waseldelivery.R;

/**
 * Created on 28 Sep 2016 10:02 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Updates the typephase to auto complete text view
 */
public class CustomFontAutoCompleteTextView extends AutoCompleteTextView {
    public CustomFontAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomFontAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (attrs!=null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
            String fontName = a.getString(R.styleable.CustomFontTextView_font_name);
            if (fontName!=null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), fontName );
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }
}
