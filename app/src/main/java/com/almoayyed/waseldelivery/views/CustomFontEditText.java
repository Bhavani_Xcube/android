package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;

import java.util.HashMap;

/**
 * Created on 10 Nov 2016 7:03 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CustomFontEditText extends EditText {
    static HashMap<String, Typeface> typefaceHashMap = new HashMap<>();

    public CustomFontEditText(Context context) {
        super(context);
    }

    public CustomFontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomFontEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }
    private void init(AttributeSet attrs) {
        if (attrs!=null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomFontEditText);
            String fontName = a.getString(R.styleable.CustomFontEditText_text_font_name);
            if (fontName!=null) {
                //Creates new typeface if it is not previously created
                // or else returns the previously created one.
                Typeface myTypeface = (typefaceHashMap.get(fontName) == null) ?
                        createTypeface(fontName) :
                        typefaceHashMap.get(fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        }
        /*//TO stop spell checking
        //working but if it is included digits limitation is no working properly
        setInputType(getInputType() | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);*/
        /*setImeOptions(getImeOptions() | EditorInfo.IME_ACTION_NEXT);*/
    }
    private Typeface createTypeface(String fontName){
        typefaceHashMap.put(fontName,Typeface.createFromAsset(getContext().getAssets(), fontName));
        return typefaceHashMap.get(fontName);
    }

    public void setTextChangedListener(ViewTextWatcherListener watcher) {
        ViewTextWatcher viewTextWatcher = new ViewTextWatcher(this);
        viewTextWatcher.setTextWatchListener(watcher);
    }

}
