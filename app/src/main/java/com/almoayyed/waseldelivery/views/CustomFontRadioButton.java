package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;

import java.util.HashMap;

/**
 * Created on 17 Nov 2016 6:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CustomFontRadioButton extends RadioButton {
    static HashMap<String, Typeface> typefaceHashMap = new HashMap<>();
    public CustomFontRadioButton(Context context) {
        super(context);
    }

    public CustomFontRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomFontRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }
    private void init(AttributeSet attrs) {
        if (attrs!=null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomFontRadioButton);
            String fontName = a.getString(R.styleable.CustomFontRadioButton_radio_button_font_name);
            if (fontName!=null) {
                //Creates new typeface if it is not previously created
                // or else returns the previously created one.
                Typeface myTypeface = (typefaceHashMap.get(fontName) == null) ?
                        createTypeface(fontName) :
                        typefaceHashMap.get(fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

    private Typeface createTypeface(String fontName){
        typefaceHashMap.put(fontName,Typeface.createFromAsset(getContext().getAssets(), fontName));
        return typefaceHashMap.get(fontName);
    }
}
