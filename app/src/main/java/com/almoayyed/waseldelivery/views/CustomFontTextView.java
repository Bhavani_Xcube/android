package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.text.Layout;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.utils.log.Loggers;
import com.almoayyed.waseldelivery.utils.ui.ViewRenderObserver;

import java.util.HashMap;

/**
 * Created on 24 Nov 2016 2:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CustomFontTextView extends TextView {
    static HashMap<String, Typeface> typefaceHashMap = new HashMap<>();
    private String fontName;
    private boolean showEllipse;

    public CustomFontTextView(Context context) {
        super(context);
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }
    private void init(AttributeSet attrs) {
        if (attrs!=null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
            fontName = a.getString(R.styleable.CustomFontTextView_font_name);
            showEllipse = a.getBoolean(R.styleable.CustomFontTextView_show_ellipse,false);
            if (fontName!=null) {
                updateFont();
            }
            a.recycle();
        }
        //TO stop spell checking
        setInputType(getInputType() | InputType.TYPE_CLASS_TEXT| InputType.TYPE_TEXT_FLAG_MULTI_LINE |InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        /*setSingleLine(false);
        setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);*/

    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        if(showEllipse && !TextUtils.isEmpty(text)){
            ViewRenderObserver viewRenderObserver = new ViewRenderObserver();
            viewRenderObserver.setOnObserverCallback(this, new ViewRenderObserver.ViewRenderObserverCallback() {
                @Override
                public void onPreDraw() {

                    //Rect bounds = new Rect();
                    Paint textPaint = getPaint();
                    float totalWidth = textPaint.measureText(getText().toString());

                    if(getMeasuredWidth()>0 && totalWidth>getMeasuredWidth()){
                        int end  = (int) (getMeasuredWidth() * getText().length()/ totalWidth);

                        end = Math.min(end, getText().length());

                        //int end = layout.getLineEnd(0);
                        StringBuffer buffer = new StringBuffer();
                        buffer.append(getText().subSequence(0, end-2));
                        buffer.append("...");
                        setText( buffer.toString() );
                        showEllipse = false;
                    }

                }

            });

        }

    }

    public static Typeface createTypeface(String fontName, Context context){
        typefaceHashMap.put(fontName,Typeface.createFromAsset(context.getAssets(), fontName));
        return typefaceHashMap.get(fontName);
    }

    //To update the font style programmatically.
    public void setFont(@NonNull String fontName){
        this.fontName = fontName;
        invalidate();
        requestLayout();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);

        updateFont();
    }

    private void updateFont() {
        //Creates new typeface if it is not previously created
        // or else returns the previously created one.
        if(!TextUtils.isEmpty(fontName)){
            Typeface myTypeface = (typefaceHashMap.get(fontName) == null) ?
                    createTypeface(fontName,getContext()) :
                    typefaceHashMap.get(fontName);
            setTypeface(myTypeface);
        }
    }

    /*public static int getHeight(Context context, CharSequence text, int textSize, int deviceWidth, Typeface typeface,int padding) {
        TextView textView = new TextView(context);
        textView.setPadding(padding,0,padding,padding);
        textView.setTypeface(typeface);
        textView.setText(text, TextView.BufferType.SPANNABLE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }*/

   /* @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        Loggers.temp("onMeasure="+getMeasuredWidth());
    }*/
}
