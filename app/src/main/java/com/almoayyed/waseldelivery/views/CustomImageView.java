package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.utils.ImageUtils;

import java.util.HashMap;

/**
 * Created on 09 Nov 2016 11:23 AM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

public class CustomImageView extends ImageView {
    private float radius;
    private Path clipPath;
    private RectF rect;

    public CustomImageView(Context context) {
        super(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }
    private void init(AttributeSet attrs) {
        if (attrs!=null) {

            clipPath = new Path();
            rect = new RectF();

            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomImageView);
            radius = a.getDimension(R.styleable.CustomImageView_radius,0);
            //Set corner radius
            a.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //float radius = 36.0f;
        rect.set(0, 0, this.getWidth(), this.getHeight());
        clipPath.addRoundRect(rect, radius, radius, Path.Direction.CW);
        canvas.clipPath(clipPath);
        super.onDraw(canvas);
    }


}
