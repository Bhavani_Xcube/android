package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.almoayyed.waseldelivery.utils.ImageUtils;

/**
 * Created on 14 Dec 2016 3:58 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public class TintedImageView extends ImageView {
    private int tintColor;
    private BitmapDrawable drawable;

    public TintedImageView(Context context) {
        super(context);
    }

    public TintedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);

        if(tintColor != 0)
        {

            if(super.isSelected())
            {
                Drawable drawableWithTint  = ImageUtils.setImageTint(this.drawable, tintColor);
                setImageDrawable(drawableWithTint);
            }
            else
            {
                Drawable drawable  = ImageUtils.setImageTint(this.drawable, getResources().getColor(android.R.color.darker_gray));
                setImageDrawable(drawable);
                //setImageDrawable(drawable);
            }

        }
    }

    public void setImageAtPath(String path)
    {
        setImageAtPathWithTint(path, tintColor);

        Bitmap imageBitmap = ImageUtils.getImageBitmap(path);
        drawable = new BitmapDrawable(getResources(), imageBitmap);
    }

    public void setImageAtPathWithTint(String path, int tintColor)
    {
        this.tintColor = tintColor;

        Bitmap imageBitmap = ImageUtils.getImageBitmap(path);
        drawable = new BitmapDrawable(getResources(), imageBitmap);
    }

    public void setImageAtUrlWithTint(String uri, int tintColor)
    {
       /* //if file exists
        if(exists)
        {
            setImageAtPathWithTint(String path, int tintColor)
        }
        else {
            //download image
        }*/
    }

}
