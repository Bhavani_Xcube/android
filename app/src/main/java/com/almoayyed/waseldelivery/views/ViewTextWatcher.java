package com.almoayyed.waseldelivery.views;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

/**
 * Created on 12 Dec 2016 7:25 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public class ViewTextWatcher implements TextWatcher{

    View view;
    private ViewTextWatcherListener textWatchListener;

    public ViewTextWatcher(View view)
    {
        this.view = view;
        ((EditText)view).addTextChangedListener(this                                                                                                                                                                                                                                                    );

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if(textWatchListener != null)
        {
            textWatchListener.onTextChange(view, s, start, before, count);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    public void setTextWatchListener(ViewTextWatcherListener textWatchListener) {
        this.textWatchListener = textWatchListener;
    }
}
