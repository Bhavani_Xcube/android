package com.almoayyed.waseldelivery.views;

import android.view.View;

/**
 * Created on 12 Dec 2016 7:37 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */
public interface ViewTextWatcherListener {

    void onTextChange(View v, CharSequence s, int start, int before, int count);
}
