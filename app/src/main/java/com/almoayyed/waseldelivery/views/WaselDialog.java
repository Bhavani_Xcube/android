package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;

/**
 * Created on 21 Nov 2016 7:53 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Desc: Creates custom dialog
 */

public class WaselDialog extends android.app.Dialog {


    private TextView mTitleView;
    private CurrencyHighlighterTextView mMessageView;
    private TextView mNeutralBtnView;
    private TextView mPositiveBtnView;
    private TextView mNegativeBtnView;
    private View dialogBglLytView;
    private View mPosNegLyt;
    private Context context;


    public WaselDialog(Context context) {
        super(context, R.style.Theme_CustomDialog);
        this.context = context;
        setCancelable(true);
        setContentView(R.layout.dialog_lyt);

        mTitleView = (TextView) findViewById(R.id.title);
        mTitleView.setVisibility(View.GONE);

        mMessageView = (CurrencyHighlighterTextView) findViewById(R.id.message);
        mMessageView.setVisibility(View.GONE);

        mPosNegLyt = findViewById(R.id.pos_neg_btn_lyt);
        mPosNegLyt.setVisibility(View.GONE);


        mNeutralBtnView = (TextView)findViewById(R.id.neutral_btn);

        mNeutralBtnView.setVisibility(View.GONE);

        mPosNegLyt = findViewById(R.id.pos_neg_btn_lyt);
        mPosNegLyt.setVisibility(View.GONE);

        mPositiveBtnView = (TextView) findViewById(R.id.positive_btn);

        mNegativeBtnView = (TextView) findViewById(R.id.negative_btn);

        dialogBglLytView = findViewById(R.id.dialog_background_lyt);
    }

    public void setPositiveBtnColor(int color){
        GradientDrawable backgroundGradient = (GradientDrawable)mPositiveBtnView.getBackground();
        backgroundGradient.setColor(getContext().getResources().getColor(color));
        backgroundGradient.setStroke(1,getContext().getResources().getColor(android.R.color.white));
    }

    public void setNegetiveBtnColor(int color){
        GradientDrawable backgroundGradient = (GradientDrawable)mNegativeBtnView.getBackground();
        backgroundGradient.setColor(getContext().getResources().getColor(color));
        backgroundGradient.setStroke(2,getContext().getResources().getColor(R.color.btn_bg_color));
    }

    public void setNeutralBtnColor(int color){
        GradientDrawable backgroundGradient = (GradientDrawable)mNeutralBtnView.getBackground();
        backgroundGradient.setColor(getContext().getResources().getColor(color));
        backgroundGradient.setStroke(2,getContext().getResources().getColor(R.color.btn_bg_color));
    }

    public WaselDialog setTitle(String title) {
        mTitleView.setText(title);
        mTitleView.setVisibility(View.VISIBLE);
        return this;
    }

    public WaselDialog setMessage(String message) {
        mMessageView.setCustomText(message);
        mMessageView.setVisibility(View.VISIBLE);
        return this;
    }

    public WaselDialog setNeutralButton(String netualButtonText, View.OnClickListener neutralButtonOnClickListener) {
        mNeutralBtnView.setText(netualButtonText);
        mNeutralBtnView.setVisibility(View.VISIBLE);
        if (neutralButtonOnClickListener == null) {
            neutralButtonOnClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            };
        }
        mNeutralBtnView.setOnClickListener(neutralButtonOnClickListener);
        return this;
    }

    public WaselDialog setPositiveButton(String positiveButtonText, View.OnClickListener positiveButtonOnClickListener) {
        mPosNegLyt.setVisibility(View.VISIBLE);
        mPositiveBtnView.setText(positiveButtonText);
        mPositiveBtnView.setVisibility(View.VISIBLE);
        if (positiveButtonOnClickListener == null) {
            positiveButtonOnClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            };
        }
        mPositiveBtnView.setOnClickListener(positiveButtonOnClickListener);
        return this;
    }

    public WaselDialog setNegativeButton(String negativeButtonText, View.OnClickListener negativeButtonOnClickListener) {
        mPosNegLyt.setVisibility(View.VISIBLE);
        mNegativeBtnView.setText(negativeButtonText);
        mNegativeBtnView.setVisibility(View.VISIBLE);
        if (negativeButtonOnClickListener == null) {
            negativeButtonOnClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            };
        }
        mNegativeBtnView.setOnClickListener(negativeButtonOnClickListener);
        return this;
    }

}
