package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.utils.ProgressBarHandler;

/**
 * Created on 21 Nov 2016 7:53 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Desc: Creates custom dialog
 */

public class WaselProgressDialog extends android.app.Dialog {


    private final ProgressBarHandler mProgressBarHandler;
    private Context context;


    public WaselProgressDialog(Context context) {
        super(context, R.style.Theme_CustomDialog);
        this.context = context;
        setCancelable(false);
        setContentView(R.layout.dialog_progress_lyt);
        mProgressBarHandler = new ProgressBarHandler(getContext(), (RelativeLayout) findViewById(R.id.progress_dialog_background_lyt));
    }

    @Override
    public void show() {
        super.show();
        mProgressBarHandler.show();
    }

    @Override
    public void hide() {
        mProgressBarHandler.hide();
        super.hide();
    }

    @Override
    public void dismiss() {
        mProgressBarHandler.hide();
        super.dismiss();
    }
}
