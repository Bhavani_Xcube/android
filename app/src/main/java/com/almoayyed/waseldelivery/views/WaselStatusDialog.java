package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.almoayyed.waseldelivery.R;

/**
 * Created on 21 Nov 2016 7:53 PM.
 *
 * @author PurpleTalk India Pvt. Ltd.
 */

/**
 * Desc: Creates custom dialog
 */

public class WaselStatusDialog extends android.app.Dialog {


    private static final long SHOW_STATUS_DLG_TIME_OUT = 3000;
    private ImageView mStatusImgView;
    private CurrencyHighlighterTextView mMessageView;
    private Context context;


    public WaselStatusDialog(Context context) {
        super(context, R.style.Theme_CustomDialog);
        this.context = context;
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        setContentView(R.layout.dialog_status_lyt);

        mStatusImgView = (ImageView) findViewById(R.id.status_img);

        mMessageView = (CurrencyHighlighterTextView) findViewById(R.id.status_message);
        mMessageView.setVisibility(View.GONE);
    }

    public WaselStatusDialog set(int resId){
        mStatusImgView.setImageResource(resId);
        return this;
    }

    public WaselStatusDialog setStatusImage(int resId){
        mStatusImgView.setImageResource(resId);
        return this;
    }

    public WaselStatusDialog setStatusImageBackgroundColor(int ARGBcolor){
        GradientDrawable gd = (GradientDrawable)mStatusImgView.getBackground();
        gd.setColor(ARGBcolor);
        return this;
    }

    public WaselStatusDialog setStatusImageStroke(int ARGBcolor){
        GradientDrawable gd = (GradientDrawable)mStatusImgView.getBackground();
        gd.setStroke(1, ARGBcolor);
        return this;
    }

    public WaselStatusDialog setMessage(String message) {
        mMessageView.setCustomText(message);
        mMessageView.setVisibility(View.VISIBLE);
        return this;
    }

    public WaselStatusDialog showDialog(){
        this.show();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(runnable, SHOW_STATUS_DLG_TIME_OUT);
        return this;
    }

}
