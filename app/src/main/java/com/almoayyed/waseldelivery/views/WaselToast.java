package com.almoayyed.waseldelivery.views;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;
import android.widget.Toast;

import com.almoayyed.waseldelivery.R;
import com.almoayyed.waseldelivery.WaselApplication;
import com.almoayyed.waseldelivery.utils.Converters;

/**
 * Display custom customToastView messages received from server
 */

public class WaselToast {


    public static void showToast(String msg){
        if(!TextUtils.isEmpty(msg)){
            createToast(WaselApplication.getContext(), Toast.LENGTH_SHORT, Gravity.BOTTOM, msg).show();
        }

    }

    private static Toast createToast(Context context, int HIDE_DELAY, int gravity, String msg) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.custom_toast, null);

        // Set layout to customToastView
        Toast toastInstance = new Toast(context);
        toastInstance.setView(layout);
        toastInstance.setGravity(gravity, 0, Converters.pxToDp(context,200));
        toastInstance.setDuration(HIDE_DELAY);
        TextView mTextView = (TextView) layout.findViewById(R.id.mbMessage);
        mTextView.setText(msg);
        return toastInstance;
    }



}